//###############################################
// Filename: Test_SqanInterface.h
// Description: Class that provides methods
//              to test SqanInterface class
//###############################################
#ifndef Test_SqanInterface_H
#define Test_SqanInterface_H

#include <QtTest/QTest>
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../SqanInterface/SqanInterfaceInit.h"
#include "../../SqanInterface/SqanInterface.h"
#include <stdio.h>
#include <array>

class TestSqanInterface : public QObject
{
    Q_OBJECT

public:
    TestSqanInterface();
    ~TestSqanInterface();

private slots:    
    // These are built into Qt:
    // called before the first test function
    void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    void cleanupTestCase();
    void TestBigToLittleEndian();

private:
    Logger *theLogger;
    DBLogger *dblogger;
    SqanInterfaceInit *sqanInterfaceInit;
    std::array<SqanInterface *, 14> *sqanInterfaceArray;
    SqanInterface *sqanInterface = nullptr;
    UsbInterface *usbInterface = nullptr;
};

#endif
