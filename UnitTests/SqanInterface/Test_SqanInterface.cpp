//###############################################
// Filename: Test_SqanInterface.cpp
// Description: Class that provides methods
//              to test SqanInterface class
//###############################################
#include "Test_SqanInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <zlib.h>

TestSqanInterface::TestSqanInterface()
{

}

TestSqanInterface::~TestSqanInterface()
{

}

void TestSqanInterface::initTestCase()
{
    // This is called before other tests are run
    std::string testFilename = "unitTestSqan";
    uint16_t debugLevel = Logger::DEBUG_SQAN_INTERFACE | Logger::DEBUG_USB_INTERFACE;
    theLogger = new Logger(testFilename, debugLevel);
    assert(theLogger);
    dblogger = new DBLogger(testFilename, "1");
    assert(dblogger);

    sqanInterfaceInit = new SqanInterfaceInit(SqanInterface::USB, theLogger, dblogger);

    sqanInterfaceArray = sqanInterfaceInit->InitSqanInterfaceArray();
    assert(*sqanInterfaceArray->begin() != nullptr);

    // Just get the first one for this test
    sqanInterface = *sqanInterfaceArray->begin();

    usbInterface = sqanInterface->GetUsbInterface();
    assert(usbInterface != nullptr);
    assert(usbInterface->IsDeviceOpen() == true);

    if ( usbInterface->GetProductId() == UsbInterfaceInit::BOARD_PRODUCT_ID )
    {
        assert(usbInterface->GetMaxPacketSizeIn() == 64);
        assert(usbInterface->GetMaxPacketSizeOut() == 64);
    }

    assert(sqanInterface->theLogger == theLogger);
    assert(sqanInterface->deviceInterface == usbInterface);
    assert(usbInterface->usbHeaderType == UsbInterface::USB_HEADER_CDC);
    assert(sqanInterface->isLittleEndian == true);
    assert(sqanInterface->GetIndex() == 0);  // First device has index 0

    //  Test QoS per endpoint - can be used for Hi/Low queues sending data
    sqanInterface->SetQosOnEndpoint(SqanInterface::HiNoDiscard, 0);
    assert(sqanInterface->sqanEndpointSendQueues[0].qOs == SqanInterface::HiNoDiscard);
    sqanInterface->SetQosOnEndpoint(SqanInterface::LowDiscard, 1);
    assert(sqanInterface->sqanEndpointSendQueues[1].qOs == SqanInterface::LowDiscard);
    sqanInterface->SetQosOnEndpoint(SqanInterface::HiDiscard, 2);
    assert(sqanInterface->sqanEndpointSendQueues[2].qOs == SqanInterface::HiDiscard);
    sqanInterface->SetQosOnEndpoint(SqanInterface::LowNoDiscard, 3);
    assert(sqanInterface->sqanEndpointSendQueues[3].qOs == SqanInterface::LowNoDiscard);

    // Test that send queues are empty at startup
    assert(sqanInterface->sqanEndpointSendQueues[0].sendQ.empty() == true);
    assert(sqanInterface->sqanEndpointSendQueues[1].sendQ.empty() == true);
    assert(sqanInterface->sqanEndpointSendQueues[2].sendQ.empty() == true);
    assert(sqanInterface->sqanEndpointSendQueues[3].sendQ.empty() == true);
    assert(sqanInterface->sqanEndpointReceiveQueues[0].receiveQ.empty() == true);
    assert(sqanInterface->sqanEndpointReceiveQueues[1].receiveQ.empty() == true);
    assert(sqanInterface->sqanEndpointReceiveQueues[2].receiveQ.empty() == true);
    assert(sqanInterface->sqanEndpointReceiveQueues[3].receiveQ.empty() == true);
}

void TestSqanInterface::init()
{

}

void TestSqanInterface::TestBigToLittleEndian()
{
    uint64_t testInput[] = {0x1234123412341234};

    uint16_t type = 16;
    sqanInterface->BigToLittleEndian(testInput, type);
    if (sqanInterface->isLittleEndian == true)
    {
       assert((uint16_t)*testInput == 0x1234);
    }
    else
    {
       assert((uint16_t)*testInput == 0x4321);
    }

    type = 32;
    sqanInterface->BigToLittleEndian(testInput, type);
    if (sqanInterface->isLittleEndian == true)
    {
       assert((uint32_t)*testInput == 0x12341234);
    }
    else
    {
       assert((uint32_t)*testInput == 0x43214321);
    }

    type = 64;
    sqanInterface->BigToLittleEndian(testInput, type);
    if (sqanInterface->isLittleEndian == true)
    {
       assert((uint64_t)*testInput == 0x1234123412341234);
    }
    else
    {
       assert((uint64_t)*testInput == 0x4321432143214321);
    }
}

void TestSqanInterface::cleanup()
{

}

void TestSqanInterface::cleanupTestCase()
{
    // Wait for all the tests to finish
    sleep(1);

    sqanInterfaceInit->ExitSqanInterfaceArray();

    // deletes sqanInterface's too
    delete(sqanInterfaceInit);

    // Calls deconstructor which closes log file
    delete(theLogger);
}

QTEST_APPLESS_MAIN(TestSqanInterface)
