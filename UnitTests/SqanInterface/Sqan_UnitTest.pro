QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += TESTING

LIBS += -L/usr/lib64 -lusb-1.0 -lz

SOURCES +=  \
    ../../Logger/Logger.cpp \
    ../../Logger/DBLogger.cpp \
    ../../Logger/DBLogRecord.cpp \
    ../../UsbInterface/UsbInterface.cpp \
#    ../../SerialInterface/SerialInterface.cpp \
#    ../../ParallelInterface/ParallelInterface.cpp \
    ../../UsbInterface/UsbInterfaceInit.cpp \
#    ../../SqanInterface/SqanFirmware.cpp \
#    ../../SqanInterface/SqanSystem.cpp \
#    ../../SqanInterface/SqanIdentity.cpp \
#    ../../SqanInterface/SqanSecurity.cpp \
#    ../../SqanInterface/SqanAssociation.cpp \
#    ../../SqanInterface/SqanStream.cpp \
#    ../../SqanInterface/SqanLink.cpp \
#    ../../SqanInterface/SqanMetrics.cpp \
#    ../../SqanInterface/SqanProduct.cpp \
#    ../../SqanInterface/SqanSolNet.cpp \
    ../../SqanInterface/SqanInterface.cpp \
    ../../SqanInterface/SqanInterfaceInit.cpp \
    Test_SqanInterface.cpp

HEADERS += \
    ../../Logger/Logger.h \
    ../../Logger/DBLogger.h \
    ../../Logger/DBLogRecord.h \
#    ../../UsbInterface/UsbInterface_global.h \
    ../../SqanInterface/Interface.h \
    ../../UsbInterface/UsbInterface.h \
#    ../../SerialInterface/SerialInterface.h \
#    ../../ParallelInterface/ParallelInterface.h \
    ../../UsbInterface/UsbInterfaceInit.h \
#    ../../SqanInterface/SqanFirmware.h \
#    ../../SqanInterface/SqanSystem.h \
#    ../../SqanInterface/SqanIdentity.h \
#    ../../SqanInterface/SqanSecurity.h \
#    ../../SqanInterface/SqanAssociation.h \
#    ../../SqanInterface/SqanStream.h \
#    ../../SqanInterface/SqanLink.h \
#    ../../SqanInterface/SqanMetrics.h \
#    ../../SqanInterface/SqanProduct.h \
#    ../../SqanInterface/SqanSolNet.h \
    ../../SqanInterface/SqanInterface.h \
    ../../SqanInterface/SqanInterfaceInit.h \
    Test_SqanInterface.h

