//###############################################
// Filename: Test_UsbInterface.cpp
// Description: Class that provides methods
//              to test UsbInterface class
//###############################################
#include "Test_UsbInterface.h"
#include <stdio.h>
#include <stdlib.h>

#define USB_ENDPOINT_IN	    (LIBUSB_ENDPOINT_IN  | 1)   /* endpoint address */
#define USB_ENDPOINT_OUT	(LIBUSB_ENDPOINT_OUT | 2)   /* endpoint address */

TestUsbInterface::TestUsbInterface()
{

}

TestUsbInterface::~TestUsbInterface()
{

}

void TestUsbInterface::initTestCase()
{
    int status = 0;

    // This is called before other tests are run
    std::string testFilename = "unitTestUsb";
    uint16_t debugLevel = Logger::DEBUG_USB_INTERFACE;
    theLogger = new Logger(testFilename, debugLevel);
    assert(theLogger);

    usbIntrfaceArray = usbInterfaceInit.InitUsbInterfaceArray(theLogger);

    assert(status == 0);
    assert(usbInterfaceInit.theLogger == theLogger);
    assert(usbIntrfaceArray != nullptr);
    assert(usbInterfaceInit.GetUsbInterfaceArray() == usbIntrfaceArray);

    for (auto itr = usbIntrfaceArray->begin(); itr != usbIntrfaceArray->end(); ++itr)
    {
        UsbInterface *usbInterface = *itr;
        assert(usbInterface != nullptr);
        assert(usbInterface->desc != nullptr);        
        assert(usbInterface->GetDeviceDescriptor() == usbInterface->desc);
        assert(usbInterface->context != nullptr);
        assert(usbInterface->GetContext() == usbInterface->context);
        assert(usbInterface->theLogger == theLogger);
        assert(usbInterface->GetVendorId() == usbInterface->desc->idVendor);
        assert(usbInterface->GetIndex() == 0); // The device index is the array index

        // Check that this is TRU config not CDC
        std::list<libusb_config_descriptor *> *configs = usbInterface->GetConfigList();
        assert(configs != nullptr);
        assert(configs->front()->bNumInterfaces == 2);
        const struct libusb_interface *interface = &configs->front()->interface[1];
        assert(interface->altsetting->bInterfaceClass == 10);

        break; //Just test one
    }

}

void TestUsbInterface::init()
{

}

void TestUsbInterface::cleanup()
{

}

void TestUsbInterface::cleanupTestCase()
{
    int status = 0;

    status = usbInterfaceInit.ExitUsbInterfaceArray();
    assert(status == 0);

    for (auto itr = usbIntrfaceArray->begin(); itr != usbIntrfaceArray->end(); ++itr)
    {
        assert(*itr == nullptr);
    }

    // Calls deconstructor which closes log file
    delete(theLogger);
}

void TestUsbInterface::testOpenUsbDevice()
{
    UsbInterface *usbInterface = *usbIntrfaceArray->begin();
    assert(usbInterface != nullptr);
    assert(theLogger == usbInterface->theLogger);
    assert(usbInterface->deviceOpen == true);
}

void TestUsbInterface::testCloseUsbDevice()
{
    UsbInterface *usbInterface = *usbIntrfaceArray->begin();
    usbInterface->CloseDevice();
    assert(usbInterface->devHandle == NULL);
    assert(usbInterface->deviceOpen == false);
    usbInterface->~UsbInterface();
}

QTEST_APPLESS_MAIN(TestUsbInterface)
