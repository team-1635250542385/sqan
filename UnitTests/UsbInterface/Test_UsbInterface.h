//###############################################
// Filename: Test_UsbInterface.h
// Description: Class that provides methods
//              to test UsbInterface class
//###############################################
#ifndef TEST_USBINTERFACE_H
#define TEST_USBINTERFACE_H

#include <QtTest/QTest>
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"

class TestUsbInterface : public QObject
{
    Q_OBJECT

public:
    TestUsbInterface();
    ~TestUsbInterface();

private slots:    
    // These are built into Qt:
    // called before the first test function
    void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    void cleanupTestCase();

    // Test UsbInterface
    void testOpenUsbDevice();
    void testCloseUsbDevice();

private:
    UsbInterfaceInit usbInterfaceInit;
    Logger *theLogger;
    std::array<UsbInterface *, MAX_DEVICES> *usbIntrfaceArray = nullptr;
};

#endif
