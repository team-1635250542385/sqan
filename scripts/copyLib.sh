#!/bin/bash -e

# Check input arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: copyLib.sh top_directory_name"
fi
echo $#

# Remove existing library and symbolic links
rm -rf /usr/lib64/libIsw*

# Copy latest ISW library
cp $1/IswInterface/libIswInterface.so.1.0.0 /usr/lib64

# Make the symbolic links for apps to work
ln -s /usr/lib64/libIswInterface.so.1.0.0 /usr/lib64/libIswInterface.so
ln -s /usr/lib64/libIswInterface.so.1.0.0 /usr/lib64/libIswInterface.so.0
ln -s /usr/lib64/libIswInterface.so.1.0.0 /usr/lib64/libIswInterface.so.0.1

# Show me what you did!
ls -lsa /usr/lib64/libIsw*

