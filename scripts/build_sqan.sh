#!/bin/bash -e

###################################
#  Script to build all the SQAN
#  SQAN Library software and run
#  the unit tests.  This will work
#  locally with one Sqan board on.
#  (Unit tests need a connection to
#  one SQAN radio due to the libusb
#  library)
###################################

# Home directory on DI2E site is /home/jenkins
HOME_DIR=/home/ctuser
BUILD_DIR=$HOME_DIR/qt_workspace/SQAN
QT_BIN_DIR=/opt/Qt/5.13.2/gcc_64/bin/
#QT_BIN_DIR=$HOME_DIR/Qt5/5.13.2/gcc_64/bin

# Check input arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: build_lats.sh branch_name {y/n}"
fi
echo $#

cd $BUILD_DIR

if [ $2 == y ]
then
	if [ -d $BUILD_DIR/$1 ]
	then
	    echo "Directory exists - removing it"
	    rm -rf $1
	fi

	mkdir -p $1
	cd $BUILD_DIR/$1

	# Clone the software onto the test vm
	git clone -b $1 https://bitbucket.di2e.net/scm/Sqan/sqan.git
fi

# Build UsbInterface Unit Tests
cd $BUILD_DIR/$1/sqan/UnitTests/
mkdir -p build-Usb_UnitTests-$1
cd $BUILD_DIR/$1/sqan/UnitTests/build-Usb_UnitTests-$1
$QT_BIN_DIR/qmake ../UsbInterface/Usb_UnitTests.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
./Usb_UnitTests

# Build SqanInterface Unit Tests
cd $BUILD_DIR/$1/sqan/UnitTests
mkdir -p build-Sqan_UnitTests-$1
cd $BUILD_DIR/$1/sqan/UnitTests/build-Sqan_UnitTests-$1
$QT_BIN_DIR/qmake ../SqanInterface/Sqan_UnitTest.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
./Sqan_UnitTest

# Build the Integration Tests:
# SqanInterface

cd $BUILD_DIR/$1/sqan/IntegrationTests
mkdir -p build-Integration_SqanTest

cd ./build-Integration_SqanTest
$QT_BIN_DIR/qmake ../SqanInterface/TestSqanInterface.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Make library
cd $BUILD_DIR/$1/sqan/
mkdir -p build-$1-SqanInterfaceLib
cd build-$1-SqanInterfaceLib
$QT_BIN_DIR/qmake ../SqanInterface/SqanInterface.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
cp ./libSqanInterface.so.0.0.2 ../SqanInterface/

# Give time for copy to complete before deleting directories
sleep 1

# Cleanup
cd $BUILD_DIR/$1/sqan/
rm -rf */build-*
rm -rf */*/build-*
rm -rf ./build-*

