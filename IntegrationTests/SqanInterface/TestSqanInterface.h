#ifndef TESTSQANINTERFACE_H
#define TESTSQANINTERFACE_H

#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "../../Logger/Logger.h"
#include "../../Logger/DBLogger.h"
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../SqanInterface/SqanInterfaceInit.h"
#include "../../SqanInterface/SqanInterface.h"

class TestSqanInterface
{

public:
    TestSqanInterface(uint testNo);
    ~TestSqanInterface();

    bool testSqanDone = false;
    void initSqanTest();
    void *runSqanTest();

private:
    static void *dequeue_messages(void *arguments);
    void exitSqanTest(void);
    int testEndianess(SqanInterface *iswInterface);

    SqanInterfaceInit *sqanInterfaceInit = nullptr;
    std::array<SqanInterface *, 14> *sqanInterfaceArray = nullptr;

    Logger *theLogger = nullptr;
    DBLogger* dbLogger = nullptr;
    // Logger Thread
    pthread_t loggerThread;
    // Used for shutdown
    bool loggerDone = false;

    struct arg_struct {
        void *thisPtr;
        Logger* logger;
        DBLogger *dbLogger;
    };

    uint testsToRun = 0;

    //! Possible tests
    //!
};

#endif // TESTSQANINTERFACE_H
