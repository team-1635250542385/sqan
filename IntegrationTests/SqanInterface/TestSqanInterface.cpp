//!######################################################
//! File: TestSqanInterface.cpp
//! Description: Used for SQAN interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//! SQAN Command Tests:
//!
//!
//!######################################################
#include "TestSqanInterface.h"


TestSqanInterface::TestSqanInterface(uint testNo):
    testsToRun(testNo)
{

}

TestSqanInterface::~TestSqanInterface()
{
    delete(theLogger);
    delete(dbLogger);
}

void *TestSqanInterface::dequeue_messages(void *arguments)
{
    struct arg_struct *args = (struct arg_struct *)arguments;

    TestSqanInterface *thisTestPtr = (TestSqanInterface *)args->thisPtr;
    Logger *theLogger = args->logger;
    DBLogger *dbLogger = args->dbLogger;
    std::cout << "Logger started" << std::endl;

    while ( !thisTestPtr->loggerDone )
    {
        // If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);
        dbLogger->deQueue();
        usleep(100);
    }

    std::cout << "Logger thread ending!" << std::endl;
}

void TestSqanInterface::initSqanTest()
{
    // Logger - beginning of filename with timestamp added
    std::string testFilename = "sqanTest";
    std::string dbLogFilename = "sqanDB";
    QString kitVersion = "1.0.0";

    uint16_t debugLevel = Logger::DEBUG_SQAN_INTERFACE | Logger::DEBUG_USB_INTERFACE;
    Logger* theLogger = new Logger(testFilename, debugLevel);

    // Create DBLogger
    dbLogFilename = "sqanDBLog";
    dbLogger = new DBLogger(dbLogFilename, kitVersion);

    // Start the Logger dequeue thread
    struct arg_struct argsLogger;
    argsLogger.thisPtr = this;
    argsLogger.logger = theLogger;
    argsLogger.dbLogger = dbLogger;

    if (pthread_create(&loggerThread, NULL, &TestSqanInterface::dequeue_messages, (void *)&argsLogger) != 0)
    {
        std::cout << "Logger thread didn't start!" << std::endl;
    }
    else
    {
        std::cout << "Logger thread started" << std::endl;
    }

    sqanInterfaceInit = new SqanInterfaceInit(SqanInterface::USB, theLogger, dbLogger);

    sqanInterfaceArray = sqanInterfaceInit->InitSqanInterfaceArray();
}

void TestSqanInterface::exitSqanTest(void)
{
    // End logger threads
    loggerDone = true;
    sleep(2);

    // Shutdown SqanInterfaces
    if ( sqanInterfaceInit != nullptr )
    {
        sqanInterfaceInit->ExitSqanInterfaceArray();
        delete(sqanInterfaceInit);
    }

    // Make sure all the threads are gone
    pthread_join(loggerThread, nullptr);

    std::cout << "Test thread ending!" << std::endl;
    testSqanDone = true;
}

int TestSqanInterface::testEndianess(SqanInterface *iswInterface)
{
    int status = 0;
    std::string msgString;

    std::cout << "Test Endianess" << std::endl;

    bool isLittleEndian = true;
    if ( htonl(47) == 47 )
    {
        //! Big Endian
        isLittleEndian = false;
    }
    else
    {
        //! Little Endian
        isLittleEndian = true;
    }

    if (iswInterface != nullptr)
    {
        if (iswInterface->IsLittleEndian() != isLittleEndian)
        {
           msgString = "iswInterface endianness != system";
           iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex());
           status = -1;
        }
        else
        {
           msgString = "iswInterface endianness OK";
           iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex());
        }
    }

    return (status);
}


void *TestSqanInterface::runSqanTest()
{
    std::cout << "******************************************************" << std::endl;
    std::cout << "Starting Sqan Interface Tests" << std::endl;
    //std::cout << "testsToRun = " << std::hex << std::to_string(testsToRun) << std::endl;
    std::cout << "******************************************************" << std::endl;

    //! Check that we have devices
    int sqanDeviceCount = 0;
    for (auto iterator = sqanInterfaceArray->begin(); iterator != sqanInterfaceArray->end(); iterator++ )
    {
        SqanInterface *sqanInterface = *iterator;
        if ( (sqanInterface != nullptr) && (sqanInterface->IsReady()) )
        {
            sqanDeviceCount++;

            if ( sqanInterface->GetUsbInterface()->IsDeviceOpen() == false )
            {
                std::cout << "Device " << std::to_string(sqanInterface->GetIndex()) << " is not open. Are you root?" << std::endl;
                sqanDeviceCount--;
            }
        }
    }

    if ( sqanDeviceCount <= 0 )
    {
        //! No devices
        std::cout << "No SQAN devices present!" << std::endl;
        exitSqanTest();
    }
    else
    {
        for (auto iterator = sqanInterfaceArray->begin(); iterator != sqanInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                SqanInterface *sqanInterface = *iterator;
                if ( sqanInterface->IsReady() )
                {
                    UsbInterface *usbInterface = sqanInterface->GetUsbInterface();

                    if ( (usbInterface == nullptr) ||
                            (usbInterface->IsDeviceOpen() == false) )
                    {
                        std::cout << "Usb Interface not Open" << std::endl;
                        continue;
                    }
                    else
                    {

                        //! Test - Get identity of locally attached devices
                        //! The should be setup by the library after the SqanInterfaces are initialized
                        std::cout << "Found SQAN Device " << std::to_string(sqanInterface->GetIndex()) << std::endl;

//                        if ( testsToRun & GetChannel )
//                        {
//                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Channel = " << std::to_string(iswStream->GetSqanChannel()) << std::endl;
//                        }
                    }

                }
            }
        }
        sleep(2);
    }

    sleep(1);

    //! Exit libusb
    exitSqanTest();
}
