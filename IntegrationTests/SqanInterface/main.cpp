//!######################################################
//! File: main.cpp
//! Description: Used for SQAN interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//!
//!######################################################

#include "TestSqanInterface.h"


int main(int argc, char *argv[])
{
//    if ( argc < 2 )
//    {
//        std::cout << "usage: ./TestSqanInterface testValue" << std::endl;
//        std::cout << "See TestSqanInterface.h file" << std::endl;
//        std::cout << "0x7FFFF will run all but the reset commands" << std::endl;
//        exit(0);
//    }

//    std::stringstream ss;
//    ss << argv[1];
    uint testNo = 0;
//    ss >> std::hex >> testNo;

    TestSqanInterface *testSqanInterface = new TestSqanInterface(testNo);

    testSqanInterface->initSqanTest();
    testSqanInterface->runSqanTest();

    while ( !testSqanInterface->testSqanDone )
    {
        sleep(1);
    }

    delete(testSqanInterface);
    std::cout << "SQAN Integration Test completed" << std::endl;

    exit(0);
}
