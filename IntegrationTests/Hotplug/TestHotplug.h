#ifndef TESTHOTPLUG_H
#define TESTHOTPLUG_H

#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "../../Logger/Logger.h"
#include "../../Logger/DBLogger.h"
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../SqanInterface/SqanInterfaceInit.h"
#include "../../SqanInterface/SqanInterface.h"

class TestHotplug
{

public:
    TestHotplug(uint testNo);
    ~TestHotplug();

    bool testSqanDone = false;
    void initSqanTest();
    void *runSqanTest();

private:
    static void *dequeue_messages(void *arguments);
    void exitSqanTest(void);
    //JR void CheckSqanCommands(void);

    SqanInterfaceInit *iswInterfaceInit = nullptr;
    std::array<SqanInterface *, 14> *iswInterfaceArray = nullptr;

    Logger *theLogger = nullptr;
    DBLogger* dbLogger = nullptr;
    // Logger Thread
    pthread_t loggerThread;
    // Used for shutdown
    bool loggerDone = false;

    struct arg_struct {
        void *thisPtr;
        Logger* logger;
        DBLogger *dbLogger;
    };

    uint testsToRun = 0;

    //! Possible tests
    //! See IntegrationTests/SqanInterface
    //! for all the ISW API commands to test
    //! This is a subset to test the ISW Library
    //! while a hotplug event is happening

    //! Identity Commands:
    static const uint SetGetDeviceType = 0x01;
    static const uint SetGetCoordinator = 0x02;
    static const uint GetMacAddress = 0x04;
    static const uint GetPersist = 0x08;

    //! Firmware:
    static const uint GetFirmwareVersion = 0x10;

    //! Product:
    static const uint GetNetworkId = 0x20;

    //! Metrics Commands:
    static const uint GetWirelessThroughput = 0x40;
    static const uint GetPhyRate = 0x80;
    static const uint GetRssi = 0x100;

    //! Stream Commands:
    static const uint GetChannel = 0x4000;

};

#endif // TESTHOTPLUG_H
