//!######################################################
//! File: main.cpp
//! Description: Used for ISW interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//! Identity Command Tests:
//! SetGetDeviceType
//! SetGetCoordinator
//! GetMacAddress
//! GetPersist
//!
//! Firmware Command Tests:
//! GetFirmwareVersion
//!
//! Product:
//! GetNetworkId
//!
//! Metrics Command Tests:
//! GetWirelessThroughput
//! GetPhyRate
//! GetRssi
//!
//! Link Command Tests:
//! SetGetScanDutyCycle
//! ResetSetGetIdleScanFrequency
//!
//! Association Command Tests:
//! ClearStartAssociation
//!
//! Stream Command Tests:
//! SetGetStreamsSpec
//! GetChannel
//! Multicast
//!
//! System Command Tests:
//! SetIndications
//! SetFactoryDefault
//! ResetRadio
//! ResetDevice
//!
//!######################################################

#include "TestHotplug.h"


int main(int argc, char *argv[])
{
    if ( argc < 1 )
    {
        std::cout << "usage: ./TestSqanInterface" << std::endl;
        exit(0);
    }

    uint testNo = 0x7FFFF;
    TestHotplug *testHotplug = new TestHotplug(testNo);

    testHotplug->initSqanTest();
    testHotplug->runSqanTest();

    while ( !testHotplug->testSqanDone )
    {
        sleep(1);
    }

    delete(testHotplug);
    std::cout << "Hotplug Test completed" << std::endl;

    exit(0);
}
