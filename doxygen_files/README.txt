1) Edit lats_doxygen file so that INPUT matches the directory that you are working in.
2) To generate doxygen files for your lats directory, run this command from the top level INPUT directory (eg. /home/ctuser/qt_workspace/master/lats directory):

doxygen doxygen_files/lats_doxyfile *

The results will be in /home/ctuser/qt_workspace/master/lats/doxygen_files/documentation/html directory.

