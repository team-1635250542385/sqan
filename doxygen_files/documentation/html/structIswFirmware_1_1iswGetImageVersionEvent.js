var structIswFirmware_1_1iswGetImageVersionEvent =
[
    [ "apiVersion", "structIswFirmware_1_1iswGetImageVersionEvent.html#abddbf323627d01f3536f120bd61280f5", null ],
    [ "reserved", "structIswFirmware_1_1iswGetImageVersionEvent.html#a6af5f5ec6b83452967869507c5011ebc", null ],
    [ "versionNumber", "structIswFirmware_1_1iswGetImageVersionEvent.html#af3173caec8ad3904da6c483cbb5b1507", null ],
    [ "versionString", "structIswFirmware_1_1iswGetImageVersionEvent.html#a9455f15d50d129cf36a97a8cadee9bff", null ]
];