var structIswLink_1_1IswSetScanDutyCycleCommand =
[
    [ "cmdContext", "structIswLink_1_1IswSetScanDutyCycleCommand.html#adb20ba6e5c30775927ff83d3c679c349", null ],
    [ "cmdType", "structIswLink_1_1IswSetScanDutyCycleCommand.html#a274cb4832633a3f28a40998de9a539fd", null ],
    [ "command", "structIswLink_1_1IswSetScanDutyCycleCommand.html#af93bc906fb31740a1899f8fd2abe7d39", null ],
    [ "persist", "structIswLink_1_1IswSetScanDutyCycleCommand.html#a893a78c6194aacecd0cac845eadf14bf", null ],
    [ "reserved1", "structIswLink_1_1IswSetScanDutyCycleCommand.html#a722b7a6383df485549b287f831c72d4c", null ],
    [ "scanDutyCycle", "structIswLink_1_1IswSetScanDutyCycleCommand.html#ae8d2b45c061af1ca45871a5b9ef2eced", null ],
    [ "startupScanDuration", "structIswLink_1_1IswSetScanDutyCycleCommand.html#a7080ded028dd8879ae3272fab3ffa5d8", null ]
];