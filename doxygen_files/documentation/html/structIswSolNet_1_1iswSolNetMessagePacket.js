var structIswSolNet_1_1iswSolNetMessagePacket =
[
    [ "headerChecksum", "structIswSolNet_1_1iswSolNetMessagePacket.html#a2ba47de71a79f9f4aa96938943573326", null ],
    [ "messageClass", "structIswSolNet_1_1iswSolNetMessagePacket.html#a139c9f5eccf99c88bb9f89a4e25909e4", null ],
    [ "messageId", "structIswSolNet_1_1iswSolNetMessagePacket.html#ab2d50069c3d07e6c396c707e6e5c7189", null ],
    [ "payload", "structIswSolNet_1_1iswSolNetMessagePacket.html#a437c97299930b5bc72f970d0ff69535c", null ],
    [ "payloadChecksum", "structIswSolNet_1_1iswSolNetMessagePacket.html#a8f2cabd3e39f8b637d71d7d42b39f45f", null ],
    [ "payloadLength", "structIswSolNet_1_1iswSolNetMessagePacket.html#a45b3defa3ceabd8a10b5e03cef40bd0c", null ],
    [ "policies", "structIswSolNet_1_1iswSolNetMessagePacket.html#aba4be38a58f88a03637595bd0e5f3e70", null ],
    [ "reserved1", "structIswSolNet_1_1iswSolNetMessagePacket.html#ad84a1f1d278fc89dfe8105fb114fb1a2", null ],
    [ "seqNumber", "structIswSolNet_1_1iswSolNetMessagePacket.html#a4596a2ef426b1bc09ca564880f5d11ac", null ],
    [ "status", "structIswSolNet_1_1iswSolNetMessagePacket.html#ac8aba68794396ece69860f7320884cef", null ]
];