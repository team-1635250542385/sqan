var structIswSolNet_1_1iswSolNetDataPacket =
[
    [ "dataflowId", "structIswSolNet_1_1iswSolNetDataPacket.html#a65a3bca6dc0e08791f20eb49ee06eba2", null ],
    [ "headerChecksum", "structIswSolNet_1_1iswSolNetDataPacket.html#a68dad7025261d5b931c22d67ece87e8a", null ],
    [ "payload", "structIswSolNet_1_1iswSolNetDataPacket.html#a43f4a6f39f96d816ea5e64ab4773987f", null ],
    [ "payloadChecksum", "structIswSolNet_1_1iswSolNetDataPacket.html#a5c90f57e2ddce56a85b7c955e28a91a7", null ],
    [ "payloadLength", "structIswSolNet_1_1iswSolNetDataPacket.html#afab625446a13ef4670576ad51cb9baa4", null ],
    [ "policies", "structIswSolNet_1_1iswSolNetDataPacket.html#aa7e761e170ae5f068232dc87b9c4fb73", null ],
    [ "reserved1", "structIswSolNet_1_1iswSolNetDataPacket.html#a59e794b1c72f00bc849ff567519997bb", null ],
    [ "reserved2", "structIswSolNet_1_1iswSolNetDataPacket.html#a3b9392f9e58ed305a11d44c69c88e81f", null ],
    [ "seqNumber", "structIswSolNet_1_1iswSolNetDataPacket.html#ac9e224299241b26d76d8f07e81656dd8", null ]
];