var structIswStream_1_1iswPeerRecord__Version4 =
[
    [ "averageRSSI", "structIswStream_1_1iswPeerRecord__Version4.html#a235c8141a4f773242ccf840e12e4f751", null ],
    [ "deviceType", "structIswStream_1_1iswPeerRecord__Version4.html#a3f7f3ebcb6ad8ff604eb63f6b0b8f1f2", null ],
    [ "linkQuality", "structIswStream_1_1iswPeerRecord__Version4.html#ae157b13df3586f3512abaf0f4abbad87", null ],
    [ "linkStatus", "structIswStream_1_1iswPeerRecord__Version4.html#aa66c4384bcf8c3656dc1489b3b652ccf", null ],
    [ "macAddress", "structIswStream_1_1iswPeerRecord__Version4.html#a74e5ef258e0a6d26a4f73c63a94b4acc", null ],
    [ "modePhyRate", "structIswStream_1_1iswPeerRecord__Version4.html#a482916e6a46725dfe970f4ab2156c4e5", null ],
    [ "peerIndex", "structIswStream_1_1iswPeerRecord__Version4.html#aed39a6bc77789f7af800c67060cc52ff", null ],
    [ "reserved1", "structIswStream_1_1iswPeerRecord__Version4.html#a5c5e9ea149ac017e6bb2621caa346e37", null ],
    [ "rxThroughput", "structIswStream_1_1iswPeerRecord__Version4.html#afebfccd60b126a361723fef050564749", null ],
    [ "txQueueDepth", "structIswStream_1_1iswPeerRecord__Version4.html#aff7751d16beacf51c27ee76877e465b3", null ],
    [ "txThroughput", "structIswStream_1_1iswPeerRecord__Version4.html#aa13954b874262107761130b898d9bf04", null ]
];