var structIswSolNet_1_1iswSolNetBatteryStatusTuple =
[
    [ "batteryCharging", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a16434f3a469a74637fb398890c322b21", null ],
    [ "batteryLevel", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a44ff77b7d870b67fc1a34b81dafc6daf", null ],
    [ "batteryState", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a0202f9c2177800668d2664937b47ec6b", null ],
    [ "length", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a6984a634f9e99820472843bf40fbba88", null ],
    [ "reserved", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a68d245c487d96efde719c53a9bd42870", null ],
    [ "tupleId", "structIswSolNet_1_1iswSolNetBatteryStatusTuple.html#a7b6e5aa5fcf6959e3b53d67148ed95c8", null ]
];