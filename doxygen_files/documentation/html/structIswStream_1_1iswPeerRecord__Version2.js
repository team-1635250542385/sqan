var structIswStream_1_1iswPeerRecord__Version2 =
[
    [ "deviceType", "structIswStream_1_1iswPeerRecord__Version2.html#ac91def1a76fde1f85a41aa223c5d24ab", null ],
    [ "linkQuality", "structIswStream_1_1iswPeerRecord__Version2.html#a6bcedd405ca8128b73884d351ffececa", null ],
    [ "linkStatus", "structIswStream_1_1iswPeerRecord__Version2.html#ac41399a29911470fba6dc1de8d19c9ee", null ],
    [ "macAddress", "structIswStream_1_1iswPeerRecord__Version2.html#a1691387bcd24d5f800750e2a2c41b75a", null ],
    [ "peerIndex", "structIswStream_1_1iswPeerRecord__Version2.html#a4ad1f6ed38e78c8765bd4b7a0d27616e", null ],
    [ "reserved1", "structIswStream_1_1iswPeerRecord__Version2.html#a856b7b926672ac317c36c2104c12d9c8", null ],
    [ "reserved2", "structIswStream_1_1iswPeerRecord__Version2.html#ab89ab3aa743f32ab3150f6788f6eb4fc", null ],
    [ "txQueueDepth", "structIswStream_1_1iswPeerRecord__Version2.html#a17b336b825f23e236cf8289d7ffb2066", null ]
];