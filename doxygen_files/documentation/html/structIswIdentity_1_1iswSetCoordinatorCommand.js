var structIswIdentity_1_1iswSetCoordinatorCommand =
[
    [ "cmdContext", "structIswIdentity_1_1iswSetCoordinatorCommand.html#adc90ddd78e91b2c2fdbb76a13d4ba15b", null ],
    [ "cmdType", "structIswIdentity_1_1iswSetCoordinatorCommand.html#a1da8a36becc44632d995640eafcccd90", null ],
    [ "command", "structIswIdentity_1_1iswSetCoordinatorCommand.html#a4dba81d457280b217024b314ae319db7", null ],
    [ "coordinatorCapable", "structIswIdentity_1_1iswSetCoordinatorCommand.html#a75f2d7f58ca45f51b188d34faf53781e", null ],
    [ "coordinatorPrecedence", "structIswIdentity_1_1iswSetCoordinatorCommand.html#a426886d6a043bb7d04fe0bfffcb936de", null ],
    [ "persist", "structIswIdentity_1_1iswSetCoordinatorCommand.html#ae594d6fadc581b0a25db8f340d821e13", null ],
    [ "reserved1", "structIswIdentity_1_1iswSetCoordinatorCommand.html#a6f3a63d3e63d369af4bee1b927f545f3", null ],
    [ "reserved2", "structIswIdentity_1_1iswSetCoordinatorCommand.html#af8599e9b1fe96cc663b277a1ddef3032", null ]
];