var structIswLink_1_1IswSetIdleScanFrequenceCommand =
[
    [ "cmdContext", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#ad1c96b55e97793b8813ec0b815b8bcf4", null ],
    [ "cmdType", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#a84bc65b38662dea3574c086da9dd9adc", null ],
    [ "command", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#a1d7b53591d460b98c44f43bc03281a38", null ],
    [ "idleScanFrequency", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#ac8aa280503725b144b98e41aaeb6505e", null ],
    [ "persist", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#aa6a81e624af40e3067dffb5d77fa6ed3", null ],
    [ "reserved1", "structIswLink_1_1IswSetIdleScanFrequenceCommand.html#a48a98cd567304175b9c6f93a664e1e5e", null ]
];