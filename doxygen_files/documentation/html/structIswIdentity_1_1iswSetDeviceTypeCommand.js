var structIswIdentity_1_1iswSetDeviceTypeCommand =
[
    [ "cmdContext", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#a887bf9eb5a9715287279f5171b9c217e", null ],
    [ "cmdType", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#a01c17441040d6b230e933f32583056e7", null ],
    [ "command", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#ada2fba5921a61fcb614dd609af0e07c5", null ],
    [ "deviceType", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#afc60586bece5f1505467414472ead7ee", null ],
    [ "reserved1", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#aac30ce9ca04df5fe8d71711b4d76ad23", null ],
    [ "reserved2", "structIswIdentity_1_1iswSetDeviceTypeCommand.html#a1540ba2a2316d98168c11ad6afd8ab33", null ]
];