var structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple =
[
    [ "azimuth", "structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple.html#a02219ac5fe05d351e1bf63ba0c9d31ea", null ],
    [ "azimuthNorthReference", "structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple.html#a3a0e59a91dab95a13170aacb02c19ee5", null ],
    [ "length", "structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple.html#ab2174061c1652994ec4075b450b309cf", null ],
    [ "reserved", "structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple.html#a6a2ed76424197cd6e27fe220842f863e", null ],
    [ "tupleId", "structIswSolNet_1_1iswSolNetLrfTargetAzimuthDataTuple.html#ac8f409ef8e0eebcdb22a0b77944e5a31", null ]
];