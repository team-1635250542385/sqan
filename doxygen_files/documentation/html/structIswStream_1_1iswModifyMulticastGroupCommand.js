var structIswStream_1_1iswModifyMulticastGroupCommand =
[
    [ "cmdContext", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a2077ebcc95c347cb9a6633c69485c303", null ],
    [ "cmdType", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a320c6be94835e4901e795252750b34b7", null ],
    [ "command", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a38a8583d1f719e22dfd490a216fe25cb", null ],
    [ "mcastPeerIndex", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a712dbbf4d39a25c0f9346c16aca54d48", null ],
    [ "mcastPhyRate", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a7f734416966d5b12ed6948d567db910e", null ],
    [ "reserved1", "structIswStream_1_1iswModifyMulticastGroupCommand.html#a76e9289bc037b26f06f81ae96edebc0d", null ],
    [ "reserved2", "structIswStream_1_1iswModifyMulticastGroupCommand.html#ad7f6564cf22896e988f7231bf8d89f2a", null ]
];