var structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc =
[
    [ "header", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#ae579e9e5af6b002f501aff551335464b", null ],
    [ "mcastAddress", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#a4ddc7c8df4a904323227b203a08090e0", null ],
    [ "mcastGroupId", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#af4b9fd062f1e0c126ba7393eea526da5", null ],
    [ "mcastMacAddress", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#ad78c5e16189daae3b6fde7769e6d9a1d", null ],
    [ "reserved1", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#adb43d07c3d33af54ac6d49ef4d4b292e", null ],
    [ "reserved2", "structIswSolNet_1_1iswSolNetPropertyMulticastGroupDesc.html#a81f5cc9be1e8d42cd94d0e93cda4b872", null ]
];