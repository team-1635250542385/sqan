var structIswMetrics_1_1iswPeerMetrics =
[
    [ "peerIndex", "structIswMetrics_1_1iswPeerMetrics.html#a3d0bd755db4936e5dbcc925d9ec8609d", null ],
    [ "phyRate", "structIswMetrics_1_1iswPeerMetrics.html#ac8a4936649892eb86629dfa317403355", null ],
    [ "receiveThroughput", "structIswMetrics_1_1iswPeerMetrics.html#ae4a7871c52450609efaad4c9f637652f", null ],
    [ "reserved", "structIswMetrics_1_1iswPeerMetrics.html#ab4dcaf73ad3968912402fbf7cfdbe622", null ],
    [ "rssi", "structIswMetrics_1_1iswPeerMetrics.html#a87757004bce41f6bcc9c505ecfecf8cf", null ],
    [ "transmitThroughput", "structIswMetrics_1_1iswPeerMetrics.html#acfe9d140b39de1fcae8a524f6cc7b85e", null ]
];