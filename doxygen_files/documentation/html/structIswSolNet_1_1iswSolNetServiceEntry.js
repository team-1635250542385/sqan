var structIswSolNet_1_1iswSolNetServiceEntry =
[
    [ "dataflowId", "structIswSolNet_1_1iswSolNetServiceEntry.html#a9f6d6a7dd027616ccd0f0a8354f5d392", null ],
    [ "dataPolicies", "structIswSolNet_1_1iswSolNetServiceEntry.html#af018b08c5b84dc4c2b57f68378d76185", null ],
    [ "endpointDistribution", "structIswSolNet_1_1iswSolNetServiceEntry.html#a10ca696194bb5b95aca7f5524f673421", null ],
    [ "endpointId", "structIswSolNet_1_1iswSolNetServiceEntry.html#af93788406b5fbe8e3bf86846af72a016", null ],
    [ "inUse", "structIswSolNet_1_1iswSolNetServiceEntry.html#a9d1a7e60891fd07a542215feb9ea8519", null ],
    [ "nextSendDataSeqNumber", "structIswSolNet_1_1iswSolNetServiceEntry.html#affcf6393058e254b802129ebc0e8f4b7", null ],
    [ "pollDataCallbackFn", "structIswSolNet_1_1iswSolNetServiceEntry.html#ac2f9a9cb5900dfd6f87737ca7ea108ee", null ],
    [ "recCallbackThisPtr", "structIswSolNet_1_1iswSolNetServiceEntry.html#a380a86a9ac35535c812951c049ef04f1", null ],
    [ "receiveCallbackFn", "structIswSolNet_1_1iswSolNetServiceEntry.html#ae5a5825b50c5df420765ee871d9c7065", null ],
    [ "registeredPeers", "structIswSolNet_1_1iswSolNetServiceEntry.html#afc7261c0ce3fd2f552cd33f3b9d91103", null ],
    [ "serviceControlDesc", "structIswSolNet_1_1iswSolNetServiceEntry.html#a7a4b2b278cc7cdde6347a54ef809cd8a", null ],
    [ "serviceDesc", "structIswSolNet_1_1iswSolNetServiceEntry.html#a6170d3ae31adb097c5e9098f6cc69c8b", null ]
];