var structIswFirmware_1_1iswLoadImageCommand =
[
    [ "blockSequence", "structIswFirmware_1_1iswLoadImageCommand.html#a2321d99272dd32102dc45cc066b4ed55", null ],
    [ "blockSize", "structIswFirmware_1_1iswLoadImageCommand.html#a6338cbebc8152d63f3ab55f4fa8871f7", null ],
    [ "cmdContext", "structIswFirmware_1_1iswLoadImageCommand.html#a5c9600bd37bc82f091172d0258c831fe", null ],
    [ "cmdType", "structIswFirmware_1_1iswLoadImageCommand.html#aaf3fb2ce28ae8576a28424ea251348b1", null ],
    [ "command", "structIswFirmware_1_1iswLoadImageCommand.html#a84e2ab533046cad10865d49c5f5f505d", null ],
    [ "data", "structIswFirmware_1_1iswLoadImageCommand.html#af6e7c91291df7014e953cf4c6f20decb", null ],
    [ "lastBlock", "structIswFirmware_1_1iswLoadImageCommand.html#ad22eba86e30055080d40c8cdf8e0a28e", null ],
    [ "reserved1", "structIswFirmware_1_1iswLoadImageCommand.html#a25c2e864b67bfe61b8f591d83584d68b", null ],
    [ "reserved2", "structIswFirmware_1_1iswLoadImageCommand.html#a271992426e2a70c474eeeefaa31e7bbc", null ],
    [ "selector", "structIswFirmware_1_1iswLoadImageCommand.html#aece084a01b3c1feb91c05bf6e12802d5", null ]
];