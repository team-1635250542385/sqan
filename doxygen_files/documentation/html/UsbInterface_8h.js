var UsbInterface_8h =
[
    [ "UsbInterface", "classUsbInterface.html", "classUsbInterface" ],
    [ "ACCESS", "UsbInterface_8h.html#a321a20f839f3d9ccd0db1dc865850dc7", null ],
    [ "LOG_LEVEL_DEBUG", "UsbInterface_8h.html#a130224df8c6bf22a688e3cb74a45689a", null ],
    [ "LOG_LEVEL_ERROR", "UsbInterface_8h.html#a742fc70e331d7e568bd893c514756a29", null ],
    [ "LOG_LEVEL_INFO", "UsbInterface_8h.html#a2e25fe130cf710da4ad800747fdd51f3", null ],
    [ "LOG_LEVEL_NONE", "UsbInterface_8h.html#a43dece650f96e7cf2a4e535c9bd4804a", null ],
    [ "LOG_LEVEL_WARNING", "UsbInterface_8h.html#af539a66abed2a7a15e3443d70a3cf1e1", null ],
    [ "USB_LOGFILE_ERR", "UsbInterface_8h.html#a1ed9193dc2c4276cf08881d7589c3b62", null ]
];