var structIswProduct_1_1iswSetHibernationModeCommand =
[
    [ "cmdContext", "structIswProduct_1_1iswSetHibernationModeCommand.html#a5799168a0ad0610504fca999f65a3f5b", null ],
    [ "cmdType", "structIswProduct_1_1iswSetHibernationModeCommand.html#a1b2a2db9b6c2d638f0b739c62987506e", null ],
    [ "command", "structIswProduct_1_1iswSetHibernationModeCommand.html#a2122416f9a30b331d6c08db2a0688a2b", null ],
    [ "hibernationMode", "structIswProduct_1_1iswSetHibernationModeCommand.html#a80b31012d933953b7d586135346be020", null ],
    [ "idleTime", "structIswProduct_1_1iswSetHibernationModeCommand.html#ae3f6154120ac6757961fdf347b6da14b", null ],
    [ "persist", "structIswProduct_1_1iswSetHibernationModeCommand.html#acc05c2a475e9f6586c03cc2f64a1e699", null ],
    [ "reserved02", "structIswProduct_1_1iswSetHibernationModeCommand.html#a028bf9afeee199f275f5c3fdc217af13", null ],
    [ "reserved1", "structIswProduct_1_1iswSetHibernationModeCommand.html#a4b4246c14d941612bd71276653df18ea", null ],
    [ "softHibernationTime", "structIswProduct_1_1iswSetHibernationModeCommand.html#a3ca6fb5d3943bd3af1cd62be7b00459a", null ]
];