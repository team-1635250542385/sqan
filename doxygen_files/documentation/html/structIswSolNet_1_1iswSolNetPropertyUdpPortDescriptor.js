var structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor =
[
    [ "direction", "structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor.html#aa6fedb7805a2e17ce582753ba3220ea9", null ],
    [ "header", "structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor.html#ab971ffa8665bc6e29d719edb56dda930", null ],
    [ "mcastIpAddress", "structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor.html#ae59e5499ca4f7f3e61beef0f192fab1b", null ],
    [ "reserved", "structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor.html#a7be40f1bac761325609998accf958322", null ],
    [ "udpPort", "structIswSolNet_1_1iswSolNetPropertyUdpPortDescriptor.html#a013ae9f9c9146a67d8027d8d99da8cab", null ]
];