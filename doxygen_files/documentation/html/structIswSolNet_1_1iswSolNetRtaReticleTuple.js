var structIswSolNet_1_1iswSolNetRtaReticleTuple =
[
    [ "length", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a9c02d7ceeea25ebd03cbf0877636d495", null ],
    [ "rtaReticleColorBlue", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a049a7361feb8de995f680ea7c778a9e7", null ],
    [ "rtaReticleColorGreen", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a0785c0de04d894041de23065ad3f4471", null ],
    [ "rtaReticleColorRed", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#ac2389eb8f82608a45965782bba3f5275", null ],
    [ "rtaReticleType", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a01c273b543de83909e3d2ef196a99046", null ],
    [ "rtaReticleXOffset", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#ad58276cca0a0b41c83193ccfa6228262", null ],
    [ "rtaReticleYOffset", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a757c19bfa56b9445bc22c6bde4bcff38", null ],
    [ "tupleId", "structIswSolNet_1_1iswSolNetRtaReticleTuple.html#a94227e0956727cedb2b1ae47cd86f540", null ]
];