var structIswStream_1_1iswJoinMulticastGroupCommand =
[
    [ "cmdContext", "structIswStream_1_1iswJoinMulticastGroupCommand.html#acb46f002364225de472b90b6aa190a09", null ],
    [ "cmdType", "structIswStream_1_1iswJoinMulticastGroupCommand.html#a8472dd5e78845585a517d4b1edcf3629", null ],
    [ "command", "structIswStream_1_1iswJoinMulticastGroupCommand.html#ad9a259f788c36d744dc1d729a8f297af", null ],
    [ "mcastAddress", "structIswStream_1_1iswJoinMulticastGroupCommand.html#a7c768f22b0e88a266bf89f1cabe05656", null ],
    [ "mcastPhyRate", "structIswStream_1_1iswJoinMulticastGroupCommand.html#ad5566f516dad2bf44613f339ec0e22c3", null ],
    [ "reserved1", "structIswStream_1_1iswJoinMulticastGroupCommand.html#aa343a8f15a32868ac7c5d62695dc6462", null ],
    [ "reserved2", "structIswStream_1_1iswJoinMulticastGroupCommand.html#a66e27926aad2e12e96635cdf4cfe14cb", null ]
];