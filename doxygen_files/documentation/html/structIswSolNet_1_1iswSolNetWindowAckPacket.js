var structIswSolNet_1_1iswSolNetWindowAckPacket =
[
    [ "dataflowId", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#ad8b0fe8bd89d08f03897973b4e4bc741", null ],
    [ "endpoint", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#a9191c3faee40e4b24469126df8b723c6", null ],
    [ "protocolClass", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#a882c50c74aefcbcb987f464e9cd0293e", null ],
    [ "reserved", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#a97a7aa3507f7a137ad111739e5607a4c", null ],
    [ "seqNumber", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#a655f62f5999753e582e5c5b1537415ed", null ],
    [ "status", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#a1ea2050488e7b12a337f0679edbea271", null ],
    [ "windowBitmap", "structIswSolNet_1_1iswSolNetWindowAckPacket.html#ab4bea7a54774b3295a854216e3b7fc01", null ]
];