var structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc =
[
    [ "flags", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#a4387fe9f56c8fd86f21f584efad73bce", null ],
    [ "header", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#ae7808c3bb9aa86d753b47bc5d7a19ee8", null ],
    [ "terminalId", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#a091b09820791d29a035ce266a2ec0709", null ],
    [ "type", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#a8f348762075dbb279255ec3731c5d9a9", null ],
    [ "xMax", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#ad869dcfb01541bc2a9c4165e2155ea8a", null ],
    [ "xMin", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#ad85689b21bec0caddd2246e87dbbfcb8", null ],
    [ "yMax", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#afab1f55b42a97c54a74b686748aec4ab", null ],
    [ "yMin", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#a849fe6ba8fb47c268ac5a5f07cb6caae", null ],
    [ "zMax", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#a16d220a7feb5a1d2038ca2462a86980f", null ],
    [ "zMin", "structIswSolNet_1_1iswSolNetPropertySimpleUiMotionDesc.html#ab7829848aae09df34544373d9aa0bce8", null ]
];