var structIswStream_1_1iswEventGetStreamSpec =
[
    [ "maxTxLatency", "structIswStream_1_1iswEventGetStreamSpec.html#a7cf0d9b61b8022c77da6e927923dcb93", null ],
    [ "peerIndex", "structIswStream_1_1iswEventGetStreamSpec.html#a44c784029e76a83f103029c39cb4e563", null ],
    [ "reserved2", "structIswStream_1_1iswEventGetStreamSpec.html#ae0ed5aa48e5bcddc764c37861b579159", null ],
    [ "subStreamTxDiscardable", "structIswStream_1_1iswEventGetStreamSpec.html#a432150d4744a4df9a355424051778609", null ],
    [ "subStreamTxPriority", "structIswStream_1_1iswEventGetStreamSpec.html#a27bff0b7e5f795c3d2195e39d33ac1a8", null ]
];