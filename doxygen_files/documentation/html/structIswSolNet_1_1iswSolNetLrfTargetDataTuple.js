var structIswSolNet_1_1iswSolNetLrfTargetDataTuple =
[
    [ "length", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#a7f549364ddd600f1747838e2b0d10a88", null ],
    [ "rangeEventId", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#acbca0044331639dd7ccb265e0e46b060", null ],
    [ "targetFlags", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#a8d21c81d9da02deb276f583099f51824", null ],
    [ "targetIndex", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#ac0f788bb1613ffd6540d717f4005e9b4", null ],
    [ "targetPropertyTuples", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#aad062c9f04ab229a0d87b8cdb4cad9ac", null ],
    [ "totalNumberOfTargets", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#ae3a374a4d13fd26e6453da5fbeedd748", null ],
    [ "tupleId", "structIswSolNet_1_1iswSolNetLrfTargetDataTuple.html#ad71b7bf2fc733fbf8bd5218b5011bc46", null ]
];