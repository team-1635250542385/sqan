var structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc =
[
    [ "header", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#a957aede77ef12e8c9146491bd29702a2", null ],
    [ "mcastAddress", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#a3d7b0397e53efd203ddcb8aa2576f17a", null ],
    [ "mcastGroupId", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#aeb345bb068912165f1b1a4330cc0f47c", null ],
    [ "mcastMacAddress", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#ae74e3a2cb010a4ebee7111a83192d034", null ],
    [ "reserved1", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#a0e7afb05496b887e8826980416500784", null ],
    [ "reserved2", "structIswSolNet_1_1iswSolNetPropertyMcastGroupDesc.html#a635e41ea055096f661e984055932cc92", null ]
];