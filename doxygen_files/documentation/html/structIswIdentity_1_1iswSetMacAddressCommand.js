var structIswIdentity_1_1iswSetMacAddressCommand =
[
    [ "cmdContext", "structIswIdentity_1_1iswSetMacAddressCommand.html#ab34261138db5d60c0f6efe7e41cdb36a", null ],
    [ "cmdType", "structIswIdentity_1_1iswSetMacAddressCommand.html#a891fdc99f1a4ef42f122775cf546e413", null ],
    [ "command", "structIswIdentity_1_1iswSetMacAddressCommand.html#a9a81cf93e86f9434584d9affb1d4c352", null ],
    [ "macAddress", "structIswIdentity_1_1iswSetMacAddressCommand.html#a4aae29a66d41eab5dc5266aeb75fbdd4", null ],
    [ "reserved1", "structIswIdentity_1_1iswSetMacAddressCommand.html#ab13e1da4189d68a1f29c6bf543abf369", null ],
    [ "reserved2", "structIswIdentity_1_1iswSetMacAddressCommand.html#a188dfe0d99c0b8160e53793420ab3ad7", null ]
];