var structIswStream_1_1iswGetPeerFirmwareVersionCommand =
[
    [ "cmdContext", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#a25d57f94e2126175a4fe421a03f834dd", null ],
    [ "cmdType", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#a45f89e152579b7a1392163e108bec93a", null ],
    [ "command", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#a9841b329564ec0f885aa1c5d09e4feb5", null ],
    [ "peerIndex", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#a7fa6fd8d621260a4c64e68b0b18cc86a", null ],
    [ "reserved1", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#abacdf1bc4e64d7d0762165e7ef0d1c85", null ],
    [ "reserved2", "structIswStream_1_1iswGetPeerFirmwareVersionCommand.html#a20c9f5f6cd88fc3efb9ceeb37459813d", null ]
];