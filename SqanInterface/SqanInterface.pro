#-------------------------------------------------
#
# Project created by QtCreator 2018-12-19T15:56:31
# Project name: IswInterface part of SQAN Linux lib
#
#-------------------------------------------------

QT       += xml dbus

QT       -= gui

TARGET = SqanInterface
TEMPLATE = lib
VERSION = 0.0.1

DEFINES += SQANINTERFACE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../Logger/Logger.cpp \
    ../UsbInterface/UsbInterface.cpp \
    ../UsbInterface/UsbInterfaceInit.cpp \
 #   ./SqanFirmware.cpp \
 #   ./SqanSystem.cpp \
 #   ./SqanIdentity.cpp \
 #   ./SqanSecurity.cpp \
 #   ./SqanAssociation.cpp \
 #   ./SqanStream.cpp \
 #   ./SqanLink.cpp \
 #   ./SqanMetrics.cpp \
 #   ./SqanProduct.cpp \
 #   ./SqanSolNet.cpp \
    ./SqanInterface.cpp \
    ./SqanInterfaceInit.cpp

HEADERS += \
    ../Logger/Logger.h \
    ./Interface.h \
    ../UsbInterface/UsbInterface.h \
    ../UsbInterface/UsbInterfaceInit.h \
 #   ./SqanFirmware.h \
 #   ./SqanSystem.h \
 #   ./SqanIdentity.h \
 #   ./SqanSecurity.h \
 #   ./SqanAssociation.h \
 #   ./SqanStream.h \
 #   ./SqanLink.h \
 #   ./SqanMetrics.h \
 #   ./SqanProduct.h \
 #   ./SqanSolNet.h \
    ./SqanInterface.h \
    ./SqanInterfaceInit.h

LIBS += -L/usr/lib64 -lusb-1.0

unix {
    target.path = /usr/lib64
    INSTALLS += target
}
