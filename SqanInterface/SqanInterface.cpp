//!##########################################################
//! Filename: SqanInterface.cpp
//! Description: Class that provides the SQAN API for the user
//!              application.  Each device has one instance
//!              of an SqanInterface object.  This is the main
//!              object that the user will use to send and
//!              receive SQAN commands.  Each SqanInterface object
//!              has links to:
//!              - An UsbInterface object
//!##########################################################

#include "SqanInterface.h"
#include <arpa/inet.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <byteswap.h>
#include <chrono>
#include <thread>
#include <string>
#include <cstring>
#include <sstream>
#include <unistd.h>
#include <iomanip>


SqanInterface::SqanInterface(Interface *devInterface, uint8_t devType, Logger *logger, DBLogger *dblogger) :
    deviceInterface(devInterface),
    deviceType(devType),
    theLogger(logger),
    dbLogger(dblogger)
{
    if (theLogger == nullptr)
    {
        std::cout << "Logger pointer!" << std::endl;
        exit(-1);
    }
    if (dbLogger == nullptr)
    {
        std::cout << "DBLogger pointer!" << std::endl;
        exit(-1);
    }
}

SqanInterface::~SqanInterface()
{

}

int SqanInterface::SqanInit(uint8_t index)
{
    std::string msgString;
    int status = 0;

    //! Internal device index for logging
    sqanListIndex = index;

    //! Clear queues to start
    // EmptyEndpointQueues();

    switch (deviceType)
    {
    case USB:
    {
        usbInterface = (UsbInterface *)deviceInterface;
        break;
    }
    default:
    {
        std::cout << "Unsupported Interface Type" << std::endl;
        exit(-1);
        break;
    }
    }

    CheckSystemEndianness();

    //InitEndpointQueues();

#ifndef TESTING  //!Don't run during unit tests
    StopSqanReceiveThread();

    if ( usbInterface->IsDeviceOpen() )
    {
       //! Start the thread to handle receive events from the hardware
       //! The sqanReceiveThread is a child thread to SqanInterface so it has
       //! access to public and protected elements and a reference to this
       //! instance of SqanInterface.
       if (pthread_create(&sqanReceiveThread, nullptr, &SqanInterface::ReceiveThreadHandler, this) != 0)
       {
        std::cout << "SQAN Receive Thread didn't start!" << std::endl;
       }

       StartSqanReceiveThread();

       while ( !sqanInterfaceInitComplete )
        {
            usleep(10); //!10 milliseconds
        }

        msgString.clear();
        msgString = "SqanInterface Init completed";
        theLogger->LogMessage(msgString, sqanListIndex);

        //! Two more threads to start
        //! 1) Thread to deliver data packets on data receive queues
        StopSqanReceiveQThread();

        if (pthread_create(&sqanReceiveQThread, nullptr, &SqanInterface::ReceiveQThreadHandler, this) != 0)
        {
//            std::cout << "SQAN Receive SolNet Data Thread didn't start!" << std::endl;
        }

        StartSqanReceiveQThread();

        //! 2) Thread to send data packets on data send queues
        StopSqanSendQThread();

        if (pthread_create(&sqanSendQThread, nullptr, &SqanInterface::SendQThreadHandler, this) != 0)
        {
            std::cout << "SQAN Send Thread didn't start!" << std::endl;
        }

        StartSqanSendQThread();
    }
    else
    {
        msgString = "SqanInterface Init not completed";
        theLogger->LogMessage(msgString, sqanListIndex);
        status = -1;
    }
#else
    StartSqanReceiveQThread();
    StartSqanSendQThread();
#endif
    return (status);
}

void SqanInterface::CheckSystemEndianness()
{
    std::string msgString = "Endianess = ";
    //! Check the system Endianess. use the built in network byte conversion
    //! operations (since network byte order is always Big Endian)
    if ( htonl(47) == 47 )
    {
        //! Big Endian
        isLittleEndian = false;
        msgString.append("Big");
    }
    msgString.append("Little");
    theLogger->LogMessage(msgString, sqanListIndex);
}

void SqanInterface::SetQosOnEndpoint(uint8_t qOs, uint8_t endpoint)
{
    // Applies to send queues only!
    sqanEndpointSendQueues[endpoint].qOs = qOs;
}

//void SqanInterface::InitEndpointQueues()
//{
//    for (uint8_t i=0; i<NumberOfSqanEndpoints; i++)
//    {
//        sqanEndpointReceiveQueues[i].qOs = HiNoDiscard;
//        memset(&sqanEndpointReceiveQueues[i].flushArray[0], 0, 0x7F);
//    }

//    // Per SQAN spec
//    sqanEndpointSendQueues[1].qOs = HiNoDiscard;
//    sqanEndpointSendQueues[2].qOs = HiNoDiscard;
//    sqanEndpointSendQueues[3].qOs = LowNoDiscard;
//}

//void SqanInterface::SetFlushReceiveQueue(uint8_t endpointId, uint8_t dataflowId, bool flush)
//{
//    sqanEndpointReceiveQueues[endpointId].flushArray[dataflowId] = flush;
//}

//void SqanInterface::SetFlushSendQueue(uint8_t endpointId, uint8_t dataflowId, bool flush)
//{
//    sqanEndpointSendQueues[endpointId].flushArray[dataflowId] = flush;
//}

//void SqanInterface::EmptyEndpointQueues()
//{
//    for (uint8_t i=0; i<NumberOfSqanEndpoints; i++)
//    {
//        // Free the SendQueues items
//        // Called on shutdown - no sleep
//        while ( !sqanEndpointSendQueues[i].sendQ.empty() )
//        {
//            RemoveSendQueueFront(i);
//        }
//        memset(&sqanEndpointSendQueues[i].flushArray[0], 0, 0x7F);

//        // Free the ReceiveQueues items
//        while ( !sqanEndpointReceiveQueues[i].receiveQ.empty() )
//        {
//            RemoveReceiveQueueFront(i);
//        }
//        memset(&sqanEndpointReceiveQueues[i].flushArray[0], 0, 0x7F);
//    }
//}


void SqanInterface::SetDeviceIsReady(bool deviceReady)
{
    sqanCmdLock.lock();
    sqanDeviceIsReady = deviceReady;
    sqanCmdLock.unlock();
}

void SqanInterface::SqanExit()
{
    if ( sqanReceiveThread != 0 )
    {
        StopSqanReceiveThread();
#ifndef TESTING  //!Don't run during unit tests
        pthread_join(sqanReceiveThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleSqanReceiveEvents ended" << std::endl;
#endif
    }

    if ( sqanReceiveQThread != 0 )
    {
        StopSqanReceiveQThread();
#ifndef TESTING  //!Don't run during unit tests
        pthread_join(sqanReceiveQThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleSqanReceiveQueues ended" << std::endl;
#endif
    }

    if ( sqanSendQThread != 0 )
    {
        StopSqanSendQThread();
#ifndef TESTING  //!Don't run during unit tests
        pthread_join(sqanSendQThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleSqanSendQueues ended" << std::endl;
#endif
    }

    //! Empty the Endpoint queues
    //JR EmptyEndpointQueues();
}

void SqanInterface::BigToLittleEndian(uint64_t *input, uint16_t type)
{
    //! This method uses GNU host to Little Endian macros
    //! If you are on a Little Endian host, it does nothing
    //! They only swap if the host is identified as Big Endian
    switch (type)
    {
    case 16:
    {
        htole16(uint16_t (*input));
        break;
    }
    case 32:
    {
        htole32(uint32_t (*input));
        break;
    }
    case 64:
    {
        htole64(uint64_t (*input));
        break;
    }
    }
}

void SqanInterface::DeliverSqanMessage(/*JR sqanRoutingHdr *routingHdr,*/ sqanEvent *event, uint8_t *data)
{
    std::string msgString;

    switch (event->eventType)
    {
        default:
        {
            msgString = "Received Another Event";
            theLogger->LogMessage(msgString, sqanListIndex);
            break;
        }
    }
}

void SqanInterface::StopSqanReceiveThread()
{
    sqanCmdLock.lock();
    enableReadSqan = false;
    sqanCmdLock.unlock();
}

void SqanInterface::StartSqanReceiveThread()
{
    sqanCmdLock.lock();
    enableReadSqan = true;
    sqanCmdLock.unlock();
}

void SqanInterface::StopSqanReceiveQThread()
{
    sqanCmdLock.lock();
    processDataReceiveQueues = false;
    sqanCmdLock.unlock();
}

void SqanInterface::StartSqanReceiveQThread()
{
    sqanCmdLock.lock();
    processDataReceiveQueues = true;
    sqanCmdLock.unlock();
}

void SqanInterface::HandleSqanReceiveQueues(void)
{
    std::string msgString;

#ifndef TESTING  //!Don't run during unit tests
    //! Wait for main thread to be ready
    while ( !processDataReceiveQueues )
    {
        usleep(10);
    }

    msgString = "HandleSqanReceiveQueues thread started";
    theLogger->LogMessage(msgString, sqanListIndex);

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleSqanReceiveQueues started" << std::endl;

    while ( processDataReceiveQueues )
    {
        // Process in this order:
        // HiNoDiscard until empty
        // HiDiscard for three - five messages
        // If HiNoDiscard queues are empty - process LowPriority queues:
        // LowNoDiscard then LowDiscard
        // By default - all queues are HiNoDiscard, so cycle through them
        // SolNet control messages use endpoint zero and queue zero
        // SolNet data queues are 1 - 3 as they use endpoints 1 - 3
        for (uint8_t endpoint=0; endpoint<NumberOfSqanEndpoints; endpoint++)
        {
            // Just send one of these - can change this later
            if ( !sqanEndpointReceiveQueues[endpoint].receiveQ.empty() )
            {
                // Get oldest item on front of queue
                sqanDataReceiveItem *receiveItem = GetNextFromReceiveQueue(endpoint);

                if ( receiveItem != nullptr )
                {
                   /*JR SqanSolNet::sqanSolNetHeader *dataPkt = (SqanSolNet::sqanSolNetHeader *)receiveItem->data;

                    // Check if we are to flush the queue for this dataflowId
                    // which happens when we shutdown a data traffic stream from the application
                    if ( sqanEndpointReceiveQueues[endpoint].flushArray[dataPkt->sqanSolNetDataPkt.dataflowId] == false )
                    {
                        // Deliver the packet to the application
                        sqanSolNet->DeliverSolNetPacket(receiveItem->peerIndex, endpoint, receiveItem->data);
                    }*/

                    // Cleanup and delete resources for item just sent
                    RemoveReceiveQueueFront(endpoint);
                }
                usleep(10);
            }
        }
        usleep(10);
    }
#endif
}

void SqanInterface::AddToReceiveQueue(uint8_t peerIndex, uint8_t endpoint, uint8_t *data)
{
    sqanEndpointReceiveQueues[endpoint].qLock.lock();

    sqanDataReceiveItem *receiveItem = new sqanDataReceiveItem;
    receiveItem->peerIndex = peerIndex;
    receiveItem->data = data;
    sqanEndpointReceiveQueues[endpoint].receiveQ.push_back(receiveItem);

    sqanEndpointReceiveQueues[endpoint].qLock.unlock();
}

SqanInterface::sqanDataReceiveItem *SqanInterface::GetNextFromReceiveQueue(uint8_t endpoint)
{
    sqanEndpointReceiveQueues[endpoint].qLock.lock();
    sqanDataReceiveItem *receiveItem = sqanEndpointReceiveQueues[endpoint].receiveQ.front();
    sqanEndpointReceiveQueues[endpoint].qLock.unlock();

    return (receiveItem);
}

void SqanInterface::RemoveReceiveQueueFront(uint8_t endpoint)
{
    sqanEndpointReceiveQueues[endpoint].qLock.lock();

    // Remove list element and delete
    std::list<sqanDataReceiveItem *>::iterator it = sqanEndpointReceiveQueues[endpoint].receiveQ.begin();
    sqanDataReceiveItem *receiveItem = *it;
    if ( receiveItem->data != nullptr )
    {
        delete (receiveItem->data);
    }
    delete (receiveItem);
    it = sqanEndpointReceiveQueues[endpoint].receiveQ.erase(it);

    sqanEndpointReceiveQueues[endpoint].qLock.unlock();
}

void SqanInterface::HandleSqanReceiveEvents(void)
{
    int status = 0;
    std::string msgString;
    int numRead = 0;

#ifndef TESTING  //!Don't run during unit tests
    //! Wait for main thread to be ready
    while ( !enableReadSqan )
    {
        usleep(10);
    }

    msgString = "HandleSqanReceiveEvents thread started";
    theLogger->LogMessage(msgString, sqanListIndex);

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleSqanReceiveEvents started" << std::endl;

    if ( usbInterface->IsDeviceOpen() )
    {
        sqanInterfaceInitComplete = true;
    }

    while ( usbInterface->IsDeviceOpen() && enableReadSqan )
    {
        static int recLength = usbInterface->GetMaxLibUsbPktSize();
        uint8_t receiveBuf[recLength];

        std::memset(receiveBuf, 0, recLength);
        numRead = 0;

        //! Check if the read thread has been stopped
        //! Could happen when test is shutting down
        if ( enableReadSqan )
        {
            switch (deviceType)
            {
                case USB:
                {
                    //! Check again in case device was removed - hotplug
                    if ( usbInterface->IsDeviceOpen() )
                    {
                        uint8_t usbEndpoint = usbInterface->GetEndpointIN();
                        status = deviceInterface->ReadDevice(receiveBuf, usbEndpoint, recLength, &numRead);
                    }
                    else
                    {
                        msgString = "HandleSqanReceiveEvents failed device not open";
                        theLogger->LogMessage(msgString, usbInterface->GetIndex());
                        status = -1;
                    }
                    break;
                }
                default:
                {
                    status = -1;
                    break;
                }
            }
        }
        else
        {
            status = -1;
        }

        if ( (status == 0) && (numRead > 0) )
        {
           /*JR SWIM_Header *swimHeader = nullptr;
            // Check for SolNet packets
            swimHeader = (SWIM_Header *)(receiveBuf);*/

            if ( theLogger->DEBUG_SqanInterface == 1 )
            {
                std::stringstream ss;
                ss << "Sqan receiving " << std::to_string(numRead) << std::endl;
                ss << "receiveBuf = ";
                ss << std::setfill('0');
                for (int i=0; i<numRead; i++)
                {
                    ss <<  std::hex << std::setw(2) << std::to_string(receiveBuf[i]) << " ";
                }
                ss << std::endl;
               //JR ss << " transferSize = " << std::to_string(swimHeader->transferSize) << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());
            }

           /*JR if ( (sqanSecurity->IsInterfaceReady() == true) &&
                 ((swimHeader->peerIndex < 0x3F) || (swimHeader->peerIndex == 0xFF) || (swimHeader->peerIndex >= 0x40 && swimHeader->peerIndex <= 0x5E)) )
            { */
                // SolNet packet
                if ( numRead /*JR> (int)( SQAN_SWIM_HDR_SIZE + SqanSolNet::Size_SolNet_Header_Only) */)
                {
                   /*JR uint8_t peerIndex = swimHeader->peerIndex;
                    uint8_t endpoint = swimHeader->streamNumber;*/

                    // SolNet Packet
                    uint8_t *solNetPtr = (uint8_t *)(receiveBuf /*JR + SQAN_SWIM_HDR_SIZE*/);
                   //JR SqanSolNet::sqanSolNetHeader *sqanSolNetHdr = (SqanSolNet::sqanSolNetHeader *)solNetPtr;
                    uint8_t *data = nullptr;

                   /*JR  if ( sqanSolNetHdr->protocolClass == SqanSolNet::MessagePacket )
                    {
                        // Buffer that will hold the complete SolNet Packet
                        //JR data = new uint8_t[swimHeader->transferSize];
                        // Copy in what we received
                        //JR memcpy(data, solNetPtr, swimHeader->transferSize);

                        // Is there more data following this first SolNet packet with header?
                        int totalSolNetPacketSize = SqanSolNet::Size_SolNet_Header_Only + SqanSolNet::Size_SolNet_MsgPktHdr + sqanSolNetHdr->sqanSolNetDataPkt.payloadLength;

                        // Buffer that will hold the complete SolNet Packet
                        data = new uint8_t[totalSolNetPacketSize];
                        // Copy in what we already received
                        //memcpy(data, solNetPtr, swimHeader->transferSize);
                        memcpy(data, solNetPtr, totalSolNetPacketSize);
                    }
                    else
                    { */
                        // SolNet Data Packet - check for multiple USB buffers

                        // Is there more data following this first SolNet packet with header?
                        int totalSolNetPacketSize = 4054; //SqanSolNet::Size_SolNet_Header_Only + SqanSolNet::Size_SolNet_DataPktHdr + sqanSolNetHdr->sqanSolNetDataPkt.payloadLength;

                        // Buffer that will hold the complete SolNet Packet
                        data = new uint8_t[totalSolNetPacketSize];
                        // Copy in what we already received
                        //memcpy(data, solNetPtr, swimHeader->transferSize);
                        memcpy(data, solNetPtr, totalSolNetPacketSize);

                        // If there is more data to get, it will be coming next from USB read
                        if ( totalSolNetPacketSize /*JR > swimHeader->transferSize*/ )
                        {
                            int bytesToGet = totalSolNetPacketSize /*JR - swimHeader->transferSize*/;
                            int bufLength = bytesToGet;
                            uint8_t bytesLeftBuf[bufLength];

                            while ( bytesToGet > 0 )
                            {
                                int numberReadIn = 0;
                                memset(bytesLeftBuf, 0, bufLength);
                                // The next incoming USB data will be the rest of the SolNet packet
                                switch (deviceType)
                                {
                                    case USB:
                                    {
                                        //! Check again in case device was removed - hotplug
                                        if ( usbInterface->IsDeviceOpen() )
                                        {
                                            uint8_t usbEndpoint = usbInterface->GetEndpointIN();
                                            status = deviceInterface->ReadDevice(bytesLeftBuf, usbEndpoint, bytesToGet, &numberReadIn);
                                        }
                                        else
                                        {
                                            msgString = "HandleSqanReceiveEvents failed device not open";
                                            theLogger->LogMessage(msgString, usbInterface->GetIndex());
                                            status = -1;
                                        }
                                        break;
                                    }
                                    default:
                                    {
                                        msgString = "HandleSqanReceiveEvents failed bad device";
                                        theLogger->LogMessage(msgString, usbInterface->GetIndex());
                                        break;
                                    }
                                }

                                if ( numberReadIn <= 0 )
                                {
                                    break;
                                }
                                else
                                {
                                    // Move pointer after data we already received
                                    uint8_t *bytesToGetPtr = (uint8_t *)(bytesLeftBuf /*JR + SQAN_SWIM_HDR_SIZE*/);
                                    // Copy in what we just received
                                    memcpy(data, bytesToGetPtr, numberReadIn);

                                    bytesToGet -= numberReadIn;
                                }
                            }
                        }
                    //JR}

                    // Add buffer with complete SolNet data packet
                    //JR AddToReceiveQueue(peerIndex, endpoint, data);
                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                    AddToReceiveQueue(0x00, usbEndpoint, data);
                   //JR AddToReceiveQueue(0x00, 0x01, data);
                }
                else
                {
                    msgString = "ReadDevice numRead on SolNet is less than header sizes";
                    theLogger->LogMessage(msgString, sqanListIndex);
                }
            }
            else
            {
                // SQAN Control Message
                //JR sqanRoutingHdr *routingHdr = nullptr;
                sqanEvent *event = nullptr;
                uint8_t *data = nullptr;

                if ( numRead >= (int)(/*JR SQAN_SWIM_HDR_SIZE + SQAN_ROUTING_HDR_SIZE + */SQAN_EVENT_SIZE) )
                {
                    //JR RemoveSwimHeader(receiveBuf, &swimHeader, &routingHdr, &event, &data);

                    if ( event != nullptr )
                    {
                        switch (event->resultCode)
                        {
                        case SQAN_CMD_SUCCESS:
                        {
                            DeliverSqanMessage(/*JRroutingHdr, */event, data);
                            break;
                        }
                        case SQAN_CMD_FAILURE_OPERATION_PENDING:
                        {
                            msgString = "ReadDevice operation pending";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_GENERAL:
                        {
                            msgString = "ReadDevice general failure";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_NOT_FOUND:
                        {
                            msgString = "ReadDevice failure - not found";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_ID_MISMATCH:
                        {
                            msgString = "ReadDevice failure - id mismatch";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_INSUFF_MEMORY:
                        {
                            msgString = "ReadDevice failure - insufficient memory";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_INVALID_ACTION:
                        {
                            msgString = "ReadDevice failure - invalid action";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        case SQAN_CMD_FAILURE_TIMEOUT:
                        {
                            msgString = "ReadDevice failure - timeout";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        default:
                        {
                            msgString = "ReadDevice bad event result code";
                            theLogger->LogMessage(msgString, sqanListIndex);
                            break;
                        }
                        }
                    }
                }
                else
                {
                    msgString = "ReadDevice numRead less than header sizes";
                    theLogger->LogMessage(msgString, sqanListIndex);
                }
            }
       /* }
        else
        {
            msgString = "ReadDevice bad status or no data";
            theLogger->LogMessage(msgString, sqanListIndex);
        }*/

        usleep(10); //! Sleep 10 microseconds
    }
#endif
}

void SqanInterface::StartSqanSendQThread()
{
    sqanCmdLock.lock();
    processDataSendQueues = true;
    sqanCmdLock.unlock();
}

void SqanInterface::StopSqanSendQThread()
{
    sqanCmdLock.lock();
    processDataSendQueues = false;
    sqanCmdLock.unlock();
}

void SqanInterface::HandleSqanSendQueues(void)
{
    int status = 0;
    std::string msgString;

#ifndef TESTING  //!Don't run during unit tests
    //! Wait for main thread to be ready
    while ( !processDataSendQueues )
    {
        usleep(10);
    }

    msgString = "HandleSqanSendQueues thread started";
    theLogger->LogMessage(msgString, sqanListIndex);

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleSqanSendQueues started" << std::endl;

    while ( processDataSendQueues )
    {
        // Process in this order:
        // HiNoDiscard until empty
        // HiDiscard for three - five messages
        // If HiNoDiscard queues are empty - process LowPriority queues:
        // LowNoDiscard then LowDiscard
        // By default - all queues are HiNoDiscard, so cycle through them
        // Queue Zero is used for broadcast or multicast SolNet data packets
        // Queues 1 - 3 are used for unicast SolNet data packets
        for (uint8_t endpoint=0; endpoint<NumberOfSqanEndpoints; endpoint++)
        {
            if ( sqanEndpointSendQueues[endpoint].qOs == HiNoDiscard )
            {
                while ( !sqanEndpointSendQueues[endpoint].sendQ.empty() )
                {
                    // Get oldest item on front of queue
                    sqanDataSendItem *sendItem = GetNextFromSendQueue(endpoint);

                   //JR SqanSolNet::sqanSolNetHeader *dataPkt = (SqanSolNet::sqanSolNetHeader *)sendItem->sqanCmd;

                    // Check if we are to flush the queue for this dataflowId
                    // which happens when we shutdown a data traffic stream from the application
                   //JR  if ( sqanEndpointSendQueues[endpoint].flushArray[dataPkt->sqanSolNetDataPkt.dataflowId] == false )
                   //JR {
                        // Deliver the packet to the radio
                        //status = SyncSendSolNetDataPacket(sendItem->sqanCmd, sendItem->peerIndex, endpoint);
                    //JR }

                    // Cleanup and delete resources for item just sent
                    RemoveSendQueueFront(endpoint);
                    usleep(10);
                }
            }
        }

        // Process low priority queues
        for (uint8_t endpoint=1; endpoint<NumberOfSqanEndpoints; endpoint++)
        {
            // Just send one of these - can change this later
            if ( (!sqanEndpointSendQueues[endpoint].sendQ.empty()) &&
                 (sqanEndpointSendQueues[endpoint].qOs != HiNoDiscard) )
            {
                // Get oldest item on front of queue
                sqanDataSendItem *sendItem = GetNextFromSendQueue(endpoint);

                //status = SyncSendSolNetDataPacket(sendItem->sqanCmd, sendItem->peerIndex, endpoint);

                // Cleanup and delete resources for item just sent
                RemoveSendQueueFront(endpoint);
                usleep(10);
            }
        }
    }
#endif
}


uint8_t SqanInterface::GetNextCmdCount()
{
    uint8_t nextCmdCount = 0;

    sqanCmdLock.lock();
    sqanCmdCount++;
    nextCmdCount = sqanCmdCount;
    sqanCmdLock.unlock();
    return (nextCmdCount);
}

int SqanInterface::SyncSendSqanCmd(sqanCommand *sqanCmmnd/*JR, sqanRoutingHdr *routingHdr*/)
{
    std::string msgString;
    int status = 0;

    //! Don't try to send if the device is not open or
    //! or the read thread hasn't started yet
    if ( usbInterface->IsDeviceOpen() && enableReadSqan )
    {
        static int bufLength = 0;
        uint32_t sendLength = 0;

        bufLength = usbInterface->GetMaxPacketSizeOut();
        uint8_t sendBuf[bufLength];
        std::memset(sendBuf, 0, bufLength);

        //! Add the SWIM Header and data to the sendBuf
        //! Send length is total amount of SQAN info in buffer
        //JR sendLength = AddSwimHeader(sendBuf, routingHdr, sqanCmmnd);

        //JR if ( sendLength > 0 )
        //JR {
            if ( theLogger->DEBUG_SqanInterface == 1 )
            {
                std::stringstream ss;
                ss << "sendBuf = ";
                ss << std::setfill('0');
                for (unsigned int i=0; i<sendLength; i++)
                {
                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";
                }
                ss << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());
            }

            switch (deviceType)
            {
            case USB:
            {
                if ( usbInterface->IsDeviceOpen() )
                {
                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                    //JR   status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                    status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
                    usleep(10);
                }
                else
                {
                    msgString = "SyncSendSqanCmd failed - device not open";
                    theLogger->LogMessage(msgString, GetIndex());
                    status = -1;
                }
                break;
            }
            default:
            {
                uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                //JR status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
                break;
            }
            }

            if ( status < 0 )
            {
                msgString = "SyncSendSqanCmd failed - Device returned status != 0";
                theLogger->LogMessage(msgString, GetIndex());
                status = -1;
            }
       /*JR }
        else
        {
            msgString = "SyncSendSqanCmd: no data to send?";
            theLogger->LogMessage(msgString, GetIndex());
            status = -1;
        }*/
    }
    else
    {
        msgString = "SyncSendSqanCmd failed - device not open";
        theLogger->LogMessage(msgString, GetIndex());
        status = -1;
    }

    return (status);
}

void SqanInterface::AddToSendQueue(sqanCommand *sqanCmmnd, uint8_t peerIndex, uint8_t endpoint)
{
    sqanEndpointSendQueues[endpoint].qLock.lock();

    sqanDataSendItem *sendItem = new sqanDataSendItem;
    sendItem->peerIndex = peerIndex;
    sendItem->sqanCmd = sqanCmmnd;
    sqanEndpointSendQueues[endpoint].sendQ.push_back(sendItem);

    sqanEndpointSendQueues[endpoint].qLock.unlock();
}

SqanInterface::sqanDataSendItem *SqanInterface::GetNextFromSendQueue(uint8_t endpoint)
{
    sqanEndpointSendQueues[endpoint].qLock.lock();
    sqanDataSendItem *sendItem = sqanEndpointSendQueues[endpoint].sendQ.front();
    sqanEndpointSendQueues[endpoint].qLock.unlock();

    return (sendItem);
}

void SqanInterface::RemoveSendQueueFront(uint8_t endpoint)
{
    sqanEndpointSendQueues[endpoint].qLock.lock();

    // Remove list element and delete
    std::list<sqanDataSendItem *>::iterator it = sqanEndpointSendQueues[endpoint].sendQ.begin();
    sqanDataSendItem *sendItem = *it;
    delete (sendItem->sqanCmd);
    delete (sendItem);
    it = sqanEndpointSendQueues[endpoint].sendQ.erase(it);

    sqanEndpointSendQueues[endpoint].qLock.unlock();
}


//int SqanInterface::SyncSendSolNetMessagePacket(sqanCommand *sqanCmmnd, uint8_t peerIndex, uint8_t endpoint)
//{
//    std::string msgString;
//    int status = 0;

//   //JR sqanSolNet->sqanSolNetCmdLock.lock();

//    //! Don't try to send if the device is not open or
//    //! or the read thread hasn't started yet
//    if ( usbInterface->IsDeviceOpen() && enableReadSqan )
//    {
//        static int bufLength = 0;
//        uint32_t sendLength = 0;

//        bufLength = usbInterface->GetMaxPacketSizeOut();
//        uint8_t sendBuf[bufLength];
//        std::memset(sendBuf, 0, bufLength);

//        //! Add the SWIM Header and data to the sendBuf
//        //! Send length is total amount of SQAN info in buffer
//        //JR sendLength = AddSolNetSwimHeader(sendBuf, sqanCmmnd, peerIndex, endpoint);

//        /*JR if ( sendLength > 0 )
//        {*/
//            if ( theLogger->DEBUG_SqanInterface == 1 )
//            {
//                std::stringstream ss;
//                ss << "sendBuf = ";
//                ss << std::setfill('0');
//                for (unsigned int i=0; i<sendLength; i++)
//                {
//                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";
//                }
//                ss << std::endl;
//                theLogger->DebugMessage(ss.str(), GetIndex());
//            }

//            switch (deviceType)
//            {
//            case USB:
//            {
//                if ( usbInterface->IsDeviceOpen() )
//                {
//                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
//                    //JR status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
//                    status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
//                    usleep(10);
//                }
//                else
//                {
//                    msgString = "SyncSendSqanCmd failed - device not open";
//                    theLogger->LogMessage(msgString, GetIndex());
//                    status = -1;
//                }
//                break;
//            }
//            default:
//            {
//                uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
//                //JR status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
//                status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
//                break;
//            }
//            }

//            if ( status < 0 )
//            {
//                msgString = "SyncSendSqanCmd failed - Device returned status != 0";
//                theLogger->LogMessage(msgString, GetIndex());
//            }
//        /* JR}
//        else
//        {
//            msgString = "SyncSendSqanCmd: no data to send?";
//            theLogger->LogMessage(msgString, GetIndex());
//            status = -1;
//        }*/
//    }
//    else
//    {
//        msgString = "SyncSendSqanCmd failed - device not open";
//        theLogger->LogMessage(msgString, GetIndex());
//        status = -1;
//    }
//    //JR sqanSolNet->sqanSolNetCmdLock.unlock();

//    return (status);
//}

//int SqanInterface::SyncSendSolNetDataPacket(sqanCommand *sqanCmmnd, uint8_t peerIndex, uint8_t endpoint)
//{
//    std::string msgString;
//    int status = 0;

//   //JR sqanSolNet->sqanSolNetCmdLock.lock();

//    //! Don't try to send if the device is not open or
//    //! or the read thread hasn't started yet
//    if ( usbInterface->IsDeviceOpen() && enableReadSqan )
//    {
//        static int bufLength = 0;
//        uint32_t sendLength = 0;

//        bufLength = usbInterface->GetMaxLibUsbPktSize();
//        uint8_t sendBuf[bufLength];
//        std::memset(sendBuf, 0, bufLength);

//        //! Add the SWIM Header and data to the sendBuf
//        //! Send length is total amount of SQAN info in buffer
//        //JR sendLength = AddSolNetSwimHeader(sendBuf, sqanCmmnd, peerIndex, endpoint);

//       //JR if ( sendLength > 0 )
//       // {
//            if ( theLogger->DEBUG_SqanInterface == 1 )
//            {
//                std::stringstream ss;
//                ss << "sendBuf = ";
//                ss << std::setfill('0');
//                for (unsigned int i=0; i<sendLength; i++)
//                {
//                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";
//                }
//                ss << std::endl;
//                theLogger->DebugMessage(ss.str(), GetIndex());
//            }

//            switch (deviceType)
//            {
//            case USB:
//            {
//                auto start = std::chrono::steady_clock::now();
//                if ( usbInterface->IsDeviceOpen() )
//                {
//                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
//                    //JR status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
//                    status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
//                    usleep(10);
//                }
//                else
//                {
//                    msgString = "SyncSendSqanCmd failed - device not open";
//                    theLogger->LogMessage(msgString, GetIndex());
//                    status = -1;
//                }

//                // auto end = std::chrono::steady_clock::now();

//                // auto microsec = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
//                // auto throughput = (sendLength/microsec)* 8; // Mb/sec
//                // std::cout << "USB Handle Send Data throughput in Mb/s: " << sendLength << std::endl;
                
//                auto end = std::chrono::steady_clock::now();

//                auto microsec = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
//                auto throughput = (bufLength/microsec)* 8; // Mb/sec
//                std::cout << "USB Handle Send Data throughput in Mb/s: " << throughput << std::endl;

//                break;
//            }
//            default:
//            {
//                uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
//                //JR status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
//                status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, bufLength);
//                break;
//            }
//            }

//            if ( status < 0 )
//            {
//                msgString = "SyncSendSqanCmd failed - Device returned status != 0";
//                theLogger->LogMessage(msgString, GetIndex());
//            }
//        /*JR }
//        else
//        {
//            msgString = "SyncSendSqanCmd: no data to send?";
//            theLogger->LogMessage(msgString, GetIndex());
//            status = -1;
//        }*/
//    }
//    else
//    {
//        msgString = "SyncSendSqanCmd failed - device not open";
//        theLogger->LogMessage(msgString, GetIndex());
//        status = -1;
//    }
//   //JR sqanSolNet->sqanSolNetCmdLock.unlock();

//    return (status);
//}

