//!###################################################
//! Filename: SqanInterfaceInit.cpp
//! Description: Class that initalizes each device
//!              Under test with an SqanInterface
//!              and a UsbInterface class.  It returns
//!              a list of SqanInterface pointers
//!###################################################

#include "SqanInterfaceInit.h"
#include "SqanInterface.h"
#include <unistd.h>
#include <signal.h>


SqanInterfaceInit::SqanInterfaceInit(uint8_t devType, Logger *logger, DBLogger *dblogger) :
    deviceType(devType),
    theLogger(logger),
    dbLogger(dblogger)
{
    if ( theLogger == nullptr )
    {
        std::cout << "Bad Logger pointer!" << std::endl;
        exit(-1);
    }
    if ( dbLogger == nullptr )
    {
        std::cout << "Bad DBLogger pointer!" << std::endl;
        exit(-1);
    }
}

SqanInterfaceInit::~SqanInterfaceInit()
{
}

std::array<SqanInterface *, MAX_SQAN_NODES> *SqanInterfaceInit::InitSqanInterfaceArray(void)
{

    //! Initialize the SQAN array to nullptrs until we get valid devices
    for (auto itr = sqanInterfaceArray.begin(); itr != sqanInterfaceArray.end(); ++itr)
    {
        *itr = nullptr;
    }

    switch (deviceType)
    {
    case SqanInterface::USB:
    {
        //! Enumerate the USB devices and get the list of just
        //! SQAN USB devices from libusb with the next calls
        usbInterfaceInit = new UsbInterfaceInit();
        std::array<UsbInterface *, MAX_DEVICES> *usbDevArray = usbInterfaceInit->InitUsbInterfaceArray(theLogger);

        if ( usbInterfaceInit->sqanDeviceCount > 0 )
        {
            for (auto itr = usbDevArray->begin(); itr != usbDevArray->end(); ++itr )
            {
                //! Add this UsbInterface to an SqanInterface to go in the list
                //! Each SQAN USB device has one SqanInterface which holds the
                //! UsbInterface reference
                UsbInterface *usbInterface = *itr;

                if ( usbInterface != nullptr )
                {
                    SqanInterface *sqanInterface = new SqanInterface(usbInterface, SqanInterface::USB, theLogger, dbLogger);
                    //! Set flag to false until the device has been authenticated
                    sqanInterface->SetDeviceIsReady(false);

                    uint8_t index = usbInterface->GetIndex();
                    //! Set the index in the SqanInterface array to be the same as the UsbInterface array index
                    sqanInterface->SetIndex(index);

                    //! Insert into the sqanInterfaceArray at same position as the USB array
                    AddSqanInterfaceToArray(index, sqanInterface);

                    //! Authenticate the device and startup all the threads for send/receive
                    sqanInterface->SqanInit(index);

                    //! Set flag to true - apps can use this device now
                    sqanInterface->SetDeviceIsReady(true);
                }
                else
                {
                    //! During initialization devices are added in order, so
                    //! leave for loop when no more are found
                    break;
                }
            }

#ifndef TESTING
            //! Setup Hotplug or hotswap handling and pass SqanInterfaceInit
            //! instance so we can update the list from the the sqanInterfaceList
            usbInterfaceInit->SetupHotplugHandling(HandleUsbHotplugEvent, (void *)this);
#endif
        }
        else
        {
            std::cout << "no SQAN devices!" << std::endl;
        }
        break;
    }
    default:
        std::cout << "Unsupported Interface Type" << std::endl;
        break;
    }

    return ( &sqanInterfaceArray );

}

void SqanInterfaceInit::ExitSqanInterfaceArray()
{
    // Stop hotplug checking
    usbInterfaceInit->SetCheckingUsbEvents(false);

    EmptyHotplugQueue();
    hotplugQueue.empty();

    for (auto itr = sqanInterfaceArray.begin(); itr != sqanInterfaceArray.end(); ++itr )
    {
        SqanInterface *sqanInterface = *itr;
        if ( sqanInterface != nullptr )
        {
            // If device isReady then the SqanInit() method was run
            // So call SqanExit()
            if ( sqanInterface->IsReady() )
            {
                // Stop SolNet first in case there are applications transmitting
                /*JR SqanSolNet *sqanSolNet = sqanInterface->GetSqanSolNet();
                sqanSolNet->StopSolNet();*/

                // Stop applications from trying to use this device
                sqanInterface->SetDeviceIsReady(false);

                // Shutdown SqanInterface
                sqanInterface->SqanExit();
            }
            // deletes all the friend SQAN classes too
            delete(sqanInterface);
        }
    }

    //! Shutdown and delete the UsbInterface list
    if ( usbInterfaceInit != nullptr )
    {
        usbInterfaceInit->ExitUsbInterfaceArray();
        usbInterfaceInit = nullptr;
    }
}

void SqanInterfaceInit::AddSqanInterfaceToArray(uint8_t index, SqanInterface *sqanInterfacePtr)
{
    sqanInitArrayCmdLock.lock();
    sqanInterfaceArray[index] = sqanInterfacePtr;
    sqanInitArrayCmdLock.unlock();
}

void SqanInterfaceInit::RemoveSqanInterfaceFromArray(uint8_t index)
{
    sqanInitArrayCmdLock.lock();
    sqanInterfaceArray[index] = nullptr;
    sqanInitArrayCmdLock.unlock();
}

void SqanInterfaceInit::AddToHotplugQueue(HotplugEvent *hotplugEvent)
{
    hotplugQueueMutex.lock();
    hotplugQueue.push(hotplugEvent);
    hotplugQueueMutex.unlock();
}

SqanInterfaceInit::HotplugEvent *SqanInterfaceInit::RemoveFromHotplugQueue()
{
    HotplugEvent *hotplugEvent;

    if ( !hotplugQueue.empty() )
    {
        hotplugQueueMutex.lock();
        hotplugEvent = hotplugQueue.front();
        hotplugQueue.pop();
        hotplugQueueMutex.unlock();
    }

    return (hotplugEvent);
}

void SqanInterfaceInit::EmptyHotplugQueue()
{
    //! Called from destructor on shutdown
    while ( !hotplugQueue.empty() )
    {
        HotplugEvent *hotplugEvent = RemoveFromHotplugQueue();
        delete(hotplugEvent);
        usleep(10);
    }
}


//! Gets called as a callback from libusb on hotplug event notification
int SqanInterfaceInit::HandleUsbHotplugEvent(struct libusb_context *ctx, struct libusb_device *dev,
                                            libusb_hotplug_event event, void *user_data)
{
    if ( user_data == nullptr )
    {
        std::cout << "HandleUsbHotplugEvent - user_data NULL" << std::endl;
        return (0);
    }

    switch ( event )
    {
        case LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT:
        {
            SqanInterfaceInit *sqanInterfaceInit = (SqanInterfaceInit *)user_data;
            SqanInterface *sqanInterface = nullptr;
            UsbInterface *usbInterface = nullptr;
            bool foundDevice = false;

            for (auto iterator = sqanInterfaceInit->sqanInterfaceArray.begin(); iterator != sqanInterfaceInit->sqanInterfaceArray.end(); iterator++)
            {
                if ( *iterator != nullptr )
                {
                    sqanInterface = *iterator;

                    if ( sqanInterface->IsReady() )
                    {
                        usbInterface = sqanInterface->GetUsbInterface();

                        if ( usbInterface->GetDevice() == dev )
                        {
                            // If removing a device - find it and set the sqanInterface->isReady flag
                            // to false so apps can't send data
                            sqanInterface->SetDeviceIsReady(false);

                            // Stop SolNet first in case there are applications transmitting
                            /*JR SqanSolNet *sqanSolNet = sqanInterface->GetSqanSolNet();
                            sqanSolNet->StopSolNet(); */

                            foundDevice = true;
                            break;
                        }
                    }
                }
            }

            if ( !foundDevice )
            {
                std::cout << "LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT - can't find device" << std::endl;
            }
            break;
        }
        case LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED:
        default:
            break;
    }

    SqanInterfaceInit *sqanInterfaceInit = (SqanInterfaceInit *)user_data;
    SqanInterfaceInit::HotplugEvent *hotplugEvent = new SqanInterfaceInit::HotplugEvent;
    hotplugEvent->ctx = ctx;
    hotplugEvent->dev = dev;
    hotplugEvent->event = event;
    sqanInterfaceInit->AddToHotplugQueue(hotplugEvent);

    //! Return zero to rearm the callback
    return (0);
}

uint8_t SqanInterfaceInit::AddHotplugDevice(HotplugEvent *hotplugEvent)
{
    //! We add this device at the first nullptr
    std::array<UsbInterface *, MAX_DEVICES> *usbDevArray = usbInterfaceInit->GetUsbInterfaceArray();

    for (auto iterator = sqanInterfaceArray.begin(); iterator != sqanInterfaceArray.end(); iterator++)
    {
        if ( *iterator != nullptr )
        {
            SqanInterface *sqanInterface = *iterator;

            if ( sqanInterface->IsReady() )
            {
                UsbInterface *usbInterface = sqanInterface->GetUsbInterface();

                if ( usbInterface->GetDevice() == hotplugEvent->dev )
                {
                    // This case can happen during initialization
                    // the device was added but hotplug still occurs
                    return (sqanInterface->GetIndex());
                }
            }
        }
    }

    int index = 0;
    bool foundSlot = false;
    for (auto itr = usbDevArray->begin(); itr != usbDevArray->end(); itr++ )
    {
        if ( *itr == nullptr )
        {
            //! Add device here
            foundSlot = true;
            break;
        }
        else
        {
            index++;
        }
    }

    if ( foundSlot )
    {
        libusb_device_descriptor *desc = new libusb_device_descriptor;
        int status = libusb_get_device_descriptor(hotplugEvent->dev, desc);

        if ( status == 0 )
        {
            UsbInterface *usbInterface = nullptr;
            usbInterfaceInit->CreateUsbInterface(hotplugEvent->dev, desc, &usbInterface, index);

            if ( usbInterface != nullptr )
            {
                SqanInterface *sqanInterface = new SqanInterface(usbInterface, SqanInterface::USB, theLogger, dbLogger);
                //! Set flag to false until the device has been authenticated
                sqanInterface->SetDeviceIsReady(false);

                index = usbInterface->GetIndex();
                //! Set the index in the SqanInterface array to be the same as the UsbInterface array index
                sqanInterface->SetIndex(index);

                //! Insert into the sqanInterfaceArray at same position as the USB array
                AddSqanInterfaceToArray(index, sqanInterface);

                //! Authenticate the device and startup all the threads for send/receive
                sqanInterface->SqanInit(index);

                //! Set flag to true - apps can use this device now
                sqanInterface->SetDeviceIsReady(true);
            }
            else
            {
                std::cout << "  usbInterface is nullptr" << std::endl;
                std::string msgString = "  usbInterface is nullptr";
                theLogger->LogMessage(msgString, SQAN_INTERFACE_LOG_INDEX);
            }
        }
        else
        {
            std::cout << "Device " << std::to_string(index) << " failed hotplug!" << std::endl;
        }
    }
    else
    {
        std::cout << "Couldn't get a slot to add hotplug device" << std::endl;
    }

    return (index);
}

uint8_t SqanInterfaceInit::RemoveHotplugDevice(HotplugEvent *hotplugEvent)
{
    std::string msgString;
    uint8_t index = 0xFF;

    for (auto iterator = sqanInterfaceArray.begin(); iterator != sqanInterfaceArray.end(); iterator++)
    {
        if ( *iterator != nullptr )
        {
            SqanInterface *sqanInterface = *iterator;
            //! Don't check for sqanInterface->isReady here because we set
            //! it to false in the interrupt handler HandleUsbHotplugEvent()

            UsbInterface *usbInterface = sqanInterface->GetUsbInterface();

            if ( usbInterface->GetDevice() == hotplugEvent->dev )
            {
                //! Close the device
                if ( usbInterface->IsDeviceOpen() == true )
                {
                    int status = usbInterface->CloseDevice();

                    if ( status == 0 )
                    {
                        msgString = "  CloseDevice successful";
                    }
                    else
                    {
                        msgString = "  CloseDevice unsuccessful";
                    }
                    usbInterface->theLogger->LogMessage(msgString, usbInterface->GetIndex());
                }

                index = usbInterface->GetIndex();

                //! Remove this device from the arrays
                usbInterfaceInit->RemoveUsbInterfaceFromArray(index);
                RemoveSqanInterfaceFromArray(index);

                //! Shutdown usbInterface
                usbInterface->UsbInterfaceExit();

                //! Shutdown SqanInterface
                sqanInterface->SqanExit();

                //! Free the resources
                delete(usbInterface);
                delete(sqanInterface);

                break;
            }
        }
    }

    return(index);
}

uint8_t SqanInterfaceInit::CheckHotplugQueue(void)
{
    std::string msgString;
    uint8_t index = 0xFF;

    if ( (usbInterfaceInit != nullptr) && (usbInterfaceInit->GetCheckingUsbEvents() == true) )
    {
        if ( !hotplugQueue.empty() )
        {
            HotplugEvent *hotplugEvent = RemoveFromHotplugQueue();

            switch ( hotplugEvent->event )
            {
                case LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED:
                {
                    msgString = "  event is LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED";
                    theLogger->LogMessage(msgString, SQAN_INTERFACE_LOG_INDEX);

                    std::cout << "    LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED" << std::endl;

                    index = AddHotplugDevice(hotplugEvent);
                    break;
                }
                case LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT:
                {
                    msgString = "  event is LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT";
                    theLogger->LogMessage(msgString, SQAN_INTERFACE_LOG_INDEX);

                    std::cout << "    LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT" << std::endl;

                    index = RemoveHotplugDevice(hotplugEvent);
                    break;
                }
                default:
                {
                    msgString = "  event is not hotplug";
                    theLogger->LogMessage(msgString, 0xFF);
                    break;
                }
            }

            delete (hotplugEvent);
        }
    }

    //! Return the index into the SqanInterface array for the device just added or removed
    return (index);
}
