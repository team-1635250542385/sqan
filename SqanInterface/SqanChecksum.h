#ifndef SQANCHECKSUM_H
#define SQANCHECKSUM_H

#include "../Logger/Logger.h"

class SqanInterface;

class SqanChecksum
{
public:
    SqanChecksum();
     ~SqanChecksum() {}

    static uint16_t SqanCheck(uint32_t count, uint16_t *addr);
    static uint16_t ComputeChecksum(void *pMsg, uint16_t MsgSize);
};

#endif // SQANCHECKSUM_H
