#include "SqanChecksum.h"

SqanChecksum::SqanChecksum()
{

}

uint16_t SqanChecksum::SqanCheck(uint32_t count, uint16_t *addr)
{
    if ( addr == nullptr )
    {
        return 0;
    }

    uint32_t sum = 0;

    while( count > 1 )
    {
        /* Sum 16 bits at a time */
        sum += (uint16_t)(*addr);
        addr++;
        count -= 2;
    }

    /* Add left-over byte, if any */
    if( count > 0 )
        sum += *((uint8_t *)addr);

    /* Fold 32-bit sum to 16 bits */
    while (sum>>16)
    {
        sum = (sum & 0xFFFF) + (sum >> 16);
    }

    /* Compute one’s compliment */
    sum = ~sum;

    /* Compute two's Complement */
    sum = -sum;

    return (sum);
}

uint16_t SqanChecksum::ComputeChecksum(void *pMsg, uint16_t MsgSize)    // pMsg is a pointer to the message buffer,
                                                                        // MsgSize is the size of the message
                                                                        // Note: The message size need to be validate to not exceed the message buffer
{
   uint8_t *pBuffer = (uint8_t*)pMsg;                                   // We cast it to a byte pointer
   uint16_t Checksum = 0;                                               // We set the checksum to zero
                                                                                    //
   for (int i=0; i < MsgSize; i++)                                      // We loop through every byte of the message
   {                                                                               //
      Checksum += *pBuffer++;                                           //  and accumulate the checksum value
   }                                                                               //
                                                                                   //
   Checksum = -Checksum;                                                 //  Take 1’s complement or negate the checksum
   return (Checksum);
}
