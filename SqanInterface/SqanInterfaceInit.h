//###############################################
// Filename: SqanInterfaceInit.h
// Description: Class that initalizes each device
//              Under test with an SqanInterface
//              and a UsbInterface class.  It returns
//              a list of SqanInterface pointers
//###############################################
#ifndef SQANINTERFACEINIT_H
#define SQANINTERFACEINIT_H

#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../SqanInterface/SqanInterface.h"

#ifdef TESTING
// For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class SqanInterfaceInit
{

public:
    SqanInterfaceInit(uint8_t devType, Logger *logger, DBLogger *dblogger);
    ~SqanInterfaceInit();

#define MAX_SQAN_NODES 14
    std::array<SqanInterface *, MAX_SQAN_NODES> *InitSqanInterfaceArray(void);
    std::array<SqanInterface *, MAX_SQAN_NODES> *GetSqanInterfaceArray(void) { return (&sqanInterfaceArray); }

    void ExitSqanInterfaceArray(void);

    // Hotplug handling
   void AddSqanInterfaceToArray(uint8_t index, SqanInterface *sqanInterfacePtr);
    void RemoveSqanInterfaceFromArray(uint8_t index);

    // Hotplug handling
    struct HotplugEvent
    {
        struct libusb_context *ctx;
        struct libusb_device *dev;
        libusb_hotplug_event event;
    };
    static int HandleUsbHotplugEvent(struct libusb_context *ctx, struct libusb_device *dev,
                                    libusb_hotplug_event event, void *user_data);
    uint8_t AddHotplugDevice(HotplugEvent *hotplugEvent);
    uint8_t RemoveHotplugDevice(HotplugEvent *hotplugEvent);
    uint8_t CheckHotplugQueue(void);

    UsbInterfaceInit *GetUsbInterfaceInit(void) { return usbInterfaceInit; }

ACCESS:
    uint8_t deviceType;
    UsbInterfaceInit *usbInterfaceInit = nullptr;

#define SQAN_INTERFACE_LOG_INDEX 0xFFFF
    Logger *theLogger = nullptr;
    DBLogger *dbLogger = nullptr;

    // SQAN network allows up to 10 devices
    std::array<SqanInterface *, MAX_SQAN_NODES> sqanInterfaceArray;

    // Hotplug handling
    std::mutex sqanInitArrayCmdLock;
    std::mutex hotplugQueueMutex;
    std::queue<HotplugEvent *> hotplugQueue;
    void AddToHotplugQueue(HotplugEvent *hotplugEvent);
    HotplugEvent *RemoveFromHotplugQueue(void);
    void EmptyHotplugQueue(void);
};

#endif // SQANINTERFACEINIT_H
