//!###############################################
//! Filename: SqanInterface.h
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsave I/O on
//!              a USB device
//!###############################################
#ifndef SQANINTERFACE_H
#define SQANINTERFACE_H

#include <mutex>
#include <pthread.h>
#include <array>
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"
#include "../UsbInterface/UsbInterface.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class SqanInterface
{

public:
    SqanInterface(Interface *devInterface, uint8_t devType, Logger *logger, DBLogger *dblogger);
    ~SqanInterface();

    Logger *theLogger = nullptr;
    DBLogger *dbLogger = nullptr;

    int SqanInit(uint8_t index);
    void SqanExit(void);
    bool IsLittleEndian(void) { return (isLittleEndian); }
    UsbInterface *GetUsbInterface(void) { return (usbInterface); }    

    //! The index is used for logging and hotplug
    //! It is the position in the array
    uint8_t GetIndex(void) { return (sqanListIndex); }
    void SetIndex(uint8_t index) { sqanListIndex = index; }

    //! Used by SQAN firmware
    uint8_t GetNextCmdCount(void);

    //! Used in handling hotplug
    bool IsReady(void) { return (sqanDeviceIsReady); }
    void SetDeviceIsReady(bool deviceReady);

    //! When stopping data traffic tests, we may
    //! want to flush the queues and not keep the
    //! send/receive and logging going - should
    //! only affect SolNet data queues and not SQAN
    // !SQAN API messages on endpoint 0
    void SetFlushReceiveQueue(u_int8_t endpointId, u_int8_t dataflowId, bool flush);
    void SetFlushSendQueue(u_int8_t endpointId, u_int8_t dataflowId, bool flush);

    enum sqanDeviceTypes: uint8_t
    {
        USB = 0
    };


ACCESS:
    uint8_t deviceType;  //! USB

    //! Not associated with SQAN peer index
    //! Just internal for logging
    uint8_t sqanListIndex = 0;

    Interface *deviceInterface = nullptr;
    UsbInterface *usbInterface = nullptr;

    //! Little or Big Endian?
    bool isLittleEndian = true;

    //! Coordinate starting the read thread
    bool enableReadSqan = false;

    //! Coordinate starting/stopping the thread
    //! that processes the data send queues
    bool processDataSendQueues = false;

    //! Coordinate starting/stopping the thread
    //! that processes the data receive queues
    bool processDataReceiveQueues = false;

    //! Done initializing the Sqan host interface
    bool sqanInterfaceInitComplete = false;

    // BufCounter
    //uint8_t bufCounter = 0;
    //uint8_t sendBulkBuf[4096];

    std::mutex sqanCmdLock;
    uint8_t sqanCmdCount = 0;

    //! Thread that polls hardware for incoming SQAN messages
    pthread_t sqanReceiveThread = 0;

    //! Thread that processes messages on the SolNet data send queues
    pthread_t sqanSendQThread = 0;

    //! Thread that processes messages on the SolNet data receive queues
    pthread_t sqanReceiveQThread = 0;

    //! Hotplug: if set - this device was added by hotplug
    //! and needs to be initialized
    bool sqanDeviceIsReady = false;

#define PEER_INDEX_PEER_MASK 0x0F
#define PEER_INDEX_CONTROL_MASK 0x10
#define PEER_INDEX_MULTICAST_MASK 0x20
#define PEER_INDEX_BROADCAST_MASK 0x40

    struct sqanMsgHdr
    {
          uint16_t synch;
          uint8_t id;
          uint8_t protocol;
          uint16_t size;
          uint16_t checksum;
    }__attribute__((packed));

    #define SQAN_MESSAGE_HDR_SIZE sizeof(sqanMsgHdr)

    struct sqanGeneralCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct sqanCommand
    {
        union
        {
            sqanGeneralCommand sqanCmd;  //! To get the command type
        };
    }__attribute__((packed));

#define SQAN_CMD_SIZE sizeof(sqanCommand)

    //! Receiving events from SQAN hw
    struct sqanEvent
    {
        uint8_t eventType;
        uint8_t event;
        uint8_t eventContext;
        uint8_t resultCode;
    }__attribute__((packed));

#define SQAN_EVENT_SIZE sizeof(sqanEvent)

    enum sqanCmdEventTypes: uint8_t // TODO: Update For SQAN
    {
        ResrvdCmdEvent0     = 0,
        ResrvdCmdEvent1     = 1,
        Firmware            = 2,
        ResrvdVendor        = 3,
        Identity            = 4,
        Security            = 5,
        Association         = 6,
        Stream              = 7,
        Link                = 8,
        Metrics             = 9,
        System              = 10,
        Product             = 12
    };

    //! SQAN command status results
    enum sqanCmdResultCodes: uint8_t // TODO: Update For SQAN
    {
        SQAN_CMD_SUCCESS                     = 0x00,
        SQAN_CMD_FAILURE_GENERAL             = 0x01,
        SQAN_CMD_FAILURE_NOT_FOUND           = 0x02,
        SQAN_CMD_FAILURE_ID_MISMATCH         = 0x03,
        SQAN_CMD_FAILURE_INSUFF_MEMORY       = 0x04,
        SQAN_CMD_FAILURE_INVALID_ACTION      = 0x05,
        //! 6-127 reserved for future definition
        SQAN_CMD_FAILURE_OPERATION_PENDING   = 0x80,
        //! 129-254 reserved for future definition
        SQAN_CMD_FAILURE_TIMEOUT             = 0xFF
    };

    enum sqanQoS: uint8_t
    {
        HiNoDiscard     = 0x01,
        HiDiscard       = 0x11,
        LowNoDiscard    = 0x00,
        LowDiscard      = 0x10
    };

    // Endpoints 0 - 3 QoS setting
    // Default on startup is HiNoDiscard for
    // all endpoints.
    // Right now - queue 0 is not being used -
    // Messages on Endpoint zero go directly out
    static const uint8_t NumberOfSqanEndpoints = 4;  //TBD update for SQAN
    static const uint8_t MAX_NUMBER_APPS = 14; //TBD update for SQAN

    struct sqanDataSendItem
    {
        uint8_t peerIndex;
        sqanCommand *sqanCmd;
    };

    struct sqanSendQueue
    {
        uint8_t qOs;
        std::array<bool, MAX_NUMBER_APPS> flushArray;
        std::list<sqanDataSendItem *> sendQ;
        std::mutex qLock;
    };
    std::array<sqanSendQueue, NumberOfSqanEndpoints> sqanEndpointSendQueues;

    struct sqanDataReceiveItem
    {
        uint8_t peerIndex;
        uint8_t *data;
    };

    struct sqanReceiveQueue
    {
        uint8_t qOs;
        std::array<bool, MAX_NUMBER_APPS> flushArray;
        std::list<sqanDataReceiveItem *> receiveQ;
        std::mutex qLock;
    };
    std::array<sqanReceiveQueue, NumberOfSqanEndpoints> sqanEndpointReceiveQueues;

    void CheckSystemEndianness(void);
    void BigToLittleEndian(uint64_t *input, uint16_t type);

    //! SqanInterface receive event handling
    void StartSqanReceiveThread(void);
    void StopSqanReceiveThread(void);
    void HandleSqanReceiveEvents(void);
    static void *ReceiveThreadHandler(void *This) {((SqanInterface *)This)->HandleSqanReceiveEvents(); return NULL;}

    void DeliverSqanMessage(/*JRsqanRoutingHdr *routingHdr,*/ sqanEvent *event, uint8_t *data);
   /*JR void DebugPrintHeaders(SWIM_Header *SwimHeader,
                           sqanRoutingHdr *routingHdr, sqanEvent *event);*/
    int SyncSendSqanCmd(sqanCommand *sqanCmmnd/*JR, sqanRoutingHdr *routingHdr*/);

    // Use sqanQoS enum to set qOs
    // Can be used for Hi/Low queues sending data
    void SetQosOnEndpoint(uint8_t qOs, uint8_t endpoint);
    void InitEndpointQueues(void);
    void EmptyEndpointQueues();


    //! SqanInterface SolNet data send queue handling
    void StartSqanSendQThread(void);
    void StopSqanSendQThread(void);
    void HandleSqanSendQueues(void);
    static void *SendQThreadHandler(void *This) {((SqanInterface *)This)->HandleSqanSendQueues(); return NULL;}
    void AddToSendQueue(sqanCommand *sqanCmmnd, uint8_t peerIndex, uint8_t endpoint);
    sqanDataSendItem *GetNextFromSendQueue(uint8_t endpoint);
    void RemoveSendQueueFront(uint8_t endpoint);

    //! SqanInterface SolNet data receive queue handling
    void StopSqanReceiveQThread();
    void StartSqanReceiveQThread();
    void HandleSqanReceiveQueues(void);
    static void *ReceiveQThreadHandler(void *This) {((SqanInterface *)This)->HandleSqanReceiveQueues(); return NULL;}
    void AddToReceiveQueue(uint8_t peerIndex, uint8_t endpoint, uint8_t *data);
    sqanDataReceiveItem *GetNextFromReceiveQueue(uint8_t endpoint);
    void RemoveReceiveQueueFront(uint8_t endpoint);

};

#endif //! SQANINTERFACE_H
