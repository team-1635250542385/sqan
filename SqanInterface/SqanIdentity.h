//!###############################################
//! Filename: SqanIdentity.h
//! Description: Class that provides and interface
//!              to SQAN identity commands
//!###############################################
#ifndef SQANIDENTITY_H
#define SQANIDENTITY_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class SqanInterface;

class SqanIdentity
{

public:
    SqanIdentity(SqanInterface *sqan_interface, Logger *logger);
    ~SqanIdentity() {}

    Logger *theLogger = nullptr;

    struct sqanIdentityCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct sqanSetMacAddressCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t macAddress[6];
        uint16_t reserved2;
    };

    struct sqanSetCoordinatorCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t coordinatorCapable;
        uint8_t coordinatorPrecedence;
        uint8_t reserved2;
        uint8_t  persist;
    }__attribute__((packed));

    struct sqanSetDeviceTypeCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t deviceType;
        uint16_t reserved2;
    }__attribute__((packed));

    enum sqanDeviceTypes: uint16_t
    {
        ReservedDeviceTypes = 0,            //! 0-0xFFFF not to be assigned
        TWS                 = 0x1000,       //! Thermal Weapon Sight
        HMD                 = 0x1001,       //! Helmet Mounted Display
        PHN                 = 0x1002,       //! Smartphone/computer
        STM                 = 0x1003,       //! STORM laser range finder
        DGR                 = 0x1004,       //! DAGR GPS device
        RLH                 = 0x1005,       //! SQAN-Mac Relay, USB Host connected
        RLD                 = 0x1006,       //! SQAN-Mac Relay, USB Device connected
        DPT                 = 0x1007,       //!  Depot device
        NET                 = 0x1008,       //!  SQAN-Mac Network device
        ENC                 = 0x1009,       //!  Video encoder (camera)
        KPD                 = 0x100A,       //!  Keypad
        NES                 = 0x100B,       //!  SQAN-Mac Network device – slave only
        HTR                 = 0x100C,       //!  Handheld tactical radio
        CHD                 = 0x100D,       //!  Command Mode Head Mounted Device
        GNM                 = 0x100E,       //!  Generic Coordinator-Capable Device
        GNS                 = 0x100F,       //!  Generic Peer Device (Non-Coordinator)
        ReservedLegacy      = 0x1010,       //!  for legacy compatibility
        NET1                = 0x1011,       //!  General Network Node Priority 1
        NET2                = 0x1012,       //!  General Network Node Priority 2
        NET3                = 0x1013,       //!  General Network Node Priority 3
        NET4                = 0x1014,       //!  General Network Node Priority 4
        NET5                = 0x1015,       //!  General Network Node Priority 5
        Generic_Biometric_Sensor        = 0x1016,       //!  (e.g. heart rate)
        Generic_Environmental_Sensor    = 0x1017,       //!  (e.g. humidity)
        Generic_Positioning_Sensor      = 0x1018        //!  (e.g. GPS, altitude)
        //! N	Reserved for future definition
    };

    uint8_t GetIndex(void);
    std::string GetMacAddressStr(void)  { return (sqanMacAddressStr); }
    uint8_t *GetMacAddress(void) { return (sqanMacAddress); };
    uint16_t GetSqanDeviceType(void)  { return (sqanDeviceType); }
    uint8_t GetCoordinatorCapable(void)  { return (sqanCoordinatorCapable); }
    uint8_t GetCoordinatorPrecedence(void)  { return (sqanCoordinatorPrecedence); }
    uint8_t GetPersist(void)  { return (sqanPersist); }
    static std::string GetDeviceTypeString(uint16_t);

    void DeliverSqanMessage(uint8_t cmd, uint8_t *data);

    //! Send cmds to hw
    int SendGetMacAddressCmd(void);
    int SendSetMacAddressCmd(uint8_t *macAddress);
    int SendSetDeviceTypeCmd(uint16_t deviceType);
    int SendGetDeviceTypeCmd(void);
    int SendSetCoordinatorCmd(uint8_t coordinatorCapable, uint8_t coordinatorPrecedence, uint8_t persist);
    int SendGetCoordinatorCmd(void);

    //! Debug and Logging
    void GetDeviceTypeDefaults(uint16_t devType, uint8_t *sqanCoordinatorCapable, uint8_t *sqanCoordinatorPrecedence);

ACCESS:
    SqanInterface *sqanInterface = nullptr;
    std::mutex SqanIdentityCmdLock;

    uint8_t sqanMacAddress[6] = {0};
    std::string sqanMacAddressStr = "12:34:56:78:9A:BC";
    uint8_t sqanDeviceRole = 0x00;  //! 0x00=Master, 0x01=Slave
    uint16_t sqanDeviceType = 0x0000;
    uint8_t sqanCoordinatorCapable = 0;
    uint8_t sqanCoordinatorPrecedence = 0;
    uint8_t sqanPersist = 0;

    //! Identity commands and events
    enum sqanIdentityCmdEventTypes: uint8_t
    {
        ReservedIdentityCmd     = 0,
        SetDeviceType           = 1,
        GetDeviceType           = 2,
        SetMACAddress           = 3,
        GetMACAddress           = 4,
        SetCoordinator          = 5,
        GetCoordinator          = 6,
        ResrvdFuture            = 7
    };

    void SetupGetMacAddressCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr);
    void SetupSetMacAddressCmd(sqanSetMacAddressCommand *sqanSetMacAddressCmd, uint8_t *macAddress, void *routingHdr);
    void SetupSetDeviceTypeCmd(sqanSetDeviceTypeCommand *sqanSetDevTypeCmd, uint16_t deviceType, void *routingHdr);
    void SetupGetDeviceTypeCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr);
    void SetupSetCoordinatorCmd(sqanSetCoordinatorCommand *sqanSetCoordCmd, uint8_t coordinatorCapable,
                                uint8_t coordinatorPrecedence, uint8_t persist, void *routingHdr);
    void SetupGetCoordinatorCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr);

};

#endif //! SQANIDENTITY_H
