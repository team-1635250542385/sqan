//!###############################################
//! Filename: Interface.h
//! Description: Base class that provides an
//!              interface to a Linux device
//!###############################################
#ifndef INTERFACE_H
#define INTERFACE_H

#include "../Logger/Logger.h"

class Interface
{

public:
    Interface(long **device, Logger *logger, uint8_t board_type, void *boardData)
    {
        theLogger = logger;
    }
    virtual ~Interface() {};

    virtual int OpenDevice(void) = 0;
    virtual int CloseDevice(void) = 0;
    virtual int ClaimInterface(void) = 0;
    virtual int SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                           uint16_t wValue, uint16_t wIndex,
                           std::string *data, uint16_t wLength,
                           unsigned int ctr_timeout) = 0;
    virtual int AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback) = 0;
    virtual int SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length) = 0;
    virtual int ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead) = 0;

    Logger *theLogger;
};

#endif //! INTERFACE_H
