//!###############################################
//! Filename: SqanIdentity.cpp
//! Description: Class that provides and interface
//!              to SQAN identity commands
//!###############################################

#include "SqanIdentity.h"
#include "SqanInterface.h"
#include <sstream>
#include <iomanip>

SqanIdentity::SqanIdentity(SqanInterface *sqan_interface, Logger *logger)
{
    if (sqan_interface == nullptr)
    {
        std::cout << "sqan_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        sqanInterface = sqan_interface;
        theLogger = logger;
    }
}

uint8_t SqanIdentity::GetIndex(void)
{
    return (sqanInterface->GetIndex());
}

std::string SqanIdentity::GetDeviceTypeString(uint16_t devType)
{
    switch (devType)
    {
        case sqanDeviceTypes::ReservedDeviceTypes: return ( "Reserved Device Types" );
        case sqanDeviceTypes::TWS: return ( "TWS" );
        case sqanDeviceTypes::HMD: return ( "HMD" );
        case sqanDeviceTypes::PHN: return ( "PHN" );
        case sqanDeviceTypes::STM: return ( "STM" );
        case sqanDeviceTypes::DGR: return ( "DGR" );
        case sqanDeviceTypes::RLH: return ( "RLH" );
        case sqanDeviceTypes::RLD: return ( "RLD" );
        case sqanDeviceTypes::DPT: return ( "DPT" );
        case sqanDeviceTypes::NET: return ( "NET" );
        case sqanDeviceTypes::ENC: return ( "ENC" );
        case sqanDeviceTypes::KPD: return ( "KPD" );
        case sqanDeviceTypes::NES: return ( "NES" );
        case sqanDeviceTypes::HTR: return ( "HTR" );
        case sqanDeviceTypes::CHD: return ( "CHD" );
        case sqanDeviceTypes::GNM: return ( "GNM" );
        case sqanDeviceTypes::GNS: return ( "GNS" );
        case sqanDeviceTypes::ReservedLegacy: return ( "Reserved Legacy" );
        case sqanDeviceTypes::NET1: return ( "NET1" );
        case sqanDeviceTypes::NET2: return ( "NET2" );
        case sqanDeviceTypes::NET3: return ( "NET3" );
        case sqanDeviceTypes::NET4: return ( "NET4" );
        case sqanDeviceTypes::NET5: return ( "NET5" );
        case sqanDeviceTypes::Generic_Biometric_Sensor: return ( "Generic_Biometric_Sensor" );
        case sqanDeviceTypes::Generic_Environmental_Sensor: return ( "Generic_Environmental_Sensor" );
        case sqanDeviceTypes::Generic_Positioning_Sensor: return ( "Generic_Positioning_Sensor" );
        default: return ( "Unknown_devType");
    }
}

void SqanIdentity::DeliverSqanMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
    case SetDeviceType:
    {
        msgString = "Received SetDeviceType confirmation";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        break;
    }
    case GetDeviceType:
    {
        msgString = "Received GetDeviceType ";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());

        sqanDeviceRole = *data;

        uint16_t devType = 0x00FF & data[3];
        devType = devType << 8;
        devType += data[2];
        sqanDeviceType = devType;

        if ( theLogger->DEBUG_SqanIdentity == 1 )
        {
            std::stringstream ss;
            msgString.clear();
            ss << std::setfill('0');
            ss << "  deviceRole = " << std::to_string(sqanDeviceRole) << std::endl;
            ss << "  deviceType = ";
            for (int i=0; i<2; i++)
            {
                ss <<  std::hex << std::setw(2) << (int)data[3-i];
            }
            msgString.append(ss.str());
            theLogger->DebugMessage(msgString, sqanInterface->GetIndex());
        }

        GetDeviceTypeDefaults(sqanDeviceType, &sqanCoordinatorCapable, &sqanCoordinatorPrecedence);
        break;
    }
    case SetMACAddress:
    {
        msgString = "Received SetMACAddress confirmation";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        break;
    }
    case GetMACAddress:
    {
        std::stringstream macAddress;
        msgString.append("Received MacAddress = ");

        for (int i=0; i<6; i++)
        {
            //Get it as a string
            macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)data[i];
            if ( i<5 )
            {
                macAddress << ":";
            }
            //Get the data
            sqanMacAddress[i] = data[i];
        }

        msgString.append(macAddress.str());
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        sqanMacAddressStr.clear();
        sqanMacAddressStr.append(macAddress.str());
        break;
    }
    case SetCoordinator:
    {
        msgString = "Received SetCoordinator confirmation";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        break;
    }
    case GetCoordinator:
    {
        msgString = "Received GetCoordinator=";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());

        //! Update class params
        SqanIdentityCmdLock.lock();
        sqanCoordinatorCapable = data[0];
        sqanCoordinatorPrecedence = data[1];
        SqanIdentityCmdLock.unlock();

        if ( theLogger->DEBUG_SqanIdentity == 1 )
        {
            std::stringstream ss;
            ss << "  Coordinator Capable = " << std::to_string(sqanCoordinatorCapable) << std::endl;
            ss << "  Coordinator Precedence = " << std::to_string(sqanCoordinatorPrecedence) << std::endl;
            theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
        }
        break;
    }
    default:
    {
        msgString = "Bad Identity event type";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        break;
    }
    }
}

void SqanIdentity::SetupGetMacAddressCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupGetMacAddressCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

    sqanIdentityCmd->cmdType = SqanInterface::Identity;
    sqanIdentityCmd->command = GetMACAddress;
    sqanIdentityCmd->cmdContext = sqanInterface->GetNextCmdCount();
    sqanIdentityCmd->reserved1 = 0x00;

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanIdentityCommand);

    if ( theLogger->DEBUG_SqanIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType = " << std::to_string(sqanIdentityCmd->cmdType) << std::endl;
        ss << "    command = " << std::to_string(sqanIdentityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(sqanIdentityCmd->cmdContext) << std::endl;
        ss << "    reserved1 = " << std::to_string(sqanIdentityCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
    }
}

int SqanIdentity::SendGetMacAddressCmd()
{
    std::string msgString;
    msgString = "SendGetMacAddressCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupGetMacAddressCmd(&sqanCmd.sqanIdentityCmd, &routingHdr);
    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendGetMacAddressCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}

void SqanIdentity::SetupSetMacAddressCmd(sqanSetMacAddressCommand *sqanSetMacAddressCmd, uint8_t *macAddress, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupSetMacAddressCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

    sqanSetMacAddressCmd->cmdType = SqanInterface::Identity;
    sqanSetMacAddressCmd->command = SetMACAddress;
    sqanSetMacAddressCmd->cmdContext = sqanInterface->GetNextCmdCount();
    sqanSetMacAddressCmd->reserved1 = 0x00;
    sqanSetMacAddressCmd->reserved2 = 0x00;
    memcpy(sqanSetMacAddressCmd->macAddress, macAddress, 6);

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanSetMacAddressCommand);

    if ( theLogger->DEBUG_SqanIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType = " << std::to_string(sqanSetMacAddressCmd->cmdType) << std::endl;
        ss << "    command = " << std::to_string(sqanSetMacAddressCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(sqanSetMacAddressCmd->cmdContext) << std::endl;
        ss << "    reserved1 = " << std::to_string(sqanSetMacAddressCmd->reserved1) << std::endl;
        ss << "    reserved2 = " << std::to_string(sqanSetMacAddressCmd->reserved2) << std::endl;
        ss << "    macAddress = ";
        for (int i=0; i<6; i++)
        {
            ss <<  std::hex << std::setfill('0') << std::setw(2) << (int)macAddress[i];
            if ( i<5 )
            {
                ss << ":";
            }
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
    }
}

int SqanIdentity::SendSetMacAddressCmd(uint8_t *macAddress)
{
    std::string msgString;
    msgString = "SendSetMacAddressCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupSetMacAddressCmd(&sqanCmd.sqanSetMacAddressCmd, macAddress, &routingHdr);

    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendSetMacAddressCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}

void SqanIdentity::SetupSetDeviceTypeCmd(sqanSetDeviceTypeCommand *sqanSetDevTypeCmd, uint16_t deviceType, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupSetDeviceTypeCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

   sqanSetDevTypeCmd->cmdType = SqanInterface::Identity;
   sqanSetDevTypeCmd->command = SetDeviceType;
   sqanSetDevTypeCmd->cmdContext = sqanInterface->GetNextCmdCount();
   sqanSetDevTypeCmd->reserved1 = 0x00;
   sqanSetDevTypeCmd->deviceType = deviceType;
   sqanSetDevTypeCmd->reserved2 = 0x00;

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanSetCoordinatorCommand);

    if ( theLogger->DEBUG_SqanIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType = " << std::to_string(sqanSetDevTypeCmd->cmdType) << std::endl;
        ss << "    command = " << std::to_string(sqanSetDevTypeCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(sqanSetDevTypeCmd->cmdContext) << std::endl;
        ss << "    reserved1 = " << std::to_string(sqanSetDevTypeCmd->reserved1) << std::endl;
        ss << "    deviceType = " << std::to_string(sqanSetDevTypeCmd->deviceType) << std::endl;
        ss << "    reserved2 = " << std::to_string(sqanSetDevTypeCmd->reserved2) << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
    }
}

int SqanIdentity::SendSetDeviceTypeCmd(uint16_t deviceType)
{
    std::string msgString;
    msgString = "SendSetDeviceTypeCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupSetDeviceTypeCmd(&sqanCmd.sqanSetDevTypeCmd, deviceType, &routingHdr);
    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendSetDeviceTypeCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}

void SqanIdentity::SetupGetDeviceTypeCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupGetDeviceTypeCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

    sqanIdentityCmd->cmdType = SqanInterface::Identity;
    sqanIdentityCmd->command = GetDeviceType;
    sqanIdentityCmd->cmdContext = sqanInterface->GetNextCmdCount();
    sqanIdentityCmd->reserved1 = 0x00;

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanIdentityCommand);

    if ( theLogger->DEBUG_SqanIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType = " << std::to_string(sqanIdentityCmd->cmdType) << std::endl;
        ss << "    command = " << std::to_string(sqanIdentityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(sqanIdentityCmd->cmdContext) << std::endl;
        ss << "    reserved1 = " << std::to_string(sqanIdentityCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
    }
}

int SqanIdentity::SendGetDeviceTypeCmd()
{
    std::string msgString;
    msgString = "SendGetDeviceTypeCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupGetDeviceTypeCmd(&sqanCmd.sqanIdentityCmd, &routingHdr);
    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendGetDeviceTypeCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}

void SqanIdentity::GetDeviceTypeDefaults(uint16_t devType, uint8_t *coordinatorCapable, uint8_t *coordinatorPrecedence)
{
        std::string msgString;
        msgString = "GetDeviceTypeDefaults";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
        std::stringstream ss;
        ss << "  For device type =";

        //! Log device type
        //! Set coordinator capable defaults base on SQAN Embedment Guide Table 14-2
        switch (devType)
        {
        case sqanDeviceTypes::TWS:
        {
            ss << " Thermal Weapon Sight" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::HMD:
        {
            ss << " Heads-Up Display" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::PHN:
        {
            ss << " Smartphone/computer" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::STM:
        {
            ss << " STORM laser range finder" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::DGR:
        {
            ss << " DAGR GPS device" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::RLH:
        {
            ss << " SQAN-Mac Relay, USB Host connected" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::RLD:
        {
            ss << " SQAN-Mac Relay, USB Device connected" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::DPT:
        {
            ss << " Depot device" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::NET:
        {
            ss << " SQAN-Mac Network device" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::ENC:
        {
            ss << " Video encoder (camera)" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::KPD:
        {
            ss << " Keypad" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::NES:
        {
           ss << " SQAN-Mac Network device – slave only" << std::endl;

           *coordinatorCapable = 0;
           *coordinatorPrecedence = 5;
           break;
        }
        case sqanDeviceTypes::HTR:
        {
            ss << " Handheld tactical radio" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::CHD:
        {
            ss << " Command Mode Head Mounted Device" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::GNM:
        {
            ss << " Generic Coordinator-Capable Device" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::GNS:
        {
            ss << " Generic Peer Device (Non-Coordinator)" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::NET1:
        {
            ss << " General Network Node Priority 1" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 1;
            break;
        }
        case sqanDeviceTypes::NET2:
        {
            ss << " General Network Node Priority 2" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 2;
            break;
        }
        case sqanDeviceTypes::NET3:
        {
            ss << " General Network Node Priority 3" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 3;
            break;
        }
        case sqanDeviceTypes::NET4:
        {
            ss << " General Network Node Priority 4" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 4;
            break;
        }
        case sqanDeviceTypes::NET5:
        {
            ss << " General Network Node Priority 5" << std::endl;

            *coordinatorCapable = 1;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::Generic_Biometric_Sensor:
        {
            ss << " Generic Biometric Sensor" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::Generic_Environmental_Sensor:
        {
            ss << " Generic Environmental Sensor" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::Generic_Positioning_Sensor:
        {
            ss << " Generic Positioning Sensor - GPS" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        case sqanDeviceTypes::ReservedLegacy:
        case sqanDeviceTypes::ReservedDeviceTypes:
        default:
        {
            ss << " Unsupported type" << std::endl;

            *coordinatorCapable = 0;
            *coordinatorPrecedence = 5;
            break;
        }
        }

        ss << "  Returned Coordinator Capable = " << std::to_string(*coordinatorCapable) << std::endl;
        ss << "  Returned Coordinator Precedence = " << std::to_string(*coordinatorPrecedence) << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
}

void SqanIdentity::SetupSetCoordinatorCmd(sqanSetCoordinatorCommand *sqanSetCoordCmd, uint8_t coordinatorCapable,
                                         uint8_t coordinatorPrecedence, uint8_t persist, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupSetCoordinatorCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

    sqanSetCoordCmd->cmdType = SqanInterface::Identity;
    sqanSetCoordCmd->command = SetCoordinator;
    sqanSetCoordCmd->cmdContext = sqanInterface->GetNextCmdCount();
    sqanSetCoordCmd->reserved1 = 0x00;
    sqanSetCoordCmd->coordinatorCapable = coordinatorCapable;  //! Can act as coordinator; 1=yes; 0=no
    sqanSetCoordCmd->coordinatorPrecedence = coordinatorPrecedence; //! 0 to 5
    sqanSetCoordCmd->reserved2 = 0x00;
    sqanSetCoordCmd->persist = persist; //! Set to 1 = save to NVRAM

    //! Update class params
    SqanIdentityCmdLock.lock();
    sqanCoordinatorCapable = coordinatorCapable;
    sqanCoordinatorPrecedence = coordinatorPrecedence;
    sqanPersist = persist;
    SqanIdentityCmdLock.unlock();

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanSetCoordinatorCommand);

    if ( theLogger->DEBUG_SqanIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType = " << std::to_string(sqanSetCoordCmd->cmdType) << std::endl;
        ss << "    command = " << std::to_string(sqanSetCoordCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(sqanSetCoordCmd->cmdContext) << std::endl;
        ss << "    reserved1 = " << std::to_string(sqanSetCoordCmd->reserved1) << std::endl;
        ss << "    coordinatorCapable = " << std::to_string(sqanSetCoordCmd->coordinatorCapable) << std::endl;
        ss << "    coordinatorPrecedence = " << std::to_string(sqanSetCoordCmd->coordinatorPrecedence) << std::endl;
        ss << "    reserved2 = " << std::to_string(sqanSetCoordCmd->reserved2) << std::endl;
        ss << "    persist = " << std::to_string(sqanSetCoordCmd->persist) << std::endl;
        theLogger->DebugMessage(ss.str(), sqanInterface->GetIndex());
    }
}

int SqanIdentity::SendSetCoordinatorCmd(uint8_t coordinatorCapable, uint8_t coordinatorPrecedence, uint8_t persist)
{
    std::string msgString;
    msgString = "SendSetCoordinatorCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupSetCoordinatorCmd(&sqanCmd.sqanSetCoordinatorCmd, coordinatorCapable, coordinatorPrecedence, persist, &routingHdr);
    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendSetCoordinatorCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}

void SqanIdentity::SetupGetCoordinatorCmd(sqanIdentityCommand *sqanIdentityCmd, void *routingHdr)
{
    std::string msgString;
    msgString = "SetupSetCoordinatorCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());

    sqanIdentityCmd->cmdType = SqanInterface::Identity;
    sqanIdentityCmd->command = GetCoordinator;
    sqanIdentityCmd->cmdContext = sqanInterface->GetNextCmdCount();
    sqanIdentityCmd->reserved1 = 0x00;

    //! Add the SQAN Routing Header to the buffer
    SqanInterface::sqanRoutingHdr *routingHdrPtr = (SqanInterface::sqanRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(sqanIdentityCommand);
}

int SqanIdentity::SendGetCoordinatorCmd(void)
{
    std::string msgString;
    msgString = "SendGetCoordinatorCmd";
    theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    int status = 0;

    SqanInterface::sqanCommand sqanCmd;
    SqanInterface::sqanRoutingHdr routingHdr;
    SetupGetCoordinatorCmd(&sqanCmd.sqanIdentityCmd, &routingHdr);
    status = sqanInterface->SyncSendSqanCmd(&sqanCmd, &routingHdr);

    if ( status < 0 )
    {
        msgString = "SendGetCoordinatorCmd failed";
        theLogger->LogMessage(msgString, sqanInterface->GetIndex());
    }

    return ( status );
}
