//!###############################################
//! Filename: UsbInterface.cpp
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsave I/O on
//!              a USB device
//!###############################################

#include "UsbInterface.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <iomanip>


UsbInterface::UsbInterface(long **device, Logger *logger, uint8_t board_type, void *boardData) : Interface(device, logger, board_type, boardData)
{
    dev = (libusb_device *)*device;
    boardType = board_type;

    switch (boardType)
    {
        case sqanRadio:
        default:
            break;
    }
}

UsbInterface::~UsbInterface()
{

}

void UsbInterface::UsbInterfaceExit(void)
{
    switch (boardType)
    {
        case sqanRadio:
        default:
            break;
    }

    if (desc != nullptr)
    {
        delete(desc);
    }

    //! Close the USB device - no more I/O
    if (devHandle != nullptr)
    {
        CloseDevice();
    }
}

void UsbInterface::UpdateFromDevice(libusb_context *ctx, libusb_device *device, libusb_device_descriptor *descriptor, uint8_t index)
{
    std::string msgString = "UpdateFromRadio";
    theLogger->LogMessage(msgString, index);

    usbCmdLock.lock();

    usbHeaderType = USB_HEADER_CDC;

    //! This index is for logging and should
    //! match the position in the array
    SetIndex(index);
    //! Initialize from libusb
    SetVendorId(descriptor->idVendor);
    SetProductId(descriptor->idProduct);
    SetDeviceDescriptor(descriptor);
    SetContext(ctx);

    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        std::stringstream ss;
        ss << "  idVendor=" << std::to_string(idVendor) << std::endl;
        ss << "  idProduct=" << std::to_string(idProduct) << std::endl;
        ss << "  index=" << std::to_string(index) << std::endl;
        theLogger->DebugMessage(ss.str(), GetIndex());
    }

    //! Fill the USB config descriptor list.
    //! It is needed later. Checks for more than one.
    std::list<libusb_config_descriptor *> *configs = GetConfigList();
    configs->clear();
    for (int i=0; i<desc->bNumConfigurations; i++)
    {
        libusb_config_descriptor *config;
        int status = libusb_get_config_descriptor(dev, i, &config);

        if ( status == 0 )
        {
            configs->push_back(config);
        }
        else
        {
            msgString = "libusb_get_config_descriptor unsuccessful";
            theLogger->LogMessage(msgString, GetIndex());
        }
    }

    GetUsbAttributes(device);

    usbCmdLock.unlock();
}

void UsbInterface::GetUsbAttributes(libusb_device *device)
{
    std::string msgString;
    std::stringstream ss;

    const struct libusb_interface *interface = nullptr;

    std::list<libusb_config_descriptor *> *configList = GetConfigList();
    for (std::list<libusb_config_descriptor *>::const_iterator iterator = configList->begin(),
         end = configList->end(); iterator != end; ++iterator)
    {
        libusb_config_descriptor *config = *iterator;

        for (int i=0; i<config->bNumInterfaces; i++)
        {
            interface = &config->interface[i];

            if ( interface->altsetting->bInterfaceClass == 10 )
            {
                for (int i=0; i<interface->altsetting->bNumEndpoints; i++)
                {
                    struct libusb_endpoint_descriptor *endpoint =
                            (struct libusb_endpoint_descriptor *)&interface->altsetting->endpoint[i];

                    if (endpoint->bEndpointAddress & 0x81)
                    {
                        //! Set the IN address
                        int inAddr = LIBUSB_ENDPOINT_IN |
                                (interface->altsetting->endpoint->bEndpointAddress & 0xF);
                        SetEndpointIN(inAddr);

                        ss.str( std:: string() );
                        ss.clear();
                        msgString = "USB_ENDPOINT_IN = 0x";
                        ss << std::hex << (int)inAddr;
                        msgString.append(ss.str());
                        theLogger->LogMessage(msgString, GetIndex());

                        //! Set max packet size
                        SetMaxPacketSizeIn(interface->altsetting->endpoint->wMaxPacketSize);
                        msgString.clear();
                        msgString = "USB_ENDPOINT_IN max packet size = ";
                        msgString.append(std::to_string(GetMaxPacketSizeIn()));
                        theLogger->LogMessage(msgString, GetIndex());
                    }
                    else if (endpoint->bEndpointAddress & 0x01)
                    {
                        //! Set the OUT address
                        int outAddr = LIBUSB_ENDPOINT_OUT |
                                (endpoint->bEndpointAddress & 0xF);
                        SetEndpointOUT(outAddr);

                        ss.str( std:: string() );
                        ss.clear();
                        msgString.clear();
                        msgString = "USB_ENDPOINT_OUT = 0x";
                        ss << std::hex << (int)outAddr;
                        msgString.append(ss.str());
                        theLogger->LogMessage(msgString, GetIndex());

                        //! Set max packet size
                        SetMaxPacketSizeOut(config->interface->altsetting->endpoint->wMaxPacketSize);
                        msgString.clear();
                        msgString = "USB_ENDPOINT_OUT max packet size = ";
                        msgString.append(std::to_string(GetMaxPacketSizeOut()));
                        theLogger->LogMessage(msgString, GetIndex());
                    }
                    else
                    {
                        //! Bad endpoint??
                        std::cout << "Bad USB endpoint" << std::endl;
                    }
                }
            }
        }
    }
}

int UsbInterface::OpenDevice(void)
{
    int err = 0;
    devHandle = nullptr;
    libusb_device_handle *dev_handle;

    err = libusb_open(dev, &dev_handle);
    if (err)
    {
        std::string errString = "Can not open USB device";
        LogErrorString(errString, err);
        deviceOpen = false;
    }
    else
    {
        std::string msgString = "libusb_open";
        theLogger->LogMessage(msgString, usbSqanListIndex);
        devHandle = dev_handle;

        //! Claim the interface from the kernel
        ClaimInterface();

        deviceOpen = true;
    }
    return err;
}

int UsbInterface::CloseDevice(void)
{
    int err = 0;

    //!Close the reference to the USB device
    //!I/O is no longer possible
    std::string msgString = "libusb_close";
    theLogger->LogMessage(msgString, usbSqanListIndex);

    libusb_close(devHandle);
    devHandle = nullptr;
    deviceOpen = false;

    return err;
}

int UsbInterface::ClaimInterface()
{
    //! Check whether a kernel driver is attached to
    //! any of the interfaces. If so, detach it.
    //! Kernel is re-attached in deconstructor
    int status = 0;
    std::string msgString;

    if (libusb_kernel_driver_active(devHandle, 0) == 1)
    {
        msgString.append("libusb_kernel_driver_active");
        theLogger->LogMessage(msgString, usbSqanListIndex);

        status = libusb_detach_kernel_driver(devHandle, 0);
        if (status == 0)
        {
            msgString.clear();
            msgString.append("libusb_detach_kernel_driver successful");
            theLogger->LogMessage(msgString, usbSqanListIndex);
        }
        else
        {
            msgString.clear();
            msgString.append("libusb_detach_kernel_driver unsuccessful");
            theLogger->LogMessage(msgString, usbSqanListIndex);
            LogError(status);
        }       
    }

    //! Claim the interface
    status = libusb_claim_interface(devHandle, 0);
    if (status == 0)
    {
        msgString.clear();
        msgString.append("libusb_claim_interface successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
    }
    else {
        msgString.clear();
        msgString.append("libusb_claim_interface NOT successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
        LogError(status);
    }

    return (status);
}

int UsbInterface::SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                                     uint16_t wValue, uint16_t wIndex,
                                     std::string *data, uint16_t wLength,
                                     unsigned int ctr_timeout)
{
    int status = 0;

    std::string msgString;
    msgString.append("libusb_control_transfer");
    theLogger->LogMessage(msgString, usbSqanListIndex);
    status = libusb_control_transfer(devHandle, bmRequestType, bRequest,
                                    wValue, wIndex, (unsigned char *)data->data(), wLength,
                                    ctr_timeout);

    if (status == 0)
    {
        msgString.clear();
        msgString.append("libusb_control_transfer successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
    }
    else {
        msgString.clear();
        msgString.append("libusb_control_transfer NOT successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
        LogError(status);
    }

    return(status);
}

int UsbInterface::AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback)
{
    int status = 0;
    libusb_transfer_cb_fn callBack = (libusb_transfer_cb_fn)callback;

    struct libusb_transfer *transfer = libusb_alloc_transfer(0);
    std::string msgString = "libusb_fill_bulk_transfer";
    theLogger->LogMessage(msgString, usbSqanListIndex);
    libusb_fill_bulk_transfer(transfer,
        devHandle,
        endpoint,
        (unsigned char *)transferBuf,
        length,
        callBack,
        nullptr/*callback data*/,
        timeout);

    status = libusb_submit_transfer(transfer);
    msgString.clear();
    msgString.append("libusb_submit_transfer");
    theLogger->LogMessage(msgString, usbSqanListIndex);

    msgString.clear();
    if (status < 0)
    {
        //! Error
        libusb_free_transfer(transfer);
        msgString.append("libusb_fill_bulk_transfer failed");
        theLogger->LogMessage(msgString, usbSqanListIndex);
        LogError(status);
    }
    else {
        msgString.append("libusb_fill_bulk_transfer successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
    }

    return (status);
}

int UsbInterface::SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length)
{
    // SQAN control commands and SQAN SolNet Message Packets use
    // this method.  They may come from different threads so get
    // the lock
    usbCmdSendLock.lock();

    //! write data out USB interface
    int status = 0;
    int transferred = 0;

    std::string msgString = "libusb_bulk_transfer write";
    theLogger->LogMessage(msgString, usbSqanListIndex);

    status = libusb_bulk_transfer(devHandle, endpoint,
                               (unsigned char *)transferBuf,
                               length,
                               &transferred, timeout);

    msgString.clear();
    if (status < 0)
    {
        msgString.append("libusb_bulk_transfer write failed");
        theLogger->LogMessage(msgString, usbSqanListIndex);
        LogError(status);
        msgString.clear();
        msgString.append(" transferred: ");
        msgString += std::to_string(transferred);
        theLogger->LogMessage(msgString, usbSqanListIndex);
    }
    else {
        msgString.append("libusb_bulk_transfer write successful");
        theLogger->LogMessage(msgString, usbSqanListIndex);
    }
    usbCmdSendLock.unlock();

    return (status);
}

int UsbInterface::ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead)
{
    //! read data in from USB interface
    int status = 0;
    std::string msgString;

    usbCmdReceiveLock.lock();

    if (deviceOpen == true)
    {
        msgString = "libusb_bulk_transfer read";
        theLogger->LogMessage(msgString, usbSqanListIndex);
        status = libusb_bulk_transfer(devHandle, endpoint,
                                   (unsigned char *)receiveBuf,
                                   length,
                                   numRead, timeout);
        msgString.clear();
        if (status != 0)
        {
            msgString.append("libusb_bulk_transfer read failed");
            theLogger->LogMessage(msgString, usbSqanListIndex);
            LogError(status);
            msgString.clear();
            msgString.append(" numRead: ");
            msgString.append(std::to_string(*numRead));
            theLogger->LogMessage(msgString, usbSqanListIndex);
            status = -1;
        }
        else
        {
            msgString = "libusb_bulk_transfer read successful";
            theLogger->LogMessage(msgString, usbSqanListIndex);
        }
    }
    else
    {
        msgString = "libusb_bulk_transfer - device not open";
        theLogger->LogMessage(msgString, usbSqanListIndex);
        status = -1;
    }
    usbCmdReceiveLock.unlock();

    return(status);
}

void UsbInterface::SetDebugLevel(int debug_level)
{
    libusb_set_debug(context, debug_level);
    debugLevel = debug_level;
}

void UsbInterface::LogError(int code)
{
    std::string msgString;

    switch(code)
    {
    case LIBUSB_ERROR_IO:
    {
        msgString = "Error: LIBUSB_ERROR_IO\nInput/output error";
        break;
    }
    case LIBUSB_ERROR_INVALID_PARAM:
    {
        msgString = "Error: LIBUSB_ERROR_INVALID_PARAM\nInvalid parameter";
        break;
    }
    case LIBUSB_ERROR_ACCESS:
    {
        msgString = "Error: LIBUSB_ERROR_ACCESS\nAccess denied (insufficient permissions)";
        break;
    }
    case LIBUSB_ERROR_NO_DEVICE:
    {
        msgString = "Error: LIBUSB_ERROR_NO_DEVICE\nNo such device (it may have been disconnected)";
        break;
    }
    case LIBUSB_ERROR_NOT_FOUND:
    {
        msgString = "Error: LIBUSB_ERROR_NOT_FOUND\nEntity not found";
        break;
    }
    case LIBUSB_ERROR_BUSY:
    {
        msgString = "Error: LIBUSB_ERROR_BUSY\nResource busy";
        break;
    }
    case LIBUSB_ERROR_TIMEOUT:
    {
        msgString = "Error: LIBUSB_ERROR_TIMEOUT\nOperation timed out";
        break;
    }
    case LIBUSB_ERROR_OVERFLOW:
    {
        msgString = "Error: LIBUSB_ERROR_OVERFLOW\nOverflow";
        break;
    }
    case LIBUSB_ERROR_PIPE:
    {
        msgString = "Error: LIBUSB_ERROR_PIPE\nPipe error";
        break;
    }
    case LIBUSB_ERROR_INTERRUPTED:
    {
        msgString = "Error:LIBUSB_ERROR_INTERRUPTED\nSystem call interrupted (perhaps due to signal)";
        break;
    }
    case LIBUSB_ERROR_NO_MEM:
    {
        msgString = "Error: LIBUSB_ERROR_NO_MEM\nInsufficient memory";
        break;
    }
    case LIBUSB_ERROR_NOT_SUPPORTED:
    {
        msgString = "Error: LIBUSB_ERROR_NOT_SUPPORTED\nOperation not supported or unimplemented on this platform";
        break;
    }
    case LIBUSB_ERROR_OTHER:
    {
        msgString = "Error: LIBUSB_ERROR_OTHER\nOther error";
        break;
    }
    default:
    {
        msgString =  "Error: unkown error";
        break;
    }
    }
    theLogger->LogMessage(msgString, usbSqanListIndex);
}

void UsbInterface::LogErrorString(std::string errString, int err)
{
    char buff[20];
    snprintf(buff, sizeof(buff), "%d", err);
    errString.append(" error=");
    errString.append(buff);
    errString.append("\n");
    theLogger->LogMessage(errString, usbSqanListIndex);
}

