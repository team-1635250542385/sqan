//!###############################################
//! Filename: UsbInterface.h
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsave I/O on
//!              a USB device
//!###############################################
#ifndef USBINTERFACE_H
#define USBINTERFACEINIT_H

#include "UsbInterface.h"

#ifdef TESTING
//! Use TESTING flag for unit tests
//! Unit testing doesnt acces HW
#define ACCESS public
#else
#define ACCESS protected
#endif

class UsbInterfaceInit
{

public:
    UsbInterfaceInit(void);
    ~UsbInterfaceInit(void);

#define MAX_DEVICES 10
    std::array<UsbInterface *, MAX_DEVICES> *GetUsbInterfaceArray(void) { return (&usbInterfaceArray); }

    std::array<UsbInterface *, MAX_DEVICES> *InitUsbInterfaceArray(Logger *logger);
    int ExitUsbInterfaceArray(void);

    //! Count of SQAN devices
    std::mutex sqanDeviceCountLock;
    int sqanDeviceCount = 0;

    //! For Handling hotplug events
    libusb_context *GetContext(void) { return (context); }
    void CreateUsbInterface(libusb_device *dev, libusb_device_descriptor *desc, UsbInterface **usbInterfacePtr, int index);
    int *GetHotplugArriveHandle(void)  { return (&hotplugArriveHandle); }
    int *GetHotplugLeaveHandle(void)  { return (&hotplugLeaveHandle); }
    static void *HandleUsbEvents(void *arguments);
    void SetupHotplugHandling(libusb_hotplug_callback_fn callbackFunction, void *thisPtr);
    void AddUsbInterfaceToArray(uint8_t index, UsbInterface *usbInterfacePtr);
    void RemoveUsbInterfaceFromArray(uint8_t index);
    bool GetCheckingUsbEvents(void) { return checkingUsbEvents; }
    void SetCheckingUsbEvents(bool checkUsbEvents) { checkingUsbEvents = checkUsbEvents; }

ACCESS:
    //! Keep libusb device list
    //! for freeing on exit
    libusb_device **usbList;
    //! libusb device count
    int deviceCount;
    //! One context per instantiation of this class
    libusb_context *context;

    //! SQAN USB devices or cable
    static const uint16_t BOARD_VENDOR_ID = 0x0483;
    std::list<uint16_t> vendorIds = {BOARD_VENDOR_ID};
    static const uint16_t BOARD_PRODUCT_ID = 0x5740;

    //! USB device list used by SQAN
    std::array<UsbInterface *, MAX_DEVICES> usbInterfaceArray;

    //! Passed in by caller or per test
    Logger *theLogger;
#define USB_INTERFACE_LOG_INDEX 0x00FF

    //! Internal index for logging SQAN devices
    //! matches the index in usbInterfaceArray
    uint8_t usbListIndex = 0;

    //! OS supports hotplug
    std::mutex usbInitCmdLock;
    bool hasHotplugCapability = true;
    libusb_hotplug_callback_handle hotplugArriveHandle;
    libusb_hotplug_callback_handle hotplugLeaveHandle;

    //! While this module is running to check events
    bool checkingUsbEvents = false;
    bool handleUsbEventsThreadDone = false;
    pthread_t threadCheckUsbEvents;
};

#endif //! UsbInterfaceInit
