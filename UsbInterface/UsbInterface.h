//!################################################
//! Filename: UsbInterface.h
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsave I/O on a USB device
//!################################################
#ifndef USBINTERFACE_H
#define USBINTERFACE_H

#include "../SqanInterface/Interface.h"
#include "../Logger/Logger.h"
#include <stdint.h>
#include <iostream>
#include <string>
#include <list>
#include <cstdio>
#include <libusb-1.0/libusb.h>

#define USB_LOGFILE_ERR     -2
//!Match libusb log levels
#define LOG_LEVEL_NONE      0   //! no messages ever printed by the library (default)
#define LOG_LEVEL_ERROR     1   //! error messages are printed to stderr
#define LOG_LEVEL_WARNING   2   //! warning and error messages are printed to stderr
#define LOG_LEVEL_INFO      3   //! informational messages are printed to stderr
#define LOG_LEVEL_DEBUG     4   //! debug and informational messages are printed to stderr

#ifdef TESTING
//! Use TESTING flag for unit tests
//! Unit testing doesnt acces HW
#define ACCESS public
#else
#define ACCESS protected
#endif

class UsbInterface : public Interface
{

public:
    UsbInterface(long **device, Logger *logger, uint8_t board_type, void *boardData);
    ~UsbInterface();

    void UpdateFromDevice(libusb_context *ctx, libusb_device *device, libusb_device_descriptor *descriptor, uint8_t index);
    void GetUsbAttributes(libusb_device *device);

    void UsbInterfaceExit(void);
    int OpenDevice(void);
    int CloseDevice(void);
    int ClaimInterface(void);
    int SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                           uint16_t wValue, uint16_t wIndex,
                           std::string *data, uint16_t wLength,
                           unsigned int ctr_timeout);
    int AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback);
    int SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length);
    int ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead);

    uint16_t GetVendorId(void) { return (idVendor); }
    uint16_t GetProductId(void) { return (idProduct); }
    libusb_device *GetDevice(void) { return (dev); }
    libusb_device_descriptor * GetDeviceDescriptor(void) { return (desc); }
    libusb_device_handle * GetDeviceHandle(void) { return (devHandle); }
    libusb_context *GetContext(void) { return (context); }
    std::list<libusb_config_descriptor *> *GetConfigList(void) { return (&configDescriptors); }
    bool IsDeviceOpen(void) { return (deviceOpen); }


    //! USB headers - they are hear due
    //! to being specific to Alereon
    static const uint8_t USB_UNDEFINED_HEADER = 0;
    static const uint8_t USB_HEADER_CDC = 1;
    uint8_t GetUsbHeaderType(void) { return (usbHeaderType); }

    enum usbBoardTypes: uint8_t
    {
        unkown        = 0,
        sqanRadio     = 1
    };

    uint8_t GetIndex(void) { return (usbSqanListIndex); }
    void SetIndex(uint8_t index) { usbSqanListIndex = index; }
    void SetDeviceDescriptor(libusb_device_descriptor *descriptor) { desc = descriptor; }
    void SetDeviceHandle(libusb_device_handle *deviceHandle) { devHandle = deviceHandle; }
    void SetContext(libusb_context *thisContext) { context = thisContext; }
    void SetVendorId(uint16_t vendorId) { idVendor = vendorId; }
    void SetProductId(uint16_t productId) { idProduct = productId; }

    uint16_t GetEndpointIN(void) { return (USB_ENDPOINT_IN); }
    uint16_t GetEndpointOUT(void) { return (USB_ENDPOINT_OUT); }
    void SetEndpointIN(uint16_t endpoint) { USB_ENDPOINT_IN = endpoint; }
    void SetEndpointOUT(uint16_t endpoint) { USB_ENDPOINT_OUT = endpoint; }
    uint16_t GetMaxPacketSizeIn(void) { return (maxPacketSize_IN); }
    uint16_t GetMaxPacketSizeOut(void) { return (maxPacketSize_OUT); }
    void SetMaxPacketSizeIn(uint16_t maxPacketSize) { maxPacketSize_IN = maxPacketSize; }
    void SetMaxPacketSizeOut(uint16_t maxPacketSize) { maxPacketSize_OUT = maxPacketSize; }

    uint16_t GetMaxLibUsbPktSize(void) { return (maxLibUsbPktSize); }


ACCESS:
    void SetDebugLevel(int log_level);
    void LogError(int error);
    void LogErrorString(std::string errString, int err);

ACCESS:
    std::mutex usbCmdLock;
    std::mutex usbCmdSendLock;
    std::mutex usbCmdReceiveLock;

    uint8_t  usbHeaderType = USB_UNDEFINED_HEADER;

    libusb_device *dev = nullptr;
    libusb_device_descriptor *desc = nullptr;
    libusb_device_handle *devHandle = nullptr;
    libusb_context *context = nullptr;
    std::list<libusb_config_descriptor *> configDescriptors;
    unsigned int timeout = 3000;     //! Connection timeout (in ms)

    uint8_t boardType = sqanRadio;
    uint16_t idVendor;
    uint16_t idProduct;
    int debugLevel = LOG_LEVEL_NONE;
    bool deviceOpen = false;

    //! Internal index for logging
    //! Matches the array index
    uint8_t usbSqanListIndex = 0;

    //! Defaults - these get set when SqanInterface reads the SQAN hardware
    uint16_t USB_ENDPOINT_IN = (LIBUSB_ENDPOINT_IN  | LIBUSB_TRANSFER_TYPE_BULK);
    uint16_t USB_ENDPOINT_OUT = (LIBUSB_ENDPOINT_OUT | LIBUSB_TRANSFER_TYPE_BULK);
    uint16_t maxPacketSize_IN = 64; //Bytes
    uint16_t maxPacketSize_OUT = 64;
    int maxLibUsbPktSize = 4064;
};

#endif //! USBINTERFACE_H
