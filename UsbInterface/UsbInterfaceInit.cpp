//!###############################################
//! Filename: UsbInterfaceInit.cpp
//! Description: Class that provides an interface
//!              to the Linux libusb to get the
//!              device list and instantiate
//!              UsbInterface class per device
//!###############################################

#include "UsbInterfaceInit.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>


UsbInterfaceInit::UsbInterfaceInit()
{   

}

UsbInterfaceInit::~UsbInterfaceInit()
{

}

void UsbInterfaceInit::AddUsbInterfaceToArray(uint8_t index, UsbInterface *usbInterfacePtr)
{
    usbInitCmdLock.lock();
    usbInterfaceArray[index] = usbInterfacePtr;
    usbInitCmdLock.unlock();

    sqanDeviceCountLock.lock();
    sqanDeviceCount++;
    sqanDeviceCountLock.unlock();
}

void UsbInterfaceInit::RemoveUsbInterfaceFromArray(uint8_t index)
{
    usbInitCmdLock.lock();
    usbInterfaceArray[index] = nullptr;
    usbInitCmdLock.unlock();

    sqanDeviceCountLock.lock();
    if ( sqanDeviceCount > 0 )
    {
        sqanDeviceCount--;
    }
    else
    {
        sqanDeviceCount = 0;
    }
    sqanDeviceCountLock.unlock();
}

void UsbInterfaceInit::CreateUsbInterface(libusb_device *dev, libusb_device_descriptor *desc, UsbInterface **usbInterfacePtr, int index)
{
    std::string msgString;

    uint8_t boardType = UsbInterface::unkown;
    UsbInterface *usbInterface = nullptr;

    uint16_t vendorId = desc->idVendor;

    switch (vendorId)
    {
        case BOARD_VENDOR_ID:
        default:
        {
            boardType = UsbInterface::sqanRadio;
            //! Instantiate a UsbInterface for this device
            usbInterface = new UsbInterface((long **)&dev, theLogger, boardType, nullptr);
        }
    }

    if ( usbInterface != nullptr )
    {
        usbInterface->UpdateFromDevice(context, dev, desc, index);

        //! Open the device
        int status = usbInterface->OpenDevice();

        if (status == 0)
        {
            msgString = "OpenDevice successful";
            theLogger->LogMessage(msgString, usbInterface->GetIndex());
        }
        else
        {
            msgString = "OpenDevice unsuccessful";
            theLogger->LogMessage(msgString, usbInterface->GetIndex());
        }

        //! Add this usbInterface into the array at the place we found
        AddUsbInterfaceToArray(index, usbInterface);
        usbInterface->SetIndex(index);

        // Pass back the pointer to the caller
        *usbInterfacePtr = usbInterface;
    }
    else
    {
        msgString = "Create UsbInterface unsuccessful";
        theLogger->LogMessage(msgString, usbInterface->GetIndex());
    }
}

std::array<UsbInterface *, MAX_DEVICES> *UsbInterfaceInit::InitUsbInterfaceArray(Logger *logger)
{
    int status = 0;

    //! Set the Logger
    theLogger = logger;
    std::string msgString;

    //! Initialize the array list that holds only SQAN devices to nullptrs until we find some
    for (auto itr = usbInterfaceArray.begin(); itr != usbInterfaceArray.end(); itr++)
    {
        *itr = nullptr;
    }

    //! Initialize libusb
    status = libusb_init(&context);

    if ( status == 0 )
    {
        msgString = "libusb_init successful";
        theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX);

        // libusb deviceCount may be different than the sqanDeviceCount
        // which is what we care about.
        deviceCount = libusb_get_device_list(context, &usbList);

        //! First go through all USB devices and get a list of just the ones we want
        //! This will be the usbInterfaceArray
        for (size_t idx = 0; idx < deviceCount; idx ++)
        {
            libusb_device *dev = usbList[idx];
            libusb_device_descriptor *desc = new libusb_device_descriptor;
            status = libusb_get_device_descriptor(dev, desc);

            if ( status == 0 )
            {
                // See if this vendor id is in our list
                //JR auto it = std::find(vendorIds.begin(), vendorIds.end(), desc->idVendor);
                // if it points to end then it means element
                // does not exists in list
                /*JR if ( it != vendorIds.end() ) */

                if(desc->idVendor == BOARD_VENDOR_ID)
                {
                    UsbInterface *usbInterface = nullptr;
                    // Instantiates usbInterface, open the device, and add to array[usbListIndex]
                    CreateUsbInterface(dev, desc, &usbInterface, usbListIndex);

                    usbInitCmdLock.lock();
                    usbListIndex++;
                    usbInitCmdLock.unlock();
                }
            }
        }

        if ( sqanDeviceCount <= 0 )
        {
            std::string msgString = "UsbInterfaceInit did not find any SQAN devices!";
            theLogger->LogMessage(msgString, 0xFF);
            status = -1;
        }

    }

    return ( &usbInterfaceArray );
}

int UsbInterfaceInit::ExitUsbInterfaceArray(void)
{
    int status = 0;
    checkingUsbEvents = false;

    if ( sqanDeviceCount > 0 )
    {

#ifndef TESTING  //!Don't run during unit tests
        while ( !handleUsbEventsThreadDone )
        {
            // Wait for HandleUsbEvents thread to stop - it's on a timer
            sleep(2);
        }

        pthread_join(threadCheckUsbEvents, nullptr);
#endif

        for (auto itr = usbInterfaceArray.begin(); itr != usbInterfaceArray.end(); ++itr)
        {
            UsbInterface *usbInterface = *itr;
            if ( usbInterface != nullptr )
            {
                usbInterface->UsbInterfaceExit();
                delete(usbInterface);
                *itr = nullptr;
            }
        }
    }

    std::list<libusb_config_descriptor *> configs;
    configs.clear();

    //! Free reference in  libusb list
    libusb_free_device_list(usbList, deviceCount);
    //! Exit from libusb
    libusb_exit(context);

    return ( status );
}

void *UsbInterfaceInit::HandleUsbEvents(void *arguments)
{
    UsbInterfaceInit *usbInterfaceInit = (UsbInterfaceInit *)arguments;
    libusb_context *context = usbInterfaceInit->GetContext();

    int count = 0;
    while ( (!usbInterfaceInit->checkingUsbEvents) && (count++ < 90) )
    {
        sleep(1);
    }

    if ( usbInterfaceInit->checkingUsbEvents )
    {
        std::cout << "checkingUsbEvents true" << std::endl;
    }

    while ( usbInterfaceInit->checkingUsbEvents )
    {
        int completed = 0;
        struct timeval tv;

        libusb_handle_events_completed(context, &completed);
        tv.tv_sec = 0;
        tv.tv_usec = (1000);
        libusb_handle_events_timeout_completed(context,
                                               &tv,
                                               &completed);
    }

    usbInterfaceInit->handleUsbEventsThreadDone = true;
    std::cout << "HandleUsbEvents ended" << std::endl;

    return (nullptr);
}

void UsbInterfaceInit::SetupHotplugHandling(libusb_hotplug_callback_fn callbackFunction, void *thisPtr)
{
    if ( libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG) )
    {
        hasHotplugCapability = true;
    }
    else
    {
        hasHotplugCapability = false;
    }

    if ( hasHotplugCapability )
    {
        //! Start the thread that will check libusb events
        if (pthread_create(&threadCheckUsbEvents, nullptr, &UsbInterfaceInit::HandleUsbEvents, (void *)this) != 0)
        {
            std::cout << "HandleUsbEvents didn't start!" << std::endl;
            return;
        }
        else
        {
            std::cout << "HandleUsbEvents started" << std::endl;
            checkingUsbEvents = true;
        }

        for (auto it = vendorIds.begin(); it != vendorIds.end(); ++it)
        {
            int status1 = LIBUSB_SUCCESS;
            int status2 = LIBUSB_SUCCESS;

            //! Register callback for hotplug events
            status1 = libusb_hotplug_register_callback(nullptr,
                                                      LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED,          //!events - device arrives
                                                      LIBUSB_HOTPLUG_ENUMERATE,                     //!flag
                                                      *it,                                          //!vendor Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!product Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!device class
                                                      callbackFunction,                             //!callback function
                                                      thisPtr,                                      //!user data
                                                      GetHotplugArriveHandle());                    //!hotplug handle for deregister

            if ( status1 != LIBUSB_SUCCESS )
            {
                std::string msgString = "libusb_hotplug_register_callback failed for LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED";
                theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX);
            }

            status2 = libusb_hotplug_register_callback(nullptr,
                                                      LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT,             //!events - device arrives
                                                      LIBUSB_HOTPLUG_ENUMERATE,                     //!flag
                                                      *it,                                          //!vendor Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!product Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!device class
                                                      callbackFunction,                             //!callback function
                                                      thisPtr,                                      //!user data
                                                      GetHotplugLeaveHandle());                     //!hotplug handle for deregister

            if ( status2 != LIBUSB_SUCCESS )
            {
                std::string msgString = "libusb_hotplug_register_callback failed for LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT";
                theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX);
            }
        }
    }
}


