#include "gstsender.h"
#include <QDebug>
#include <unistd.h>
#include <gst/app/gstappsrc.h>
#include <gst/video/video.h>


GstSender::GstSender(uint8_t testType, uint16_t videoType, uint16_t pixelFormat):
    videoTestType(testType),
    videoFormatType(videoType),
    videoPixelFormat(pixelFormat)
{
    initGStreamerPipeline();
}

GstSender::~GstSender()
{    
    gst_bus_remove_watch(bus);
    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);
}

void GstSender::initGStreamerPipeline()
{
    // Use GStreamer pipeline to display media from radio
    gst_init(NULL, NULL);

    // Create the elements
    // videoTestType = 0 is camera; 1 is pattern test
    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        source = (GstAppSrc*)gst_element_factory_make("v4l2src", "source");

    }
    else
    {
        source = (GstAppSrc*)gst_element_factory_make("videotestsrc", "source");
    }

    // This queues up the data and provides multi-threaded handling
    videoQueue = gst_element_factory_make("queue", "video_queue");

    // This converts the incoming stream to a video format for play
    // It is flexible and can determine the format of the stream
    videoConvert = gst_element_factory_make ("videoconvert", "video_convert");

    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        // Scales to whatever is in caps filter
        videoScale = gst_element_factory_make ("videoscale", "video_scale");

        capsFilter2  = gst_element_factory_make("capsfilter", "caps_filter2");

        videoRate = gst_element_factory_make ("videorate", "video_rate");

        videoQueue2 = gst_element_factory_make("queue2", "video_queue2");
    }

    // This is the endpoint or sink.  We use "fakesink" so
    // we can just grab the buffers and send to sqanTestTool
    sink = gst_element_factory_make("fakesink", "sink");

    // Create the pipeline
    pipeline = gst_pipeline_new("Source Pipeline");

    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        if (!pipeline || !source || !videoQueue || !videoConvert || !videoScale || !capsFilter2 || !videoRate || !videoQueue2 || !sink)
        {
            qDebug() << "Not all GST elements could not be created";
            return;
        }

    }
    else
    {
        if (!pipeline || !source || !videoQueue || !videoConvert || !sink)
        {
            qDebug() << "Not all GST elements could not be created";
            return;
        }
    }

    // Build the pipeline
    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        gst_bin_add_many(GST_BIN(pipeline), (GstElement*)source, videoQueue, videoConvert, videoScale, capsFilter2, videoRate, videoQueue2, sink, NULL);
    }
    else
    {
        gst_bin_add_many(GST_BIN(pipeline), (GstElement*)source, videoQueue, videoConvert, sink, NULL);
    }

    // Link the elements in the pipeline together
    if ( gst_element_link((GstElement*)source, videoQueue) != TRUE )
    {
        qDebug() << "GST source and videoQueue elements could not be linked";
        gst_object_unref (pipeline);
        return;
    }

    if ( gst_element_link(videoQueue, videoConvert) != TRUE )
    {
        qDebug() << "GST videoQueue and videoConvert elements could not be linked";
        gst_object_unref(pipeline);
        return;
    }

    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        if ( gst_element_link(videoConvert, videoScale) != TRUE )
        {
            qDebug() << "GST videoConvert and videoScale elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }

        if ( gst_element_link(videoScale, capsFilter2) != TRUE )
        {
            qDebug() << "GST videoScale and capsFilter2 elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }

        if ( gst_element_link(capsFilter2, videoRate) != TRUE )
        {
            qDebug() << "GST capsFilter2 and videoRate elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }

        if ( gst_element_link(videoRate, videoQueue2) != TRUE )
        {
            qDebug() << "GST videoRate and videoQueue2 elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }

        if ( gst_element_link(videoQueue2, sink) != TRUE )
        {
            qDebug() << "GST videoQueue2 and sink elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }
    }
    else
    {
        if ( gst_element_link(videoConvert, sink) != TRUE )
        {
            qDebug() << "GST videoConvert and sink elements could not be linked";
            gst_object_unref(pipeline);
            return;
        }

    }

    // The pad allows us to probe the "sink" for buffers
    pad = gst_element_get_static_pad(sink, "sink");
    if ( !pad )
    {
        qDebug() << "Pad element could not be created";
    }
    else
    {
        gst_pad_add_probe(pad, GST_PAD_PROBE_TYPE_BUFFER, (GstPadProbeCallback)probePipeline, this, NULL);
        gst_object_unref(pad);
    }

    // Set the caps
    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        // Camera raw format
        GstCaps *caps = gst_caps_new_simple ("video/x-raw",
                                             "format", G_TYPE_STRING, "YV12",
                                             "framerate", GST_TYPE_FRACTION, 5, 1,
                                             "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
                                             "width", G_TYPE_INT, 1280,
                                             "height", G_TYPE_INT, 720,
                                             NULL);

        gst_app_src_set_caps(source, caps);

        GstCaps *caps2 = gst_caps_new_simple ("video/x-raw",
                                              "format", G_TYPE_STRING, "YV12",
                                              "framerate", GST_TYPE_FRACTION, 5, 1,
                                              "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
                                              "width", G_TYPE_INT, 320,
                                              "height", G_TYPE_INT, 240,
                                              NULL);


        // Set caps on cap filter
        g_object_set(G_OBJECT(capsFilter2), "caps", caps2,
                                            NULL);
    }
    else
    {
        /*****************************************
         * See GstVideoTestSrcPattern in GST docs
         * Set the pattern number to see one of
         * the 24 test patterns available.
         * 0 - Shows colored bars with moving snow
         * ***************************************/
        g_object_set(source, "pattern", videoTestType, NULL);

        GstCaps *caps = gst_caps_new_simple ("video/x-raw",
                                             "format", G_TYPE_STRING, "I420",
                                             "framerate", GST_TYPE_FRACTION, 30, 1,
                                             "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
                                             "width", G_TYPE_INT, 320,
                                             "height", G_TYPE_INT, 240,
                                             NULL);

        gst_app_src_set_caps(source, caps);
    }


    // Most devices send an EndOfStream signal
    // if the input signal quality drops too low,
    // causing GStreamer to finish capturing.
    // To prevent the device from sending EOS
    // set num-buffers = -1
    g_object_set(source, "num-buffers", -1,
                "do-timestamp", true,
                 "is-live", true,
                NULL);

    // Disable clock sync the of the output sink
    // It's supposed to speed up the pipeline
    g_object_set(sink, "sync", false,
                       NULL);

    gst_element_set_state(pipeline, GST_STATE_NULL);
}

void GstSender::startSending()
{
    // Go to playing
    int ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
    if ( ret == GST_STATE_CHANGE_FAILURE )
    {
       qDebug() << "Unable to set the GST pipeline to the playing state";
       gst_object_unref(pipeline);
       return;
    }

    isSending = true;

    // Add watch for messages
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
    if ( bus != nullptr )
    {
        gst_bus_add_watch(bus, (GstBusFunc)getBusMessage, this);
    }
}

void GstSender::stopSending()
{
    isSending = false;

    gst_element_send_event(pipeline, gst_event_new_eos());

    /* clean up */
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    gst_bus_remove_watch(bus);
}

void GstSender::getBusMessage(GstBus *bus, GstMessage *message, void *userData)
{
    GstSender *thisPtr = (GstSender *)userData;

    GstMessageType msgTypes =(GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
    GstMessage *msg = gst_bus_timed_pop_filtered(thisPtr->bus, GST_CLOCK_TIME_NONE, msgTypes);
    GError *err;

    gchar *debug_info;
    switch(GST_MESSAGE_TYPE(msg))
    {
        case GST_MESSAGE_ERROR:
        {
            gst_message_parse_error(msg, &err, &debug_info);
            g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
            g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
            g_clear_error(&err);
            g_free(debug_info);
            break;
        }
        case GST_MESSAGE_EOS:
        {
            g_print("End-Of-Stream reached.\n");
            gst_element_set_state(thisPtr->pipeline, GST_STATE_READY);
            break;
        }
        case GST_MESSAGE_CLOCK_LOST:
        {
            // Get a new clock
            gst_element_set_state (thisPtr->pipeline, GST_STATE_PAUSED);
            gst_element_set_state (thisPtr->pipeline, GST_STATE_PLAYING);
            break;
        }
        default:
        {
            // We should not reach here because we only asked for ERRORs and EOS
            g_printerr("Unexpected message received.\n");
            break;
        }
    }
}

GstPadProbeReturn GstSender::probePipeline(GstPad *pad, GstPadProbeInfo *info, gpointer userData)
{
    GstSender *thisPtr = (GstSender *)userData;

    if ( thisPtr->isSending )
    {
        GstBuffer *gstBuffer = GST_PAD_PROBE_INFO_BUFFER (info);
        int size = GST_PAD_PROBE_INFO_SIZE(info);

        // emit the signal and send the data
        GstMapInfo mapInfo;
        gst_buffer_map(gstBuffer, &mapInfo, GstMapFlags::GST_MAP_READ);

        QByteArray byteArray = QByteArray((const char*)mapInfo.data, mapInfo.size);
        emit thisPtr->videoFromSourceAvailable(byteArray);

        //qDebug() << "emit the signal from " << thisPtr << " and send dataSize=" << QString::number(mapInfo.size);

        gst_buffer_unmap(gstBuffer, &mapInfo);
    }

    return GST_PAD_PROBE_OK;
}

