#include "socketscpi.h"
#include <QDebug>
#include <QTcpSocket>
#include <QDataStream>
#include "sqantesttool.h"
#include <QtMath>

SocketScpi::SocketScpi()
{

}

SocketScpi::SocketScpi(QString ipAddr, int port)
{
    socketIpAddr = ipAddr;
    socketPort = port;
    socket = nullptr;
    connectedToHost = false;
}

void SocketScpi::connect()
{
    socket = new QTcpSocket();
    if(!connectedToHost)
    {
        socket->connectToHost(socketIpAddr, (uint16_t)socketPort);
        socket->waitForConnected(10000);

        if(socket->state() == QAbstractSocket::ConnectedState)
        {
            qDebug() << "Socket Connected";
            connectedToHost = true;
        }
        else
        {
            qDebug() << "Socket Connecting";
        }
    }
    else
    {
        qDebug() <<"Socket Not Connected";
    }
}

void SocketScpi::disconnect()
{
    if(connectedToHost)
    {
       socket->disconnectFromHost();
       socket->close();
       connectedToHost = false;
       qDebug() << "Socket Disconnected";
    }
    else
    {
        qDebug() << "Socket Not Connected";
    }
}

void SocketScpi::send(QString command)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        QString cmd = "DISPlay:TEXT \"Hello World\"\n";

        socket->write(command.toUtf8().data());
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);

        qDebug() << "reading" << socket->bytesAvailable();
    }
    else
    {
        qDebug() << "Socket Not Connected -- Can't Send Command";
    }
}

double SocketScpi::recv()
{
    double value = 0.0;
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        QString reply = socket->readAll();
        reply.chop(1);

        qDebug() << reply;

        value = reply.toDouble();
    }
    else
    {
        qDebug() << "Socket Not Connected -- Can't Send Command";
    }

    return value;
}
