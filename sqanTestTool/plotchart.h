﻿#ifndef PLOTCHART_H
#define PLOTCHART_H

#include <QtCharts/QChart>
#include <QtCore/QTimer>
#include "../Logger/Logger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../SqanInterface/SqanInterfaceInit.h"
#include "../SqanInterface/SqanInterface.h"
#include "../SqanInterface/SqanIdentity.h"
#include "../SqanInterface/SqanFirmware.h"
#include "../SqanInterface/SqanAssociation.h"
#include "../SqanInterface/SqanLink.h"
#include "../SqanInterface/SqanMetrics.h"
#include "../SqanInterface/SqanSecurity.h"
#include "../SqanInterface/SqanProduct.h"
#include "../SqanInterface/SqanSolNet.h"
#include <QFile>
#include <QTextStream>
#include <QDir>

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QValueAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class PlotChart: public QChart
{
public:
    PlotChart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0, int updateInterval = 1, int xMax = 100, int yMax = 80, QString fileName = QDir::currentPath() + "/dataoutput.txt");
    void setYreal(double dataYPoint);
    double getYreal() {return m_y;}
    void startPlot();
    void stopPlot();
    virtual ~PlotChart();

    bool setPlotFlag = 0;

public slots:
    void handleTimeout();

private:
    QTimer m_timer;
    QLineSeries *m_series;
    QStringList m_titles;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    qreal m_step;
    int m_counter;
    int m_xMax;
    int m_yMax;
    double y_out;
    int m_x;
    int m_y;
    int m_updateInterval;
    QFile file;
    QTextStream outStream;
};

#endif // PLOTCHART_H
