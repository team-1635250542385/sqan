#include "filedialog_firmware_update.h"
#include "ui_filedialog_firmware_update.h"

FileDialog_Firmware_Update::FileDialog_Firmware_Update(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileDialog_Firmware_Update)
{
    ui->setupUi(this);
}

FileDialog_Firmware_Update::~FileDialog_Firmware_Update()
{
    delete ui;
}
