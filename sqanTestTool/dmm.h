#ifndef DMM_H
#define DMM_H
#include <QString>
#include "socketscpi.h"

class Dmm
{
public:
    Dmm();
    Dmm(QString ipAddr1);
    void connect();
    void disconnect();
    double getVolt();
    double getCurrent();

private:
    QString device;
    QString ipAddr;
    int port;
    SocketScpi* scpi = nullptr;
};

#endif // DMM_H
