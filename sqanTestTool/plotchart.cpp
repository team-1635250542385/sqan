
#include "plotchart.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QRandomGenerator>
#include <QtCore/QDebug>
#include "../Logger/Logger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../SqanInterface/SqanInterfaceInit.h"
#include "../SqanInterface/SqanInterface.h"
#include "../SqanInterface/SqanIdentity.h"
#include "../SqanInterface/SqanFirmware.h"
#include "../SqanInterface/SqanAssociation.h"
#include "../SqanInterface/SqanLink.h"
#include "../SqanInterface/SqanMetrics.h"
#include "../SqanInterface/SqanSecurity.h"
#include "../SqanInterface/SqanProduct.h"
#include "../SqanInterface/SqanSolNet.h"
#include "sqantesttool.h"
#include <QFile>
#include <QTextStream>

PlotChart::PlotChart(QGraphicsItem *parent, Qt::WindowFlags wFlags, int updateInterval, int xMax, int yMax, QString fileName):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(0),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_step(0),
    m_counter(0),
    m_xMax(xMax),
    m_yMax(yMax),
    y_out(0),
    m_x(0),
    m_y(0),
    m_updateInterval(updateInterval)
{
    QObject::connect(&m_timer, &QTimer::timeout, this, &PlotChart::handleTimeout);
    m_timer.setInterval(updateInterval);
    setPreferredSize(850,500);

    m_series = new QLineSeries(this);
    QPen black(Qt::black);
    black.setWidth(3);
    m_series->setPen(black);
    m_series->append(m_x, m_y);

    file.setFileName(fileName);


    outStream.setDevice(&file);


    addSeries(m_series);
    legend()->hide();
    //createDefaultAxes();

    addAxis(m_axisX,Qt::AlignBottom);
    addAxis(m_axisY,Qt::AlignLeft);
    m_series->attachAxis(m_axisX);
    m_series->attachAxis(m_axisY);

    m_axisX->setRange(0, xMax);
    m_axisY->setRange(0, yMax);
}

PlotChart::~PlotChart()
{

}

void PlotChart::startPlot()
{
    m_timer.start();
    setPlotFlag = 0;
    file.open(QIODevice::WriteOnly);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        qDebug() << "output file did not open";
        return;
    }

}

void PlotChart::stopPlot()
{
    m_timer.stop();
    setPlotFlag = 1;
    file.close();
}

void PlotChart::setYreal(double dataYPoint)
{
    m_y = dataYPoint;
    y_out = dataYPoint;
    qDebug() << m_y << "y value in Set";
    handleTimeout();
}

void PlotChart::handleTimeout()
{
    m_counter += 1;
    m_x = m_counter*m_updateInterval/1000;
    m_y = getYreal();
    m_series->append(m_x, m_y);
    outStream << m_x << "," << y_out << "\n";
   // qDebug() << m_y << "y value in Handle";
    if (m_y > m_yMax)
    {
        m_axisY->setMax(ceil(m_y));
        m_yMax = ceil(m_y);
    }
    if (m_x > m_xMax)
    {
        m_xMax = m_xMax + 20;
        m_axisX->setMax(m_xMax);
    }
}

