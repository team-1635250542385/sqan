//###################################################
// Filename: packetGenerator.cpp
// Description: SQAN Test Tool Packet Generator Class
//###################################################

#include "packetgenerator.h"
#include "ui_packetgenerator.h"
#include <unistd.h>
#include <QDebug>
#include <QButtonGroup>
#include <QMessageBox>

packetGenerator::packetGenerator(sqanTestTool *testToolPtr, void *userData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::packetGenerator),
    sqanToolPtr(testToolPtr)
{
    ui->setupUi(this);
    setWindowTitle("Packet Generator Test");

    dataParams = (SendDataParams *)userData;

    // Setup the control buttons: start, stop, cancel
    startTestPushButton = new QPushButton(this);
    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
    startTestPushButton->setText("Start");
    startTestPushButton->setGeometry(QRect(120, 95, 122, 28));
    startTestPushButton->setCheckable(true);
    startTestPushButton->setAutoExclusive(true);
    stopTestPushButton = new QPushButton(this);
    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
    stopTestPushButton->setText("Stop");
    stopTestPushButton->setGeometry(QRect(370, 95, 122, 28));
    stopTestPushButton->setCheckable(true);
    stopTestPushButton->setAutoExclusive(true);

    startTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");
    stopTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(startTestPushButton);
    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);
    buttonGroup->addButton(stopTestPushButton);
    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

    createDataTestTable();

    ui->topWidget->repaint();
}

packetGenerator::~packetGenerator()
{
    delete (dataParams->cbp);
    delete (dataParams);
    delete (ui);
}

void packetGenerator::createDataTestTable()
{
    // Table used in the Data Traffic tab
    // Format Active Links Table
    ui->dataTestTable->setRowCount(1);
    ui->dataTestTable->setColumnCount(6);

    QStringList tableHeader;
    tableHeader << "Src" << "Dest" << "Packet Size"  << "Packets Sent" << "Packets Rcvd" << "";
    ui->dataTestTable->setHorizontalHeaderLabels(tableHeader);
    ui->dataTestTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->dataTestTable->horizontalHeader()->setResizeContentsPrecision(true);
    ui->dataTestTable->horizontalHeader()->setStyleSheet("QHeaderView { font-size: 8pt; }");
    //ui->dataTestTable->horizontalHeader()->setStretchLastSection(false);

    uint8_t row = 0;

    srcColumn.setText(dataParams->cbp->source);
    ui->dataTestTable->setItem(row, 0, &srcColumn);

    dstColumn.setText(dataParams->cbp->destination);
    ui->dataTestTable->setItem(row, 1, &dstColumn);

    packetSizeColumn.setText("4064");
    ui->dataTestTable->setItem(row, 2, &packetSizeColumn);

    numPacketsToSendColumn.setText("0");
    ui->dataTestTable->setItem(row, 3, &numPacketsToSendColumn);

    numPacketsRecvdColumn.setText("0");
    ui->dataTestTable->setItem(row, 4, &numPacketsRecvdColumn);

    //Clear Packet Generator data transmition Button
    clearButton.setText("Clear");
    clearButton.setCheckable(true);
    clearButton.setChecked(false);
    clearButton.setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    ui->dataTestTable->setCellWidget(row, 5, &clearButton);
    connect(&clearButton, SIGNAL(toggled(bool)), this, SLOT(clear()));

    connect(this, SIGNAL(packetReceived()), this, SLOT(updateNumPacketsReceived()));
}

void packetGenerator::onGroupButtonClicked(int id)
{
    //qDebug() << id;

    switch(id)
    {
        case START_BUTTON_ID:
        {
            startDataTest();
            break;
        }
        case STOP_BUTTON_ID:
        {
            stopDataTest();
            break;
        }
        default:
            break;
    }
}

void packetGenerator::startDataTest()
{
    if ( isRunning )
    {
        displayMessage("Stop current test first.");
        return;
    }

    isRunning = true;

    numPacketsReceived = 0;
    numPacketsToSend = numPacketsToSendColumn.text().toInt();

    if ( (dataParams->cbp->endpointDistribution >= 0x01) && (dataParams->cbp->endpointDistribution <= 0xFF) )
    {
        //Multicast or Broadcast
        peerIndex = dataParams->cbp->endpointDistribution;
    }
    else
    {
        //Unicast
         peerIndex = dataParams->cbp->peerIndex;
    }
    endpointId = dataParams->entry->endpointId;
    dataflowId = dataParams->entry->dataflowId;

    sqanIntrfcSrcPtr = sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanInterface;
    sqanSolNetSrcPtr = sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanSolNet;
    sqanIntrfcSrcPtr->SetFlushSendQueue(endpointId, dataflowId, false);

    sqanIntrfcDstPtr = sqanToolPtr->DynArray[dataParams->cbp->destIndex].sqanInterface;
    sqanIntrfcDstPtr->SetFlushReceiveQueue(endpointId, dataflowId, false);

    future = QtConcurrent::run(sendData, this);
}


void packetGenerator::stopDataTest()
{
    isRunning = false;
    usleep(100); //Let the other thread know we stopped

    // Remove all packets on the outgoing queue for this flow
    sqanIntrfcSrcPtr->SetFlushSendQueue(endpointId, dataflowId, true);

    // Remove all packets on the receiver's incoming queue for this flow
    sqanIntrfcDstPtr->SetFlushReceiveQueue(endpointId, dataflowId, true);
}

void packetGenerator::updateNumPacketsReceived()
{
    numPacketsRecvdColumn.setText(QString::number(numPacketsReceived));
    ui->dataTestTable->repaint();
    QCoreApplication::processEvents();
}

void packetGenerator::receiveDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr)
{
    if ( transferSize > 0 )
    {
        packetGenerator *packetGen = (packetGenerator *)thisPtr;
        // Not doing anything with data right now

        // Increment the number of SolNet data packets we get
        packetGen->numPacketsReceived++;

        // emit the signal approximately every second
        if ( (packetGen->numPacketsReceived % 100) == 0 )
        {
            emit packetGen->packetReceived();
        }
    }
}

void packetGenerator::clear()
{
    if ( clearButton.isChecked() )
    {
        // Clear the number of packets sent and received columns
        numPacketsToSendColumn.setText("0");

        numPacketsReceived = 0;
        numPacketsRecvdColumn.setText(QString::number(numPacketsReceived));

        clearButton.setChecked(false);
    }
}

void packetGenerator::displayMessage(QString string)
{
    QMessageBox msgBox;
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);

    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(2000);

    msgBox.exec();
}

void packetGenerator::sendData(packetGenerator *thisPtr)
{
    uint16_t packetSize = SqanSolNet::payloadDataBufferSize;
    uint32_t packetsSent = 0;

    // Fill a databuffer with alternating 10 0's and 10 1's
    uint8_t data[SqanSolNet::payloadDataBufferSize];
    memset(data, 0xFF, packetSize);

    while ( (thisPtr->sqanSolNetSrcPtr != nullptr) &&
            (thisPtr->sqanSolNetSrcPtr->IsSolNetStopped() == false) &&
            (thisPtr->isRunning) &&
            ((packetsSent < thisPtr->numPacketsToSend) ||
             (thisPtr->numPacketsToSend == 0)) )
    {
        // Send this service data to peer
        uint8_t policies = 0;
        thisPtr->sqanSolNetSrcPtr->SendSolNetDataPacket(thisPtr->peerIndex,
                                                 thisPtr->endpointId,
                                                 thisPtr->dataflowId,
                                                 policies,
                                                 data,
                                                 packetSize);

        packetsSent++;
        usleep(10);
    }
}
