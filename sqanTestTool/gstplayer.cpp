#include "gstplayer.h"
#include <QDebug>
#include <unistd.h>


GstPlayer::GstPlayer(uint8_t testType, uint16_t videoType, uint16_t pixelFormat):
    videoTestType(testType),
    videoFormatType(videoType),
    videoPixelFormat(pixelFormat)
{
    initGStreamerPipeline();
}

GstPlayer::~GstPlayer()
{    
    gst_bus_remove_watch(bus);
    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);
}

void GstPlayer::initGStreamerPipeline()
{
    // Uncomment this to see all the GST debug
    //setenv("GST_DEBUG","*:6", 1);

    // init GStreamer
    gst_init(NULL, NULL);

    // Create the elements

    // This is the source which reads in the data sent from the radio
    source = gst_element_factory_make ("appsrc", "source");

    // By adding a queue next, the pipeline is multi-threaded and the
    // incoming data can queue up
    videoQueue2 = gst_element_factory_make ("queue2", "video_queue2");

    rawVideoParse = gst_element_factory_make ("rawvideoparse", "raw_video_parse");

    // This converts the incoming stream to a video format for play
    // It is flexible and can determine the format of the stream
    videoConvert = gst_element_factory_make ("videoconvert", "video_convert");

    // This is a test player used in GStreamer testing that plays
    // whatever is coming down the pipeline
    sink = gst_element_factory_make ("autovideosink", "sink");

    // Create the pipeline
    pipeline = gst_pipeline_new("Player Pipeline");
    if (!pipeline || !source || !videoQueue2 || !rawVideoParse || !videoConvert || !sink)
    {
        qDebug() << "Not all GST elements could be created";
        return;
    }

    // Build the pipeline
    gst_bin_add_many (GST_BIN (pipeline), source, videoQueue2, rawVideoParse, videoConvert, sink, NULL);

    if ( gst_element_link(source, videoQueue2) != TRUE )
    {
        qDebug() << "GST source and videoQueue2 elements could not be linked";
        gst_object_unref (pipeline);
        return;
    }

    if (gst_element_link (videoQueue2, rawVideoParse) != TRUE)
    {
        qDebug() << "GST videoQueue2 and rawVideoParse elements could not be linked";
        gst_object_unref (pipeline);
        return;
    }

    if (gst_element_link (rawVideoParse, videoConvert) != TRUE)
    {
        qDebug() << "GST rawVideoParse and videoConvert elements could not be linked";
        gst_object_unref (pipeline);
        return;
    }

    if (gst_element_link (videoConvert, sink) != TRUE)
    {
        qDebug() << "GST videoConvert and sink elements could not be linked";
        gst_object_unref (pipeline);
        return;
    }

    GstCaps *caps;
    if ( videoTestType == CAMERA_TEST_TYPE )
    {
        // Set the caps on the source from my webcam - used the following
        caps = gst_caps_new_simple ("video/x-raw",
                                     "format", G_TYPE_STRING, "YV12",
                                     "framerate", GST_TYPE_FRACTION, 5, 1,
                                     "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
                                     "width", G_TYPE_INT, 320,
                                     "height", G_TYPE_INT, 240,
                                      NULL);
    }
    else
    {
        // Set the caps on the source from my webcam - used the following
        caps = gst_caps_new_simple ("video/x-raw",
                                     "format", G_TYPE_STRING, "I420",
                                     "framerate", GST_TYPE_FRACTION, 30, 1,
                                     "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
                                     "width", G_TYPE_INT, 320,
                                     "height", G_TYPE_INT, 240,
                                     NULL);

    }

    gst_app_src_set_caps(GST_APP_SRC(source), (const GstCaps *)caps);

    g_object_set(source, "do-timestamp", true,
                                NULL);

    // Tell rawvideoparse to use the source caps
    g_object_set(rawVideoParse, "use-sink-caps", true,
                                NULL);

    // Set stream type to no seeking or playback
    gst_app_src_set_stream_type((GstAppSrc *)source, GST_APP_STREAM_TYPE_STREAM);

    // Disable clock sync the of the output sink
    // It's supposed to speed up the pipeline
    g_object_set(sink, "sync", false,
                       NULL);

    gst_element_set_state(pipeline, GST_STATE_NULL);
}

void GstPlayer::startReceiving()
{
    // Go to playing
    int ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
    if ( ret == GST_STATE_CHANGE_FAILURE )
    {
       qDebug() << "Unable to set the GST pipeline to the playing state";
       gst_object_unref(pipeline);
       return;
    }
    else if (ret == GST_STATE_CHANGE_NO_PREROLL)
    {
        is_live = true;
        g_print("Stream is_live is true\n");
    }

    isPlaying = true;

    // Add watch for messages
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
    if ( bus != nullptr )
    {
        gst_bus_add_watch(bus, (GstBusFunc)getBusMessage, this);
    }
}

void GstPlayer::stopReceiving()
{
    isPlaying = false;

    // GStreamer appsrc has it's own API for sending EOS
    gst_app_src_end_of_stream((GstAppSrc *)source);

    /* clean up */
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    gst_bus_remove_watch(bus);
}


void GstPlayer::getBusMessage(GstBus *bus, GstMessage *message, void *userData)
{
    GstPlayer *thisPtr = (GstPlayer *)userData;

    GstMessageType msgTypes =(GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
    GstMessage *msg = gst_bus_timed_pop_filtered(thisPtr->bus, GST_CLOCK_TIME_NONE, msgTypes);
    GError *err;

    gchar *debug_info;
    switch(GST_MESSAGE_TYPE(msg))
    {
        case GST_MESSAGE_ERROR:
        {
            gst_message_parse_error(msg, &err, &debug_info);
            g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
            g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
            g_clear_error(&err);
            g_free(debug_info);
            break;
        }
        case GST_MESSAGE_EOS:
        {
            g_print("End-Of-Stream reached.\n");
            gst_element_set_state(thisPtr->pipeline, GST_STATE_READY);
            break;
        }
        case GST_MESSAGE_BUFFERING:
        {
            gint percent = 0;

            // If the stream is live, we do not care about buffering.
            if (thisPtr->is_live) break;

            gst_message_parse_buffering (msg, &percent);
            g_print("Buffering (%3d%%)\r", percent);

            // Wait until buffering is complete before start/resume playing
            if ( percent < 100 )
            {
                gst_element_set_state (thisPtr->pipeline, GST_STATE_PAUSED);
            }
            else
            {
                gst_element_set_state (thisPtr->pipeline, GST_STATE_PLAYING);
            }
            break;
        }
        case GST_MESSAGE_CLOCK_LOST:
        {
            // Get a new clock
            gst_element_set_state (thisPtr->pipeline, GST_STATE_PAUSED);
            gst_element_set_state (thisPtr->pipeline, GST_STATE_PLAYING);
            break;
        }
        default:
        {
            // We should not reach here because we only asked for ERRORs and EOS
            g_printerr("Unexpected message received.\n");
            break;
        }
    }
}

void GstPlayer::receiveVideoDataInPlayer(const QByteArray& bytes)
{
    if ( isPlaying && bytes.size() > 0 )
    {        
        //qDebug() << "getVideoDataFromSource push buffer size=" << QString::number(frameData->size()) << " bytes";
        GstBuffer *buffer = gst_buffer_new_allocate(NULL, bytes.size(), NULL);

        // Clear memory
        gst_buffer_memset(buffer, 0, 0, bytes.size());

        // Copy the date into buffer
        gst_buffer_fill(buffer, 0, bytes.data(), bytes.size());

        gst_app_src_push_buffer(GST_APP_SRC(source), buffer);
    }
}
