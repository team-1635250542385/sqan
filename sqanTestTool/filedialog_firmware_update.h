#ifndef FILEDIALOG_FIRMWARE_UPDATE_H
#define FILEDIALOG_FIRMWARE_UPDATE_H

#include <QDialog>

namespace Ui {
class FileDialog_Firmware_Update;
}

class FileDialog_Firmware_Update : public QDialog
{
    Q_OBJECT

public:
    explicit FileDialog_Firmware_Update(QWidget *parent = nullptr);
    ~FileDialog_Firmware_Update();

private:
    Ui::FileDialog_Firmware_Update *ui;
};

#endif // FILEDIALOG_FIRMWARE_UPDATE_H
