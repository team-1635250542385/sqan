#ifndef SQANTESTTOOL_H
#define SQANTESTTOOL_H

#include <stdio.h>
#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QListWidgetItem>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>
#include <QThread>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QXmlSimpleReader>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QElapsedTimer>
#include <QBuffer>
#include "../SqanInterface/SqanInterfaceInit.h"
#include "../Logger/DBLogger.h"
/*JR#include "node.h"
#include "dmm.h"
#include "plotchart.h"
#include "packetgenerator.h"
#include "videogenerator.h"
*/

//                          void(name) forces compiler to check if variable exists
#define GET_VAR_NAME(name) (void(name),varName(#name))
#define NAME_VAL_NUM(name) (void(name),nameValNum(#name, QString::number((name))))
#define NAME_VAL_TEXT(name) (void(name),nameValText(#name, QString(reinterpret_cast<char *>((name)))))

namespace Ui {
class sqanTestTool;
}

class QPushButton;
class Node;

class sqanTestTool : public QMainWindow
{
    Q_OBJECT

friend class videoGenerator;

public:
    explicit sqanTestTool(QWidget *parent = nullptr);
    ~sqanTestTool();

    /*****************************************************
     * Macro Convenience: NAME_VAL_NUM and NAME_VAL_TEXT
     * **************************************************/
    struct NameVal {
        QString name;
        QString val;
    };
    QString varName(const char*);
    NameVal nameValNum(const char* c, QString v) { return (NameVal){varName(c), v}; };
    NameVal nameValText(const char* c, QString v) { return (NameVal){varName(c), v}; };


    /*****************************************************
     * sqanTestTool UI Input Validators
     * **************************************************/
    const QRegExpValidator *textValidator = new QRegExpValidator(QRegExp("^[A-Za-z0-9 '_-]{,15}$"));
    const QIntValidator *uint8Validator = new QIntValidator(0, 255);
    const QIntValidator *uint16Validator = new QIntValidator(0, 65535);
    const QDoubleValidator *uint32Validator = new QDoubleValidator(0, 4294967295, 0);
    const QDoubleValidator *pfloatValidator = new QDoubleValidator(0, 3.4E38, 7);

    /*****************************************************
     * sqanTestTool DB Logging
     * **************************************************/
    void dbLogDevice(int);
    void dbLogDevices();
    void dbLogDevice(long long, int, int);
  /*JR  void dbLogService(long long, int, int, SqanSolNet::sqanSolNetServiceEntry *);
    void dbLogService(int, SqanSolNet::sqanSolNetServiceEntry *);
  */
    /*****************************************************
     * SQAN API handling
     * **************************************************/
    struct SolNetPeerListEntry {
        QString devName;
        QString macAddr;
        uint16_t devType;
        uint8_t peerIndex;
        uint16_t recvThroughput;
        uint16_t xmtThroughput;
    }__attribute__((packed));

    struct MulticastGroupRecord {
        uint8_t mcastStatus;
        uint8_t mcastPeerIndex;
        uint16_t mcastAddress;
        uint8_t mcastPhyRate;
        uint16_t txQueueDepth;
    };

    struct DeviceMulticastGroups {
        MulticastGroupRecord startGroup;
        MulticastGroupRecord joinGroup;
    };

    //SQAN Device Attributes and Friend Classes
    struct SqanAttr {
        std::mutex attributesLock;
        bool isReady = false;
        QString devName = "";
        QString macAddr = "";
        QString firmwareVer = "";
        uint32_t netId = 0;
        uint8_t coordPriority = 0;
        uint8_t role = 0;
        uint8_t channel = 0;
        uint16_t devType = 0;
        float phy_rate = 0.0;
        uint16_t throughput = 0;
        SqanInterface *sqanInterface = nullptr;
        Node *node = nullptr;
        QVector<SolNetPeerListEntry *> peerList;
        DeviceMulticastGroups multicastGroup;
        QStringList mcastGroupList;
    }__attribute__((packed));

    // Main data structure
    // one entry for each device to
    // hold all device attributes
    SqanAttr *DynArray;

    /*****************************************************
     * Data Traffic Testing
     * **************************************************/

protected:
    /*****************************************************
     * Logging
     * **************************************************/
    static void* dequeue_messages(void *arguments);
    static void* dequeue_dbmessages(void *arguments);

    /*****************************************************
     * sqanTestTool startup/shutdown
     * **************************************************/
    void initTestTool();
    void stopAllThreads(void);

    /*****************************************************
     * Network Configuration
     * **************************************************/
    void clearDevices();
    void clearDynArrayEntry(uint8_t selected);
    void clearDynArray(void);
    void clearSolNetPeerList(uint8_t selected);
    void clearDeviceListComboBox();
    void clearDeviceList();
    void clearPeerListWidget();
    void clearEdges();
    void drawEdges();

    void scanDevice(uint8_t selected, int posX, int posY);
    void scanDevices();

    void updatePeers(int selected, bool locked);
    void updateSqanAttr(int selected);
    void getMulticastGroup(int selected);
    void updateDeviceUiForSelected(int selected);

    /******************************************************
     * sqanTestTool SolNet Service Descriptor Configuration
     * ***************************************************/
    void createServiceConfigurationTable();
//JR    void addServiceDescriptorsUI();
    void addControlDescriptorUI();
    void addVideoDescriptorUI();
    void addVideoDataPayloadUI();
    void addRtaDescriptorUI();
    void addRtaDataPayloadUI();
    void addArOverlayDescriptorUI();
    void addPacketGeneratorDescriptorUI();
    void addStatusDescriptorUI();
    void addStatusDataPayloadUI();
    void addSimpleUiDescriptorUI();
    void addSimpleUiDataPayloadUI();
    void addLrfDescriptorUI();
    void addLrfControlDescriptorUI();
    QString getDeviceName();
    QString getDeviceSerialNumber();
    QString getDeviceManufacturer();
    QString getDeviceFriendlyName();
    int getEndpointId(QComboBox *endpointIdComboBox);
    int getEndpointDistribution(QComboBox *endpointDistributionComboBox, uint8_t selected);
    uint8_t getEndpointDataPolicies(QComboBox *endpointDataPoliciesComboBox);
    int getLabel(QLineEdit *labelLineEdit, uint8_t *labelBuf);
    uint32_t getRtaImuRate();
    int getControlServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getVideoServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getRtaServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getPacketGeneratorServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getStatusServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getSimpleUiServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getArOverlayServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getLrfServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getLrfControlServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getRegistrationStatus(int selected, QString destService);
    void getServiceIdFromText(QString columnHdrText, uint16_t *descriptorId);
    void getPropertyDescriptorIdFromText(QString descriptorText, uint16_t *descriptorId);
    uint16_t getVideoFormatResolutionXServiceDescriptor();
    uint16_t getVideoFormatResolutionYServiceDescriptor();
    float getVideoFormatFrameRateDescriptor();
    uint16_t getVideoFormatBitDepthDescriptor();
    uint16_t getVideoFormatPixelFormatDescriptor();
    uint16_t getVideoProtocolRawDescriptor();
    float getVideoIfovDescriptor();
    uint16_t getVideoPrincipalPointXDescriptor();
    uint16_t getVideoPrincipalPointYDescriptor();
    float getVideoLensDistortionCoefficientsDescriptor();
    uint16_t getVideoControlBitmapDescriptor();
    uint16_t getArOverlayMinWidthDescriptor();
    uint16_t getArOverlayMinHeightDescriptor();
    uint8_t getArOverlayRleVersionsDescriptor();
    uint8_t getButtonCount(QComboBox *simpleUiButtonCountComboBox);
    uint8_t getStateCount(QComboBox *simpleUiStateCountComboBox);

    // #WIP shawn10 UI additions
   /*JR void loadControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadVideoDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadRtaDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadArOverlayDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadPacketGeneratorDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadStatusDescriptorUI(SqanSolNet::sqanSolNetServiceEntry *);
    void loadSimpleUiDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadLrfDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    void loadLrfControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *);
    */
    int getEndpointDataPoliciesIndex(uint8_t);
    int getEndpointDistributionIndex(uint8_t);
    int getVideoFormatPixelFormatDescriptorIndex(uint16_t);
    int getVideoProtocolRawDescriptorIndex(uint16_t);
    int getButtonCountIndex(uint8_t);
    int getStateCountIndex(uint8_t);
    void showDefaultServiceParameters(QString);
    void showSavedServiceParameters(QString, int, int);
    void exportSavedServiceParameters();
    bool insertPropertyField(QString, QString, QString, int, QString, QString, NameVal);
    QString getPropertyType(uint16_t);
   /*JR int exportVideoDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportSimpleUiDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportStatusDescriptorUI(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportRtaDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportLrfDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportLrfControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportArOverlayDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
    int exportPacketGeneratorDescriptor(SqanSolNet::sqanSolNetServiceEntry *, QString);
   */
    void importStoredServiceParameters(QListWidgetItem *);
    QSqlQuery selectStoredServiceProperties(QString, QString, QString);
    void importControlDescriptor(QString, QString);
    void importVideoDescriptor(QString, QString);
    void importRtaDescriptor(QString, QString);
    void importArOverlayDescriptor(QString, QString);
    void importPacketGeneratorDescriptor(QString, QString);
    void importStatusDescriptorUI(QString, QString);
    void importSimpleUiDescriptor(QString, QString);
    void importLrfDescriptor(QString, QString);
    void importLrfControlDescriptor(QString, QString);

  /*JR  void CreateDeviceServiceDescriptor(SqanSolNet *sqanSolNet);
    void CreateVideoServiceDescriptor(SqanSolNet *sqanSolNet);
    void CreateRtaServiceDescriptor(SqanSolNet *sqanSolNet);
*/
    /*****************************************************
     * SolNet Registration Service Table
     * **************************************************/
    QMutex registerServicesTableLock;
    void addDeviceToRegisterServicesTable(uint8_t selected);
    void removeDeviceFromRegisterServicesTable(uint8_t selected);
    void createRegisterServicesTable();
    void clearRegisterServicesTable();
    void addServiceToRegisterServicesTable(uint8_t selected, QString serviceName, uint8_t endpointDistribution, uint8_t multicastGroupId);
    int registerForService(uint8_t selected, QString destService);
    int deregisterFromService(uint8_t selected, QString destService);
    int revokeRegistrationOfService(uint8_t rowIndex, QString destService);

    /*****************************************************
     * Data Traffic Testing
     * **************************************************/
    struct CommonButtonParameters {
        QModelIndex cindex;
        QString source = "";
        QString destination = "";
        uint8_t sourceIndex = 0xFF;
        uint8_t destIndex = 0xFF;
        uint8_t peerIndex = 0xFF;
        uint8_t flowId = 0xFF;
        uint8_t endpointDistribution = 0xFF;
    };

    // Used to pass params to another thread
    struct SendParams {
        CommonButtonParameters *cbp;
        //JR SqanSolNet::sqanSolNetServiceEntry *entry;
    };

    QMutex availableLinksTableLock;
    void createAvailableLinksTable();
    void clearAvailableLinksTable();
    void addAvailableLink(uint8_t row, uint8_t sourceIndex, uint8_t destIndex);
    void removeAvailableLinks(QString devName);
    CommonButtonParameters* commonStartButtonProcess(uint16_t descriptorId);

    QByteArray returnfile = 0;    
    QByteArray getReturnFile() {return returnfile;}
    void setReturnFile(QByteArray srf) {returnfile = srf;}

    uint32_t numPackets = 0;
    uint32_t getNumPac() {return numPackets;}
    void setNumPac(uint32_t np) {numPackets = np;}

    uint16_t getNextFrameNumber();
    uint16_t getNextPacketCount();

    int sendVideoDataPayload(uint8_t currentIndex);
    int sendLrfDataPayload(uint8_t currentIndex);
    int sendLrfControlDataPayload(uint8_t currentIndex);
    int sendRtaDataPayload(uint8_t currentIndex);
    int sendStatusDataPayload(uint8_t currentIndex);

    /******************************************************
     * SQAN API Validation Testing
     * ***************************************************/
    void validateSqanIdentity(int selected);
    void validateSqanSecurity(int selected);
    void validateSqanAssociation(int selected);
    void validateSqanStream(int selected);
    void validateSqanLink(int selected);
    void validateSqanMetrics(int selected);
    void validateSqanProduct(int selected);
    void validateSqanSystem(int selected);
    void validateBrowseAdvertiseServices(int selected);
    void validateRegisterRequestResponse(int selected);
    void validateGetStatusRequestResponse(int selected);
    void validateRevokeRegistrationRequestResponse(int selected);
    void validateDeregisterRequestResponse(int selected);
    void validateRevokeNetAssociationRequestResponse(int selected);

    /****************************************************
    * SqanTestTool utilities
    * ***************************************************/
    QString findNameFromDeviceType(uint16_t devType);
    void displayMessage(QString string);
    void hoverEnter(QHoverEvent *event);
    void hoverLeave(QHoverEvent *event);
    void hoverMove(QHoverEvent *event);
    bool event(QEvent *event);
    int getSqanArrayIndex(QString devName);
    int getDynArrayIndexFromMac(QString);


private slots:
    /****************************************************
    * Hotplug handling slots
    * ***************************************************/
    void checkHotplugEvents();

    /****************************************************
    * Network Configuration slots
    * ***************************************************/
    void on_scanButton_clicked();
    void on_actionExit_triggered();
    void on_netDeviceList_clicked(const QModelIndex &index);
    void showNetDeviceListContextMenu(const QPoint& pos);
    void on_netDeviceList_itemClicked(QListWidgetItem *item);
    void on_deviceComboBox_activated(const QString &arg1);
    void on_netDeviceList_customContextMenuRequested(const QPoint &pos);

//JR    void updateFirmwareDialog();
    void on_actionUpdate_Firmware_triggered();
    void on_firmwareDialogToolButton_clicked();
//JR    void on_updateFirmwareButton_clicked();
    void on_clearUpdateFirmwareTextPushButton_clicked();

    void clearAssociation();
 //JR   void inviteMode();
 //JR   void searchMode();

    void setCoordPriority();
    void setDeviceType();
    void setMacAddress();
//JR    void idleRadio();
//JR    void resetRadio();

    void startMulticastGroup();
    void joinMulticastGroup();
    void leaveMulticastGroup();
    void modifyMulticastGroup();
//JR    void streamSpecCommand();

    /*****************************************************
     * Network Throughput
     * **************************************************/
//JR    void refreshNetworkThroughputData();
//JR    void generateNetworkStatisticsDataPlot();

//    void on_activeLinksComboBox_activated(const QString &arg1);

   /****************************************************
    * SolNet Service Descriptor Configuration slots
    * ***************************************************/
//JR    void on_pushPrintServicesButton();
    void on_cancelPushButton_clicked();
   //JR void on_serviceDescTreeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void on_clearServiceDescriptorTextPushButton_clicked();
    void on_saveServiceDescriptorTextPushButton_clicked();
    // Common to all services added
    void saveCommonConfigButton_clicked(int appIndex);

//    void saveRtaConfigButton_clicked();
//    void saveStatusConfigButton_clicked();
//    void saveSimpleUiConfigButton_clicked();
//    void saveArConfigButton_clicked();
//    void saveLrfConfigButton_clicked();
//    void saveLrfControlConfigButton_clicked();
//    void saveLrfConfigButton_clicked();
//    void saveLrfControlConfigButton_clicked();
//    void savePacketGeneratorConfigButton_clicked();
//    void on_savePlotPushButton_clicked();

    void saveVideoDataPayloadConfigButton_clicked();
    void saveRtaDataPayloadConfigButton_clicked();
    void saveStatusDataPayloadConfigButton_clicked();

    // #WIP shawn10 UI additions
 //JR   void on_pushServiceDescAddButton();
    void on_pushServiceDescRemoveButton();
    void on_pushDescPlanImportButton();
    void on_pushDescPlanExportButton();
//JR    void serviceChosen(const QString &);
   //JR void on_serviceDescTreeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
//JR    void on_serviceDescListWidget_itemDoubleClicked(QListWidgetItem *item);
    void showDescriptorPlansListContextMenu(const QPoint& pos);
    void deleteDescPlan();
//JR    void showServiceDescriptors();

    /****************************************************
    * SolNet Service Descriptor Registration slots
    * ***************************************************/
//JR    void browseAllServices_clicked();
//JR    void registerBox_checked(bool checked = false);
//JR    void deregisterBox_checked(bool checked = false);
//JR    void revokeBox_checked(bool checked = false);
//JR    void getRegistrationStatusBox_checked(bool checked = false);
//JR    void getAllStatusButton_clicked();
//JR    void registerAllButton_clicked();
//JR    void deregisterAllButton_clicked();

    /*****************************************************
     * Data Traffic Testing slots
     * **************************************************/
 //JR   void packetGenStart();
 //JR   void videoGenStart();


    /*******************************************
     * Power Measurment Slots
     ******************************************/
//JR    void on_pwrMeasurementStartBtn_clicked();
//JR    void refreshPwr();
//JR    void on_pwrMeasurementStopBtn_clicked();
    void on_performancePlotPushButton_clicked();
    void on_actionAttenuator_Control_triggered();
    void on_savePowerPlotPushButton_clicked();
//JR    void generatePowerMeasurementsDataPlot();

    /*******************************************
     * SQAN Validation Slots
     ******************************************/
    void on_solnetValidationButton_clicked();
 //JR   void enableObserverMode();
 //JR   void disableObserverMode();
    void on_clearTestAutomationTextPushButton_clicked();
    void on_saveTestAutomationTextPushButton_clicked();
    void on_apiComplianceButton_clicked();
 //JR   void setHibernationMode();
 //JR   void getHibernationMode();


private:
    // Main UI for sqanTestTool
    Ui::sqanTestTool *ui;

    // SqanTestTool version
    QString kitVersion;

    // dynArray is main struct
    // lock on any changes
    QMutex dynArrayLock;

    // Initializing the SQAN Library
    SqanInterfaceInit *sqanInterfaceInit = nullptr;
    std::array<SqanInterface *, 14> *sqanInterfaceArray = nullptr;
    // Used to check for device count
    UsbInterfaceInit *usbInterfaceInit;
    QString unsupportedDevTypeStr = "Unsupported Device Type";

    //Logger Debug Level set by user
    uint16_t debugLevel;
    QStringList debugLevelList;

    // Logging to text file
    //Logger - beginning of filename with timestamp added
    std::string loggerFilename = "sqanTest";
    Logger* theLogger = nullptr;
    //Dequeue messages from Logger queue
    pthread_t threadLoggerDequeue = 0;
    //Structure for Logger dequeue thread
    struct loggerArgStruct {
        Logger* logger;
        sqanTestTool* sqanToolPtr;
    };    

    // DBLogger text file - formatted for Postgres COPY
    // Logger - beginning of filename with timestamp added
    std::string dbLogFilename = "sqanDBLog";
    DBLogger* dbLogger = nullptr;
    // Dequeue messages from DBLogger queue
    pthread_t threadDbLoggerdequeue = 0;
    // Structure for Thread -- DBLogger
    struct dbLoggerArgStruct {
        DBLogger* dbLogger;
        sqanTestTool* sqanToolPtr;
    };

    // Database Handling
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    QSqlQuery qry;
    bool dbConnected;
//    void openDatabase();
//    void closeDatabase();

    // Hotplug event handling
    bool checkingHotplug = false;
    QTimer *hotplugTimer = nullptr;

    // UI setup
    QGraphicsScene *netTopologyScene =  nullptr;
//JR    QMessageBox msgBox;
//JR    QList<Edge *> edges;
    QBrush brush;
    QPen outlinePen;
    QGraphicsTextItem *text;
//JR    QLabel *label;
    QGraphicsProxyWidget *proxy;
    QColor mcastColor;
    QColor nodeColor;
    QColor coordColor;
    QString indexDelim = "-";

    // Network throughput
//JR    PlotChart *dataPlotChart =  nullptr;
//JR    QChartView *dataPlotChartView = nullptr;
    QGraphicsScene *dataPlotScene =  nullptr;
    QTimer *data_timer = nullptr;

    // Firmware Update
    int updateFirmwareTabWidgetLocation;
    QString filename_firmware;
    QString filename_video;
    QString filename_audio;
    QString filename_scenario;

    // SolNet descriptor handling
    QMutex nextFrameNumberMutex;
    QMutex nextPacketCountMutex;
    uint16_t nextFrameNumber = 0;
    uint16_t nextPacketCount = 0;
    QStringList unicastEndpointIds;
    QStringList broadcastEndpointId;
    QStringList multicastEndpointId;
    QStringList endpointDistribution;
    QStringList endpointDataPolicies;
    QStringList pixelFormat;
    QStringList videoProtocol;
    QStringList buttonCount;
    QStringList stateCount;
    QStringList videoPolarity;
    QStringList videoImageCalibration;
    QStringList rtaEnabled;
    QStringList rtaMode;
    QStringList rtaPolarity;
    QStringList rtaZoom;
    QStringList rtaBubble;
    QStringList rtaManualAlignment;
    QStringList rtaReticleInVideo;
    QStringList rtaReticleType;
    QStringList batteryStateStatus;
    QStringList batteryChargingStatus;
    QStringList deviceStateStatus;
    QStringList displayStateStatus;
    QStringList weaponSightModeStatus;
    QStringList weaponActiveReticleIDStatus;
    QStringList weaponSymbologyStateStatus;
    QComboBox *deviceEndpointIdComboBox = nullptr;
    QLineEdit *deviceEndpointDataflowLineEdit = nullptr;
    QComboBox *deviceEndpointDistributionComboBox = nullptr;
    QComboBox *deviceEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *deviceNameLineEdit = nullptr;
    QLineEdit *deviceSerialNumberLineEdit = nullptr;
    QLineEdit *deviceManufacturerLineEdit = nullptr;
    QLineEdit *deviceFriendlyNameLineEdit = nullptr;
    QComboBox *videoEndpointIdComboBox = nullptr;
    QLineEdit *videoEndpointDataflowLineEdit = nullptr;
    QComboBox *videoEndpointDistributionComboBox = nullptr;
    QComboBox *videoEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *videoFormatResolutionXServiceDescriptorLineEdit = nullptr;
    QLineEdit *videoFormatResolutionYServiceDescriptorLineEdit = nullptr;
    QLineEdit *videoFormatFrameRateDescriptorLineEdit = nullptr;
    QLineEdit *videoFormatBitDepthDescriptorLineEdit = nullptr;
    QComboBox *videoFormatPixelFormatDescriptorComboBox = nullptr;
    QComboBox *videoProtocolRawDescriptorComboBox = nullptr;
    QLineEdit *videoIfovDescriptorLineEdit = nullptr;
    QLineEdit *videoPPointXLineEdit = nullptr;
    QLineEdit *videoPPointYLineEdit = nullptr;
    QLineEdit *videoLabelLineEdit = nullptr;
    QLineEdit *videoLensDistortionCoefficientsLineEdit = nullptr;
    QLineEdit *videoControlBitmapLineEdit = nullptr;
    QComboBox *videoPolarityComboBox = nullptr;
    QComboBox *videoImageCalibrationComboBox = nullptr;
    QComboBox *rtaEndpointIdComboBox = nullptr;
    QLineEdit *rtaEndpointDataflowLineEdit = nullptr;
    QComboBox *rtaEndpointDistributionComboBox = nullptr;
    QComboBox *rtaEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *rtaImuRateLineEdit = nullptr;
    QLineEdit *rtaLabelLineEdit = nullptr;
    QComboBox *rtaEnabledComboBox = nullptr;
    QComboBox *rtaModeComboBox = nullptr;
    QComboBox *rtaPolarityComboBox = nullptr;
    QComboBox *rtaZoomComboBox = nullptr;
    QComboBox *rtaBubbleComboBox = nullptr;
    QComboBox *rtaManualAlignmentComboBox = nullptr;
    QComboBox *rtaReticleInVideoComboBox = nullptr;
    QComboBox *rtaReticleTypeComboBox = nullptr;
    QLineEdit *rtaQuaternionReal = nullptr;
    QLineEdit *rtaQuaternionI = nullptr;
    QLineEdit *rtaQuaternionJ = nullptr;
    QLineEdit *rtaQuaternionK = nullptr;
    QComboBox *packetGenEndpointIdComboBox = nullptr;
    QLineEdit *packetGenEndpointDataflowLineEdit = nullptr;
    QComboBox *packetGenEndpointDistributionComboBox = nullptr;
    QComboBox *packetGenEndpointDataPoliciesComboBox = nullptr;
    QComboBox *statusEndpointIdComboBox = nullptr;
    QLineEdit *statusEndpointDataflowLineEdit = nullptr;
    QComboBox *statusEndpointDistributionComboBox = nullptr;
    QComboBox *statusEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *statusLabelLineEdit = nullptr;
    QLineEdit *batteryLevelLineEdit = nullptr;
    QComboBox *batteryStateComboBox = nullptr;
    QComboBox *batteryChargingComboBox = nullptr;
    QComboBox *deviceStateComboBox = nullptr;
    QComboBox *displayStateComboBox = nullptr;
    QComboBox *weaponSightModeComboBox = nullptr;
    QComboBox *weaponActiveReticleIDComboBox = nullptr;
    QComboBox *weaponSymbologyStateComboBox = nullptr;
    QComboBox *simpleUiEndpointIdComboBox = nullptr;
    QLineEdit *simpleUiEndpointDataflowLineEdit = nullptr;
    QComboBox *simpleUiEndpointDistributionComboBox = nullptr;
    QComboBox *simpleUiEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *simpleUiTerminalIdLineEdit = nullptr;
    QLineEdit *simpleUiCapabilitiesLineEdit = nullptr;
    QComboBox *simpleUiButtonCountComboBox = nullptr;
    QComboBox *simpleUiStateCountComboBox = nullptr;
    QLineEdit *simpleUiLabelLineEdit = nullptr;
    QLineEdit *packetGenLabelLineEdit = nullptr;
    QComboBox *arOverlayEndpointIdComboBox = nullptr;
    QLineEdit *arOverlayEndpointDataflowLineEdit = nullptr;
    QComboBox *arOverlayEndpointDistributionComboBox = nullptr;
    QComboBox *arOverlayEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *arOverlayLabelLineEdit = nullptr;
    QLineEdit *arOverlayMinWidthLineEdit = nullptr;
    QLineEdit *arOverlayMinHeightLineEdit = nullptr;
    QLineEdit *arOverlayRleVersionsLineEdit = nullptr;
    QComboBox *lrfEndpointIdComboBox = nullptr;
    QLineEdit *lrfEndpointDataflowLineEdit = nullptr;
    QComboBox *lrfEndpointDistributionComboBox = nullptr;
    QComboBox *lrfEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *lrfLabelLineEdit = nullptr;
    QComboBox *lrfControlEndpointIdComboBox = nullptr;
    QLineEdit *lrfControlEndpointDataflowLineEdit = nullptr;
    QComboBox *lrfControlEndpointDistributionComboBox = nullptr;
    QComboBox *lrfControlEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *lrfControlLabelLineEdit = nullptr;

    // For registerServicesTable
    const int defaultRegSrvRowColCount = 1;
    QMutex registerMutex;
    QMutex deregisterMutex;
    QMutex revokeMutex;
    QMutex getStatusMutex;
    struct tableCell
    {
        int rowIndex;
        int colIndex;
//JR        QLabel *statusLabel;
//JR        QLabel *distributionLabel;
    };
//JR   QMap<QCheckBox *, tableCell *> checkBoxMap;
    QPushButton *browseAllButton = nullptr;
    QPushButton *regAllButton = nullptr;
    QPushButton *getAllStatusButton = nullptr;
    QPushButton *deregAllButton = nullptr;
    QMap<QString, QString> endpointDistributionMap;
    //Service List For SolNet registration
    QStringList serviceList;

    //Elasped Timer
    QElapsedTimer elapsedTimer;

    // Data Traffic testing
    bool dataTrafficButtonClicked;
    QBuffer transmitBuffer;
    QBuffer receiverBuffer;
    QMap<QString, int> sourceMap;

    //Power Measurements
    QGraphicsScene *powerMeasurementScene =  nullptr;
/*JR    PlotChart *pwrPlotChart = nullptr;
    PlotChart *powerMeasurementsPlotChart = nullptr;
    Dmm *dmmV = nullptr;
    Dmm *dmmI = nullptr;
*/
    QTimer *pwr_timer = nullptr;
    double Voltage = 0;
    double Current = 0;
    double Power = 0;
//JR    QChartView *powerMeasurementsChartView = nullptr;
};

#endif // SQANTESTTOOL_H
