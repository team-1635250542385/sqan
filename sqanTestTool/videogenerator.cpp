//###################################################
// Filename: videoGenerator.cpp
// Description: SQAN Test Tool VideoGenerator Class
//###################################################

#include "videogenerator.h"
#include "ui_videogenerator.h"
#include <unistd.h>
#include <QDebug>
#include <QButtonGroup>
#include <QMessageBox>

videoGenerator::videoGenerator(sqanTestTool *testToolPtr, void *userData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::videoGenerator),
    sqanToolPtr(testToolPtr)
{
    ui->setupUi(this);
    setWindowTitle("Video Tests");

    dataParams = (SendDataParams *)userData;

    // Setup the control buttons: start, stop, cancel
    startTestPushButton = new QPushButton(this);
    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
    startTestPushButton->setText("Start");
    startTestPushButton->setGeometry(QRect(60, 520, 122, 28));
    startTestPushButton->setCheckable(true);
    startTestPushButton->setAutoExclusive(true);
    stopTestPushButton = new QPushButton(this);
    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
    stopTestPushButton->setText("Stop");
    stopTestPushButton->setGeometry(QRect(220, 520, 122, 28));
    stopTestPushButton->setCheckable(true);
    stopTestPushButton->setAutoExclusive(true);

    startTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");
    stopTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(startTestPushButton);
    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);
    buttonGroup->addButton(stopTestPushButton);
    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

        // Setup the video test subwindows
    grid = new QGridLayout;
    ui->topWidget->setLayout(grid);
    grid->addWidget(createVideoTestGroup(), 0, 0);

    ui->topWidget->repaint();
}

videoGenerator::~videoGenerator()
{
    delete (dataParams->cbp);
    delete (dataParams);

    delete cameraButton;

    delete pattern0Button;
    delete pattern1Button;
    delete pattern2Button;
    delete pattern3Button;
    delete pattern4Button;
    delete pattern5Button;
    delete pattern6Button;
    delete pattern7Button;
    delete pattern8Button;
    delete pattern9Button;
    delete pattern10Button;
    delete pattern11Button;
    delete pattern12Button;
    delete pattern13Button;
    delete pattern14Button;
    delete pattern15Button;
    delete pattern16Button;
    delete pattern17Button;
    delete pattern18Button;
    delete pattern19Button;
    delete pattern20Button;
    delete pattern21Button;
    delete pattern22Button;
    delete pattern23Button;
    delete pattern24Button;

    delete videoTestsGroupBox;

    delete grid;
    delete ui;
}


QGroupBox *videoGenerator::createVideoTestGroup()
{
    videoTestsGroupBox = new QGroupBox(tr("Select a Test"));

    pattern0Button = new QRadioButton(tr("Pattern 0  - SMPTE 100% color bars"));
    pattern1Button = new QRadioButton(tr("Pattern 1  - Snow"));
    pattern2Button = new QRadioButton(tr("Pattern 2  - Black"));
    pattern3Button = new QRadioButton(tr("Pattern 3  - White"));
    pattern4Button = new QRadioButton(tr("Pattern 4  - Red"));
    pattern5Button = new QRadioButton(tr("Pattern 5  - Green"));
    pattern6Button = new QRadioButton(tr("Pattern 6  - Blue"));
    pattern7Button = new QRadioButton(tr("Pattern 7  - Checkers-1"));
    pattern8Button = new QRadioButton(tr("Pattern 8  - Checkers-2"));
    pattern9Button = new QRadioButton(tr("Pattern 9  - Checkers-4"));
    pattern10Button = new QRadioButton(tr("Pattern 10 - Checkers-8"));
    pattern11Button = new QRadioButton(tr("Pattern 11 - Circular"));
    pattern12Button = new QRadioButton(tr("Pattern 12 - Blink"));
    pattern13Button = new QRadioButton(tr("Pattern 13 - SMPTE 75% color bars"));
    pattern14Button = new QRadioButton(tr("Pattern 14 - Zone plate"));
    pattern15Button = new QRadioButton(tr("Pattern 15 - Gamut checkers"));
    pattern16Button = new QRadioButton(tr("Pattern 16 - Chroma zone plate"));
    pattern17Button = new QRadioButton(tr("Pattern 17 - Solid color"));
    pattern18Button = new QRadioButton(tr("Pattern 18 - Moving ball"));
    pattern19Button = new QRadioButton(tr("Pattern 19 - SMPTE 100% color bars"));
    pattern20Button = new QRadioButton(tr("Pattern 20 - Bar"));
    pattern21Button = new QRadioButton(tr("Pattern 21 - Pinwheel"));
    pattern22Button = new QRadioButton(tr("Pattern 22 - Spokes"));
    pattern23Button = new QRadioButton(tr("Pattern 23 - Gradient"));
    pattern24Button = new QRadioButton(tr("Pattern 24 - Colors"));

    cameraButton = new QRadioButton(tr("Camera"));

    pattern0Button->setChecked(false);
    pattern1Button->setChecked(false);
    pattern2Button->setChecked(false);
    pattern3Button->setChecked(false);
    pattern4Button->setChecked(false);
    pattern5Button->setChecked(false);
    pattern6Button->setChecked(false);
    pattern7Button->setChecked(false);
    pattern8Button->setChecked(false);
    pattern9Button->setChecked(false);
    pattern10Button->setChecked(false);
    pattern11Button->setChecked(false);
    pattern12Button->setChecked(false);
    pattern13Button->setChecked(false);
    pattern14Button->setChecked(false);
    pattern15Button->setChecked(false);
    pattern16Button->setChecked(false);
    pattern17Button->setChecked(false);
    pattern18Button->setChecked(false);
    pattern19Button->setChecked(false);
    pattern20Button->setChecked(false);
    pattern21Button->setChecked(false);
    pattern22Button->setChecked(false);
    pattern23Button->setChecked(false);
    pattern24Button->setChecked(false);

    cameraButton->setChecked(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(pattern0Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern1Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern2Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern3Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern4Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern5Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern6Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern7Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern8Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern9Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern10Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern11Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern12Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern13Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern14Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern15Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern16Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern17Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern18Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern19Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern20Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern21Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern22Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern23Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern24Button, 0, Qt::AlignTop);

    vbox->addWidget(cameraButton);
    vbox->setAlignment(Qt::AlignTop);
    videoTestsGroupBox->setLayout(vbox);

    return videoTestsGroupBox;
}

void videoGenerator::onGroupButtonClicked(int id)
{
    //qDebug() << id;

    switch(id)
    {
        case START_BUTTON_ID:
        {
            startVideoTest();
            break;
        }
        case STOP_BUTTON_ID:
        {
            stopVideoTest();
            break;
        }
        default:
            break;
    }
}

void videoGenerator::startVideoTest()
{
    if ( isRunning )
    {
        displayMessage("Stop current test first.");
        return;
    }

    isRunning = true;

    if ( (dataParams->cbp->endpointDistribution >= 0x01) && (dataParams->cbp->endpointDistribution <= 0xFF) )
    {
        //Multicast or Broadcast
        peerIndex = dataParams->cbp->endpointDistribution;
    }
    else
    {
        //Unicast
         peerIndex = dataParams->cbp->peerIndex;
    }
    endpointId = dataParams->entry->endpointId;
    dataflowId = dataParams->entry->dataflowId;

    sqanIntrfcSrcPtr = sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanInterface;
    sqanSolNetSrcPtr = sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanSolNet;
    sqanIntrfcSrcPtr->SetFlushSendQueue(endpointId, dataflowId, false);

    sqanIntrfcDstPtr = sqanToolPtr->DynArray[dataParams->cbp->destIndex].sqanInterface;
    sqanIntrfcDstPtr->SetFlushReceiveQueue(endpointId, dataflowId, false);

    // Setup Video Sender and Player
    uint8_t videoTestType = 0;
    uint16_t videoFormatType = 0;
    uint16_t videoPixelFormat = 0;

    if ( cameraButton->isChecked() )
    {
        // Camera test
        videoTestType = 30;
    }
    else
    {
        // One of the pattern tests
        // There are 0 - 24
        int buttonCount = 0;
        foreach (QRadioButton *button, videoTestsGroupBox->findChildren<QRadioButton*>())
        {
            if (button->isChecked())
            {
                videoTestType = buttonCount;
                break;
            }
            buttonCount++;
        }
    }

    // We only have RAW format right now
    // and YV12 from webcam. For other
    // formats use SqanSolNet::sqanSolNetRawVideoPixelFormat enums
    videoFormatType = SqanSolNet::VideoFormatRaw;
    videoPixelFormat = GstSender::PIXEL_FORMAT_YV12;

    // Setup for the GstSender to stream video data to sqanTestTool
    gstSender = new GstSender(videoTestType, videoFormatType, videoPixelFormat);
    connect(gstSender, SIGNAL(videoFromSourceAvailable(const QByteArray&)),
            this, SLOT(sendVideoToRadio(const QByteArray&)), Qt::DirectConnection);

    // Setup for the sink that plays video
    gstPlayer = new GstPlayer(videoTestType, videoFormatType, videoPixelFormat);
    // Connect data from sqanTestTool to GstPlayer
    connect(this, SIGNAL(videoFramesAvailable(const QByteArray&)),
            gstPlayer, SLOT(receiveVideoDataInPlayer(const QByteArray&)), Qt::DirectConnection);

    sleep(1);

    // Debug on signals / slots
    //QObject::dumpObjectInfo();

    gstPlayerThread = QtConcurrent::run(gstPlayer, &GstPlayer::startReceiving);

    while ( !gstPlayer->isPlayerRunning() )
    {
        usleep(100);
    }

    gstSenderThread = QtConcurrent::run(gstSender, &GstSender::startSending);
}

void videoGenerator::stopVideoTest()
{
    if ( gstSender != nullptr )
    {
        gstSender->stopSending();

        while ( gstSender->isSenderRunning() )
        {
            usleep(100);
        }

        delete gstSender;
        gstSender = nullptr;

    }
    if ( gstPlayer != nullptr )
    {
        gstPlayer->stopReceiving();

        while ( gstPlayer->isPlayerRunning() )
        {
            usleep(100);
        }

        delete gstPlayer;
        gstPlayer = nullptr;
    }

    gstSenderThread.cancel();
    gstPlayerThread.cancel();

    gstSenderThread.waitForFinished();
    gstPlayerThread.waitForFinished();

    // Remove all packets on the outgoing queue for this flow
    sqanIntrfcSrcPtr->SetFlushSendQueue(endpointId, dataflowId, true);

    // Remove all packets on the receiver's incoming queue for this flow
    sqanIntrfcDstPtr->SetFlushReceiveQueue(endpointId, dataflowId, true);

    startTestPushButton->setChecked(false);
    stopTestPushButton->setChecked(false);

    isRunning = false;
    //usleep(100); //Let the other thread know we stopped
}

//! VideoGenerator GstSender calls this to deliver data to a radio
//! for transmission over the wireless network
void videoGenerator::sendVideoToRadio(const QByteArray& sourceData)
{
    if ( sourceData.size() > 0 )
    {
        // Incoming data from the video source written to a buffer
        //qDebug() << "sendVideoToRadio: sourceData.size=" << QString::number(sourceData.size());

        // Send the video data to the other radio
        uint16_t blockSize = SqanSolNet::payloadDataBufferSize;
        uint8_t policies = 0;

        // Have to copy here because we are in a slot and passing to the outgoing network
        int dataSize = sourceData.size();
        uint8_t buffer[dataSize];
        memset(buffer, 0, dataSize);
        memcpy(buffer, (uint8_t *)(sourceData.data()), dataSize);

        int numBlocks = 1;
        if ( dataSize > blockSize )
        {
            numBlocks = dataSize / blockSize;

            if ( (dataSize % blockSize) > 0 )
            {
                numBlocks++;
            }

            int offset = 0;
            int bytesLeft = dataSize;
            for (int i=0; i<numBlocks; i++)
            {
                // Send raw frame data from the camera over the wire to the receiving radio
                if ( bytesLeft < blockSize )
                {
                    sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanSolNet->SendSolNetDataPacket(dataParams->cbp->peerIndex,
                                                                                                       dataParams->entry->endpointId,
                                                                                                       dataParams->entry->dataflowId,
                                                                                                       policies,
                                                                                                       (uint8_t *)(buffer + offset),
                                                                                                       bytesLeft);
                    //qDebug() << "Sending to SolNet at offset=" << QString::number(offset) << " bytes=" << QString::number(bytesLeft);
                    break;
                }
                else
                {
                    sqanToolPtr->DynArray[dataParams->cbp->sourceIndex].sqanSolNet->SendSolNetDataPacket(dataParams->cbp->peerIndex,
                                                                                                       dataParams->entry->endpointId,
                                                                                                       dataParams->entry->dataflowId,
                                                                                                       policies,
                                                                                                       (uint8_t *)(buffer + offset),
                                                                                                       blockSize);
                    offset += blockSize;
                    bytesLeft -= blockSize;
                    //qDebug() << "Sending to SolNet at offset=" << QString::number(offset) << " bytes=" << QString::number(blockSize);

                }
                usleep(10);
            }
        }
    }
}

// Receives data as a callback from SqanSolNet::DeliverSolNetData()
// data coming in from the wireless network
void videoGenerator::receiveVideoDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr)
{
    if ( transferSize > 0 )
    {
        videoGenerator *videoGenPtr = (videoGenerator *)thisPtr;
        // Data from DeliverSolNetData to this callback
        //qDebug() << "transferSize=" << QString::number(transferSize);

        QByteArray byteArray = QByteArray((const char*)data, transferSize);
        emit videoGenPtr->videoFramesAvailable(byteArray);
    }
}

void videoGenerator::displayMessage(QString string)
{
    QMessageBox msgBox;
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);

    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(2000);

    msgBox.exec();
}
