#include <QDebug>
#include "../SqanInterface/SqanSolNet.h"
#include "../SqanInterface/SqanIdentity.h"
#include "servicechooser.h"
#include "ui_servicechooser.h"

//serviceChooser::serviceChooser(DBLogger *dbLogger, const QString &devStr, QListWidget *serviceDescListWidget, QWidget *parent) :
serviceChooser::serviceChooser(DBLogger *dbLogger, uint16_t devType, QListWidget *serviceDescListWidget, QWidget *parent) :
    QDialog(parent),
    dbLogger(dbLogger),
    ui(new Ui::serviceChooser),
    deviceType(devType),
    device(QString::fromStdString(SqanIdentity::GetDeviceTypeString(devType)))
{
    ui->setupUi(this);
    setWindowTitle("Select a service to add");

    qDebug() << Q_FUNC_INFO;
    qDebug() << "device type: '" << device << "'";

    // Define which services are available for each type of device
    if ( deviceType == SqanIdentity::sqanDeviceTypes::HTR ) {
        strList << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
    } else if ( deviceType == SqanIdentity::sqanDeviceTypes::TWS ) {
        strList << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
    } else if ( deviceType == SqanIdentity::sqanDeviceTypes::PHN ) {
        strList << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
    } else if ( deviceType == SqanIdentity::sqanDeviceTypes::STM ) {
        strList << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
    } else if ( deviceType == SqanIdentity::sqanDeviceTypes::HMD ) {
        strList << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay))
                << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
    }

    ui->serviceListWidget->addItems(strList);

    // Remove services from the chooser that have previously been chosen
    for (int i = 0; i < ui->serviceListWidget->count(); i++)
    {
        curItem = ui->serviceListWidget->item(i);
        QString pat = "([0-9]+" + QRegExp::escape("-") + ")?" + QRegExp::escape(curItem->text()) + "(" + QRegExp::escape(UNSAVED_STR) + ")?";
        auto duplicateServiceList = serviceDescListWidget->findItems(pat, Qt::MatchRegExp);
        if (duplicateServiceList.length() == 0) {
            curItem->setFlags(curItem->flags() | Qt::ItemIsSelectable);
            curItem->setSelected(false);
        } else {
            delete curItem;
            i--;
        }
    }

    curItem = nullptr;
}

serviceChooser::~serviceChooser()
{
    delete ui;
}

std::string serviceChooser::getCurItemStr()
{
    if (curItem == nullptr) {
        return ("");
    } else {
        return (curItem->text().toStdString());
    }
}

void serviceChooser::on_serviceListWidget_itemClicked(QListWidgetItem *item)
{
    curItem = item;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("curItem->text()",getCurItemStr());
    dbLogger->dbLog(dblr);
}

void serviceChooser::on_serviceListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    curItem = item;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("curItem->text()",getCurItemStr());
    dbLogger->dbLog(dblr);

    emit chooseService(curItem->text());
    accept();
}

void serviceChooser::on_buttonBox_clicked(QAbstractButton *button)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("button->text()",button->text().toStdString());
    dblr.addUIObservation("curItem->text()", getCurItemStr());
    dbLogger->dbLog(dblr);

    if (static_cast<QPushButton*>(button) == ui->buttonBox->button(QDialogButtonBox::Ok)) {
        if (curItem != nullptr) {
            emit chooseService(curItem->text());
        }
    }
}

