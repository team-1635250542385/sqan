#ifndef SQUADLEADERMODEDIALOG_H
#define SQUADLEADERMODEDIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class SquadLeaderModeDialog;
}

class SquadLeaderModeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SquadLeaderModeDialog(QWidget *parent = nullptr);
    ~SquadLeaderModeDialog();
    uint8_t getRssiConnect();
    uint8_t getRssiDisconnect();
    uint8_t getPhyRateCapIndex();

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::SquadLeaderModeDialog *ui;
};

#endif // SQUADLEADERMODEDIALOG_H
