#include "sqantesttool.h"
#include <QApplication>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if(getuid())
    {
        qDebug() << "Not Logged As Root User";
        return 0;
    }
    else
    {
       sqanTestTool w;
       w.show();
       return a.exec();
    }
}
