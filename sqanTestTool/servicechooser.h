#ifndef SERVICECHOOSER_H
#define SERVICECHOOSER_H

#include <QDialog>
#include <QListWidget>
#include <QAbstractButton>
#include <QPushButton>
#include "../Logger/DBLogger.h"

#define UNSAVED_STR " *"

namespace Ui {
class serviceChooser;
}

class serviceChooser : public QDialog
{
    Q_OBJECT

public:
//    explicit serviceChooser(DBLogger *dbLogger, const QString &devStr, QListWidget *serviceDescListWidget, QWidget *parent = nullptr);
    explicit serviceChooser(DBLogger *dbLogger, uint16_t devType, QListWidget *serviceDescListWidget, QWidget *parent = nullptr);
    ~serviceChooser();

    std::string getCurItemStr();

    DBLogger *dbLogger = nullptr;

signals:
    void chooseService(const QString &);

private slots:
    void on_serviceListWidget_itemClicked(QListWidgetItem *curItem);
    void on_serviceListWidget_itemDoubleClicked(QListWidgetItem *curItem);
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::serviceChooser *ui;
    uint16_t deviceType;
    QString device;
    QListWidget* listWidget;
    QStringList strList;
    QListWidgetItem* curItem = nullptr;
};

#endif // SERVICECHOOSER_H
