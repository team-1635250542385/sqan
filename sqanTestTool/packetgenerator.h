#ifndef PACKETGENERATOR_H
#define PACKETGENERATOR_H

#include <QDialog>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QGridLayout>
#include <QTableWidget>
#include <pthread.h>
#include "sqantesttool.h"


namespace Ui {
class packetGenerator;
}

class sqanTestTool;

class packetGenerator : public QDialog
{
    Q_OBJECT

public:
    explicit packetGenerator(sqanTestTool *testToolPtr, void *userData, QWidget *parent = nullptr);
    ~packetGenerator(void);

    bool isTestRunning(void) { return isRunning; }
    QButtonGroup *buttonGroup = nullptr;

public slots:
    void onGroupButtonClicked(int id);
    static void receiveDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr);
    void updateNumPacketsReceived();

signals:
    void packetReceived();

private slots:
    void startDataTest();
    void stopDataTest();
    void clear(void);

private:
    void createDataTestTable(void);
    void displayMessage(QString string);

private:
    Ui::packetGenerator *ui;
    QPushButton *startTestPushButton;
    QPushButton *stopTestPushButton;

    sqanTestTool *sqanToolPtr = nullptr;
    bool isRunning = false;

    struct CommonButtonParameters {
        QModelIndex cindex;
        QString source = "";
        QString destination = "";
        uint8_t sourceIndex = 0xFF;
        uint8_t destIndex = 0xFF;
        uint8_t peerIndex = 0xFF;
        uint8_t flowId = 0xFF;
        uint8_t endpointDistribution = 0xFF;
    };

    // Used to pass params to another thread
    struct SendDataParams {
        CommonButtonParameters *cbp;
        SqanSolNet::sqanSolNetServiceEntry *entry;
    };
    SendDataParams *dataParams = nullptr;

    static const int START_BUTTON_ID = 1;
    static const int STOP_BUTTON_ID = 2;

    QPushButton clearButton;
    QTableWidgetItem srcColumn;
    QTableWidgetItem dstColumn;
    QTableWidgetItem packetSizeColumn;
    QTableWidgetItem numPacketsToSendColumn;
    QTableWidgetItem numPacketsRecvdColumn;

    // Number of SolNet Data Packets we receive
    uint32_t numPacketsReceived = 0;
    // Number of SolNet Data Packets the user configures to send
    uint32_t numPacketsToSend = 0;

    QFuture<void> future;
    static void sendData(packetGenerator *thisPtr);

    // These are used to stop the data flow down in SqanInterface
    SqanInterface *sqanIntrfcSrcPtr= nullptr;
    SqanSolNet *sqanSolNetSrcPtr= nullptr;
    SqanInterface *sqanIntrfcDstPtr= nullptr;
    uint8_t peerIndex;
    uint8_t endpointId;
    uint8_t dataflowId;
};

#endif // PACKETGENERATOR_H
