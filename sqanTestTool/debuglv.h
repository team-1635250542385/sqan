#ifndef DEBUGLV_H
#define DEBUGLV_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

namespace Ui {
class debugLv;
}

class debugLv : public QDialog
{
    Q_OBJECT

public:
    explicit debugLv(QWidget *parent = nullptr);
    ~debugLv();

    uint16_t getDebugLevel();

private slots:
    void on_okPushButton_clicked();

    void on_cancelPushButton_clicked();

    void on_checkAllPushButton_clicked();

private:
    Ui::debugLv *ui;
    QListWidget* listWidget;
    QDialogButtonBox* buttonBoxDebugLv;
    QGroupBox* viewBoxDebugLv;
    QPushButton* confirmButton;
    QPushButton* closeButton;
    QStringList strList;
    QListWidgetItem* item = 0;
    QStringList debugList;

    uint16_t dbL_v1 = 0x0000;
    uint16_t dbL_v2 = 0x0000;
    uint16_t dbL_v3 = 0x0000;
    uint16_t dbL_v4 = 0x0000;
    uint16_t dbL_v5 = 0x0000;
    uint16_t dbL_v6 = 0x0000;
    uint16_t dbL_v7 = 0x0000;
    uint16_t dbL_v8 = 0x0000;
    uint16_t dbL_v9 = 0x0000;
    uint16_t dbL_v10 = 0x0000;
    uint16_t dbL_v11 = 0x0000;
    uint16_t dbL_v12 = 0x0000;

    uint16_t debugLevel = 0;

};

#endif // DEBUGLV_H
