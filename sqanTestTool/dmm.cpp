#include "dmm.h"
#include "socketscpi.h"

Dmm::Dmm()
{
    device = "Keysight 34461A";
    ipAddr = "169.254.4.61";
    port = 5025;
    scpi = new SocketScpi(ipAddr, port);
}

Dmm::Dmm(QString ipAddr1)
{
    device = "Keysight 34461A";
    ipAddr = ipAddr1;
    port = 5025;
    scpi = new SocketScpi(ipAddr, port);
    qDebug() << ipAddr1 << "is connecting";
}

void Dmm::connect()
{
    scpi->connect();
}

void Dmm::disconnect()
{
    scpi->disconnect();
}

double Dmm::getVolt()
{
    scpi->send("MEAS:VOLT:DC? 10,0.001\n");
    return (scpi->recv());
}

double Dmm::getCurrent()
{
    scpi->send("MEAS:CURR:DC? 1\n");
    return (scpi->recv());
}

