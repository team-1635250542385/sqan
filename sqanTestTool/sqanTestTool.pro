#-------------------------------------------------
#
# Project created by QtCreator 2019-01-15T08:28:21
#
#-------------------------------------------------

QT       += core gui sql charts multimedia multimediawidgets xml network opengl concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sqanTestTool
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += DEBUG

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH +=  /usr/local/include/ \
                /usr/include \
                /usr/include/gstreamer-1.0 \
                /usr/include/glib-2.0 \
                /usr/lib64/glib-2.0/include \
                /usr/lib/x86_64-linux-gnu/glib-2.0/include \
                /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include

SOURCES += \
    ../Logger/DBLogRecord.cpp \
    ../Logger/DBLogger.cpp \
    ../Logger/Logger.cpp \
    ../UsbInterface/UsbInterface.cpp \
    ../UsbInterface/UsbInterfaceInit.cpp \
    ../SqanInterface/SqanInterface.cpp \
    ../SqanInterface/SqanInterfaceInit.cpp \
    debuglv.cpp \
    sqantesttool.cpp \
    main.cpp


HEADERS += \
    ../Logger/DBLogRecord.h \
    ../Logger/DBLogger.h \
    ../Logger/Logger.h \
    ../SqanInterface/Interface.h \
    ../UsbInterface/UsbInterface.h \
    ../UsbInterface/UsbInterfaceInit.h \
    ../SqanInterface/SqanInterface.h \
    ../SqanInterface/SqanInterfaceInit.h \
    debuglv.h \
    sqantesttool.h

FORMS += \
        debuglv.ui \
	sqantesttool.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#LIBS += -L/usr/lib64 -lusb-1.0 -lSqanInterface -lz
LIBS += -L/usr/lib64 -lusb-1.0 -lz
LIBS += -L/usr/lib64/gstreamer-1.0 -lgstreamer-1.0 -lgstapp-1.0 -lgstvideo-1.0 -lgobject-2.0 -lglib-2.0 -lgio-2.0

DISTFILES += \
    DebugLevel.qml \
    DebugLevel.ui.qml
