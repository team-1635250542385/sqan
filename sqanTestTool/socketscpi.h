#ifndef SOCKETSCPI_H
#define SOCKETSCPI_H
#include <QTcpSocket>


class SocketScpi
{

public:
    SocketScpi();
    SocketScpi(QString ipAddr, int port);
    void checkError();
    void connect();
    void disconnect();
    double recv();
    void send(QString command);
    //double qStringtoDouble(QString string);

private:
    QTcpSocket* socket = nullptr;
    QString socketIpAddr = "";
    int socketPort = 0;
    bool connectedToHost;
};

#endif // SOCKETSCPI_H
