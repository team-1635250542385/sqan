#include "debuglv.h"
#include "ui_debuglv.h"
#include <QDebug>

debugLv::debugLv(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::debugLv)
{
    ui->setupUi(this);
    setWindowTitle("Set Debug Level");

    strList << "DEBUG_SQAN_INTERFACE" << "DEBUG_SQAN_USB";

    ui->debugLevelListWidget->addItems(strList);

    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Unchecked);
    }
}

debugLv::~debugLv()
{
    delete ui;
}

void debugLv::on_okPushButton_clicked()
{
    // Get All Selected Items
    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        if(item->checkState() == Qt::Checked)
        {
            debugList << item->text();
        }
    }

    if ( debugList.contains("DEBUG_SQAN_INTERFACE") )
    {
        dbL_v1 = dbL_v1 + 0x0001;
    }
    if ( debugList.contains("DEBUG_SQAN_USB") )
    {
        dbL_v2 = dbL_v2 + 0x0002;
    }
   /*JR if ( debugList.contains("DEBUG_SQAN_FIRMWARE") )
    {
        dbL_v3 = dbL_v3 + 0x0004;
    }
    if ( debugList.contains("DEBUG_SQAN_IDENTITY") )
    {
        dbL_v4 = dbL_v4 + 0x0008;
    }
    if ( debugList.contains("DEBUG_SQAN_SYSTEM") )
    {
        dbL_v5 = dbL_v5 + 0x0010;
    }
    if ( debugList.contains("DEBUG_SQAN_ASSOCIATION") )
    {
        dbL_v6 = dbL_v6 + 0x0020;
    }
    if ( debugList.contains("DEBUG_SQAN_STREAM") )
    {
        dbL_v7 = dbL_v7 + 0x0040;
    }
    if ( debugList.contains("DEBUG_SQAN_SOLNET") )
    {
        dbL_v8 = dbL_v8 + 0x0080;
    }
    if ( debugList.contains("DEBUG_SQAN_METRICS") )
    {
        dbL_v9 = dbL_v9 + 0x0100;
    }
    if ( debugList.contains("DEBUG_SQAN_LINK") )
    {
        dbL_v10 = dbL_v10 + 0x0200;
    }
    if ( debugList.contains("DEBUG_SQAN_PRODUCT") )
    {
        dbL_v11 = dbL_v11 + 0x0400;
    }
    */

    debugLevel = dbL_v1 | dbL_v2;
    //JR| dbL_v3 | dbL_v4 | dbL_v5 | dbL_v6 | dbL_v7 | dbL_v8 | dbL_v9 | dbL_v10 | dbL_v11;

    close();
}

uint16_t debugLv::getDebugLevel()
{
    return debugLevel;
}

void debugLv::on_cancelPushButton_clicked()
{

}

void debugLv::on_checkAllPushButton_clicked()
{
    // Get All Selected Items and Enable Check
    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        item->setCheckState(Qt::Checked);
    }
}
