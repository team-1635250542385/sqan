#ifndef GstPlayer_H
#define GstPlayer_H

#include <QThread>
#include <QByteArray>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/gstclock.h>


class GstPlayer : public QObject
{
        Q_OBJECT
public:
    GstPlayer(uint8_t testType, uint16_t videoType, uint16_t pixelFormat);
    ~GstPlayer();

    bool isPlayerRunning() { return isPlaying; }

    void startReceiving();
    void stopReceiving();

public Q_SLOTS:
    void receiveVideoDataInPlayer(const QByteArray& bytes);

private:
    void initGStreamerPipeline();
    static void getBusMessage(GstBus *bus, GstMessage *message, void *thisPtr);

private:
    bool isPlaying = false; // This is for our test app control
    bool is_live = false; // This is a GStreamer setting

    static const uint8_t CAMERA_TEST_TYPE = 30;
    uint8_t videoTestType = 0; //default is pattern test zero
    uint16_t videoFormatType = 0;
    uint16_t videoPixelFormat = 0;

    // playbin flags
    typedef enum
    {
        GST_PLAY_FLAG_VIDEO         = (1 << 0), /* We want video output */
        GST_PLAY_FLAG_AUDIO         = (1 << 1), /* We want audio output */
        GST_PLAY_FLAG_TEXT          = (1 << 2)  /* We want subtitle output */
    } GstPlayFlags;

    GstElement *pipeline = nullptr;
    GstBus *bus = nullptr;

    GstElement *source = nullptr;
    GstElement *videoQueue2 = nullptr;
    GstElement *rawVideoParse = nullptr;
    GstElement *videoConvert = nullptr;
    GstElement *sink = nullptr;

    GstClockTime timestamp = 0;

    int frameSize = 0; //for YV12 format
    QByteArray *frameData;
};

#endif
