#ifndef GstSender_H
#define GstSender_H

#include <QThread>
#include <QByteArray>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>


class GstSender : public QObject
{
        Q_OBJECT
public:
    GstSender(uint8_t testType, uint16_t videoType, uint16_t pixelFormat);
    ~GstSender();

    bool isSenderRunning() { return isSending; }

    void startSending();
    void stopSending();

    const static uint16_t PIXEL_FORMAT_YV12 = 0x0032;
    const static uint16_t PIXEL_FORMAT_Y = 0x0001;
    const static uint16_t PIXEL_FORMAT_NV21 = 0x0002;
    const static uint16_t PIXEL_FORMAT_YUY2 = 0x0003;
    const static uint16_t PIXEL_FORMAT_NV16 = 0x0004;
    const static uint16_t PIXEL_FORMAT_RGBA_4444 = 0x0020;
    const static uint16_t PIXEL_FORMAT_RGBA_8888 = 0x0021;
    const static uint16_t PIXEL_FORMAT_RGBA_5551 = 0x0022;
    const static uint16_t PIXEL_FORMAT_RGBX_8888 = 0x0023;
    const static uint16_t PIXEL_FORMAT_RGB_332 = 0x0030;
    const static uint16_t PIXEL_FORMAT_RGB_565 = 0x0031;
    const static uint16_t PIXEL_FORMAT_RGB_888 = 0x0032;

Q_SIGNALS:
    void videoFromSourceAvailable(const QByteArray& sourceData);

private:
    void initGStreamerPipeline();
    static void getBusMessage(GstBus *bus, GstMessage *message, void *thisPtr);
    static GstPadProbeReturn probePipeline (GstPad *pad, GstPadProbeInfo *info, gpointer userData);

private:
    bool isSending = false;
    static const uint8_t CAMERA_TEST_TYPE = 30;
    uint8_t videoTestType = 0; //default is pattern test zero
    uint16_t videoFormatType = 0;
    uint16_t videoPixelFormat = 0;

    GstElement *pipeline = nullptr;
    GstBus *bus = nullptr;

    GstAppSrc *source = nullptr;
    GstElement *videoQueue = nullptr;
    GstElement *videoConvert = nullptr;
    GstElement *videoScale = nullptr;
    GstElement *capsFilter2 = nullptr;
    GstElement *videoRate = nullptr;
    GstElement *videoQueue2 = nullptr;
    GstElement *sink = nullptr;
    GstPad *pad = nullptr;
};

#endif
