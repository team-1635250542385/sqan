#ifndef VIDEOGENERATOR_H
#define VIDEOGENERATOR_H

#include <QDialog>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QGridLayout>
#include "sqantesttool.h"
#include "gstsender.h"
#include "gstplayer.h"

namespace Ui {
class videoGenerator;
}

class sqanTestTool;

class videoGenerator : public QDialog
{
    Q_OBJECT

public:
    explicit videoGenerator(sqanTestTool *testToolPtr, void *userData, QWidget *parent = nullptr);
    ~videoGenerator(void);

    bool isTestRunning() { return isRunning; }
    QButtonGroup* buttonGroup = nullptr;

Q_SIGNALS:
    void videoFramesAvailable(const QByteArray& data);

public Q_SLOTS:
    void sendVideoToRadio(const QByteArray& sourceData);
    static void receiveVideoDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr);

private slots:
    void startVideoTest();
    void stopVideoTest();
    void onGroupButtonClicked(int id);

private:
    QGroupBox *createVideoTestGroup(void);
    void displayMessage(QString string);

private:
    Ui::videoGenerator *ui;
    QPushButton *startTestPushButton;
    QPushButton *stopTestPushButton;

    sqanTestTool *sqanToolPtr = nullptr;
    bool isRunning = false;

    struct CommonButtonParameters {
        QModelIndex cindex;
        QString source = "";
        QString destination = "";
        uint8_t sourceIndex = 0xFF;
        uint8_t destIndex = 0xFF;
        uint8_t peerIndex = 0xFF;
        uint8_t flowId = 0xFF;
        uint8_t endpointDistribution = 0xFF;
    };

    // Used to pass params to another thread
    struct SendDataParams {
        CommonButtonParameters *cbp;
        SqanSolNet::sqanSolNetServiceEntry *entry;
    };
    SendDataParams *dataParams = nullptr;

    static const int START_BUTTON_ID = 1;
    static const int STOP_BUTTON_ID = 2;

    QGridLayout *grid = nullptr;
    QGroupBox *videoTestsGroupBox = nullptr;
    QRadioButton *cameraButton = nullptr;
    QRadioButton *pattern0Button = nullptr;
    QRadioButton *pattern1Button = nullptr;
    QRadioButton *pattern2Button = nullptr;
    QRadioButton *pattern3Button = nullptr;
    QRadioButton *pattern4Button = nullptr;
    QRadioButton *pattern5Button = nullptr;
    QRadioButton *pattern6Button = nullptr;
    QRadioButton *pattern7Button = nullptr;
    QRadioButton *pattern8Button = nullptr;
    QRadioButton *pattern9Button = nullptr;
    QRadioButton *pattern10Button = nullptr;
    QRadioButton *pattern11Button = nullptr;
    QRadioButton *pattern12Button = nullptr;
    QRadioButton *pattern13Button = nullptr;
    QRadioButton *pattern14Button = nullptr;
    QRadioButton *pattern15Button = nullptr;
    QRadioButton *pattern16Button = nullptr;
    QRadioButton *pattern17Button = nullptr;
    QRadioButton *pattern18Button = nullptr;
    QRadioButton *pattern19Button = nullptr;
    QRadioButton *pattern20Button = nullptr;
    QRadioButton *pattern21Button = nullptr;
    QRadioButton *pattern22Button = nullptr;
    QRadioButton *pattern23Button = nullptr;
    QRadioButton *pattern24Button = nullptr;

    GstSender *gstSender = nullptr;
    QFuture<void> gstSenderThread;
    GstPlayer *gstPlayer = nullptr;
    QFuture<void> gstPlayerThread;

    // These are used to stop the data flow down in SqanInterface
    SqanInterface *sqanIntrfcSrcPtr= nullptr;
    SqanSolNet *sqanSolNetSrcPtr= nullptr;
    SqanInterface *sqanIntrfcDstPtr= nullptr;
    uint8_t peerIndex;
    uint8_t endpointId;
    uint8_t dataflowId;
};

#endif // VIDEOGENERATOR_H
