#include "squadleadermodedialog.h"
#include "ui_squadleadermodedialog.h"
#include <QDebug>

SquadLeaderModeDialog::SquadLeaderModeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SquadLeaderModeDialog)
{
    ui->setupUi(this);
}

SquadLeaderModeDialog::~SquadLeaderModeDialog()
{
    delete ui;
}

void SquadLeaderModeDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if((QPushButton *)button== ui->buttonBox->button(QDialogButtonBox::Ok) )
    {
        //qDebug() << getPhyRateCap();
    }
}

uint8_t SquadLeaderModeDialog::getRssiConnect()
{
    uint8_t rssiConnect = ui->rssiConnectLineEdit->text().toUInt();
    return (rssiConnect);
}

uint8_t SquadLeaderModeDialog::getRssiDisconnect()
{
    uint8_t rssiDisconnect = ui->rssiDisconnectLineEdit->text().toUInt();
    return (rssiDisconnect);
}

uint8_t SquadLeaderModeDialog::getPhyRateCapIndex()
{
    uint8_t phyRateCapIndex = ui->phyRateCapComboBox->currentIndex();
    return (phyRateCapIndex);
}
