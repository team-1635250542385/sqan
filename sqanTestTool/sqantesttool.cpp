﻿//###############################################
// Filename: sqantesttool.cpp
// Description: SQAN Independent Verification and Validation (iV&V) Kit Application
//###############################################

#include "sqantesttool.h"
#include "ui_sqantesttool.h"

// C++ includes
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <string.h>
#include <new>
/*JR
#include <node.h>
#include <edge.h>
*/
#include <vector>
#include <array>
#include <sstream>

// Qt includes
#include <qobjectdefs.h>
#include <QStringListModel>
#include <QString>
#include <QMessageBox>
#include <QComboBox>
#include <QGraphicsItem>
#include <QGraphicsSimpleTextItem>
#include <QList>
#include <QDebug>
#include <QListIterator>
#include <QTextStream>
#include <QPoint>
#include <QMenu>
#include <QAction>
#include <QTimer>
#include <QtConcurrent/QtConcurrent>
#include <QStringList>
#include <iomanip>
#include <QTableWidgetItem>
#include <QTableWidget>
#include <QStringList>
#include <QMap>
#include <QMapIterator>
#include <QStringRef>
#include <QVector>
#include <QProcess>
#include <QProgressBar>
#include <QFuture>
#include <QFutureWatcher>
#include <QThread>
#include <QGraphicsProxyWidget>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QDataStream>
#include <QXmlSimpleReader>
#include <QFile>
#include <QMediaMetaData>
#include <QVideoWidget>
#include <QDateTime>
#include <QElapsedTimer>
#include <QMediaContent>
#include <QProcess>
#include <QtConcurrent>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QFileDialog>

// Project includes
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../SqanInterface/SqanInterfaceInit.h"
#include "../SqanInterface/SqanInterface.h"
//JR #include "plotchart.h"
#include "debuglv.h"
/*JR
#include "dmm.h"
#include "squadleadermodedialog.h"
#include "servicechooser.h"
*/

QT_CHARTS_USE_NAMESPACE

// Temp use these until shutdown notification done
bool loggerDone = false;
bool dbLoggerDone = false;

//! sqanTestTool Constructor
sqanTestTool::sqanTestTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::sqanTestTool)
{
    ui->setupUi(this);

    kitVersion = "0.0.1";
    this->setWindowTitle("Squad Area Network (SQAN) iV&V Kit - " + kitVersion);

    // Logger Debug Level
    debugLv *debugLvl = new debugLv();
    debugLvl->exec();
    debugLevel = debugLvl->getDebugLevel();

    // Create Logger
    theLogger = new Logger(loggerFilename, debugLevel);
    // Start the Logger dequeue thread
    struct loggerArgStruct args1;
    args1.logger = theLogger;
    args1.sqanToolPtr = this;
    if (pthread_create(&threadLoggerDequeue, NULL, &dequeue_messages, (void *)&args1) != 0)
    {
        qDebug() << "Logger dequeue thread didn't start!";
    }
    else
    {
        qDebug() << "Logger dequeue thread started";
    }

    // Create DBLogger
    dbLogger = new DBLogger(dbLogFilename, kitVersion);
    // Start the DB dequeue thread
    struct dbLoggerArgStruct args2;
    args2.dbLogger = dbLogger;
    args2.sqanToolPtr = this;
    if (pthread_create(&threadDbLoggerdequeue, NULL, &dequeue_dbmessages, (void *)&args2) != 0)
    {
        qDebug() << "thread DB Logger dequeue didn't start!";
    }
    else
    {
        qDebug() << "thread DB Logger dequeue started";
    }

    // Initialize UWB Devices
    sqanInterfaceInit = new SqanInterfaceInit(SqanInterface::USB, theLogger, dbLogger);

    if ( sqanInterfaceInit != nullptr )
    {
        sqanInterfaceArray = sqanInterfaceInit->InitSqanInterfaceArray();
        usbInterfaceInit = sqanInterfaceInit->GetUsbInterfaceInit();

        // If there are SQAN devices - proceed
        if ( usbInterfaceInit->sqanDeviceCount > 0 )
        {
            brush = Qt::black;
            outlinePen.setColor(Qt::black);

            mcastColor = Qt::red;
            nodeColor = Qt::gray;
            coordColor = Qt::green;

            // Used for firmware download
            filename_firmware = "";

            updateFirmwareTabWidgetLocation = 6;
            // Update Firmware Tab Disabled
            ui->tabWidget->setTabEnabled(updateFirmwareTabWidgetLocation,false);
            ui->tabWidget->setTabEnabled(6, false);

            // Initialize DynArray Structure
            #define DYN_ARRAY_SIZE 10
            DynArray =  new (std::nothrow) SqanAttr[DYN_ARRAY_SIZE];

            // Create Network Topology Graphics Scene
            netTopologyScene = new QGraphicsScene(this);
            ui->netTopologyGraphicsView->setScene(netTopologyScene);

            // Initialize Network Device List
            initTestTool();

            // Create Context Menu For Each Device
            ui->netDeviceList->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(ui->netDeviceList, SIGNAL(customContextMenuRequested(QPoint)),
                    this, SLOT(showNetDeviceListContextMenu(QPoint)));

            // Create Context Menu For Descriptor Plans
            ui->descPlansListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(ui->descPlansListWidget, SIGNAL(customContextMenuRequested(QPoint)),
                this, SLOT(showDescriptorPlansListContextMenu(QPoint)));

            // Format push buttons with Dark Blue text like HTML - all the same for consistancy
            ui->scanButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
            ui->clearServiceDescriptorTextPushButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
            ui->saveServiceDescriptorTextPushButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
            ui->savePowerPlotPushButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

            // Establish local database schema
//            openDatabase();
            if ( dbConnected )
            {
                QSqlQuery qryTbDescriptorPlansExists("SELECT EXISTS ( "
                                                 "SELECT * FROM information_schema.tables "
                                                 "WHERE table_schema = 'public' "
                                                 "AND table_name = 'tb_descriptor_plans' )");
                if ( qryTbDescriptorPlansExists.lastError().isValid() ) {
                    qDebug() << " " << qryTbDescriptorPlansExists.lastQuery();
                    qDebug() << " " << qryTbDescriptorPlansExists.lastError().databaseText();
                } else if ( qryTbDescriptorPlansExists.next() ) {
                    if ( !qryTbDescriptorPlansExists.value(0).toBool() ) {
                        qDebug() << "Local DB: tb_descriptor_plans does NOT exist!";
                        QSqlQuery qryCreateTbDescriptorPlans("CREATE TABLE tb_descriptor_plans "
                                                           "( "
                                                           "ts text NOT NULL, "
                                                           "device_type text NOT NULL, "
                                                           "service_name text NOT NULL, "
                                                           "ord integer NOT NULL, "
                                                           "property_type text, "
                                                           "property_name text, "
                                                           "field_name text, "
                                                           "field_value text, "
                                                           "CONSTRAINT tb_descriptor_plans_pk PRIMARY KEY (ts, device_type, service_name, ord) "
                                                           ")");
                        if ( qryCreateTbDescriptorPlans.lastError().isValid() ) {
                            qDebug() << " " << qryCreateTbDescriptorPlans.lastQuery();
                            qDebug() << " " << qryCreateTbDescriptorPlans.lastError().databaseText();
                        } else {
                            qDebug() << "Local DB: tb_descriptor_plans has been created";
                        }
                    } else {
        //	                qDebug() << "Local DB: tb_descriptor_plans already exists";
                    }
                }
//                closeDatabase();
            }

            // Aggregate Network Statistics Data Plot
            //JR generateNetworkStatisticsDataPlot();
        }
    }
}

//! sqanTestTool Destructor
//TODO: ADD MORE OBJECTS TO CLOSE
sqanTestTool::~sqanTestTool()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    stopAllThreads();

    if ( usbInterfaceInit->sqanDeviceCount > 0)
    {
        // Shutdown the SQAN library
        sqanInterfaceInit->ExitSqanInterfaceArray();

        // Stop the hotplug thread first
        checkingHotplug = false;
        if ( hotplugTimer != nullptr )
        {
            hotplugTimer->stop();
            delete(hotplugTimer);
            hotplugTimer = nullptr;
        }

        // Stop the timer and delete it
        if ( data_timer != nullptr)
        {
            data_timer->stop();
            delete data_timer;
            data_timer = nullptr;
        }

        // Stop chart plotting and delete
/*JR        if ( dataPlotChart != nullptr )
        {
            dataPlotChart->stopPlot();
            delete dataPlotChart;
        }
*/
        // Delete the plot chart widgets
        if ( dataPlotScene != nullptr )
            delete dataPlotScene;

/*JR        if ( dataPlotChartView != nullptr )
            delete dataPlotChartView;
*/
        if ( deviceEndpointIdComboBox != nullptr )
            delete(deviceEndpointIdComboBox);

        if ( deviceEndpointDistributionComboBox != nullptr )
            delete(deviceEndpointDistributionComboBox);

        if ( deviceEndpointDistributionComboBox != nullptr )
            delete(deviceEndpointDataPoliciesComboBox);

        if ( deviceNameLineEdit != nullptr )
            delete(deviceNameLineEdit);

        if ( deviceSerialNumberLineEdit != nullptr )
            delete(deviceSerialNumberLineEdit);

        if ( deviceManufacturerLineEdit != nullptr )
            delete(deviceManufacturerLineEdit);

        if ( deviceFriendlyNameLineEdit != nullptr )
            delete(deviceFriendlyNameLineEdit);

        if ( videoEndpointIdComboBox != nullptr )
            delete(videoEndpointIdComboBox);

        if ( videoEndpointDistributionComboBox != nullptr )
            delete(videoEndpointDistributionComboBox);

        if ( videoEndpointDataPoliciesComboBox != nullptr )
            delete(videoEndpointDataPoliciesComboBox);

        if ( videoFormatResolutionXServiceDescriptorLineEdit != nullptr )
            delete(videoFormatResolutionXServiceDescriptorLineEdit);

        if ( videoFormatResolutionYServiceDescriptorLineEdit != nullptr )
            delete(videoFormatResolutionYServiceDescriptorLineEdit);

        if ( videoFormatFrameRateDescriptorLineEdit != nullptr )
            delete(videoFormatFrameRateDescriptorLineEdit);

        if ( videoFormatBitDepthDescriptorLineEdit != nullptr )
            delete(videoFormatBitDepthDescriptorLineEdit);

        if ( videoFormatPixelFormatDescriptorComboBox != nullptr )
            delete(videoFormatPixelFormatDescriptorComboBox);

        if ( videoProtocolRawDescriptorComboBox != nullptr )
            delete(videoProtocolRawDescriptorComboBox);

        if ( videoIfovDescriptorLineEdit != nullptr )
            delete(videoIfovDescriptorLineEdit);

        if ( videoPPointXLineEdit != nullptr )
            delete(videoPPointXLineEdit);

        if ( videoPPointYLineEdit != nullptr )
            delete(videoPPointYLineEdit);

        if ( videoLabelLineEdit != nullptr )
            delete(videoLabelLineEdit);

        if ( videoLensDistortionCoefficientsLineEdit != nullptr )
            delete(videoLensDistortionCoefficientsLineEdit);

        if ( videoControlBitmapLineEdit != nullptr )
            delete(videoControlBitmapLineEdit);

        if ( videoPolarityComboBox != nullptr )
            delete(videoPolarityComboBox);

        if ( videoImageCalibrationComboBox != nullptr )
            delete(videoImageCalibrationComboBox);

        if ( rtaEndpointIdComboBox != nullptr )
            delete(rtaEndpointIdComboBox);

        if ( rtaEndpointDistributionComboBox != nullptr )
            delete(rtaEndpointDistributionComboBox);

        if ( rtaEndpointDataPoliciesComboBox != nullptr )
            delete(rtaEndpointDataPoliciesComboBox);

        if ( rtaImuRateLineEdit != nullptr )
            delete(rtaImuRateLineEdit);

        if ( rtaLabelLineEdit != nullptr )
            delete(rtaLabelLineEdit);

        if ( rtaEnabledComboBox != nullptr )
            delete(rtaEnabledComboBox);

        if ( rtaModeComboBox != nullptr )
        delete(rtaModeComboBox);

        if ( rtaPolarityComboBox != nullptr )
            delete(rtaPolarityComboBox);

        if ( rtaZoomComboBox != nullptr )
            delete(rtaZoomComboBox);

        if ( rtaBubbleComboBox != nullptr )
            delete(rtaBubbleComboBox);

        if ( rtaManualAlignmentComboBox != nullptr )
            delete(rtaManualAlignmentComboBox);

        if ( rtaReticleInVideoComboBox != nullptr )
            delete(rtaReticleInVideoComboBox);

        if ( rtaReticleTypeComboBox != nullptr )
            delete(rtaReticleTypeComboBox);

        if ( rtaQuaternionReal != nullptr )
            delete(rtaQuaternionReal);

        if ( rtaQuaternionI != nullptr )
            delete(rtaQuaternionI);

        if ( rtaQuaternionJ != nullptr )
            delete(rtaQuaternionJ);

        if ( rtaQuaternionK != nullptr )
            delete(rtaQuaternionK);

        if ( packetGenEndpointIdComboBox != nullptr )
            delete(packetGenEndpointIdComboBox);

        if ( packetGenEndpointDistributionComboBox != nullptr )
            delete(packetGenEndpointDistributionComboBox);

        if ( packetGenEndpointDataPoliciesComboBox != nullptr )
            delete(packetGenEndpointDataPoliciesComboBox);

        if ( statusEndpointIdComboBox != nullptr )
            delete(statusEndpointIdComboBox);

        if ( statusEndpointDistributionComboBox != nullptr )
            delete(statusEndpointDistributionComboBox);

        if ( statusEndpointDataPoliciesComboBox != nullptr )
            delete(statusEndpointDataPoliciesComboBox);

        if ( statusLabelLineEdit != nullptr )
            delete(statusLabelLineEdit);

        if ( batteryLevelLineEdit != nullptr )
            delete(batteryLevelLineEdit);

        if ( batteryStateComboBox != nullptr )
            delete(batteryStateComboBox);

        if ( batteryChargingComboBox != nullptr )
            delete(batteryChargingComboBox);

        if ( deviceStateComboBox != nullptr )
            delete(deviceStateComboBox);

        if ( displayStateComboBox != nullptr )
            delete(displayStateComboBox);

        if ( weaponSightModeComboBox != nullptr )
            delete(weaponSightModeComboBox);

        if ( weaponActiveReticleIDComboBox != nullptr )
            delete(weaponActiveReticleIDComboBox);

        if ( weaponSymbologyStateComboBox != nullptr )
            delete(weaponSymbologyStateComboBox);

        if ( simpleUiEndpointIdComboBox != nullptr )
            delete(simpleUiEndpointIdComboBox);

        if ( simpleUiEndpointDistributionComboBox != nullptr )
            delete(simpleUiEndpointDistributionComboBox);

        if ( simpleUiEndpointDataPoliciesComboBox != nullptr )
            delete(simpleUiEndpointDataPoliciesComboBox);

        if ( simpleUiTerminalIdLineEdit != nullptr )
            delete(simpleUiTerminalIdLineEdit);

        if ( simpleUiCapabilitiesLineEdit != nullptr )
            delete(simpleUiCapabilitiesLineEdit);

        if ( simpleUiButtonCountComboBox != nullptr )
            delete(simpleUiButtonCountComboBox);

        if ( simpleUiStateCountComboBox != nullptr )
            delete(simpleUiStateCountComboBox);

        if ( simpleUiLabelLineEdit != nullptr )
            delete(simpleUiLabelLineEdit);

        if ( packetGenLabelLineEdit != nullptr )
            delete(packetGenLabelLineEdit);

        if ( arOverlayEndpointIdComboBox != nullptr )
            delete(arOverlayEndpointIdComboBox);

        if ( arOverlayEndpointDistributionComboBox != nullptr )
            delete(arOverlayEndpointDistributionComboBox);

        if ( arOverlayEndpointDataPoliciesComboBox != nullptr )
            delete(arOverlayEndpointDataPoliciesComboBox);

        if ( arOverlayLabelLineEdit != nullptr )
            delete(arOverlayLabelLineEdit);

        if ( arOverlayMinWidthLineEdit != nullptr )
            delete(arOverlayMinWidthLineEdit);

        if ( arOverlayMinHeightLineEdit != nullptr )
            delete(arOverlayMinHeightLineEdit);

        if ( arOverlayRleVersionsLineEdit != nullptr )
            delete(arOverlayRleVersionsLineEdit);

        if ( lrfEndpointIdComboBox != nullptr )
            delete(lrfEndpointIdComboBox);

        if ( lrfEndpointDistributionComboBox != nullptr )
            delete(lrfEndpointDistributionComboBox);

        if ( lrfEndpointDataPoliciesComboBox != nullptr )
            delete(lrfEndpointDataPoliciesComboBox);

        if ( lrfLabelLineEdit != nullptr )
            delete(lrfLabelLineEdit);

        if ( lrfControlEndpointIdComboBox != nullptr )
            delete(lrfControlEndpointIdComboBox);

        if ( lrfControlEndpointDistributionComboBox != nullptr )
            delete(lrfControlEndpointDistributionComboBox);

        if ( lrfControlEndpointDataPoliciesComboBox != nullptr )
            delete(lrfControlEndpointDataPoliciesComboBox);

        if ( lrfControlLabelLineEdit != nullptr )
            delete(lrfControlLabelLineEdit);

        /* JR
        if ( dmmV != nullptr )
        {
            dmmV->disconnect();
            delete dmmV;
        }

        if ( dmmI != nullptr )
        {
            dmmI->disconnect();
            delete dmmI;
        }
        */
        clearDevices();
        delete[] DynArray;
    }

    delete(theLogger);
    delete(dbLogger);

    // Delete this last
    if ( sqanInterfaceInit != nullptr )
        delete(sqanInterfaceInit);

    delete ui;
}


/*****************************************************
 * Logging
 * **************************************************/
void* sqanTestTool::dequeue_messages(void *arguments)
{
    struct loggerArgStruct *args = (struct loggerArgStruct *)arguments;
    Logger *theLogger = args->logger;

    while ( loggerDone == false )
    {
        // If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);  // 100 millisecs
    }
}

void* sqanTestTool::dequeue_dbmessages(void *arguments)
{
    struct dbLoggerArgStruct *args = (struct dbLoggerArgStruct *)arguments;
    DBLogger *dbLogger = args->dbLogger;

    while ( dbLoggerDone == false )
    {
        // If items on queue - dequeue and print
        dbLogger->deQueue();
        usleep(100);  // 100 millisecs
    }
}

void sqanTestTool::dbLogDevice(int selected)
{
    DBLogRecord dblr(tbDevices, dbLogger->updateEpochTime(), dbLogger->getUIIndex());
    dynArrayLock.lock();
    if (DynArray[selected].isReady)
    {
        dblr.addDevicesObservation(selected
                                   ,DynArray[selected].devName
                                   ,DynArray[selected].macAddr
                                   ,DynArray[selected].firmwareVer
                                   ,DynArray[selected].netId
                                   ,DynArray[selected].coordPriority
                                   ,DynArray[selected].role
                                   ,DynArray[selected].channel
                                   ,DynArray[selected].devType
                                   ,DynArray[selected].phy_rate
                                   ,DynArray[selected].multicastGroup.startGroup.mcastAddress
                                   ,DynArray[selected].multicastGroup.startGroup.mcastPeerIndex
                                   ,DynArray[selected].multicastGroup.joinGroup.mcastAddress
                                   ,DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex);
    }
    dynArrayLock.unlock();
    dbLogger->dbLog(dblr);
}

void sqanTestTool::dbLogDevices()
{
    DBLogRecord dblr(tbDevices, dbLogger->updateEpochTime(), dbLogger->getUIIndex());
    dynArrayLock.lock();
    for (int i=0; i<DYN_ARRAY_SIZE; i++)
    {
        if (!DynArray[i].isReady) { continue; }
        dblr.addDevicesObservation(i
                                   ,DynArray[i].devName
                                   ,DynArray[i].macAddr
                                   ,DynArray[i].firmwareVer
                                   ,DynArray[i].netId
                                   ,DynArray[i].coordPriority
                                   ,DynArray[i].role
                                   ,DynArray[i].channel
                                   ,DynArray[i].devType
                                   ,DynArray[i].phy_rate
                                   ,DynArray[i].multicastGroup.startGroup.mcastAddress
                                   ,DynArray[i].multicastGroup.startGroup.mcastPeerIndex
                                   ,DynArray[i].multicastGroup.joinGroup.mcastAddress
                                   ,DynArray[i].multicastGroup.joinGroup.mcastPeerIndex);
    }
    dynArrayLock.unlock();
    dbLogger->dbLog(dblr);
}

/*JRvoid sqanTestTool::dbLogService(int selected, SqanSolNet::sqanSolNetServiceEntry *serviceEntry) // #WIP
{
    DBLogRecord dblr(tbServices, dbLogger->updateEpochTime(), dbLogger->getUIIndex());
    std::stringstream sdss;
    std::stringstream cdss;
    std::stringstream pss;
    std::stringstream vss;

    if (serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Control)
    {
        sdss    << std::to_string(serviceEntry->serviceControlDesc.serviceSelector)
                << "," << std::to_string(serviceEntry->serviceControlDesc.protocolId)
                << "," << std::to_string(serviceEntry->serviceControlDesc.reserved)
                << "," << SqanSolNet::GetServiceIdString(serviceEntry->serviceControlDesc.header.descriptorId).c_str();

        if ( serviceEntry->serviceControlDesc.childBlock )
        {
            uint16_t childblockLength = serviceEntry->serviceControlDesc.header.length - (SqanSolNet::selectorSize +
                                                                                         sizeof(serviceEntry->serviceControlDesc.protocolId) +
                                                                                         sizeof(serviceEntry->serviceControlDesc.reserved));
            uint16_t offset = 0;

            while ( offset < childblockLength )
            {
                SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = (SqanSolNet::sqanSolNetDescCommonHeader *)&serviceEntry->serviceControlDesc.childBlock[offset];
                if ( commonHeader->descriptorId )
                {
                    uint8_t *data = &serviceEntry->serviceControlDesc.childBlock[offset];
                    if (cdss.rdbuf()->in_avail())
                    {
                        cdss << ",";
                        pss << ",";
                        vss << ",";
                    }
                    SqanSolNet::PrintDescriptorId(&cdss, &pss, &vss, commonHeader->descriptorId, data);
                    offset += commonHeader->length + SqanSolNet::commonHeaderSize;
                }
                else
                {
                    break;
                }
            }
        }
    } else {
        sdss    << std::to_string(serviceEntry->serviceDesc.serviceSelector)
                << "," // placeholder for sqanSolNetServiceControlDesc.protocolId
                << "," // placeholder for sqanSolNetServiceControlDesc.reserved
                << "," << SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId).c_str();

        if (serviceEntry->serviceDesc.childBlock)
        {
            uint16_t childblockLength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
            uint16_t offset = 0;

            while (offset < childblockLength )
            {
                SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = (SqanSolNet::sqanSolNetDescCommonHeader *)&serviceEntry->serviceDesc.childBlock[offset];
                if ( commonHeader->descriptorId )
                {
                    uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
                    if (cdss.rdbuf()->in_avail())
                    {
                        cdss << ",";
                        pss << ",";
                        vss << ",";
                    }
                    SqanSolNet::PrintDescriptorId(&cdss, &pss, &vss, commonHeader->descriptorId, data);
//                        SqanSolNet::PrintDescriptorId(&dbo.oss, &pss, &vss, commonHeader->descriptorId, data);
                    offset += commonHeader->length + SqanSolNet::commonHeaderSize;
                }
                else
                {
                    break;
                }
            }
        }
    }

    dblr.addServicesObservation(selected
                                ,serviceEntry->dataflowId
                                ,serviceEntry->endpointId
                                ,serviceEntry->endpointDistribution
                                ,serviceEntry->dataPolicies
                                ,&sdss
                                ,&cdss
                                ,&pss
                                ,&vss);
    dbLogger->dbLog(dblr);
}
*/
/********************************************
 * sqanTestTool startup/shutdown
 *******************************************/

void sqanTestTool::initTestTool()
{
    // Sleep for 1 second
    sleep(1);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->resetUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    // Clears the GUI of devices
    clearDevices();

    // Service Configuration tab setup
    createServiceConfigurationTable();

    // Table on Data Traffic tab
    createAvailableLinksTable();

    // Scan for Available Devices and update the GUI
    scanDevices();

    // Create the registerServicesTable for sources to register with dest SolNet services
    createRegisterServicesTable();

    // Start a timer to check for hotplug events
    hotplugTimer = new QTimer(this);
    connect(hotplugTimer, SIGNAL(timeout()), this, SLOT(checkHotplugEvents()));
    hotplugTimer->start(5000);
    checkingHotplug = true;

    dbLogDevices();
}

void sqanTestTool::stopAllThreads()
{
    // Stop the logger threads
    loggerDone = true;
    usleep(100);
    dbLoggerDone = true;
    usleep(100);

    if ( threadLoggerDequeue != 0 )
    {
        pthread_join(threadLoggerDequeue, nullptr);
    }

    if ( threadDbLoggerdequeue != 0 )
    {
        pthread_join(threadDbLoggerdequeue, nullptr);
    }
}

/*****************************************************
 * Network Configuration
 * **************************************************/

//! Clears the GUI from devices
void sqanTestTool::clearDevices()
{
    //Clear Source Map
    sourceMap.clear();

    //Clear Service Map
    serviceList.clear();

    //Clear all edges first
    clearEdges();

    // Clean up DynArray before reloading
    clearDynArray();
}

void sqanTestTool::scanDevice(uint8_t selected, int posX, int posY)
{
    // The decision is made outside this function whether to
    // call scanDevice based if the device is new or needs to be updated
    DynArray[selected].attributesLock.lock();

    //Get Friend Classes
  /*JR  DynArray[selected].sqanIdentity = DynArray[selected].sqanInterface->GetSqanIdentity();
    DynArray[selected].sqanFirmware = DynArray[selected].sqanInterface->GetSqanFirmware();
    DynArray[selected].sqanStream = DynArray[selected].sqanInterface->GetSqanStream();
    DynArray[selected].sqanSystem = DynArray[selected].sqanInterface->GetSqanSystem();
    DynArray[selected].sqanAssociation = DynArray[selected].sqanInterface->GetSqanAssociation();
    DynArray[selected].sqanLink = DynArray[selected].sqanInterface->GetSqanLink();
    DynArray[selected].sqanMetrics = DynArray[selected].sqanInterface->GetSqanMetrics();
    DynArray[selected].sqanProduct = DynArray[selected].sqanInterface->GetSqanProduct();
    DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

    DynArray[selected].sqanStream->InitSqanPeerRecords();
    DynArray[selected].sqanStream->InitSqanMulticastRecords();

    // Ask the firmware for these things in case we didn't get them yet
    DynArray[selected].sqanIdentity->SendGetDeviceTypeCmd();
    DynArray[selected].sqanStream->SendGetStreamsStatusCmd();
    DynArray[selected].sqanFirmware->SendGetFirmwareVersionCmd(1);
    DynArray[selected].sqanProduct->SendGetNetworkIdCmd();
    DynArray[selected].sqanIdentity->SendGetMacAddressCmd();
    DynArray[selected].sqanIdentity->SendGetCoordinatorCmd();
    DynArray[selected].sqanMetrics->SendGetWirelessMetrics();

    //Store SQAN Attributes For Each Device
    DynArray[selected].macAddr = QString::fromStdString(DynArray[selected].sqanIdentity->GetMacAddressStr());
    DynArray[selected].firmwareVer = QString::fromStdString(DynArray[selected].sqanFirmware->GetActiveFirmwareVersion());
    DynArray[selected].devType = DynArray[selected].sqanIdentity->GetSqanDeviceType();
    DynArray[selected].role = DynArray[selected].sqanStream->GetSqanRole();
    DynArray[selected].channel = DynArray[selected].sqanStream->GetSqanChannel();
    DynArray[selected].coordPriority = DynArray[selected].sqanIdentity->GetCoordinatorPrecedence();
    DynArray[selected].netId = DynArray[selected].sqanProduct->GetSqanNetworkId();
    DynArray[selected].phy_rate = DynArray[selected].sqanMetrics->GetPhyRate();
*/
    QString devName = QString::number(selected) + indexDelim + findNameFromDeviceType(DynArray[selected].devType);

    // Remove the characters after the ':' in the name found
    int startPosition = devName.indexOf(':');
    int numRemainingChars = devName.size() - startPosition;
    devName.remove(startPosition, numRemainingChars);
    DynArray[selected].devName = devName;

    sourceMap.remove(DynArray[selected].devName);
    sourceMap.insert(DynArray[selected].devName, selected);

    // Add Device to Device List/Combo Box
    ui->netDeviceList->takeItem(selected);
    ui->netDeviceList->insertItem(selected, DynArray[selected].devName);
    ui->netDeviceList->update();

    // Add Device to test automation list
    ui->testAutomationDeviceList->takeItem(selected);
    ui->testAutomationDeviceList->insertItem(selected, DynArray[selected].devName);
    ui->testAutomationDeviceList->update();

    ui->deviceComboBox->removeItem(selected);
    ui->deviceComboBox->insertItem(selected,DynArray[selected].devName);
    ui->deviceComboBox->update();

    // Create Node Object and Add to topology the first time we scan this device
 /*JR   DynArray[selected].node = new Node(ui->netTopologyGraphicsView);
    // Add Node to Graphics Scene and Set Position
    netTopologyScene->addItem(DynArray[selected].node);

    label = new QLabel(DynArray[selected].devName+"\n"+DynArray[selected].macAddr.toUpper());
    proxy = new QGraphicsProxyWidget(DynArray[selected].node);
    proxy->setWidget(label);

    DynArray[selected].node->setPos(posX,posY);
    DynArray[selected].node->update();

    // Convert Role Value to Coordinator/Non-Coordinator
    switch(DynArray[selected].role)
    {
        case 0:
            DynArray[selected].node->setNodeColor(coordColor);
            DynArray[selected].node->update();
            break;
        default:
            DynArray[selected].node->setNodeColor(nodeColor);
            DynArray[selected].node->update();
            break;
    }
*/
    // Set this device ready to use
    DynArray[selected].isReady = true;

    DynArray[selected].attributesLock.unlock();

    // Get a copy of the Multicast Group Records
    //JR getMulticastGroup(selected);

    // Get a copy of the peerlist from firmware
    updatePeers(selected, true);

    if ( ui->deviceComboBox->currentIndex() == selected )
    {
        // Update the view of this device
        updateDeviceUiForSelected(selected);
    }
}

//! Scans local machine for Alereon devices
void sqanTestTool::scanDevices()
{
    //Initial Node Position
    int posX = 0;
    int posY = 0;

    //Initialize i (index) to zero
    int i = 0;

    // Clear the edges because we have to redraw them
    // Clear and repaint views that show devices
    clearEdges();

    if ( data_timer != nullptr )
    {
        data_timer->stop();
    }

    dynArrayLock.lock();

    //Add Each Device To the DynArray and update the netDeviceList and peerList widgets
    for (auto iterator = sqanInterfaceArray->begin(); iterator != sqanInterfaceArray->end(); iterator++ )
    {
         if (*iterator != nullptr)
         {
             SqanInterface *sqanInterface = *iterator;

             i = sqanInterface->GetIndex();
             DynArray[i].sqanInterface = sqanInterface;

             scanDevice(i, posX, posY);

             posX += 20;
             posY += 50;
         }
    }

    for (uint8_t i=0; i<MAX_DEVICES; i++)
    {
        // Update everyone's peerlists again
        // Now that all the devices are in
        updatePeers(i, true);
    }

    dynArrayLock.unlock();

    // Now that the DynArray has been created, draw edges between connected nodes
    // Adds each edge to the availableLinksTable
    drawEdges();

    if ( data_timer != nullptr )
    {
        data_timer->start();
    }

    ui->netDeviceList->repaint();
    ui->testAutomationDeviceList->repaint();
    ui->deviceComboBox->repaint();
}

void sqanTestTool::on_scanButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("next", "sqanTestTool::scanDevices()");
    dbLogger->dbLog(dblr);

    //Message to inform user that list is updating
    displayMessage("Please Wait... NetDevice List is Updating");

    //Update NetDevice List
    scanDevices();

    dbLogDevices();

    //Message to inform user that list is complete
    displayMessage("Update Device List Completed");
}

void sqanTestTool::clearDynArrayEntry(uint8_t selected)
{
    DynArray[selected].attributesLock.lock();

    // Set DynArray to Null Pointers
    DynArray[selected].isReady = false;

    // Clear the peerList
    clearSolNetPeerList(selected);

    // Clear the SqanInterface class pointers
    DynArray[selected].sqanInterface = nullptr;
/*JR    DynArray[selected].sqanIdentity = nullptr;
    DynArray[selected].sqanFirmware = nullptr;
    DynArray[selected].sqanStream = nullptr;
    DynArray[selected].sqanAssociation =nullptr;
    DynArray[selected].sqanSystem = nullptr;
    DynArray[selected].sqanMetrics = nullptr;
    DynArray[selected].sqanLink = nullptr;
    DynArray[selected].sqanProduct = nullptr;
    DynArray[selected].sqanSolNet = nullptr;
    DynArray[selected].node = nullptr;
    DynArray[selected].multicastGroup.startGroup.mcastStatus = SqanStream::McastGroupOriginatedAndPaused;
    DynArray[selected].multicastGroup.startGroup.mcastPeerIndex = 0;
    DynArray[selected].multicastGroup.startGroup.mcastAddress = 0;
    DynArray[selected].multicastGroup.startGroup.mcastPhyRate = 0;
    DynArray[selected].multicastGroup.joinGroup.mcastStatus = SqanStream::McastGroupOriginatedAndPaused;
    DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex = 0;
    DynArray[selected].multicastGroup.joinGroup.mcastAddress = 0;
    DynArray[selected].multicastGroup.joinGroup.mcastPhyRate = 0;
*/
    DynArray[selected].attributesLock.unlock();
}

void sqanTestTool::clearDynArray()
{
    // Removes all edges in the DynArray
    // Has the lock

    if ( netTopologyScene != nullptr )
    {
        netTopologyScene->clear();
    }

    // Clear and repaint views that show devices
    dynArrayLock.lock();

    clearDeviceList();
    clearPeerListWidget();
    clearDeviceListComboBox();
    clearRegisterServicesTable();
    clearAvailableLinksTable();

    // Set DynArray to Null Pointers
    for (int i=0; i<DYN_ARRAY_SIZE; i++)
    {
        clearDynArrayEntry(i);
    }

    dynArrayLock.unlock();
}

void sqanTestTool::clearSolNetPeerList(uint8_t selected)
{
    while ( !DynArray[selected].peerList.empty() )
    {
        SolNetPeerListEntry *entry = DynArray[selected].peerList.front();
        if ( entry != nullptr )
        {
           delete (entry);
           DynArray[selected].peerList.pop_front();
        }
    }
}

void sqanTestTool::clearDeviceListComboBox()
{
    // While the number of items in the deviceComboBox List is greater than zero,
    // remove the top item from the list and move the remaining items up
    for (int i=0; i<ui->deviceComboBox->count(); i++)
    {
        ui->deviceComboBox->removeItem(i);
    }
    ui->deviceComboBox->clear();
    ui->deviceComboBox->repaint();
}

void sqanTestTool::clearDeviceList()
{
    // While the number of items in the Net Device List is greater than zero,
    // remove the top item from the list and move the remaining items up
    while (ui->netDeviceList->count() > 0)
    {
        ui->netDeviceList->takeItem(0);
    }
    ui->netDeviceList->clear();
    ui->netDeviceList->repaint();

    while (ui->testAutomationDeviceList->count() > 0)
    {
        ui->testAutomationDeviceList->takeItem(0);
    }
    ui->testAutomationDeviceList->clear();
    ui->testAutomationDeviceList->repaint();
    ui->netDeviceList->repaint();
}

void sqanTestTool::clearPeerListWidget()
{
    // While the number of items in the Peer List is greater than zero, remove the top item from the list and move the remaining items up
    while (ui->peerListWidget->count() > 0)
    {
        ui->peerListWidget->takeItem(0);
    }
    ui->peerListWidget->clear();
    ui->peerListWidget->repaint();
}

void sqanTestTool::clearEdges()
{
    dynArrayLock.lock();

    // Clear the edgeList for each device in DynArray
    for (int i=0; i<DYN_ARRAY_SIZE; i++)
    {
        if ( DynArray[i].isReady &&
             (DynArray[i].node != nullptr) )
        {
          /*JR  edges = DynArray[i].node->edges();
            if (!edges.empty())
            {
                // Delete the edges and clear edges for this node
                QList<Edge *>::iterator itr = edges.begin();
                while (itr != edges.end())
                {
                    Edge *edge = *itr;
                    if (edge != nullptr)
                    {
                        if ( netTopologyScene != nullptr )
                        {
                            netTopologyScene->removeItem(edge);
                        }
                        itr = edges.erase(itr);
                    }
                    else
                    {
                        ++itr;
                    }
                }
                edges.clear();

                netTopologyScene->removeItem(DynArray[i].node);
                netTopologyScene->update();
            }
            */

            // Remove rows from the available links table
            // for this device
            removeAvailableLinks(DynArray[i].devName);
        }
    }    

    dynArrayLock.unlock();
}

void sqanTestTool::drawEdges()
{
    int row = 0;

    dynArrayLock.lock();

    // Create the edges in the network topology
    for (int i=0; i<DYN_ARRAY_SIZE; i++)
    {
        if ( (DynArray[i].sqanInterface != nullptr) &&
             (DynArray[i].isReady) )
        {
            for (auto itr = DynArray[i].peerList.begin(); itr < DynArray[i].peerList.end(); itr++)
            {
                SolNetPeerListEntry *entry = *itr;

                QString deviceStr;
                // Run through the DynArray and find the entry with this macAddress
                for (int j=0; j<DYN_ARRAY_SIZE; j++)
                {
                    if ( (DynArray[j].sqanInterface != nullptr) &&
                         (DynArray[j].isReady) )
                    {
                        // Find the peer in the overall DynArray
                        // add an edge if they are on the same channel
                        if ( (DynArray[j].macAddr == entry->macAddr) &&
                             (DynArray[j].channel == DynArray[i].channel) )
                        {
                            // Link Active Nodes
                          /*JR  Edge *edge =  new Edge(DynArray[i].node, DynArray[j].node);
                            DynArray[i].node->addEdge(edge);
                            netTopologyScene->addItem(edge);
                          */
                            netTopologyScene->update();

                            // Add a row to the availableLinksTable for
                            // Data Traffic testing
                            addAvailableLink(row, i, j);
                            row++;
                            break;
                        }
                    }
                }
            }
            //JR netTopologyScene->addItem(DynArray[i].node);
            netTopologyScene->update();
        }
    }
    dynArrayLock.unlock();
}

/*****************************************************
 * sqanTestTool Hotplug Handling
 * **************************************************/
void sqanTestTool::checkHotplugEvents()
{
    if ( checkingHotplug == true )
    {
        // CheckHotplugQueue returns 0xFF if there is nothing on the queue
        uint8_t index = sqanInterfaceInit->CheckHotplugQueue();

        // If there is no event the index will be 0xFF
        if ( index != 0xFF )
        {
            // Check the SQAN Library

            // If the sqanInterface pointer = nullptr - it was removed
            if ( sqanInterfaceArray->at(index) == nullptr )
            {
                clearEdges();

                // Remove this device from service configuration
                QString blank = "";
                ui->deviceComboBox->setItemText(index, blank);
               //JR auto *model = qobject_cast<QStandardItemModel*>(ui->deviceComboBox->model());
               //JR model->item(index)->setEnabled(false);
                for (int i=0; i<ui->deviceComboBox->count(); i++)
                {
                    if ( i != index )
                    {
                      // Reset current item showing
                      ui->deviceComboBox->setCurrentIndex(i);
                      break;
                    }
                }
                ui->deviceComboBox->repaint();

                // Clear this item from lists
                // but leave the space blank so rows match
                // DynArray index
                ui->netDeviceList->item(index)->setText(blank);
                ui->netDeviceList->item(index)->setFlags(ui->netDeviceList->item(index)->flags() & ~Qt::ItemIsSelectable);
                ui->netDeviceList->repaint();

                ui->testAutomationDeviceList->item(index)->setText(blank);
                ui->testAutomationDeviceList->item(index)->setFlags(ui->testAutomationDeviceList->item(index)->flags() & ~Qt::ItemIsSelectable);
                ui->testAutomationDeviceList->repaint();

                // Remove any references to this device from the register services table
                removeDeviceFromRegisterServicesTable(index);

                // The device was removed
                clearDynArrayEntry(index);

                for (uint8_t i=0; i<MAX_DEVICES; i++)
                {
                    if ( (DynArray[i].sqanInterface != nullptr) &&
                          DynArray[i].sqanInterface->IsReady() &&
                          (i != index) )
                    {
                        updatePeers(i, false);
                    }
                }

                drawEdges();

                qDebug() << "Device " + QString::number(index) << " was removed from DynArray";

                //DBLog DynArray changes
                dbLogDevice(index);
            }
            else
            {
                // The device was added
                SqanInterface *sqanInterface = sqanInterfaceArray->at(index);

                if ( DynArray[index].sqanInterface == nullptr )
                {
                    DynArray[index].sqanInterface = sqanInterface;

                    clearEdges();

                    scanDevice(index, 55, 55);  // put it somewhere on the topology

                    //JR addDeviceToRegisterServicesTable(index);

                    for (uint8_t i=0; i<MAX_DEVICES; i++)
                    {
                        // Update everyone's peerlists again
                        // Now that a device has been removed
                        updatePeers(i, false);
                    }

                    drawEdges();

                    qDebug() << "Device " + QString::number(index) << " was added to DynArray";
                }
                else
                {
                    qDebug() << "Device " + QString::number(index) << " exists in DynArray";
                }

                //DBLog DynArray changes
                dbLogDevice(index);
            }
        }
    }
}


void sqanTestTool::updatePeers(int selected, bool locked)
{
    if (!locked)
    {
        dynArrayLock.lock();
    }

    // Need these checks in case hotplug event is occuring
    if ( (DynArray[selected].sqanInterface != nullptr) &&
         (DynArray[selected].sqanInterface->IsReady()) /*JR&&
         (DynArray[selected].sqanStream != nullptr)*/ )
    {

        //Get Peer Record For Selected Device
        //This is a copy or snapshot of the sqanStream->sqanPeers record array
        //because it is constantly being updated under a lock
       //JR std::array<SqanStream::sqanPeerRecord, MAX_PEERS> sqanPeers;
      //JR  DynArray[selected].sqanStream->GetPeers((std::array<SqanStream::sqanPeerRecord, MAX_PEERS> *)&sqanPeers);

        // Empty the SolNetPeerList in DynArray
        clearSolNetPeerList(selected);

        uint16_t devType;
        std::stringstream macAddress;
        macAddress << "";

        bool foundPeer = false;
      /*JR  for(uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
        {
            macAddress.str("");
            if (sqanPeers[peerIndex].inUse)
            {
                switch (sqanPeers[peerIndex].recordVersion)
                {
                case 2:
                {
                    if (sqanPeers[peerIndex].sqanPeerRecord2.linkStatus == 1)
                    {
                        for (int a=0; a<6; a++)
                        {
                           macAddress << std::hex << std::setfill('0') << std::setw(2) << (int)sqanPeers[peerIndex].sqanPeerRecord2.macAddress[a];
                           if ( a < 5 )
                           {
                               macAddress << ":";
                           }

                        }
                        devType = sqanPeers[peerIndex].sqanPeerRecord2.deviceType;
                        foundPeer = true;
                    }
                    break;
                }
                case 3:
                {
                    if (sqanPeers[peerIndex].sqanPeerRecord3.linkStatus == 1)
                    {
                        for (int a=0; a<6; a++)
                        {
                           macAddress << std::hex << std::setfill('0') << std::setw(2) << (int)sqanPeers[peerIndex].sqanPeerRecord3.macAddress[a];
                           if ( a < 5 )
                           {
                               macAddress << ":";
                           }
                        }
                        devType = sqanPeers[peerIndex].sqanPeerRecord3.deviceType;
                        foundPeer = true;
                    }
                    break;
                }
                case 4:
                {
                    if (sqanPeers[peerIndex].sqanPeerRecord4.linkStatus == 1)
                    {
                        for (int a=0; a<6; a++)
                        {
                           macAddress << std::hex << std::setfill('0') << std::setw(2) << (int)sqanPeers[peerIndex].sqanPeerRecord4.macAddress[a];
                           if ( a < 5 )
                           {
                               macAddress << ":";
                           }
                        }
                        devType = sqanPeers[peerIndex].sqanPeerRecord4.deviceType;
                        foundPeer = true;
                    }
                    break;
                }
                case 1:
                default:
                    break;
                }

                if ( foundPeer )
                {
                    QString macaddr = QString::fromStdString(macAddress.str()); // need to fix this later if we ever deal with sqanPeerRecord1 devices
                    int dai = getDynArrayIndexFromMac(macaddr);
                    QString devName;

                    if (dai >= 0) {
                        devName = DynArray[dai].devName;
                    } else {
                        devName = "ERR" + indexDelim + findNameFromDeviceType(devType);

                        if ( !devName.contains(unsupportedDevTypeStr) ) {
                            // Remove the characters after the ':' in the name found
                            int startPosition = devName.indexOf(':');
                            int numRemainingChars = devName.size() - startPosition;
                            devName.remove(startPosition, numRemainingChars);
                        }
                    }

                    if ( !devName.contains(unsupportedDevTypeStr) ) {
                        // Add to peerList for this device
                        SolNetPeerListEntry *entry = new SolNetPeerListEntry;

                        entry->macAddr.append(QString::fromStdString(macAddress.str()));
                        entry->devType = devType;
                        entry->devName = devName;
                        entry->peerIndex = peerIndex;
                        DynArray[selected].peerList.push_back(entry);
                    }

                    foundPeer = false;
                }
            }
        }*/
    }

    if (!locked)
    {
        dynArrayLock.unlock();
    }
}


void sqanTestTool::updateDeviceUiForSelected(int selected)
{
    // Update the UI for this device
    ui->macAddressEdit->setText(DynArray[selected].macAddr.toUpper());
    ui->coordPriorityEdit->setText(QString::number(DynArray[selected].coordPriority));
    ui->phyRateEdit->setText(QString::number(DynArray[selected].phy_rate)+" Mbps");

    ui->devTypeEdit->setText(findNameFromDeviceType(DynArray[selected].devType));

    switch(DynArray[selected].role)
    {
        case 0:
        {
            ui->coordPeerEdit->setText("Coordinator");
            break;
        }
        case 1:
        default:
        {
            ui->coordPeerEdit->setText("Non-Coordinator");
            break;
        }
    }

    ui->firmwareEdit->setText(DynArray[selected].firmwareVer);
    ui->channelEdit->repaint();
    switch(DynArray[selected].channel){
    case 9:
        ui->channelEdit->setText("Channel 9: BG 1/TFC 1");
        break;
    case 10:
        ui->channelEdit->setText("Channel 10: BG 1/TFC 2");
        break;
    case 11:
        ui->channelEdit->setText("Channel 11: BG 1/TFC 3");
        break;
    case 12:
        ui->channelEdit->setText("Channel 12: BG 1/TFC 4");
        break;
    case 13:
        ui->channelEdit->setText("Channel 13: BG 1/TFC 5");
        break;
    case 14:
        ui->channelEdit->setText("Channel 14: BG 1/TFC 6");
        break;
    case 15:
        ui->channelEdit->setText("Channel 15: BG 1/TFC 7");
        break;
    case 72:
        ui->channelEdit->setText("Channel 72: BG 1/TFC 8");
        break;
    case 73:
        ui->channelEdit->setText("Channel 73: BG 1/TFC 9");
        break;
    case 74:
        ui->channelEdit->setText("Channel 74: BG 1/TFC 10");
        break;
    case 25:
        ui->channelEdit->setText("Channel 25: BG 3/TFC 1");
        break;
    case 26:
        ui->channelEdit->setText("Channel 26: BG 3/TFC 2");
        break;
    case 27:
        ui->channelEdit->setText("Channel 27: BG 3/TFC 3");
        break;
    case 28:
        ui->channelEdit->setText("Channel 28: BG 3/TFC 4");
        break;
    case 29:
        ui->channelEdit->setText("Channel 29: BG 3/TFC 5");
        break;
    case 30:
        ui->channelEdit->setText("Channel 30: BG 3/TFC 6");
        break;
    case 31:
        ui->channelEdit->setText("Channel 31: BG 3/TFC 7");
        break;
    case 88:
        ui->channelEdit->setText("Channel 88: BG 3/TFC 8");
        break;
    case 89:
        ui->channelEdit->setText("Channel 89: BG 3/TFC 9");
        break;
    case 90:
        ui->channelEdit->setText("Channel 90: BG 3/TFC 10");
        break;
    case 49:
        ui->channelEdit->setText("Channel 49: BG 6/TFC 1");
        break;
    case 50:
        ui->channelEdit->setText("Channel 50: BG 6/TFC 2");
        break;
    case 51:
        ui->channelEdit->setText("Channel 51: BG 6/TFC 3");
        break;
    case 52:
        ui->channelEdit->setText("Channel 52: BG 6/TFC 4");
        break;
    case 54:
        ui->channelEdit->setText("Channel 54: BG 6/TFC 6");
        break;
    case 55:
        ui->channelEdit->setText("Channel 55: BG 6/TFC 7");
        break;
    case 112:
        ui->channelEdit->setText("Channel 112: BG 6/TFC 8");
        break;
    case 113:
        ui->channelEdit->setText("Channel 113: BG 6/TFC 9");
        break;
    case 114:
        ui->channelEdit->setText("Channel 114: BG 6/TFC 10");
        break;
    default:
        ui->channelEdit->setText("Unsupported Channel");
        break;
    }

    ui->coordPriorityEdit->setText(QString::number(DynArray[selected].coordPriority));
    ui->netIdEdit->setText(QString::number(DynArray[selected].netId, 16).toUpper());

    // Clear the peerListWidget
    clearPeerListWidget();

    // Display the current peers
    for (auto itr = DynArray[selected].peerList.begin(); itr != DynArray[selected].peerList.end(); ++itr)
    {
        SolNetPeerListEntry *entry = *itr;
        ui->peerListWidget->addItem(entry->devName);
    }
    ui->peerListWidget->repaint();
    ui->peerEdit->setText(QString::number(DynArray[selected].peerList.size()));
    ui->mcastAddrLineEdit->setText(QString::number(DynArray[selected].multicastGroup.startGroup.mcastAddress));
    ui->mcastAddrLineEdit_2->setText(QString::number(DynArray[selected].multicastGroup.joinGroup.mcastAddress));

    // Repaint the windows
    ui->netDeviceList->repaint();
    ui->testAutomationDeviceList->repaint();
    ui->deviceComboBox->repaint();
}

/*
void sqanTestTool::updateSqanAttr(int selected)
{
    DynArray[selected].attributesLock.lock();

    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        //Update SQAN Attributes For Each Device
        DynArray[selected].macAddr = QString::fromStdString(DynArray[selected].sqanIdentity->GetMacAddressStr());
        DynArray[selected].firmwareVer = QString::fromStdString(DynArray[selected].sqanFirmware->GetActiveFirmwareVersion());
        DynArray[selected].devType = DynArray[selected].sqanIdentity->GetSqanDeviceType();
        DynArray[selected].role = DynArray[selected].sqanStream->GetSqanRole();
        DynArray[selected].channel = DynArray[selected].sqanStream->GetSqanChannel();
        DynArray[selected].coordPriority = DynArray[selected].sqanIdentity->GetCoordinatorPrecedence();
        DynArray[selected].netId = DynArray[selected].sqanProduct->GetSqanNetworkId();
        DynArray[selected].phy_rate = DynArray[selected].sqanMetrics->GetPhyRate();

        // Convert Role Value to Coordinator/Non-Coordinator
        // These values come from streams status indication message
        switch(DynArray[selected].role){
        case 0:
            ui->coordPeerEdit->setText("Coordinator");
            DynArray[selected].node->setNodeColor(coordColor);
            DynArray[selected].node->update();
            break;
        default:
            ui->coordPeerEdit->setText("Non-Coordinator");
            DynArray[selected].node->setNodeColor(nodeColor);
            DynArray[selected].node->update();
            break;
        }

        getMulticastGroup(selected);

        // Clear the peerListWidget
        clearPeerListWidget();

        // Get a copy of the peerlist from firmware
        updatePeers(selected, false);
    }
    else
    {
        clearPeerListWidget();
        ui->peerListWidget->repaint();
        ui->peerEdit->setText(QString::number(0));
    }

    DynArray[selected].attributesLock.unlock();

    // Repaint the windows
    ui->netDeviceList->repaint();
    ui->peerListWidget->repaint();
    ui->testAutomationDeviceList->repaint();
}

void sqanTestTool::getMulticastGroup(int selected)
{
    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        // Clear the Multicast Records
        DynArray[selected].sqanStream->InitSqanMulticastRecords();
        // Get Multicast Records fresh
        DynArray[selected].sqanStream->SendGetMulticastGroupsStatusCmd();
        sleep(2);

        // Clear the current records in DynArray
        DynArray[selected].multicastGroup.startGroup.mcastStatus = 0;
        DynArray[selected].multicastGroup.startGroup.mcastPeerIndex = 0;
        DynArray[selected].multicastGroup.startGroup.mcastAddress = 0;
        DynArray[selected].multicastGroup.startGroup.mcastPhyRate = 0;
        DynArray[selected].multicastGroup.joinGroup.mcastStatus = 0;
        DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex = 0;
        DynArray[selected].multicastGroup.joinGroup.mcastAddress = 0;
        DynArray[selected].multicastGroup.joinGroup.mcastPhyRate = 0;
        bool foundMulticastGroup1 = false;
        bool foundMulticastGroup2 = false;
        DynArray[selected].mcastGroupList.clear();  //Remove all multicast address strings
        DynArray[selected].mcastGroupList.removeAll(QString("")); //Remove null strings

        // Get a new copy of the Multicast Group Records from SqanLibrary
        std::array<SqanStream::multicastGroupRecords, MAX_MCAST_RECORDS> multicastGroupRecords;
        memset(&multicastGroupRecords, 0, MAX_MCAST_RECORDS*sizeof(SqanStream::multicastGroupRecords));
        DynArray[selected].sqanStream->GetMulticastGroupRecords(&multicastGroupRecords);

        // Spec says we can have two multicast groups: 1) this device started 2) this device joined
        // and they don't have to be the same.  Search for two possible multicast groups
        for (int i=0; i<MAX_MCAST_RECORDS; i++)
        {
            if ((multicastGroupRecords[i].inUse) &&
                 (multicastGroupRecords[i].sqanMcastRecord.mcastPeerIndex >= 0x01) &&
                 (multicastGroupRecords[i].sqanMcastRecord.mcastPeerIndex <= 0xFF))
            {
                QString mcastAddrStr = "";
                // If this device started the mcast group - put the address in mcast startGroup
                if ( multicastGroupRecords[i].sqanMcastRecord.mcastStatus == SqanStream::McastGroupOriginatedAndActive )
                {
                    DynArray[selected].multicastGroup.startGroup.mcastStatus = multicastGroupRecords[i].sqanMcastRecord.mcastStatus;
                    DynArray[selected].multicastGroup.startGroup.mcastPeerIndex = multicastGroupRecords[i].sqanMcastRecord.mcastPeerIndex;
                    DynArray[selected].multicastGroup.startGroup.mcastAddress = multicastGroupRecords[i].sqanMcastRecord.mcastAddress;
                    DynArray[selected].multicastGroup.startGroup.mcastPhyRate = multicastGroupRecords[i].sqanMcastRecord.mcastPhyRate;
                    mcastAddrStr = QString::number(DynArray[selected].multicastGroup.startGroup.mcastAddress);

                    if ( ui->deviceComboBox->currentIndex() == selected )
                    {
                        ui->mcastAddrLineEdit->setText(mcastAddrStr);
                        ui->mcastAddrLineEdit->update();
                    }

                    DynArray[selected].node->setNodeColor(mcastColor);
                    DynArray[selected].node->update();

                    DynArray[selected].mcastGroupList << mcastAddrStr;
                    foundMulticastGroup1 = true;
                }
                else if ( multicastGroupRecords[i].sqanMcastRecord.mcastStatus == SqanStream::McastGroupJoined )
                {
                    // This device joined the group - put the address in mcastAddr2
                    DynArray[selected].multicastGroup.joinGroup.mcastStatus = multicastGroupRecords[i].sqanMcastRecord.mcastStatus;
                    DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex = multicastGroupRecords[i].sqanMcastRecord.mcastPeerIndex;
                    DynArray[selected].multicastGroup.joinGroup.mcastAddress = multicastGroupRecords[i].sqanMcastRecord.mcastAddress;
                    DynArray[selected].multicastGroup.joinGroup.mcastPhyRate = multicastGroupRecords[i].sqanMcastRecord.mcastPhyRate;
                    mcastAddrStr = QString::number(DynArray[selected].multicastGroup.joinGroup.mcastAddress);

                    if ( ui->deviceComboBox->currentIndex() == selected )
                    {
                        ui->mcastAddrLineEdit_2->setText(mcastAddrStr);
                        ui->mcastAddrLineEdit_2->update();
                    }

                    DynArray[selected].node->setNodeColor(mcastColor);
                    DynArray[selected].node->update();

                    DynArray[selected].mcastGroupList << mcastAddrStr;
                    foundMulticastGroup2 = true;
                }
                else if ( multicastGroupRecords[i].sqanMcastRecord.mcastStatus == SqanStream::McastGroupLeft )
                {
                    // Remove the mcast color from the node
                    if (DynArray[selected].role == 0)
                    {
                        DynArray[selected].node->setNodeColor(coordColor);
                        DynArray[selected].node->update();
                    }
                    else
                    {
                        DynArray[selected].node->setNodeColor(nodeColor);
                        DynArray[selected].node->update();
                    }

                    // Remove the text string for the mcast address
                    mcastAddrStr = QString::number(multicastGroupRecords[i].sqanMcastRecord.mcastAddress);
                    if ( ui->deviceComboBox->currentIndex() == selected )
                    {
                        if ( ui->mcastAddrLineEdit->text().contains(mcastAddrStr) )
                        {
                            ui->mcastAddrLineEdit->setText("");
                            ui->mcastAddrLineEdit->update();
                        }

                        if ( ui->mcastAddrLineEdit_2->text().contains(mcastAddrStr) )
                        {
                            ui->mcastAddrLineEdit_2->setText("");
                            ui->mcastAddrLineEdit_2->update();
                        }
                    }

                    // Remove this mcast address from the list
                    DynArray[selected].mcastGroupList.removeAll(mcastAddrStr);
                }
                else
                {
                    if ( multicastGroupRecords[i].sqanMcastRecord.mcastStatus == SqanStream::McastGroupAvailableToJoin )
                    {
                        mcastAddrStr = QString::number(multicastGroupRecords[i].sqanMcastRecord.mcastAddress);
                        DynArray[selected].mcastGroupList << mcastAddrStr;
                    }
                }
            }

            if ( (foundMulticastGroup1 == true) && (foundMulticastGroup2 == true) )
            {
                DynArray[selected].node->setNodeColor(mcastColor);
                DynArray[selected].node->update();
                break;
            }
            usleep(10);
        }

        // Set mcast addr text lines on GUI to empty if no addresses found
        if ( ui->deviceComboBox->currentIndex() == selected )
        {
            if ( foundMulticastGroup1 != true )
            {                
                ui->mcastAddrLineEdit->setText("");
                ui->mcastAddrLineEdit->update();
            }

            if ( foundMulticastGroup2 != true )
            {
                ui->mcastAddrLineEdit_2->setText("");
                ui->mcastAddrLineEdit_2->update();
            }
        }

        // If this node is not part of any multicast group - set the node colors back
        if ( (foundMulticastGroup1 != true) && (foundMulticastGroup2 != true) )
        {
            // Remove the mcast color from the node
            if (DynArray[selected].role == 0)
            {
                DynArray[selected].node->setNodeColor(coordColor);
                DynArray[selected].node->update();
            }
            else
            {
                DynArray[selected].node->setNodeColor(nodeColor);
                DynArray[selected].node->update();
            }
        }
    }
    ui->netDeviceList->repaint();
}
*/
void sqanTestTool::startMulticastGroup()
{
    int status = 0;

    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        bool ok;
        int testPhyRate = QInputDialog::getInt(this, tr("Start Multicast "), tr("Enter Phy Rate:"), 25, 0, 100, 1, &ok);

        //Send Start Multicast Group Command to Firmware
        //JR status = DynArray[selected].sqanStream->SendStartMulticastGroupCmd(testPhyRate);
        displayMessage("Starting Multicast Group");
        sleep(2);

        // Get the results of sending the multicast command
        //JR getMulticastGroup(selected);
    }

}

void sqanTestTool::joinMulticastGroup()
{
    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        //Get Multicast Status
        //JR getMulticastGroup(selected);

        if ( DynArray[selected].mcastGroupList.count() == 0 )
        {
            displayMessage("No Multicast Addresses Available to Join");
        }
        else
        {
            // Select Multicast Address From Multicast Group List
            bool ok;
             QString mcastAddressInput = QInputDialog::getItem(this, tr("Join Multicast Group"), tr("Select Multicast Address: "),
                                                              DynArray[selected].mcastGroupList, 0,
                                                              false, &ok);

            if (ok && !mcastAddressInput.isEmpty())
            {
                uint16_t mcastAddressInt = mcastAddressInput.toUInt();
                int testPhyRate = 1;
                int status = 0;

                displayMessage(DynArray[selected].devName + " sending Join Multicast Group " + mcastAddressInput);

                //Send Join Multicast Group Command to Firmware
                //JR status = DynArray[selected].sqanStream->SendJoinMulticastGroupCmd(mcastAddressInt, testPhyRate);
                sleep(2);

                // Get the results of sending the multicast command
                //JR getMulticastGroup(selected);

                if ( ((DynArray[selected].multicastGroup.joinGroup.mcastAddress == mcastAddressInt) /*JR&&
                      (DynArray[selected].multicastGroup.joinGroup.mcastStatus == SqanStream::McastGroupJoined))*/ ) )
                {
                    displayMessage(DynArray[selected].devName + " Joined Multicast Group " + mcastAddressInput);
                }
                else
                {
                    displayMessage(DynArray[selected].devName + " Not Joined to Multicast Group ");
                }

            }
        }
    }
}

void sqanTestTool::leaveMulticastGroup()
{
    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        //Get Multicast Status
        //JR getMulticastGroup(selected);

        if ( DynArray[selected].mcastGroupList.count() == 0 )
        {
            displayMessage("No Multicast Addresses Available to Leave");
        }
        else
        {
            // Select Multicast Address From Multicast Group List
            bool ok;
           QString mcastAddressInput = QInputDialog::getItem(this, tr("Leave Multicast Group"), tr("Select Multicast Address: "),
                                                              DynArray[selected].mcastGroupList, 0,
                                                              false, &ok);

            if ( ok && !mcastAddressInput.isEmpty() )
            {
                uint16_t mcastAddressInt = mcastAddressInput.toUInt();
                uint8_t peerIndex = 0;
                bool startGroup = false;
                bool joinGroup = false;

                if ( DynArray[selected].multicastGroup.startGroup.mcastAddress == mcastAddressInt )
                {
                    peerIndex = DynArray[selected].multicastGroup.startGroup.mcastPeerIndex;
                    startGroup = true;
                }
                else if ( DynArray[selected].multicastGroup.joinGroup.mcastAddress == mcastAddressInt )
                {
                    peerIndex = DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex;
                    joinGroup = true;
                }

                if ( peerIndex != 0 )
                {
                    displayMessage(DynArray[selected].devName + " sending Leave Multicast Group " + mcastAddressInput);

                    //Send Join Multicast Group Command to Firmware
                   //JR int status = DynArray[selected].sqanStream->SendLeaveMulticastGroupCmd(peerIndex);
                    sleep(2);

                    // Get the results of sending the multicast command
                    //JR getMulticastGroup(selected);

                    if ( ((startGroup == true) &&
                          (DynArray[selected].multicastGroup.startGroup.mcastAddress == 0))  ||
                         ((joinGroup == true) &&
                          (DynArray[selected].multicastGroup.joinGroup.mcastAddress == 0)) )
                    {
                        displayMessage(DynArray[selected].devName + " Left Multicast Group with Address " + mcastAddressInput);
                    }
                    else
                    {
                        displayMessage(DynArray[selected].devName + " Did not Leave Multicast Group ");
                    }
                }
                else
                {
                    displayMessage(DynArray[selected].devName + " Did not Leave Multicast Group ");
                }
            }
        }
    }
}

void sqanTestTool::modifyMulticastGroup()
{
    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if ( (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) )
    {
        //Get Multicast Status
        //JR getMulticastGroup(selected);

        if ( DynArray[selected].mcastGroupList.count() == 0 )
        {
            displayMessage("No Multicast Addresses Available -- Must Start Group Prior To Modify");
        }
        else
        {
            // Select Multicast Address From Multicast Group List
            bool ok;
            QString mcastAddressInput = QInputDialog::getItem(this, tr("Modify Multicast Group"), tr("Select Multicast Address: "),
                                                              DynArray[selected].mcastGroupList, 0,
                                                              false, &ok);

            if ( ok && !mcastAddressInput.isEmpty() )
            {
                bool ok;
                int testPhyRate = QInputDialog::getInt(this, tr("Modify Multicast "), tr("Enter Phy Rate:"), 25, 0, 100, 1, &ok);

                uint16_t mcastAddressInt = mcastAddressInput.toUInt();
                int status = 0;
                uint8_t peerIndex = 0;

                if ( DynArray[selected].multicastGroup.startGroup.mcastAddress == mcastAddressInt )
                {
                    peerIndex = DynArray[selected].multicastGroup.startGroup.mcastPeerIndex;
                }
                else if ( DynArray[selected].multicastGroup.joinGroup.mcastAddress == mcastAddressInt )
                {
                    peerIndex = DynArray[selected].multicastGroup.joinGroup.mcastPeerIndex;
                }
                if ( peerIndex != 0 )
                {
                    displayMessage(DynArray[selected].devName + " sending Modify Multicast Address " + mcastAddressInput);

                    //Send Modify Multicast Group Command to Firmware
                    //JR status = DynArray[selected].sqanStream->SendModifyMulticastGroupCmd(peerIndex, testPhyRate);
                    sleep(2);

                    displayMessage("Modified Multicast Address " + mcastAddressInput);
                    // Get the results of sending the multicast command
                    //JR getMulticastGroup(selected);
                }
                else
                {
                    displayMessage("Multicast Address " + mcastAddressInput + " Not Modified");
                }
            }
        }
    }
}

/*JR
void sqanTestTool::streamSpecCommand()
{
    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    if ((DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady))
    {
        // Test for the first peer in the list only for now
        if (!DynArray[selected].peerList.empty())
        {
            uint8_t peerIndex = 0;
            // Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the sub-stream is High Priority
            uint8_t txPriority = 1;
            // Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the SQAN-MAC
            // may automatically discard the sub-stream data in the event of network congestions
            uint8_t txDiscard = 3;
            // Max duration (in microseconds) between transmit opportunities
            uint32_t maxTxLatency = 100;
            int status = DynArray[selected].sqanStream->SendSetStreamsSpecCmd(peerIndex, txPriority, txDiscard, maxTxLatency);
            sleep(1);
            status = DynArray[selected].sqanStream->SendGetStreamSpecCmd(peerIndex);
            usleep(100);

        }
    }
}
*/
void sqanTestTool::on_netDeviceList_clicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);

    updateDeviceUiForSelected(selected);
}

void sqanTestTool::on_netDeviceList_itemClicked(QListWidgetItem *item)
{
    //Get Selected Item
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);

    // Refresh the view for this device and it's peer lists
    updateDeviceUiForSelected(selected);
}

void sqanTestTool::showNetDeviceListContextMenu(const QPoint &pos)
{
    //Handle Global Position
    QPoint globalPos = ui->netDeviceList->mapToGlobal(pos);

    //Create Menu and Insert Actions
    QMenu *deviceListMenu = new QMenu();
    QMenu *startAssociationMenu = deviceListMenu->addMenu("Start Association");
    QMenu *multicastMenu = deviceListMenu->addMenu("Multicast");
    QMenu *observerModeMenu = deviceListMenu->addMenu("Observer Mode");

    deviceListMenu->addAction("Change Device Type", this, SLOT(setDeviceType()));
    deviceListMenu->addAction("Set MAC Address", this, SLOT(setMacAddress()));
    deviceListMenu->addAction("Set Coordinator", this, SLOT(setCoordPriority()));
    deviceListMenu->addAction("Clear Association", this, SLOT(clearAssociation()));
    deviceListMenu->addAction("Update Firmware", this, SLOT(updateFirmwareDialog()));
    deviceListMenu->addAction("Idle Radio", this, SLOT(idleRadio()));
    deviceListMenu->addAction("Reset Radio", this, SLOT(resetRadio()));

//    deviceListMenu->addAction("Set Hibernation", this, SLOT(setHibernationMode()));
//    deviceListMenu->addAction("Get Hibernation", this, SLOT(getHibernationMode()));

    //Add Invite/Search Sub Menus to Start Association Menu
    startAssociationMenu->addAction("Invite Mode", this, SLOT(inviteMode()));
    startAssociationMenu->addAction("Search Mode", this, SLOT(searchMode()));

    //Add Create/Start/Join/Leave/Modify Sub Menus to Multicast Menu
    multicastMenu->addAction("Start Multicast Group", this, SLOT(startMulticastGroup()));
    multicastMenu->addAction("Join Multicast Group", this, SLOT(joinMulticastGroup()));
    multicastMenu->addAction("Leave Multicast Group", this, SLOT(leaveMulticastGroup()));
    multicastMenu->addAction("Modify Multicast Group", this, SLOT(modifyMulticastGroup()));

    //Add Enable/Disable Observer Mode Sub Menus to Observer Mode Menu
    observerModeMenu->addAction("Enable", this, SLOT(enableObserverMode()));
    observerModeMenu->addAction("Disable", this, SLOT(disableObserverMode()));

    //Show Context Menu At Handling Position
    deviceListMenu->exec(globalPos);
}

void sqanTestTool::on_netDeviceList_customContextMenuRequested(const QPoint &pos)
{
    Q_UNUSED(pos);
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Nothing
}

/*JR
void sqanTestTool::inviteMode()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    //Invite Mode
    uint8_t type = 1;
    uint8_t timeout = 30;

    if (DynArray[selected].sqanAssociation != nullptr)
    {
        //Invite Mode
        DynArray[selected].sqanAssociation->SendStartAssociationCmd(type, timeout);
        sleep(1);
    }

}

void sqanTestTool::searchMode()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if (DynArray[selected].sqanAssociation != nullptr)
    {
        //Clear Association
        displayMessage("Please Wait, Devices Associating...");

        // First clear the old association
        clearAssociation();

        // Start Search Mode
        uint8_t type = 2;
        uint8_t timeout = 40;

        elapsedTimer.start();
        int status = DynArray[selected].sqanAssociation->SendStartAssociationCmd(type, timeout);
        if(status == 0)
        {
            displayMessage("Network Association Completed");
        }
        ui->connectionTimeEdit->setText(QString::number((double)elapsedTimer.elapsed()/1000));

        // Clear the edges because we have to redraw them
        // Clear and repaint views that show devices
        clearEdges();

        for (uint8_t i=0; i<MAX_DEVICES; i++)
        {
            if ( (DynArray[i].sqanInterface != nullptr) && (DynArray[i].isReady) )
            {
                // Update devices and peerLists
                updateSqanAttr(i);
            }
        }

        // Update the view of this device
        updateDeviceUiForSelected(selected);

        // Redraw the edges - makes the Data Traffic table too
        drawEdges();

        //DBLog DynArray changes
        dbLogDevice(selected);
    }
}
*/

void sqanTestTool::clearAssociation()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

/*JR    if (DynArray[selected].sqanAssociation != nullptr)
    {
        //Clear Association
        displayMessage("Clearing Association...");
        DynArray[selected].sqanAssociation->SendClearAssociationCmd();
        usleep(10);

        // Reset the radio to force disconnect
        displayMessage(DynArray[selected].devName + " Reset Radio");
        DynArray[selected].sqanSystem->SendResetRadioCmd();
        usleep(10);

        DynArray[selected].sqanStream->InitSqanPeerRecords();
        DynArray[selected].sqanStream->InitSqanMulticastRecords();
*/
        // Leave device in the net device list but un-associated

        // Clear the edges because we have to redraw them
        // Clear and repaint views that show devices
        clearEdges();

        for (uint8_t i=0; i<MAX_DEVICES; i++)
        {
            if ( (DynArray[i].sqanInterface != nullptr) && (DynArray[i].isReady) )
            {
                // Update devices and peerLists
               //JR updateSqanAttr(i);
            }
        }

        updateDeviceUiForSelected(selected);

        // Redraw the edges - makes the Data Traffic table too
        drawEdges();

        //DBLog DynArray changes
        dbLogDevice(selected);        
 //JR   }
}


void sqanTestTool::setCoordPriority()
{
    uint8_t coordinatorCapable = 0; // 0=non-coordinator, 1=coordinator
    uint8_t coordinatorPrecedence = 0; // 0-5 on precedence
    uint8_t persist = 1;

    QStringList coordCapableList;
    coordCapableList << "Non-Coordinator Capable";
    coordCapableList << "Coordinator Capable";

    //Set the coordinator priority via the enter command
    QListWidgetItem *coordCapabletem = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(coordCapabletem);

    bool okCoordCapable;
    QString coordCapableInput = QInputDialog::getItem(this, tr("Change Coordinator Capable"), tr("Select Value:"),
                                                           coordCapableList, 0,
                                                           false, &okCoordCapable);

    int coordCapableListIndex = coordCapableList.indexOf(coordCapableInput);

    if (okCoordCapable && !coordCapableInput.isEmpty())
    {
        switch (coordCapableListIndex)
        {
            case 0:
                coordinatorCapable = 0; // Non-Coordinator Capable
                break;
            case 1:
                coordinatorCapable = 1; // Coordinator Capable
                break;
            default:
                displayMessage("Unsupported Coordinator Capable Value");
                break;
        }
    }

    QStringList items;
    items << QString("0");
    items << QString("1");
    items << QString("2");
    items << QString("3");
    items << QString("4");
    items << QString("5");

    //Set the coordinator priority via the enter command
    QListWidgetItem *coordPriorityItem = ui->netDeviceList->currentItem();

    bool okCoordPriority;
    QString coordPriorityInput = QInputDialog::getItem(this, tr("Change Coordinator Priority"), tr("Select Value:"),
                                                       items, 0,
                                                       false, &okCoordPriority);
    int index = items.indexOf(coordPriorityInput);

    if (okCoordPriority && !coordPriorityInput.isEmpty())
    {
        switch(index)
        {
            case 0:
                coordinatorPrecedence = 0;
                break;
            case 1:
                coordinatorPrecedence = 1;
                break;
            case 2:
                coordinatorPrecedence = 2;
                break;
            case 3:
                coordinatorPrecedence = 3;
                break;
            case 4:
                coordinatorPrecedence = 4;
                break;
            case 5:
                coordinatorPrecedence = 5;
                break;
            default:
                displayMessage("Unsupported Coordinator Priority Value");
                break;
        }
    }

    /*JRif ( (DynArray[selected].sqanIdentity != nullptr) &&
         (DynArray[selected].sqanInterface->IsReady()) &&
         (DynArray[selected].sqanStream != nullptr) )
    {
        int status = DynArray[selected].sqanIdentity->SendSetCoordinatorCmd(coordinatorCapable, coordinatorPrecedence, persist);
        sleep(1);
        displayMessage("Please Wait... Changing Coordinator Settings");

        //Get Coordinator
        int status_getCoordinator = DynArray[selected].sqanIdentity->SendGetCoordinatorCmd();
        sleep(1);

        if ( status_getCoordinator == 0 )
        {
            // Update device and peerLists
            updateSqanAttr(selected);
        }

        // Update the view of this device
        updateDeviceUiForSelected(selected);

        ui->netDeviceList->repaint();

        displayMessage("Change Coordinator Settings Completed");

        //DBLog DynArray changes
        dbLogDevice(selected);
    } */
}



void sqanTestTool::setDeviceType()
{
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);
    int status = 0;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

   if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
    {
       /*JR  if ( DynArray[selected].sqanIdentity == nullptr )
        {
            DynArray[selected].sqanIdentity = DynArray[selected].sqanInterface->GetSqanIdentity();
        } */

        QStringList items;
        items << QString("TWS: Thermal Weapon Sight");
        items << QString("HMD: Helmet-Mounted Display");
        items << QString("PHN: Smartphone/Computer");
        items << QString("STM: STORM Laser Range Finder");
        items << QString("DGR: DAGR GPS Device");
        items << QString("RLH: SQAN-Mac Relay, USB Host connected");
        items << QString("RLD: SQAN-Mac Relay, ISB Device connected");
        items << QString("DPT: Depot Device");
        items << QString("NET: SQAN-Mac Network Device");
        items << QString("ENC: Video Encoder (Camera)");
        items << QString("KPD: Keypad");
        items << QString("NES: SQAN-Mac Network Device - Slave Only");
        items << QString("HTR: Handheld Tactical Radio");
        items << QString("CHD: Command Mode Head Mounted Device");
        items << QString("GNM: Generic Coordinator-Capable Device");
        items << QString("GNS: Generic Peer Device (Non-Coordinator)");
        items << QString("NET1: General Network Node Priority 1");
        items << QString("NET2: General Network Node Priority 2");
        items << QString("NET3: General Network Node Priority 3");
        items << QString("NET4: General Network Node Priority 4");
        items << QString("NET5: General Network Node Priority 5");
        items << QString("Generic Biometric Sensor (e.g. heart rate)");
        items << QString("Generic Environmental Sensor (e.g. humidity)");
        items << QString("Generic Positioning Sensor (e.g. GPS, altitude)");

        bool ok;
        QString devTypeInput = QInputDialog::getItem(this, tr("Change Device Type"), tr("Select Device Type:"),
                                                       items, 0,
                                                       false, &ok);
        if(ok && !devTypeInput.isEmpty())
        {
            uint16_t devType = 0;

           /* if ( devTypeInput == "TWS: Thermal Weapon Sight" )
            {
                devType = SqanIdentity::TWS;
            }
            else if (devTypeInput == "HMD: Helmet-Mounted Display" )
            {
                devType = SqanIdentity::HMD;
            }
            else if ( devTypeInput == "PHN: Smartphone/Computer" )
            {
                devType = SqanIdentity::PHN;
            }
            else if ( devTypeInput == "STM: STORM Laser Range Finder" )
            {
                devType = SqanIdentity::STM;
            }
            else if ( devTypeInput == "DGR: DAGR GPS Device" )
            {
                devType = SqanIdentity::DGR;
            }
            else if ( devTypeInput == "RLH: SQAN-Mac Relay, USB Host connected" )
            {
                devType = SqanIdentity::RLH;
            }
            else if ( devTypeInput == "RLD: SQAN-Mac Relay, ISB Device connected" )
            {
                devType = SqanIdentity::RLD;
            }
            else if ( devTypeInput == "DPT: Depot Device" )
            {
                devType = SqanIdentity::DPT;
            }
            else if ( devTypeInput == "NET: SQAN-Mac Network Device" )
            {
                devType = SqanIdentity::NET;
            }
            else if ( devTypeInput == "ENC: Video Encoder (Camera)" )
            {
                devType = SqanIdentity::ENC;
            }
            else if ( devTypeInput == "KPD: Keypad" )
            {
                devType = SqanIdentity::KPD;
            }
            else if ( devTypeInput == "NES: SQAN-Mac Network Device - Slave Only" )
            {
                devType = SqanIdentity::NES;
            }
            else if ( devTypeInput == "HTR: Handheld Tactical Radio" )
            {
                devType = SqanIdentity::HTR;
            }
            else if ( devTypeInput == "CHD: Command Mode Head Mounted Device" )
            {
                devType = SqanIdentity::CHD;
            }
            else if ( devTypeInput == "GNM: Generic Coordinator-Capable Device" )
            {
                devType = SqanIdentity::GNM;
            }
            else if ( devTypeInput == "GNS: Generic Peer Device (Non-Coordinator)" )
            {
                devType = SqanIdentity::GNS;
            }
            else if ( devTypeInput == "NET1: General Network Node Priority 1" )
            {
                devType = SqanIdentity::NET1;
            }
            else if ( devTypeInput == "NET2: General Network Node Priority 2" )
            {
                devType = SqanIdentity::NET2;
            }
            else if ( devTypeInput == "NET3: General Network Node Priority 3" )
            {
                devType = SqanIdentity::NET3;
            }
            else if ( devTypeInput == "NET4: General Network Node Priority 4" )
            {
                devType = SqanIdentity::NET4;
            }
            else if ( devTypeInput == "NET5: General Network Node Priority 5" )
            {
                devType = SqanIdentity::NET5;
            }
            else if ( devTypeInput == "Generic Biometric Sensor (e.g. heart rate)" )
            {
                devType = SqanIdentity::Generic_Biometric_Sensor;
            }
            else if ( devTypeInput == "Generic Environmental Sensor (e.g. humidity)" )
            {
                devType = SqanIdentity::Generic_Environmental_Sensor;
            }
            else if ( devTypeInput == "Generic Positioning Sensor (e.g. GPS, altitude)" )
            {
                devType = SqanIdentity::Generic_Positioning_Sensor;
            }
            */

            if ( /*JR(DynArray[selected].sqanIdentity != nullptr) && */(DynArray[selected].sqanInterface->IsReady()) )
            {
                //JRstatus = DynArray[selected].sqanIdentity->SendSetDeviceTypeCmd(devType);

                displayMessage("Please Wait... Changing Device Type");
                sleep(1);
                //JR status = DynArray[selected].sqanIdentity->SendGetDeviceTypeCmd();
                sleep(1);

                //JR DynArray[selected].devType = DynArray[selected].sqanIdentity->GetSqanDeviceType();
                QString devName = findNameFromDeviceType(devType);
                if ( devName != unsupportedDevTypeStr )
                {
                    ui->devTypeEdit->setText(devName);

                   //JR updateSqanAttr(selected);

                    // Update the view of this device
                    updateDeviceUiForSelected(selected);

                    displayMessage("Change Device Type Completed");
                }
                else
                {
                    displayMessage(unsupportedDevTypeStr);
                }
            }
        }
    }
}


void sqanTestTool::setMacAddress()
{
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);
//    int status = 0;
    bool ok;
    std::ostringstream asciiStream;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            QString macAddrString = QInputDialog::getText(this, tr("Change MAC Address"), tr("Enter MAC Address"), QLineEdit::Normal, "00:1d:8e:5e:47:7a", &ok);
            if(ok && !macAddrString.isEmpty())
            {
                QStringList macAddrList = macAddrString.split(":");
                QString tmp1 = macAddrList[1];

                QString hexData_str = macAddrList.join("");

                std::string hexData = hexData_str.toUtf8().constData();

                assert( hexData.size() % 2 == 0 );//this can cause crash
                std::istringstream hexDataStream(hexData);

                std::vector<char> buf( 3 ); // two chars for the hex char, one for trailing zero
                while ( hexDataStream.good() )
                {
                     hexDataStream.get( &buf[0], buf.size() );
                     asciiStream << static_cast<char>( std::strtol( &buf[0], 0, 16 ) );
                }
                std::string asciiData = asciiStream.str();

                uint8_t PublicKeyData[3];//range from 0 to 127 ?
                std::copy(asciiData.begin(), asciiData.end(), PublicKeyData);

                //JR DynArray[selected].sqanIdentity->SendSetMacAddressCmd((uint8_t* )PublicKeyData);

                // Wait for firmware to change MAC address
                sleep(3);

                //look into sqanIdentity.cpp why the return of GetMacAddress is not refreshed
                ui->macAddressEdit->setText(DynArray[selected].macAddr.toUpper());

               //JR updateSqanAttr(selected);

                // Update the view of this device
                updateDeviceUiForSelected(selected);

                displayMessage("Set MAC Address");
            }
        }
    }
}


void sqanTestTool::on_firmwareDialogToolButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    filename_firmware = QFileDialog::getOpenFileName(this,tr("Load Firmware"),QDir::currentPath(),tr("ELF File (*elf)"));
    ui->firmwareUpdateEdit->setText(filename_firmware);
}

/*JR
void sqanTestTool::on_updateFirmwareButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Get Selected Item
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // Set Indications command
    // Set association | connection | reset | or streams status to zero
    // Look for the SetIndications message in the log file, then see if any
    // of the messages set to zero came after that.
    uint8_t wireInterfaceEnable = 1;  // Disabling this disables the ability to monitor for connections/disconnections
    uint8_t associationIndEnable = 0;
    uint8_t connectionIndEnable = 0;
    uint8_t resetIndEnable = 1;
    uint8_t streamsStatusIndEnable = 0; // Default
    DynArray[selected].sqanSystem->SendSetIndicationsCmd(wireInterfaceEnable, associationIndEnable,
                                                        connectionIndEnable, resetIndEnable, streamsStatusIndEnable);
    sleep(5);

    //Check If Field Is Empty
    if(ui->firmwareUpdateEdit->text() == "")
    {
        ui->firmwareUpdateTextEdit->setText("Field Cannot Be Empty");
    }
    else
    {
        //Load Image To Standby
        ui->firmwareUpdateTextEdit->setText(DynArray[selected].devName + " Loading Image " + filename_firmware);
        int position = filename_firmware.toStdString().find("-");
        std::string label = filename_firmware.toStdString().substr(position+1,5);
        label = "TRU_USB_"+label;
        qDebug() << "Label: " << QString::fromStdString(label);
        ui->firmwareUpdateTextEdit->append("Label: " + QString::fromStdString(label));

        //Send Load Image Command To Device
        DynArray[selected].sqanFirmware->SendLoadImageCmd(filename_firmware.toStdString(), label);
        ui->firmwareUpdateTextEdit->append("Loading Image to Device... Please Wait");

        int count = 0;
        while ( !DynArray[selected].sqanFirmware->ImageLoaded() && count<25 )
        {
            sleep(5);
            count++;
        }

        if ( (DynArray[selected].sqanFirmware != nullptr) && DynArray[selected].sqanFirmware->ImageLoaded() )
        {
            ui->firmwareUpdateTextEdit->append("Image Loaded to Device... Switching to New Image");
            DynArray[selected].sqanFirmware->SendRunImageCmd();
            count = 0;
            while ( !DynArray[selected].sqanFirmware->RunCommandConfirmed() )
            {
                usleep(100);
            }

            // Reset to run the new image
            ui->firmwareUpdateTextEdit->append("Resetting Device ...");

            DynArray[selected].sqanSystem->SendResetRadioCmd();
            usleep(100);
            ui->firmwareUpdateTextEdit->append("Image Successfully Loading and Active");
            ui->firmwareUpdateTextEdit->append("Done");

            QString activeFirmware = QString::fromStdString(DynArray[selected].sqanFirmware->GetActiveFirmwareVersion());

            ui->activeFirmwareEdit->setText(activeFirmware);
        }
        else
        {
           ui->firmwareUpdateTextEdit->append("Load Image Failed");
        }
    }
}

void sqanTestTool::updateFirmwareDialog()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString());
    dbLogger->dbLog(dblr);

    //Set the Update Firmware Tab to True and Switch to Tab
    ui->tabWidget->setTabEnabled(updateFirmwareTabWidgetLocation, true);
    ui->tabWidget->setCurrentIndex(updateFirmwareTabWidgetLocation);

    //Disable Other Tabs
    for(int a = 0; a < ui->tabWidget->count()-1; a++)
    {
        ui->tabWidget->setTabEnabled(a,false);
    }

    //Display Device Name
    ui->firmwareDeviceEdit->setText(DynArray[selected].devName);

    //Display Active Firmware
    QString activeFirmware = QString::fromStdString(DynArray[selected].sqanFirmware->GetActiveFirmwareVersion());
    ui->activeFirmwareEdit->setText(activeFirmware);
}

*/

//! Calls The Update Firmware Dialog To Update The Firmware
void sqanTestTool::on_actionUpdate_Firmware_triggered()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Call updateFirmwareDialog Function
    //JR updateFirmwareDialog();
}

void sqanTestTool::on_actionExit_triggered()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Close GUI Application
    close();
}

/*JR
void sqanTestTool::idleRadio()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);

    if ( DynArray[selected].sqanSystem != nullptr )
    {
        // Send Idle Radio Command to Firmware
        displayMessage(DynArray[selected].devName + " Idle Radio");
        DynArray[selected].sqanSystem->SendIdleRadioCmd();
        sleep(1);

        if ( DynArray[selected].sqanSystem->GetSqanRadioIdle() == false )
        {
            displayMessage(DynArray[selected].devName + " Idle Radio not set");
        }
        else
        {
            displayMessage("Idle Command Done");
        }
    }
}


void sqanTestTool::resetRadio()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);


    if ( DynArray[selected].sqanSystem != nullptr )
    {
        // Reset the radio to "un-idle"
        displayMessage(DynArray[selected].devName + " Reset Radio");
        DynArray[selected].sqanSystem->SendResetRadioCmd();
        usleep(10);

        displayMessage("Reset Command Done");
    }
}
*/

/******************************************************
 * sqanTestTool Network throughput
 * ***************************************************/

/*JR
void sqanTestTool::refreshNetworkThroughputData()
{
    // This runs off a timer and had a race condition
    // with hotplug events.  So, get the hotplug lock
    // and use the SQAN Library version of things.
    // DynArray will get updated later
    for (auto iterator = sqanInterfaceArray->begin(); iterator != sqanInterfaceArray->end(); iterator++ )
    {
         if (*iterator != nullptr)
         {
             SqanInterface *sqanInterface = *iterator;

             //Check if device is ready to receive commands
             if ( sqanInterface->IsReady() )
             {
                SqanMetrics *sqanMetrics = sqanInterface->GetSqanMetrics();

                if ( sqanMetrics != nullptr )
                {
                    sqanMetrics->SendGetWirelessMetrics();
                    uint16_t throughput = sqanMetrics->GetWirelessThroughput();

                    ui->RSSIEdit_1->setText(QString::number(sqanMetrics->GetRssi()));
                    ui->TransmitEdit_2->setText(QString::number(throughput)+" Mbps");
                    ui->PHYRateEdit_3->setText(QString::number(sqanMetrics->GetPhyRate())+" Mbps");

                    dataPlotChart->setYreal(throughput);
                }
                break;
             }
         }
    }
}


void sqanTestTool::generateNetworkStatisticsDataPlot()
{
    // Create Data Plot Chart and Graphics Scene
    dataPlotChart =  new PlotChart(0,0,1000,80,80);

    dataPlotScene = new QGraphicsScene(this);
    ui->dataPlotGraphicsView_2->setScene(dataPlotScene);

    dataPlotChartView =  new QChartView();
    dataPlotChart->setAnimationOptions(QChart::AllAnimations);
    dataPlotScene->addItem(dataPlotChart);
    ui->dataPlotGraphicsView_2->setScene(dataPlotScene);
    dataPlotChart->startPlot();

    // Start a timer to refresh stored network throughput data from firmware
    data_timer = new QTimer(this);
    connect(data_timer, SIGNAL(timeout()), this, SLOT(refreshNetworkThroughputData()));
    data_timer->start(1000);
}

*/

/******************************************************
 * sqanTestTool SolNet Service Descriptor Configuration
 * ***************************************************/
void sqanTestTool::createServiceConfigurationTable()
{
    // Add Service Descriptors to Service Descriptor Tree Widget
    ui->serviceDescTreeWidget->setHeaderLabel("Device/Service Descriptors");
    ui->serviceDescDataPayloadTreeWidget->setHeaderLabel("Service Descriptor Data Payload");

    unicastEndpointIds << "1" << "2" << "3";
    broadcastEndpointId << "0" ;
    multicastEndpointId << "0" ;
    endpointDataPolicies << "High Priority, Non-Discardable"  << "High Priority, Discardable" << "Low Priority, Non-Discardable" << "Low Priority, Discardable";

    // Generic for services that allow any of these types
    endpointDistribution << "Unicast" << "Broadcast" << "Multicast";

    buttonCount << "One Button" << "Two Buttons" << "Three Buttons";
    stateCount << "One State" << "Two States";
    pixelFormat << "PIXEL_FORMAT_Y" << "PIXEL_FORMAT_NV21" << "PIXEL_FORMAT_YUY2" << "PIXEL_FORMAT_NV16" << "PIXEL_FORMAT_RGBA_4444" << "PIXEL_FORMAT_RGBA_8888" << "PIXEL_FORMAT_RGBA_5551" << "PIXEL_FORMAT_RGBX_8888" << "PIXEL_FORMAT_RGB_332" << "PIXEL_FORMAT_RGB_565" << "PIXEL_FORMAT_RGB_888";

    videoProtocol << "FVTS";
    videoPolarity << "White Hot" << "Black Hot" << "Outline";
    videoImageCalibration << "No Image Calibration" << "Image Calibration Process Occurring" << "Image Calibration Needed From FWS-I";

    rtaEnabled << "RTA Operation Disabled" << "RTA Operation Enabled";
    rtaMode << "Spatially Align Mode" << "Goggle video w/ FWS-I PIP mode" << "FWS-I video w/ Goggle PIP mode" << "Full Screen FWS-I video mode";
    rtaPolarity << "White Hot" << "Black Hot";
    rtaZoom << "RTA bubble video zoom disabled" << "RTA bubble video zoom enable";
    rtaBubble << "RTA bubble disabled" << "RTA bubble enabled";
    rtaManualAlignment << "Manual alignment not requested" << "Manual alignment requested by user";
    rtaReticleInVideo << "No reticle in FWS-I video" << "Reticle included in FWS-I video";
    rtaReticleType << "Cross Hair" << "Circular";

    batteryStateStatus << "Battery OK" << "Battery Low" << "Battery Critical";
    batteryChargingStatus << "Battery is not charging" << "Battery is charging";
    deviceStateStatus << "Device Turning Off" << "Device Turning On";
    displayStateStatus << "Display Off" << "Display On";
    weaponSightModeStatus << "Standalone" << "CCO" << "RCO" << "Full Standalone";
    weaponActiveReticleIDStatus << "None" << "M4/M16" << "M249 S" << "M249 L" << "10 MIL" << "M136" << "M141" << "MIL-SCALE" << "RTA Boresight";
    weaponSymbologyStateStatus << "Symbology Off" << "Symbology On";

    //addServiceDescriptorsUI();
}

/*void sqanTestTool::addServiceDescriptorsUI()
{
    ui->printServicesPushButton->setStyleSheet("QPushButton { color : darkblue; padding: 1px; }");
    connect(ui->printServicesPushButton, SIGNAL(clicked()), this, SLOT(on_pushPrintServicesButton()));

    ui->serviceDescAddPushButton->setStyleSheet("QPushButton { color : darkblue; padding: 1px; }");
    connect(ui->serviceDescAddPushButton, SIGNAL(clicked()), this, SLOT(on_pushServiceDescAddButton()));

    ui->serviceDescRemovePushButton->setStyleSheet("QPushButton { color : darkblue; padding: 1px; }");
    connect(ui->serviceDescRemovePushButton, SIGNAL(clicked()), this, SLOT(on_pushServiceDescRemoveButton()));

    ui->descPlanImportPushButton->setStyleSheet("QPushButton { color : darkblue; padding: 1px; }");
    connect(ui->descPlanImportPushButton, SIGNAL(clicked()), this, SLOT(on_pushDescPlanImportButton()));

    ui->descPlanExportPushButton->setStyleSheet("QPushButton { color : darkblue; padding: 1px; }");
    connect(ui->descPlanExportPushButton, SIGNAL(clicked()), this, SLOT(on_pushDescPlanExportButton()));
}*/

/*************************************************
 *
 * addControlDescriptor
 *
 * Property UI Elements:
 * deviceEndpointIdComboBox
 * deviceEndpointDistributionComboBox
 * deviceEndpointDataPoliciesComboBox
 * deviceNameLineEdit
 * deviceSerialNumberLineEdit
 * deviceManufacturerLineEdit
 * deviceFriendlyNameLineEdit
 * **********************************************/
void sqanTestTool::addControlDescriptorUI()
{
    //Control Descriptor information
    //This applies to all services added for the device
    QTreeWidgetItem *controlServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    controlServiceDescriptor->setText(0, tr("Control"));

        //Control Endpoint Descriptors
        QTreeWidgetItem *controlEndpointServiceDescriptor = new QTreeWidgetItem(controlServiceDescriptor);
        controlEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *controlEndpointIDServiceDescriptor = new QTreeWidgetItem(controlEndpointServiceDescriptor);
            controlEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *controlEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(controlEndpointIDServiceDescriptor);
                deviceEndpointIdComboBox =  new QComboBox(this);
                deviceEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(controlEndpointIDServiceDescriptorTxt, 0, deviceEndpointIdComboBox);
                controlEndpointIDServiceDescriptor->addChild(controlEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *controlEndpointDataflowNumber = new QTreeWidgetItem(controlEndpointServiceDescriptor);
            controlEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *controlEndpointDataflowNumberTxt = new QTreeWidgetItem(controlEndpointDataflowNumber);
                deviceEndpointDataflowLineEdit =  new QLineEdit(this);
                deviceEndpointDataflowLineEdit->setReadOnly(true);
                deviceEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(controlEndpointDataflowNumberTxt, 0, deviceEndpointDataflowLineEdit);
                controlEndpointIDServiceDescriptor->addChild(controlEndpointDataflowNumberTxt);

            QTreeWidgetItem *controlEndpointDistributionServiceDescriptor = new QTreeWidgetItem(controlEndpointServiceDescriptor);
            controlEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *controlEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(controlEndpointDistributionServiceDescriptor);
                deviceEndpointDistributionComboBox = new QComboBox(this);
                deviceEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(controlEndpointDistributionServiceDescriptorTxt, 0, deviceEndpointDistributionComboBox);
                controlEndpointDistributionServiceDescriptor->addChild(controlEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *controlEndpointDataPoliciesDescriptor = new QTreeWidgetItem(controlEndpointServiceDescriptor);
            controlEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *controlEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(controlEndpointDataPoliciesDescriptor);
                deviceEndpointDataPoliciesComboBox =  new QComboBox(this);
                deviceEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(controlEndpointDataPoliciesDescriptorTxt, 0, deviceEndpointDataPoliciesComboBox);
                controlEndpointDataPoliciesDescriptor->addChild(controlEndpointDataPoliciesDescriptorTxt);

        //Device Property Descriptors
        QTreeWidgetItem *controlPropertiesServiceDescriptor = new QTreeWidgetItem(controlServiceDescriptor);
        controlPropertiesServiceDescriptor->setText(0, tr("Properties"));

            //Device Name
            QTreeWidgetItem *deviceNameDescriptor = new QTreeWidgetItem(controlPropertiesServiceDescriptor);
            deviceNameDescriptor->setText(0, tr("Device Name"));
                QTreeWidgetItem *deviceNameDescriptorTxt = new QTreeWidgetItem(deviceNameDescriptor);
                deviceNameLineEdit =  new QLineEdit(this);
                deviceNameLineEdit->setValidator(textValidator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(deviceNameDescriptorTxt, 0, deviceNameLineEdit);
                deviceNameDescriptor->addChild(deviceNameDescriptorTxt);

            //Device Serial Number
            QTreeWidgetItem *deviceSerialNumberDescriptor = new QTreeWidgetItem(controlPropertiesServiceDescriptor);
            deviceSerialNumberDescriptor->setText(0, tr("Serial Number"));
                QTreeWidgetItem *deviceSerialNumberDescriptorTxt = new QTreeWidgetItem(deviceSerialNumberDescriptor);
                deviceSerialNumberLineEdit =  new QLineEdit(this);
                deviceSerialNumberLineEdit->setValidator(textValidator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(deviceSerialNumberDescriptorTxt, 0, deviceSerialNumberLineEdit);
                deviceSerialNumberDescriptor->addChild(deviceSerialNumberDescriptorTxt);

            //Device Manufacturer
            QTreeWidgetItem *deviceManufacturerDescriptor = new QTreeWidgetItem(controlPropertiesServiceDescriptor);
            deviceManufacturerDescriptor->setText(0, tr("Manufacturer"));
                QTreeWidgetItem *deviceManufacturerDescriptorTxt = new QTreeWidgetItem(deviceManufacturerDescriptor);
                deviceManufacturerLineEdit =  new QLineEdit(this);
                deviceManufacturerLineEdit->setValidator(textValidator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(deviceManufacturerDescriptorTxt, 0, deviceManufacturerLineEdit);
                deviceManufacturerDescriptor->addChild(deviceManufacturerDescriptorTxt);

            //Device Friendly Name
            QTreeWidgetItem *deviceFriendlyNameDescriptor = new QTreeWidgetItem(controlPropertiesServiceDescriptor);
            deviceFriendlyNameDescriptor->setText(0, tr("Device Friendly Name"));
                QTreeWidgetItem *deviceFriendlyNameDescriptorTxt = new QTreeWidgetItem(deviceFriendlyNameDescriptor);
                deviceFriendlyNameLineEdit =  new QLineEdit(this);
                deviceFriendlyNameLineEdit->setValidator(textValidator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(deviceFriendlyNameDescriptorTxt, 0, deviceFriendlyNameLineEdit);
                deviceFriendlyNameDescriptor->addChild(deviceFriendlyNameDescriptorTxt);

        //Control Save Configuration Button
        QTreeWidgetItem *saveControlConfigItem = new QTreeWidgetItem(controlServiceDescriptor);
        QPushButton *saveControlConfigButton = new QPushButton("Save Control Config", this);
        saveControlConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveControlConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(0); });

    ui->serviceDescTreeWidget->setItemWidget(saveControlConfigItem, 0, saveControlConfigButton);

    QList <QTreeWidgetItem *> controlChildrenServiceDescriptors;
    controlChildrenServiceDescriptors.append(controlEndpointServiceDescriptor);
    controlChildrenServiceDescriptors.append(controlPropertiesServiceDescriptor);
    controlChildrenServiceDescriptors.append(saveControlConfigItem);

    controlServiceDescriptor->addChildren(controlChildrenServiceDescriptors);

    controlServiceDescriptor->setExpanded(true);
        controlEndpointServiceDescriptor->setExpanded(true);
            controlEndpointIDServiceDescriptor->setExpanded(true);
            controlEndpointDataflowNumber->setExpanded(true);
            controlEndpointDistributionServiceDescriptor->setExpanded(true);
            controlEndpointDataPoliciesDescriptor->setExpanded(true);
        controlPropertiesServiceDescriptor->setExpanded(true);
            deviceNameDescriptor->setExpanded(true);
            deviceSerialNumberDescriptor->setExpanded(true);
            deviceManufacturerDescriptor->setExpanded(true);
            deviceFriendlyNameDescriptor->setExpanded(true);
}

/*************************************************
 *
 * addVideoDescriptor
 *
 * Property UI Elements:
 * videoEndpointIdComboBox
 * videoEndpointDistributionComboBox
 * videoEndpointDataPoliciesComboBox
 * videoLabelLineEdit
 * videoFormatResolutionXServiceDescriptorLineEdit
 * videoFormatResolutionYServiceDescriptorLineEdit
 * videoFormatFrameRateDescriptorLineEdit
 * videoFormatBitDepthDescriptorLineEdit
 * videoFormatPixelFormatDescriptorComboBox
 * videoProtocolRawDescriptorComboBox
 * videoIfovDescriptorLineEdit
 * videoPPointXLineEdit
 * videoPPointYLineEdit
 * **********************************************/
void sqanTestTool::addVideoDescriptorUI()
{
    //Video Service Descriptor
    QTreeWidgetItem *videoServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    videoServiceDescriptor->setText(0, tr("Video"));

        //Video Endpoint Descriptors
        QTreeWidgetItem *videoEndpointServiceDescriptor = new QTreeWidgetItem(videoServiceDescriptor);
        videoEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *videoEndpointIDServiceDescriptor = new QTreeWidgetItem(videoEndpointServiceDescriptor);
            videoEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *videoEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(videoEndpointIDServiceDescriptor);
                videoEndpointIdComboBox =  new QComboBox(this);
                videoEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(videoEndpointIDServiceDescriptorTxt, 0, videoEndpointIdComboBox);
                videoEndpointIDServiceDescriptor->addChild(videoEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *videoEndpointDataflowNumber = new QTreeWidgetItem(videoEndpointServiceDescriptor);
            videoEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *videoEndpointDataflowNumberTxt = new QTreeWidgetItem(videoEndpointDataflowNumber);
                videoEndpointDataflowLineEdit =  new QLineEdit(this);
                videoEndpointDataflowLineEdit->setReadOnly(true);
                videoEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDataflowNumberTxt, 0, videoEndpointDataflowLineEdit);
                videoEndpointDataflowNumber->addChild(videoEndpointDataflowNumberTxt);

            QTreeWidgetItem *videoEndpointDistributionServiceDescriptor = new QTreeWidgetItem(videoEndpointServiceDescriptor);
            videoEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *videoEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(videoEndpointDistributionServiceDescriptor);
                videoEndpointDistributionComboBox = new QComboBox(this);
                videoEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDistributionServiceDescriptorTxt, 0, videoEndpointDistributionComboBox);
                videoEndpointDistributionServiceDescriptor->addChild(videoEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *videoEndpointDataPoliciesDescriptor = new QTreeWidgetItem(videoEndpointServiceDescriptor);
            videoEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *videoEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(videoEndpointDataPoliciesDescriptor);
                videoEndpointDataPoliciesComboBox =  new QComboBox(this);
                videoEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDataPoliciesDescriptorTxt, 0, videoEndpointDataPoliciesComboBox);
                videoEndpointDataPoliciesDescriptor->addChild(videoEndpointDataPoliciesDescriptorTxt);

        //Video Properties Descriptors
        QTreeWidgetItem *videoPropertiesServiceDescriptor = new QTreeWidgetItem(videoServiceDescriptor);
        videoPropertiesServiceDescriptor->setText(0, tr("Properties"));

            QTreeWidgetItem *videoFormatServiceDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
            videoFormatServiceDescriptor->setText(0, tr("Video Format"));

            QTreeWidgetItem *videoFormatResolutionXServiceDescriptor = new QTreeWidgetItem(videoFormatServiceDescriptor);
            videoFormatResolutionXServiceDescriptor->setText(0, tr("Resolution X"));
                QTreeWidgetItem *videoFormatResolutionXServiceDescriptorTxt = new QTreeWidgetItem(videoFormatResolutionXServiceDescriptor);
                videoFormatResolutionXServiceDescriptorLineEdit =  new QLineEdit();
                videoFormatResolutionXServiceDescriptorLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(videoFormatResolutionXServiceDescriptorTxt, 0, videoFormatResolutionXServiceDescriptorLineEdit);
                videoFormatResolutionXServiceDescriptor->addChild(videoFormatResolutionXServiceDescriptorTxt);

            QTreeWidgetItem *videoFormatResolutionYServiceDescriptor = new QTreeWidgetItem(videoFormatServiceDescriptor);
            videoFormatResolutionYServiceDescriptor->setText(0, tr("Resolution Y"));
                QTreeWidgetItem *videoFormatResolutionYServiceDescriptorTxt = new QTreeWidgetItem(videoFormatResolutionYServiceDescriptor);
                videoFormatResolutionYServiceDescriptorLineEdit =  new QLineEdit();
                videoFormatResolutionYServiceDescriptorLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(videoFormatResolutionYServiceDescriptorTxt, 0, videoFormatResolutionYServiceDescriptorLineEdit);
                videoFormatResolutionYServiceDescriptor->addChild(videoFormatResolutionYServiceDescriptorTxt);

            QTreeWidgetItem *videoFormatFrameRateDescriptor = new QTreeWidgetItem(videoFormatServiceDescriptor);
            videoFormatFrameRateDescriptor->setText(0, tr("Frame Rate"));
                QTreeWidgetItem *videoFormatFrameRateDescriptorTxt = new QTreeWidgetItem(videoFormatFrameRateDescriptor);
                videoFormatFrameRateDescriptorLineEdit =  new QLineEdit();
                videoFormatFrameRateDescriptorLineEdit->setValidator(pfloatValidator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(videoFormatFrameRateDescriptorTxt, 0, videoFormatFrameRateDescriptorLineEdit);
                videoFormatFrameRateDescriptor->addChild(videoFormatFrameRateDescriptorTxt);

            QTreeWidgetItem *videoFormatBitDepthDescriptor = new QTreeWidgetItem(videoFormatServiceDescriptor);
            videoFormatBitDepthDescriptor->setText(0, tr("Bit Depth"));
                QTreeWidgetItem *videoFormatBitDepthDescriptorTxt = new QTreeWidgetItem(videoFormatBitDepthDescriptor);
                videoFormatBitDepthDescriptorLineEdit =  new QLineEdit();
                videoFormatBitDepthDescriptorLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(videoFormatBitDepthDescriptorTxt, 0, videoFormatBitDepthDescriptorLineEdit);
                videoFormatBitDepthDescriptor->addChild(videoFormatBitDepthDescriptorTxt);

            QTreeWidgetItem *videoFormatPixelFormatServiceDescriptor = new QTreeWidgetItem(videoFormatServiceDescriptor);
            videoFormatPixelFormatServiceDescriptor->setText(0, tr("Pixel Format"));
                QTreeWidgetItem *videoFormatPixelFormatServiceDescriptorTxt = new QTreeWidgetItem(videoFormatPixelFormatServiceDescriptor);
                videoFormatPixelFormatDescriptorComboBox =  new QComboBox(this);
                videoFormatPixelFormatDescriptorComboBox->insertItems(0, pixelFormat);
                ui->serviceDescTreeWidget->setItemWidget(videoFormatPixelFormatServiceDescriptorTxt, 0, videoFormatPixelFormatDescriptorComboBox);
                videoFormatPixelFormatServiceDescriptor->addChild(videoFormatPixelFormatServiceDescriptorTxt);

            QTreeWidgetItem *videoProtocolServiceDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
            videoProtocolServiceDescriptor->setText(0, tr("Video Protocol"));

                QTreeWidgetItem *protocolServiceDescriptor = new QTreeWidgetItem(videoProtocolServiceDescriptor);
                protocolServiceDescriptor->setText(0, tr("Protocol"));
                     QTreeWidgetItem *procotolServiceDescriptorTxt = new QTreeWidgetItem(protocolServiceDescriptor);
                     videoProtocolRawDescriptorComboBox = new QComboBox(this);
                     videoProtocolRawDescriptorComboBox->insertItems(0, videoProtocol);
                     ui->serviceDescTreeWidget->setItemWidget(procotolServiceDescriptorTxt, 0, videoProtocolRawDescriptorComboBox);
                     videoFormatPixelFormatServiceDescriptor->addChild(procotolServiceDescriptorTxt);

            QTreeWidgetItem *videoIFOVServiceDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
            videoIFOVServiceDescriptor->setText(0, tr("Video IFOV"));
                QTreeWidgetItem *videoIfovDescriptorTxt = new QTreeWidgetItem(videoIFOVServiceDescriptor);
                 videoIfovDescriptorLineEdit = new QLineEdit();
                 videoIfovDescriptorLineEdit->setValidator(pfloatValidator); // #WIP
                 ui->serviceDescTreeWidget->setItemWidget(videoIfovDescriptorTxt, 0, videoIfovDescriptorLineEdit);
                 videoFormatPixelFormatServiceDescriptor->addChild(videoIfovDescriptorTxt);

            QTreeWidgetItem *videoPPServiceDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
            videoPPServiceDescriptor->setText(0, tr("Video PP"));

                QTreeWidgetItem *videoPPPointXServiceDescriptor = new QTreeWidgetItem(videoPPServiceDescriptor);
                videoPPPointXServiceDescriptor->setText(0, tr("Point X"));
                     QTreeWidgetItem *videoPPPointXServiceDescriptorTxt = new QTreeWidgetItem(videoPPPointXServiceDescriptor);
                     videoPPointXLineEdit =  new QLineEdit();
                     videoPPointXLineEdit->setValidator(uint16Validator); // #WIP
                     ui->serviceDescTreeWidget->setItemWidget(videoPPPointXServiceDescriptorTxt, 0, videoPPointXLineEdit);
                     videoPPPointXServiceDescriptor->addChild(videoPPPointXServiceDescriptorTxt);

                QTreeWidgetItem *videoPPPointYServiceDescriptor = new QTreeWidgetItem(videoPPServiceDescriptor);
                videoPPPointYServiceDescriptor->setText(0, tr("Point Y"));
                    QTreeWidgetItem *videoPPPointYServiceDescriptorTxt = new QTreeWidgetItem(videoPPPointYServiceDescriptor);
                    videoPPointYLineEdit =  new QLineEdit();
                    videoPPointYLineEdit->setValidator(uint16Validator); // #WIP
                    ui->serviceDescTreeWidget->setItemWidget(videoPPPointYServiceDescriptorTxt, 0, videoPPointYLineEdit);
                    videoPPPointYServiceDescriptor->addChild(videoPPPointYServiceDescriptorTxt);

//            QTreeWidgetItem *videoLensDistortionDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
//            videoLensDistortionDescriptor->setText(0, tr("Video Lens Distortion"));
//                 QTreeWidgetItem *videoLensDistortionDescriptorTxt = new QTreeWidgetItem(videoLensDistortionDescriptor);
//                 videoLensDistortionCoefficientsLineEdit =  new QLineEdit(); // #WIP Validator needed
//                 ui->serviceDescTreeWidget->setItemWidget(videoLensDistortionDescriptorTxt, 0, videoLensDistortionCoefficientsLineEdit);
//                 videoLensDistortionDescriptor->addChild(videoLensDistortionDescriptorTxt);

//            QTreeWidgetItem *videoControlDescriptor = new QTreeWidgetItem(videoPropertiesServiceDescriptor);
//            videoControlDescriptor->setText(0, tr("Video Control"));
//                 QTreeWidgetItem *videoControlDescriptorTxt = new QTreeWidgetItem(videoControlDescriptor);
//                 videoControlBitmapLineEdit =  new QLineEdit(); // #WIP Validator needed
//                 ui->serviceDescTreeWidget->setItemWidget(videoControlDescriptorTxt, 0, videoControlBitmapLineEdit);
//                 videoControlDescriptor->addChild(videoControlDescriptorTxt);

        //Video Label Descriptor
        QTreeWidgetItem *videoLabelServiceDescriptor = new QTreeWidgetItem(videoServiceDescriptor);
        videoLabelServiceDescriptor->setText(0, tr("Label"));
        QTreeWidgetItem *videoLabelServiceDescriptorTxt = new QTreeWidgetItem(videoLabelServiceDescriptor);
        videoLabelLineEdit =  new QLineEdit();
        videoLabelLineEdit->setValidator(textValidator); // #WIP
        ui->serviceDescTreeWidget->setItemWidget(videoLabelServiceDescriptorTxt, 0, videoLabelLineEdit);
        videoLabelServiceDescriptor->addChild(videoLabelServiceDescriptorTxt);

        //Video Save Configuration Button
        QTreeWidgetItem *saveVideoConfigItem = new QTreeWidgetItem(videoServiceDescriptor);
        QPushButton *saveVideoConfigButton = new QPushButton("Save Video Config", this);
        saveVideoConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveVideoConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(1); });

    ui->serviceDescTreeWidget->setItemWidget(saveVideoConfigItem, 0, saveVideoConfigButton);

    QList <QTreeWidgetItem *> videoChildrenServiceDescriptors;
    videoChildrenServiceDescriptors.append(videoEndpointServiceDescriptor);
    videoChildrenServiceDescriptors.append(videoPropertiesServiceDescriptor);
    videoChildrenServiceDescriptors.append(videoLabelServiceDescriptor);
    videoChildrenServiceDescriptors.append(saveVideoConfigItem);

    videoServiceDescriptor->addChildren(videoChildrenServiceDescriptors);

    videoServiceDescriptor->setExpanded(true);
        videoEndpointServiceDescriptor->setExpanded(true);
            videoEndpointIDServiceDescriptor->setExpanded(true);
            videoEndpointDataflowNumber->setExpanded(true);
            videoEndpointDistributionServiceDescriptor->setExpanded(true);
            videoEndpointDataPoliciesDescriptor->setExpanded(true);
        videoPropertiesServiceDescriptor->setExpanded(true);
            videoFormatServiceDescriptor->setExpanded(true);
                videoFormatResolutionXServiceDescriptor->setExpanded(true);
                videoFormatResolutionYServiceDescriptor->setExpanded(true);
                videoFormatFrameRateDescriptor->setExpanded(true);
                videoFormatBitDepthDescriptor->setExpanded(true);
                videoFormatPixelFormatServiceDescriptor->setExpanded(true);
            videoProtocolServiceDescriptor->setExpanded(true);
                protocolServiceDescriptor->setExpanded(true);
            videoIFOVServiceDescriptor->setExpanded(true);
            videoPPServiceDescriptor->setExpanded(true);
                videoPPPointYServiceDescriptor->setExpanded(true);
                videoPPPointXServiceDescriptor->setExpanded(true);
        videoLabelServiceDescriptor->setExpanded(true);
}

void sqanTestTool::addVideoDataPayloadUI()
{
    //Video Data Payload
    QTreeWidgetItem *videoDataPayload = new QTreeWidgetItem(ui->serviceDescDataPayloadTreeWidget);
    videoDataPayload->setText(0, tr("Video Data Payload"));

//        //Video Frame Metadata
//        QTreeWidgetItem *videoFrameMetadata = new QTreeWidgetItem(videoDataPayload);
//        videoFrameMetadata->setText(0, tr("Video Frame Metadata"));

//            QTreeWidgetItem *videoEndpointIDServiceDescriptor = new QTreeWidgetItem(videoFrameMetadata);
//            videoEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
//                QTreeWidgetItem *videoEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(videoEndpointIDServiceDescriptor);
//                videoEndpointIdComboBox =  new QComboBox(this);
//                videoEndpointIdComboBox->insertItems(0, endpointIds);
//                ui->serviceDescTreeWidget->setItemWidget(videoEndpointIDServiceDescriptorTxt, 0, videoEndpointIdComboBox);
//                videoEndpointIDServiceDescriptor->addChild(videoEndpointIDServiceDescriptorTxt);

//            QTreeWidgetItem *videoEndpointDataflowNumber = new QTreeWidgetItem(videoEndpointServiceDescriptor);
//            videoEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
//                QTreeWidgetItem *videoEndpointDataflowNumberTxt = new QTreeWidgetItem(videoEndpointDataflowNumber);
//                videoEndpointDataflowLineEdit =  new QLineEdit(this);
//                videoEndpointDataflowLineEdit->setReadOnly(true);
//                videoEndpointDataflowLineEdit->setEnabled(false);
//                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDataflowNumberTxt, 0, videoEndpointDataflowLineEdit);
//                videoEndpointDataflowNumber->addChild(videoEndpointDataflowNumberTxt);

//            QTreeWidgetItem *videoEndpointDistributionServiceDescriptor = new QTreeWidgetItem(videoFrameMetadata);
//            videoEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
//                QTreeWidgetItem *videoEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(videoEndpointDistributionServiceDescriptor);
//                videoEndpointDistributionComboBox = new QComboBox(this);
//                videoEndpointDistributionComboBox->insertItems(0, endpointDistributions);
//                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDistributionServiceDescriptorTxt, 0, videoEndpointDistributionComboBox);
//                videoEndpointDistributionServiceDescriptor->addChild(videoEndpointDistributionServiceDescriptorTxt);

//            QTreeWidgetItem *videoEndpointDataPoliciesDescriptor = new QTreeWidgetItem(videoFrameMetadata);
//            videoEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
//                QTreeWidgetItem *videoEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(videoEndpointDataPoliciesDescriptor);
//                videoEndpointDataPoliciesComboBox =  new QComboBox(this);
//                videoEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
//                ui->serviceDescTreeWidget->setItemWidget(videoEndpointDataPoliciesDescriptorTxt, 0, videoEndpointDataPoliciesComboBox);
//                videoEndpointDataPoliciesDescriptor->addChild(videoEndpointDataPoliciesDescriptorTxt);

        //Video RTA Metadata
        QTreeWidgetItem *videoRtaMetadata = new QTreeWidgetItem(videoDataPayload);
        videoRtaMetadata->setText(0, tr("Video RTA Metadata"));
            QTreeWidgetItem *videoPolarityTreeItem = new QTreeWidgetItem(videoRtaMetadata);
            videoPolarityTreeItem->setText(0, tr("Video Polarity"));
                QTreeWidgetItem *videoPolarityTxt = new QTreeWidgetItem(videoPolarityTreeItem);
                videoPolarityComboBox =  new QComboBox(this);
                videoPolarityComboBox->insertItems(0, videoPolarity);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(videoPolarityTxt, 0, videoPolarityComboBox);
                videoPolarityTreeItem->addChild(videoPolarityTxt);

            QTreeWidgetItem *videoImageCalibrationTreeItem = new QTreeWidgetItem(videoRtaMetadata);
            videoImageCalibrationTreeItem->setText(0, tr("Video Image Calibration"));
                QTreeWidgetItem *videoImageCalibrationTxt = new QTreeWidgetItem(videoImageCalibrationTreeItem);
                videoImageCalibrationComboBox =  new QComboBox(this);
                videoImageCalibrationComboBox->insertItems(0, videoImageCalibration);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(videoImageCalibrationTxt, 0, videoImageCalibrationComboBox);
                videoImageCalibrationTreeItem->addChild(videoImageCalibrationTxt);

        //Video Data Payload Save Configuration Button
        QTreeWidgetItem *saveVideoDataPayloadConfigItem = new QTreeWidgetItem(videoDataPayload);
        QPushButton *saveVideoDataPayloadConfigButton = new QPushButton("Save Video Data Payload Config", this);
        saveVideoDataPayloadConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        connect(saveVideoDataPayloadConfigButton, SIGNAL(clicked()), this, SLOT(saveVideoDataPayloadConfigButton_clicked()));
        ui->serviceDescDataPayloadTreeWidget->setItemWidget(saveVideoDataPayloadConfigItem, 0, saveVideoDataPayloadConfigButton);

        QList <QTreeWidgetItem *> videoDataPayloadChildren;
        //videoDataPayloadChildren.append(videoFrameMetadata);
        videoDataPayloadChildren.append(videoRtaMetadata);
        videoDataPayloadChildren.append(saveVideoDataPayloadConfigItem);

        videoDataPayload->addChildren(videoDataPayloadChildren);
}

/*************************************************
 *
 * addRtaDescriptor
 *
 * Property UI Elements:
 * rtaEndpointIdComboBox
 * rtaEndpointDistributionComboBox
 * rtaEndpointDataPoliciesComboBox
 * rtaLabelLineEdit
 * rtaImuRateLineEdit
 * **********************************************/
void sqanTestTool::addRtaDescriptorUI()
{
    //RTA Service Descriptor Tree
    QTreeWidgetItem *rtaServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    rtaServiceDescriptor->setText(0, tr("RTA"));

        //RTA Endpoint Descriptors
        QTreeWidgetItem *rtaEndpointServiceDescriptor = new QTreeWidgetItem(rtaServiceDescriptor);
        rtaEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *rtaEndpointIDServiceDescriptor = new QTreeWidgetItem(rtaEndpointServiceDescriptor);
            rtaEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *rtaEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(rtaEndpointIDServiceDescriptor);
                rtaEndpointIdComboBox =  new QComboBox(this);
                rtaEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(rtaEndpointIDServiceDescriptorTxt, 0, rtaEndpointIdComboBox);
                rtaEndpointIDServiceDescriptor->addChild(rtaEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *rtaEndpointDataflowNumber = new QTreeWidgetItem(rtaEndpointServiceDescriptor);
            rtaEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *rtaEndpointDataflowNumberTxt = new QTreeWidgetItem(rtaEndpointDataflowNumber);
                rtaEndpointDataflowLineEdit =  new QLineEdit(this);
                rtaEndpointDataflowLineEdit->setReadOnly(true);
                rtaEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(rtaEndpointDataflowNumberTxt, 0, rtaEndpointDataflowLineEdit);
                rtaEndpointDataflowNumber->addChild(rtaEndpointDataflowNumberTxt);

            QTreeWidgetItem *rtaEndpointDistributionServiceDescriptor = new QTreeWidgetItem(rtaEndpointServiceDescriptor);
            rtaEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *rtaEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(rtaEndpointDistributionServiceDescriptor);
                rtaEndpointDistributionComboBox = new QComboBox(this);
                rtaEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(rtaEndpointDistributionServiceDescriptorTxt, 0, rtaEndpointDistributionComboBox);
                rtaEndpointDistributionServiceDescriptor->addChild(rtaEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *rtaEndpointDataPoliciesDescriptor = new QTreeWidgetItem(rtaEndpointServiceDescriptor);
            rtaEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *rtaEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(rtaEndpointDataPoliciesDescriptor);
                rtaEndpointDataPoliciesComboBox =  new QComboBox(this);
                rtaEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(rtaEndpointDataPoliciesDescriptorTxt, 0, rtaEndpointDataPoliciesComboBox);
                rtaEndpointDataPoliciesDescriptor->addChild(rtaEndpointDataPoliciesDescriptorTxt);

        //RTA Properties Descriptors
        QTreeWidgetItem *rtaPropertiesServiceDescriptor = new QTreeWidgetItem(rtaServiceDescriptor);
        rtaPropertiesServiceDescriptor->setText(0, tr("Properties"));

            QTreeWidgetItem *rtaImuDescriptor = new QTreeWidgetItem(rtaPropertiesServiceDescriptor);
            rtaImuDescriptor->setText(0, tr("RTA IMU"));

                QTreeWidgetItem *rtaRateServiceDescriptor = new QTreeWidgetItem(rtaImuDescriptor);
                rtaRateServiceDescriptor->setText(0, tr("Rate"));
                    QTreeWidgetItem *rtaRateServiceDescriptorTxt = new QTreeWidgetItem(rtaRateServiceDescriptor);
                    rtaImuRateLineEdit =  new QLineEdit();
                    rtaImuRateLineEdit->setValidator(uint32Validator); // #WIP
                    ui->serviceDescTreeWidget->setItemWidget(rtaRateServiceDescriptorTxt, 0, rtaImuRateLineEdit);
                    rtaRateServiceDescriptor->addChild(rtaRateServiceDescriptorTxt);

        //RTA Label Descriptor
        QTreeWidgetItem *rtaLabelServiceDescriptor = new QTreeWidgetItem(rtaServiceDescriptor);
        rtaLabelServiceDescriptor->setText(0, tr("Label"));
        QTreeWidgetItem *rtaLabelServiceDescriptorTxt = new QTreeWidgetItem(rtaLabelServiceDescriptor);
        rtaLabelLineEdit =  new QLineEdit();
        rtaLabelLineEdit->setValidator(textValidator); // #WIP
        ui->serviceDescTreeWidget->setItemWidget(rtaLabelServiceDescriptorTxt, 0, rtaLabelLineEdit);
        rtaLabelServiceDescriptor->addChild(rtaLabelServiceDescriptorTxt);

        //RTA Save Configuration Button
        QTreeWidgetItem *saveRtaConfigItem = new QTreeWidgetItem(rtaServiceDescriptor);
        QPushButton *saveRtaConfigButton = new QPushButton("Save RTA Config", this);
        saveRtaConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveRtaConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(3); });

    ui->serviceDescTreeWidget->setItemWidget(saveRtaConfigItem, 0, saveRtaConfigButton);

    QList <QTreeWidgetItem *> rtaChildrenServiceDescriptors;
    rtaChildrenServiceDescriptors.append(rtaEndpointServiceDescriptor);
    rtaChildrenServiceDescriptors.append(rtaPropertiesServiceDescriptor);
    rtaChildrenServiceDescriptors.append(rtaLabelServiceDescriptor);
    rtaChildrenServiceDescriptors.append(saveRtaConfigItem);

    rtaServiceDescriptor->addChildren(rtaChildrenServiceDescriptors);

    rtaServiceDescriptor->setExpanded(true);
        rtaEndpointServiceDescriptor->setExpanded(true);
            rtaEndpointIDServiceDescriptor->setExpanded(true);
            rtaEndpointDataflowNumber->setExpanded(true);
            rtaEndpointDistributionServiceDescriptor->setExpanded(true);
            rtaEndpointDataPoliciesDescriptor->setExpanded(true);
        rtaPropertiesServiceDescriptor->setExpanded(true);
            rtaImuDescriptor->setExpanded(true);
                rtaRateServiceDescriptor->setExpanded(true);
        rtaLabelServiceDescriptor->setExpanded(true);
}

void sqanTestTool::addRtaDataPayloadUI()
{
    //RTA Data Payload
    QTreeWidgetItem *rtaDataPayload = new QTreeWidgetItem(ui->serviceDescDataPayloadTreeWidget);
    rtaDataPayload->setText(0, tr("RTA Data Payload"));

        //RTA Status Tuple
        QTreeWidgetItem *rtaStatusTuple = new QTreeWidgetItem(rtaDataPayload);
        rtaStatusTuple->setText(0, tr("RTA Status"));

            QTreeWidgetItem *rtaEnabledTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaEnabledTreeItem->setText(0, tr("RTA Enabled"));
                QTreeWidgetItem *rtaEnabledTxt = new QTreeWidgetItem(rtaEnabledTreeItem);
                rtaEnabledComboBox =  new QComboBox(this);
                rtaEnabledComboBox->insertItems(0, rtaEnabled);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaEnabledTxt, 0, rtaEnabledComboBox);
                rtaEnabledTreeItem->addChild(rtaEnabledTxt);

            QTreeWidgetItem *rtaModeTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaModeTreeItem->setText(0, tr("RTA Mode"));
                 QTreeWidgetItem *rtaModeTxt = new QTreeWidgetItem(rtaModeTreeItem);
                 rtaModeComboBox =  new QComboBox(this);
                 rtaModeComboBox->insertItems(0, rtaMode);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaModeTxt, 0, rtaModeComboBox);
                 rtaModeTreeItem->addChild(rtaModeTxt);

            QTreeWidgetItem *rtaPolarityTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaPolarityTreeItem->setText(0, tr("RTA Polarity"));
                 QTreeWidgetItem *rtaPolarityTxt = new QTreeWidgetItem(rtaPolarityTreeItem);
                 rtaPolarityComboBox =  new QComboBox(this);
                 rtaPolarityComboBox->insertItems(0, rtaPolarity);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaPolarityTxt, 0, rtaPolarityComboBox);
                 rtaPolarityTreeItem->addChild(rtaPolarityTxt);

            QTreeWidgetItem *rtaZoomTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaZoomTreeItem->setText(0, tr("RTA Zoom"));
                 QTreeWidgetItem *rtaZoomTxt = new QTreeWidgetItem(rtaZoomTreeItem);
                 rtaZoomComboBox =  new QComboBox(this);
                 rtaZoomComboBox->insertItems(0, rtaZoom);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaZoomTxt, 0, rtaZoomComboBox);
                 rtaZoomTreeItem->addChild(rtaZoomTxt);

            QTreeWidgetItem *rtaBubbleTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaBubbleTreeItem->setText(0, tr("RTA Bubble On/Off"));
                 QTreeWidgetItem *rtaBubbleTxt = new QTreeWidgetItem(rtaBubbleTreeItem);
                 rtaBubbleComboBox =  new QComboBox(this);
                 rtaBubbleComboBox->insertItems(0, rtaBubble);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaBubbleTxt, 0, rtaBubbleComboBox);
                 rtaBubbleTreeItem->addChild(rtaBubbleTxt);

            QTreeWidgetItem *rtaManualAlignmentTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaManualAlignmentTreeItem->setText(0, tr("RTA Manual Alignment"));
                 QTreeWidgetItem *rtaManualAlignmentTxt = new QTreeWidgetItem(rtaManualAlignmentTreeItem);
                 rtaManualAlignmentComboBox =  new QComboBox(this);
                 rtaManualAlignmentComboBox->insertItems(0, rtaManualAlignment);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaManualAlignmentTxt, 0, rtaManualAlignmentComboBox);
                 rtaManualAlignmentTreeItem->addChild(rtaManualAlignmentTxt);

            QTreeWidgetItem *rtaReticleInVideoTreeItem = new QTreeWidgetItem(rtaStatusTuple);
            rtaReticleInVideoTreeItem->setText(0, tr("RTA Reticle In Video"));
                 QTreeWidgetItem *rtaReticleInVideoTxt = new QTreeWidgetItem(rtaReticleInVideoTreeItem);
                 rtaReticleInVideoComboBox =  new QComboBox(this);
                 rtaReticleInVideoComboBox->insertItems(0, rtaReticleInVideo);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaReticleInVideoTxt, 0, rtaReticleInVideoComboBox);
                 rtaReticleInVideoTreeItem->addChild(rtaReticleInVideoTxt);

        // RTA Reticle Tuple
        QTreeWidgetItem *rtaReticleTuple = new QTreeWidgetItem(rtaDataPayload);
        rtaReticleTuple->setText(0, tr("RTA Reticle"));
            QTreeWidgetItem *rtaReticleTypeTreeItem = new QTreeWidgetItem(rtaReticleTuple);
            rtaReticleTypeTreeItem->setText(0, tr("RTA Reticle Type"));
                QTreeWidgetItem *rtaReticleTypeTxt = new QTreeWidgetItem(rtaReticleTypeTreeItem);
                rtaReticleTypeComboBox =  new QComboBox(this);
                rtaReticleTypeComboBox->insertItems(0, rtaReticleType);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaReticleTypeTxt, 0, rtaReticleTypeComboBox);
                rtaReticleTypeTreeItem->addChild(rtaReticleTypeTxt);

        // RTA IMU Data Tuple
        QTreeWidgetItem *rtaImuDataTuple = new QTreeWidgetItem(rtaDataPayload);
        rtaImuDataTuple->setText(0, tr("RTA IMU Data"));
            QTreeWidgetItem *rtaQuaternionRealTreeItem = new QTreeWidgetItem(rtaImuDataTuple);
            rtaQuaternionRealTreeItem->setText(0, tr("Quaternion Real"));
                QTreeWidgetItem *rtaQuaternionRealTxt = new QTreeWidgetItem(rtaQuaternionRealTreeItem);
                rtaQuaternionReal =  new QLineEdit(this);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaQuaternionRealTxt, 0, rtaQuaternionReal);
                rtaQuaternionRealTreeItem->addChild(rtaQuaternionRealTxt);

            QTreeWidgetItem *rtaQuaternionITreeItem = new QTreeWidgetItem(rtaImuDataTuple);
            rtaQuaternionITreeItem->setText(0, tr("Quaternion I"));
                QTreeWidgetItem *rtaQuaternionITxt = new QTreeWidgetItem(rtaQuaternionITreeItem);
                rtaQuaternionI =  new QLineEdit(this);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaQuaternionITxt, 0, rtaQuaternionI);
                rtaQuaternionITreeItem->addChild(rtaQuaternionITxt);

            QTreeWidgetItem *rtaQuaternionJTreeItem = new QTreeWidgetItem(rtaImuDataTuple);
            rtaQuaternionJTreeItem->setText(0, tr("Quaternion J"));
                QTreeWidgetItem *rtaQuaternionJTxt = new QTreeWidgetItem(rtaQuaternionJTreeItem);
                rtaQuaternionJ =  new QLineEdit(this);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaQuaternionJTxt, 0, rtaQuaternionJ);
                rtaQuaternionJTreeItem->addChild(rtaQuaternionJTxt);

            QTreeWidgetItem *rtaQuaternionKTreeItem = new QTreeWidgetItem(rtaImuDataTuple);
            rtaQuaternionKTreeItem->setText(0, tr("Quaternion K"));
                QTreeWidgetItem *rtaQuaternionKTxt = new QTreeWidgetItem(rtaQuaternionKTreeItem);
                rtaQuaternionK =  new QLineEdit(this);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(rtaQuaternionKTxt, 0, rtaQuaternionK);
                rtaQuaternionKTreeItem->addChild(rtaQuaternionKTxt);

        //RTA Data Payload Save Configuration Button
        QTreeWidgetItem *saveRtaDataPayloadConfigItem = new QTreeWidgetItem(rtaDataPayload);
        QPushButton *saveRtaDataPayloadConfigButton = new QPushButton("Save RTA Data Payload Config", this);
        saveRtaDataPayloadConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        connect(saveRtaDataPayloadConfigButton, SIGNAL(clicked()), this, SLOT(saveRtaDataPayloadConfigButton_clicked()));
        ui->serviceDescDataPayloadTreeWidget->setItemWidget(saveRtaDataPayloadConfigItem, 0, saveRtaDataPayloadConfigButton);

    QList <QTreeWidgetItem *> rtaDataPayloadChildren;
    rtaDataPayloadChildren.append(rtaStatusTuple);
    rtaDataPayloadChildren.append(rtaReticleTuple);
    rtaDataPayloadChildren.append(rtaImuDataTuple);
    rtaDataPayloadChildren.append(saveRtaDataPayloadConfigItem);

    rtaDataPayload->addChildren(rtaDataPayloadChildren);
}

/*************************************************
 *
 * addArOverlayDescriptor
 *
 * Property UI Elements:
 * arOverlayEndpointIdComboBox
 * arOverlayEndpointDistributionComboBox
 * arOverlayEndpointDataPoliciesComboBox
 * arOverlayLabelLineEdit
 * arOverlayMinWidthLineEdit
 * arOverlayMinHeightLineEdit
 * arOverlayRleVersionsLineEdit
 * **********************************************/
void sqanTestTool::addArOverlayDescriptorUI()
{
    //AR Service Descriptor Tree
    QTreeWidgetItem *arOverlayServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    arOverlayServiceDescriptor->setText(0, tr("AR Overlay"));

        //AR Endpoint Descriptors
        QTreeWidgetItem *arOverlayEndpointServiceDescriptor = new QTreeWidgetItem(arOverlayServiceDescriptor);
        arOverlayEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *arOverlayEndpointIDServiceDescriptor = new QTreeWidgetItem(arOverlayEndpointServiceDescriptor);
            arOverlayEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *arOverlayEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(arOverlayEndpointIDServiceDescriptor);
                arOverlayEndpointIdComboBox =  new QComboBox(this);
                arOverlayEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(arOverlayEndpointIDServiceDescriptorTxt, 0, arOverlayEndpointIdComboBox);
                arOverlayEndpointIDServiceDescriptor->addChild(arOverlayEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *arOverlayEndpointDataflowNumber = new QTreeWidgetItem(arOverlayEndpointServiceDescriptor);
            arOverlayEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *aroverlayEndpointDataflowNumberTxt = new QTreeWidgetItem(arOverlayEndpointDataflowNumber);
                arOverlayEndpointDataflowLineEdit =  new QLineEdit(this);
                arOverlayEndpointDataflowLineEdit->setReadOnly(true);
                arOverlayEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(aroverlayEndpointDataflowNumberTxt, 0, arOverlayEndpointDataflowLineEdit);
                arOverlayEndpointDataflowNumber->addChild(aroverlayEndpointDataflowNumberTxt);

            QTreeWidgetItem *arOverlayEndpointDistributionServiceDescriptor = new QTreeWidgetItem(arOverlayEndpointServiceDescriptor);
            arOverlayEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *arOverlayEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(arOverlayEndpointDistributionServiceDescriptor);
                arOverlayEndpointDistributionComboBox = new QComboBox(this);
                arOverlayEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(arOverlayEndpointDistributionServiceDescriptorTxt, 0, arOverlayEndpointDistributionComboBox);
                arOverlayEndpointDistributionServiceDescriptor->addChild(arOverlayEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *arOverlayEndpointDataPoliciesDescriptor = new QTreeWidgetItem(arOverlayEndpointServiceDescriptor);
            arOverlayEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *arEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(arOverlayEndpointDataPoliciesDescriptor);
                arOverlayEndpointDataPoliciesComboBox =  new QComboBox(this);
                arOverlayEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(arEndpointDataPoliciesDescriptorTxt, 0, arOverlayEndpointDataPoliciesComboBox);
                arOverlayEndpointDataPoliciesDescriptor->addChild(arEndpointDataPoliciesDescriptorTxt);

        //AR Overlay Properties Descriptors
        QTreeWidgetItem *arOverlayPropertiesServiceDescriptor = new QTreeWidgetItem(arOverlayServiceDescriptor);
        arOverlayPropertiesServiceDescriptor->setText(0, tr("Properties"));

            QTreeWidgetItem *arOverlayMinWidthDescriptor = new QTreeWidgetItem(arOverlayPropertiesServiceDescriptor);
            arOverlayMinWidthDescriptor->setText(0, tr("Minimum Width"));
                QTreeWidgetItem *arOverlayMinWidthServiceDescriptorTxt = new QTreeWidgetItem(arOverlayMinWidthDescriptor);
                arOverlayMinWidthLineEdit =  new QLineEdit();
                arOverlayMinWidthLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(arOverlayMinWidthServiceDescriptorTxt, 0, arOverlayMinWidthLineEdit);
                arOverlayMinWidthDescriptor->addChild(arOverlayMinWidthServiceDescriptorTxt);

            QTreeWidgetItem *arOverlayMinHeightDescriptor = new QTreeWidgetItem(arOverlayPropertiesServiceDescriptor);
            arOverlayMinHeightDescriptor->setText(0, tr("Minimum Height"));
                QTreeWidgetItem *arOverlayMinHeightServiceDescriptorTxt = new QTreeWidgetItem(arOverlayMinHeightDescriptor);
                arOverlayMinHeightLineEdit =  new QLineEdit();
                arOverlayMinHeightLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(arOverlayMinHeightServiceDescriptorTxt, 0, arOverlayMinHeightLineEdit);
                arOverlayMinHeightDescriptor->addChild(arOverlayMinHeightServiceDescriptorTxt);

            QTreeWidgetItem *arOverlayRleVersionsDescriptor = new QTreeWidgetItem(arOverlayPropertiesServiceDescriptor);
            arOverlayRleVersionsDescriptor->setText(0, tr("Supported RLE Versions"));
                QTreeWidgetItem *arOverlayRleVersionsServiceDescriptorTxt = new QTreeWidgetItem(arOverlayRleVersionsDescriptor);
                arOverlayRleVersionsLineEdit =  new QLineEdit();
                arOverlayRleVersionsLineEdit->setValidator(uint16Validator); // #WIP
                ui->serviceDescTreeWidget->setItemWidget(arOverlayRleVersionsServiceDescriptorTxt, 0, arOverlayRleVersionsLineEdit);
                arOverlayRleVersionsDescriptor->addChild(arOverlayRleVersionsServiceDescriptorTxt);

        //AR Overlay Label Descriptor
        QTreeWidgetItem *arOverlayLabelServiceDescriptor = new QTreeWidgetItem(arOverlayServiceDescriptor);
        arOverlayLabelServiceDescriptor->setText(0, tr("Label"));
            QTreeWidgetItem *arOverlayLabelServiceDescriptorTxt = new QTreeWidgetItem(arOverlayLabelServiceDescriptor);
            arOverlayLabelLineEdit =  new QLineEdit();
            arOverlayLabelLineEdit->setValidator(textValidator); // #WIP
            ui->serviceDescTreeWidget->setItemWidget(arOverlayLabelServiceDescriptorTxt, 0, arOverlayLabelLineEdit);
            arOverlayLabelServiceDescriptor->addChild(arOverlayLabelServiceDescriptorTxt);

        //AR Overlay Save Configuration Button
        QTreeWidgetItem *saveArConfigItem = new QTreeWidgetItem(arOverlayServiceDescriptor);
        QPushButton *saveArConfigButton = new QPushButton("Save AR Config", this);
        saveArConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveArConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(6); });

    ui->serviceDescTreeWidget->setItemWidget(saveArConfigItem, 0, saveArConfigButton);

    QList <QTreeWidgetItem *> arChildrenServiceDescriptors;
    arChildrenServiceDescriptors.append(arOverlayEndpointServiceDescriptor);
    arChildrenServiceDescriptors.append(arOverlayPropertiesServiceDescriptor);
    arChildrenServiceDescriptors.append(arOverlayLabelServiceDescriptor);
    arChildrenServiceDescriptors.append(saveArConfigItem);

    arOverlayServiceDescriptor->addChildren(arChildrenServiceDescriptors);

    arOverlayServiceDescriptor->setExpanded(true);
        arOverlayEndpointServiceDescriptor->setExpanded(true);
            arOverlayEndpointIDServiceDescriptor->setExpanded(true);
            arOverlayEndpointDataflowNumber->setExpanded(true);
            arOverlayEndpointDistributionServiceDescriptor->setExpanded(true);
            arOverlayEndpointDataPoliciesDescriptor->setExpanded(true);
        arOverlayPropertiesServiceDescriptor->setExpanded(true);
            arOverlayMinWidthDescriptor->setExpanded(true);
            arOverlayMinHeightDescriptor->setExpanded(true);
            arOverlayRleVersionsDescriptor->setExpanded(true);
        arOverlayLabelServiceDescriptor->setExpanded(true);
}

/*************************************************
 *
 * addPacketGeneratorDescriptor
 *
 * Property UI Elements:
 * packetGenEndpointIdComboBox
 * packetGenEndpointDistributionComboBox
 * packetGenEndpointDataPoliciesComboBox
 * packetGenLabelLineEdit
 * **********************************************/
void sqanTestTool::addPacketGeneratorDescriptorUI()
{
    //Packet Generator Service Descriptor Tree
    QTreeWidgetItem *packetGenServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    packetGenServiceDescriptor->setText(0, tr("Packet Generator"));

        //Packet Generator Endpoint Descriptors
        QTreeWidgetItem *packetGenEndpointServiceDescriptor = new QTreeWidgetItem(packetGenServiceDescriptor);
        packetGenEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *packetGenEndpointIDServiceDescriptor = new QTreeWidgetItem(packetGenEndpointServiceDescriptor);
            packetGenEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *packetGenEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(packetGenEndpointIDServiceDescriptor);
                packetGenEndpointIdComboBox =  new QComboBox(this);
                packetGenEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(packetGenEndpointIDServiceDescriptorTxt, 0, packetGenEndpointIdComboBox);
                packetGenEndpointIDServiceDescriptor->addChild(packetGenEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *packetGenEndpointDataflowNumber = new QTreeWidgetItem(packetGenEndpointServiceDescriptor);
            packetGenEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *packetGenEndpointDataflowNumberTxt = new QTreeWidgetItem(packetGenEndpointDataflowNumber);
                packetGenEndpointDataflowLineEdit =  new QLineEdit(this);
                packetGenEndpointDataflowLineEdit->setReadOnly(true);
                packetGenEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(packetGenEndpointDataflowNumberTxt, 0, packetGenEndpointDataflowLineEdit);
                packetGenEndpointDataflowNumber->addChild(packetGenEndpointDataflowNumberTxt);

            QTreeWidgetItem *packetGenEndpointDistributionServiceDescriptor = new QTreeWidgetItem(packetGenEndpointServiceDescriptor);
            packetGenEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *packetGenEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(packetGenEndpointDistributionServiceDescriptor);
                packetGenEndpointDistributionComboBox = new QComboBox(this);
                packetGenEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(packetGenEndpointDistributionServiceDescriptorTxt, 0, packetGenEndpointDistributionComboBox);
                packetGenEndpointDistributionServiceDescriptor->addChild(packetGenEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *packetGenEndpointDataPoliciesDescriptor = new QTreeWidgetItem(packetGenEndpointServiceDescriptor);
            packetGenEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *packetGenEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(packetGenEndpointDataPoliciesDescriptor);
                packetGenEndpointDataPoliciesComboBox =  new QComboBox(this);
                packetGenEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(packetGenEndpointDataPoliciesDescriptorTxt, 0, packetGenEndpointDataPoliciesComboBox);
                packetGenEndpointDataPoliciesDescriptor->addChild(packetGenEndpointDataPoliciesDescriptorTxt);

        //Packet Generator Label Descriptor
        QTreeWidgetItem *packetGenLabelServiceDescriptor = new QTreeWidgetItem(packetGenServiceDescriptor);
        packetGenLabelServiceDescriptor->setText(0, tr("Label"));
            QTreeWidgetItem *packetGenLabelServiceDescriptorTxt = new QTreeWidgetItem(packetGenLabelServiceDescriptor);
            packetGenLabelLineEdit =  new QLineEdit();
            packetGenLabelLineEdit->setValidator(textValidator); // #WIP
    //        packetGenLabelLineEdit->setValidator(new QRegExpValidator(QRegExp(""), &packetGenLabelLineEdit)); //#WIP
            ui->serviceDescTreeWidget->setItemWidget(packetGenLabelServiceDescriptorTxt, 0, packetGenLabelLineEdit);
            packetGenLabelServiceDescriptor->addChild(packetGenLabelServiceDescriptorTxt);

    //Control Save Configuration Button
    QTreeWidgetItem *savePacketGenConfigItem = new QTreeWidgetItem(packetGenServiceDescriptor);
    QPushButton *savePacketGenConfigButton = new QPushButton("Save Packet Generator Config", this);
    savePacketGenConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(savePacketGenConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(2); });

   // connect(savePacketGenConfigButton, SIGNAL(clicked()), this, SLOT(savePacketGeneratorConfigButton_clicked()));
    ui->serviceDescTreeWidget->setItemWidget(savePacketGenConfigItem, 0, savePacketGenConfigButton);

    QList <QTreeWidgetItem *> packetGenChildrenServiceDescriptors;
    packetGenChildrenServiceDescriptors.append(packetGenEndpointServiceDescriptor);
    packetGenChildrenServiceDescriptors.append(packetGenLabelServiceDescriptor);
    packetGenChildrenServiceDescriptors.append(savePacketGenConfigItem);

    packetGenServiceDescriptor->addChildren(packetGenChildrenServiceDescriptors);

    packetGenServiceDescriptor->setExpanded(true);
        packetGenEndpointServiceDescriptor->setExpanded(true);
            packetGenEndpointIDServiceDescriptor->setExpanded(true);
            packetGenEndpointDataflowNumber->setExpanded(true);
            packetGenEndpointDistributionServiceDescriptor->setExpanded(true);
            packetGenEndpointDataPoliciesDescriptor->setExpanded(true);
         packetGenLabelServiceDescriptor->setExpanded(true);
}

/*************************************************
 *
 * addStatusDescriptor
 *
 * Property UI Elements:
 * statusEndpointIdComboBox
 * statusEndpointDistributionComboBox
 * statusEndpointDataPoliciesComboBox
 * statusLabelLineEdit
 * **********************************************/
void sqanTestTool::addStatusDescriptorUI()
{
    //Status Service Descriptor Tree
    QTreeWidgetItem *statusServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    statusServiceDescriptor->setText(0, tr("Status"));

        //Status Endpoint Descriptors

        //Unicast Endpoint
        QTreeWidgetItem *statusUnicastEndpointServiceDescriptor = new QTreeWidgetItem(statusServiceDescriptor);
        statusUnicastEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *statusUnicastEndpointIDServiceDescriptor = new QTreeWidgetItem(statusUnicastEndpointServiceDescriptor);
            statusUnicastEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *statusUnicastEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(statusUnicastEndpointIDServiceDescriptor);
                statusEndpointIdComboBox =  new QComboBox(this);
                statusEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(statusUnicastEndpointIDServiceDescriptorTxt, 0, statusEndpointIdComboBox);
                statusUnicastEndpointIDServiceDescriptor->addChild(statusUnicastEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *statusUnicastEndpointDataflowNumber = new QTreeWidgetItem(statusUnicastEndpointServiceDescriptor);
            statusUnicastEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *statusEndpointDataflowNumberTxt = new QTreeWidgetItem(statusUnicastEndpointDataflowNumber);
                statusEndpointDataflowLineEdit =  new QLineEdit(this);
                statusEndpointDataflowLineEdit->setReadOnly(true);
                statusEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(statusEndpointDataflowNumberTxt, 0, statusEndpointDataflowLineEdit);
                statusUnicastEndpointDataflowNumber->addChild(statusEndpointDataflowNumberTxt);

            QTreeWidgetItem *statusUnicastEndpointDistributionServiceDescriptor = new QTreeWidgetItem(statusUnicastEndpointServiceDescriptor);
            statusUnicastEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *statusUnicastEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(statusUnicastEndpointDistributionServiceDescriptor);
                statusEndpointDistributionComboBox = new QComboBox(this);
                statusEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(statusUnicastEndpointDistributionServiceDescriptorTxt, 0, statusEndpointDistributionComboBox);
                statusUnicastEndpointDistributionServiceDescriptor->addChild(statusUnicastEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *statusUnicastEndpointDataPoliciesDescriptor = new QTreeWidgetItem(statusUnicastEndpointServiceDescriptor);
            statusUnicastEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *statusUnicastEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(statusUnicastEndpointDataPoliciesDescriptor);
                statusEndpointDataPoliciesComboBox =  new QComboBox(this);
                statusEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(statusUnicastEndpointDataPoliciesDescriptorTxt, 0, statusEndpointDataPoliciesComboBox);
                statusUnicastEndpointDataPoliciesDescriptor->addChild(statusUnicastEndpointDataPoliciesDescriptorTxt);

        //Status Label Descriptor
        QTreeWidgetItem *statusLabelServiceDescriptor = new QTreeWidgetItem(statusServiceDescriptor);
        statusLabelServiceDescriptor->setText(0, tr("Label"));
            QTreeWidgetItem *statusLabelServiceDescriptorTxt = new QTreeWidgetItem(statusLabelServiceDescriptor);
            statusLabelLineEdit =  new QLineEdit();
            statusLabelLineEdit->setValidator(textValidator); // #WIP
            ui->serviceDescTreeWidget->setItemWidget(statusLabelServiceDescriptorTxt, 0, statusLabelLineEdit);
            statusLabelServiceDescriptor->addChild(statusLabelServiceDescriptorTxt);

        //Status Save Configuration Button
        QTreeWidgetItem *saveStatusConfigItem = new QTreeWidgetItem(statusServiceDescriptor);
        QPushButton *saveStatusConfigButton = new QPushButton("Save Status Config", this);
        saveStatusConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveStatusConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(4); });

    ui->serviceDescTreeWidget->setItemWidget(saveStatusConfigItem, 0, saveStatusConfigButton);

    QList <QTreeWidgetItem *> statusChildrenServiceDescriptors;
    statusChildrenServiceDescriptors.append(statusUnicastEndpointServiceDescriptor);
    statusChildrenServiceDescriptors.append(statusUnicastEndpointDistributionServiceDescriptor);
    statusChildrenServiceDescriptors.append(statusLabelServiceDescriptor);
    statusChildrenServiceDescriptors.append(saveStatusConfigItem);

    statusServiceDescriptor->addChildren(statusChildrenServiceDescriptors);

    statusServiceDescriptor->setExpanded(true);
        statusUnicastEndpointServiceDescriptor->setExpanded(true);
            statusUnicastEndpointIDServiceDescriptor->setExpanded(true);
            statusUnicastEndpointDataflowNumber->setExpanded(true);
            statusUnicastEndpointDistributionServiceDescriptor->setExpanded(true);
            statusUnicastEndpointDataPoliciesDescriptor->setExpanded(true);
        statusLabelServiceDescriptor->setExpanded(true);
}

void sqanTestTool::addStatusDataPayloadUI()
{
    // Status Data Payload
    QTreeWidgetItem *statusDataPayload = new QTreeWidgetItem(ui->serviceDescDataPayloadTreeWidget);
    statusDataPayload->setText(0, tr("Status Data Payload"));

        // Battery Status Tuple
        QTreeWidgetItem *batteryStatusTuple = new QTreeWidgetItem(statusDataPayload);
        batteryStatusTuple->setText(0, tr("Battery Status"));

            QTreeWidgetItem *batteryLevelTreeItem = new QTreeWidgetItem(batteryStatusTuple);
            batteryLevelTreeItem->setText(0, tr("Battery Level"));
                QTreeWidgetItem *batteryLevelTxt = new QTreeWidgetItem(batteryLevelTreeItem);
                batteryLevelLineEdit =  new QLineEdit(this);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(batteryLevelTxt, 0, batteryLevelLineEdit);
                batteryLevelTreeItem->addChild(batteryLevelTxt);

            QTreeWidgetItem *batteryStateTreeItem = new QTreeWidgetItem(batteryStatusTuple);
            batteryStateTreeItem->setText(0, tr("Battery State"));
                 QTreeWidgetItem *batteryStateTxt = new QTreeWidgetItem(batteryStateTreeItem);
                 batteryStateComboBox =  new QComboBox(this);
                 batteryStateComboBox->insertItems(0, batteryStateStatus);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(batteryStateTxt, 0, batteryStateComboBox);
                 batteryStateTreeItem->addChild(batteryStateTxt);

            QTreeWidgetItem *batteryChargingTreeItem = new QTreeWidgetItem(batteryStatusTuple);
            batteryChargingTreeItem->setText(0, tr("Battery Charging"));
                 QTreeWidgetItem *batteryChargingTxt = new QTreeWidgetItem(batteryChargingTreeItem);
                 batteryChargingComboBox =  new QComboBox(this);
                 batteryChargingComboBox->insertItems(0, batteryChargingStatus);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(batteryChargingTxt, 0, batteryChargingComboBox);
                 batteryChargingTreeItem->addChild(batteryChargingTxt);

        // Device Status Tuple
        QTreeWidgetItem *deviceStatusTuple = new QTreeWidgetItem(statusDataPayload);
        deviceStatusTuple->setText(0, tr("Device Status"));

            QTreeWidgetItem *deviceStateTreeItem = new QTreeWidgetItem(deviceStatusTuple);
            deviceStateTreeItem->setText(0, tr("Device State"));
                 QTreeWidgetItem *deviceStateTxt = new QTreeWidgetItem(deviceStateTreeItem);
                 deviceStateComboBox =  new QComboBox(this);
                 deviceStateComboBox->insertItems(0, deviceStateStatus);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(deviceStateTxt, 0, deviceStateComboBox);
                 deviceStateTreeItem->addChild(deviceStateTxt);

            QTreeWidgetItem *displayStateTreeItem = new QTreeWidgetItem(deviceStatusTuple);
            displayStateTreeItem->setText(0, tr("Display State"));
                 QTreeWidgetItem *displayStateTxt = new QTreeWidgetItem(displayStateTreeItem);
                 displayStateComboBox =  new QComboBox(this);
                 displayStateComboBox->insertItems(0, displayStateStatus);
                 ui->serviceDescDataPayloadTreeWidget->setItemWidget(displayStateTxt, 0, displayStateComboBox);
                 displayStateTreeItem->addChild(displayStateTxt);

        // Weapon Status Tuple
        QTreeWidgetItem *weaponStatusTuple = new QTreeWidgetItem(statusDataPayload);
        weaponStatusTuple->setText(0, tr("Weapon Status"));

            QTreeWidgetItem *weaponSightModeTreeItem = new QTreeWidgetItem(weaponStatusTuple);
            weaponSightModeTreeItem->setText(0, tr("Weapon Sight Mode"));
                QTreeWidgetItem *weaponSightModeTxt = new QTreeWidgetItem(weaponSightModeTreeItem);
                weaponSightModeComboBox =  new QComboBox(this);
                weaponSightModeComboBox->insertItems(0, weaponSightModeStatus);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(weaponSightModeTxt, 0, weaponSightModeComboBox);
                weaponSightModeTreeItem->addChild(weaponSightModeTxt);

            QTreeWidgetItem *weaponActiveReticleIDTreeItem = new QTreeWidgetItem(weaponStatusTuple);
            weaponActiveReticleIDTreeItem->setText(0, tr("Active Reticle ID"));
                QTreeWidgetItem *weaponActiveReticleIDTxt = new QTreeWidgetItem(weaponActiveReticleIDTreeItem);
                weaponActiveReticleIDComboBox =  new QComboBox(this);
                weaponActiveReticleIDComboBox->insertItems(0, weaponActiveReticleIDStatus);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(weaponActiveReticleIDTxt, 0, weaponActiveReticleIDComboBox);
                weaponActiveReticleIDTreeItem->addChild(displayStateTxt);

            QTreeWidgetItem *weaponSymbologyStateTreeItem = new QTreeWidgetItem(weaponStatusTuple);
            weaponSymbologyStateTreeItem->setText(0, tr("Symbology State"));
                QTreeWidgetItem *weaponSymbologyStateTxt = new QTreeWidgetItem(weaponSymbologyStateTreeItem);
                weaponSymbologyStateComboBox =  new QComboBox(this);
                weaponSymbologyStateComboBox->insertItems(0, weaponSymbologyStateStatus);
                ui->serviceDescDataPayloadTreeWidget->setItemWidget(weaponSymbologyStateTxt, 0, weaponSymbologyStateComboBox);
                weaponSymbologyStateTreeItem->addChild(weaponSymbologyStateTxt);

        // Status Data Payload Save Configuration Button
        QTreeWidgetItem *saveStatusDataPayloadConfigItem = new QTreeWidgetItem(statusDataPayload);
        QPushButton *saveStatusDataPayloadConfigButton = new QPushButton("Save Status Data Payload Config", this);
        saveStatusDataPayloadConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        connect(saveStatusDataPayloadConfigButton, SIGNAL(clicked()), this, SLOT(saveStatusDataPayloadConfigButton_clicked()));
        ui->serviceDescDataPayloadTreeWidget->setItemWidget(saveStatusDataPayloadConfigItem, 0, saveStatusDataPayloadConfigButton);

        QList <QTreeWidgetItem *> statusDataPayloadChildren;
        statusDataPayloadChildren.append(batteryStatusTuple);
        statusDataPayloadChildren.append(deviceStatusTuple);
        statusDataPayloadChildren.append(weaponStatusTuple);
        statusDataPayloadChildren.append(saveStatusDataPayloadConfigItem);

        statusDataPayload->addChildren(statusDataPayloadChildren);
}

/*************************************************
 *
 * addSimpleUiDescriptor
 *
 * Property UI Elements:
 * simpleUiEndpointIdComboBox
 * simpleUiEndpointDistributionComboBox
 * simpleUiEndpointDataPoliciesComboBox
 * simpleUiLabelLineEdit
 * simpleUiButtonCountComboBox
 * simpleUiStateCountComboBox
 * **********************************************/
void sqanTestTool::addSimpleUiDescriptorUI()
{
    //Simple UI Service Descriptor Tree
    QTreeWidgetItem *simpleUiServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    simpleUiServiceDescriptor->setText(0, tr("Simple UI"));

        //Simple UI Endpoint Descriptors
        QTreeWidgetItem *simpleUiEndpointServiceDescriptor = new QTreeWidgetItem(simpleUiServiceDescriptor);
        simpleUiEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *simpleUiEndpointIDServiceDescriptor = new QTreeWidgetItem(simpleUiEndpointServiceDescriptor);
            simpleUiEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *simpleUiEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(simpleUiEndpointIDServiceDescriptor);
                simpleUiEndpointIdComboBox =  new QComboBox(this);
                simpleUiEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(simpleUiEndpointIDServiceDescriptorTxt, 0, simpleUiEndpointIdComboBox);
                simpleUiEndpointIDServiceDescriptor->addChild(simpleUiEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *simpleUiEndpointDataflowNumber = new QTreeWidgetItem(simpleUiEndpointServiceDescriptor);
            simpleUiEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *simpleuiEndpointDataflowNumberTxt = new QTreeWidgetItem(simpleUiEndpointDataflowNumber);
                simpleUiEndpointDataflowLineEdit =  new QLineEdit(this);
                simpleUiEndpointDataflowLineEdit->setReadOnly(true);
                simpleUiEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(simpleuiEndpointDataflowNumberTxt, 0, simpleUiEndpointDataflowLineEdit);
                simpleUiEndpointDataflowNumber->addChild(simpleuiEndpointDataflowNumberTxt);

            QTreeWidgetItem *simpleUiEndpointDistributionServiceDescriptor = new QTreeWidgetItem(simpleUiEndpointServiceDescriptor);
            simpleUiEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *simpleUiEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(simpleUiEndpointDistributionServiceDescriptor);
                simpleUiEndpointDistributionComboBox = new QComboBox(this);
                simpleUiEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(simpleUiEndpointDistributionServiceDescriptorTxt, 0, simpleUiEndpointDistributionComboBox);
                simpleUiEndpointDistributionServiceDescriptor->addChild(simpleUiEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *simpleUiEndpointDataPoliciesDescriptor = new QTreeWidgetItem(simpleUiEndpointServiceDescriptor);
            simpleUiEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *simpleUiEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(simpleUiEndpointDataPoliciesDescriptor);
                simpleUiEndpointDataPoliciesComboBox =  new QComboBox(this);
                simpleUiEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(simpleUiEndpointDataPoliciesDescriptorTxt, 0, simpleUiEndpointDataPoliciesComboBox);
                simpleUiEndpointDataPoliciesDescriptor->addChild(simpleUiEndpointDataPoliciesDescriptorTxt);

        //Simple UI Button Descriptor
        QTreeWidgetItem *simpleUiButtonServiceDescriptor = new QTreeWidgetItem(simpleUiServiceDescriptor);
        simpleUiButtonServiceDescriptor->setText(0, tr("Button"));

    //        QTreeWidgetItem *simpleUiTerminalIdServiceDescriptor = new QTreeWidgetItem(simpleUiButtonServiceDescriptor);
    //        simpleUiTerminalIdServiceDescriptor->setText(0, tr("Terminal ID"));
    //                QTreeWidgetItem *simpleUiTerminalIdServiceDescriptorTxt = new QTreeWidgetItem(simpleUiTerminalIdServiceDescriptor);
    //                simpleUiTerminalIdLineEdit =  new QLineEdit(); // #WIP Validator needed
    //                ui->serviceDescTreeWidget->setItemWidget(simpleUiTerminalIdServiceDescriptorTxt, 0, simpleUiTerminalIdLineEdit);
    //                simpleUiTerminalIdServiceDescriptor->addChild(simpleUiTerminalIdServiceDescriptorTxt);

    //        QTreeWidgetItem *simpleUiCapabilitiesServiceDescriptor = new QTreeWidgetItem(simpleUiButtonServiceDescriptor);
    //        simpleUiCapabilitiesServiceDescriptor->setText(0, tr("Capabilities"));
    //                QTreeWidgetItem *simpleUiCapabilitiesServiceDescriptorTxt = new QTreeWidgetItem(simpleUiCapabilitiesServiceDescriptor);
    //                simpleUiCapabilitiesLineEdit =  new QLineEdit(); // #WIP Validator needed
    //                ui->serviceDescTreeWidget->setItemWidget(simpleUiCapabilitiesServiceDescriptorTxt, 0, simpleUiCapabilitiesLineEdit);
    //                simpleUiCapabilitiesServiceDescriptor->addChild(simpleUiCapabilitiesServiceDescriptorTxt);

        QTreeWidgetItem *simpleUiButtonCountServiceDescriptor = new QTreeWidgetItem(simpleUiButtonServiceDescriptor);
        simpleUiButtonCountServiceDescriptor->setText(0, tr("Button Count"));
            QTreeWidgetItem *simpleUiButtonCountServiceDescriptorTxt = new QTreeWidgetItem(simpleUiButtonCountServiceDescriptor);
            simpleUiButtonCountComboBox =  new QComboBox();
            simpleUiButtonCountComboBox->insertItems(0, buttonCount);
            ui->serviceDescTreeWidget->setItemWidget(simpleUiButtonCountServiceDescriptorTxt, 0, simpleUiButtonCountComboBox);
            simpleUiButtonCountServiceDescriptor->addChild(simpleUiButtonCountServiceDescriptorTxt);

        QTreeWidgetItem *simpleUiStateCountServiceDescriptor = new QTreeWidgetItem(simpleUiButtonServiceDescriptor);
        simpleUiStateCountServiceDescriptor->setText(0, tr("State Count"));
            QTreeWidgetItem *simpleUiStateCountServiceDescriptorTxt = new QTreeWidgetItem(simpleUiStateCountServiceDescriptor);
            simpleUiStateCountServiceDescriptorTxt->setText(0, "State Count");
            simpleUiStateCountComboBox =  new QComboBox();
            simpleUiStateCountComboBox->insertItems(0, stateCount);
            ui->serviceDescTreeWidget->setItemWidget(simpleUiStateCountServiceDescriptorTxt, 0, simpleUiStateCountComboBox);
            simpleUiStateCountServiceDescriptor->addChild(simpleUiStateCountServiceDescriptorTxt);

        //Simple UI Label Descriptor
        QTreeWidgetItem *simpleUiLabelServiceDescriptor = new QTreeWidgetItem(simpleUiServiceDescriptor);
        simpleUiLabelServiceDescriptor->setText(0, tr("Label"));
            QTreeWidgetItem *simpleUiLabelServiceDescriptorTxt = new QTreeWidgetItem(simpleUiLabelServiceDescriptor);
            simpleUiLabelLineEdit =  new QLineEdit();
            simpleUiLabelLineEdit->setValidator(textValidator); // #WIP
            ui->serviceDescTreeWidget->setItemWidget(simpleUiLabelServiceDescriptorTxt, 0, simpleUiLabelLineEdit);
            simpleUiLabelServiceDescriptor->addChild(simpleUiLabelServiceDescriptorTxt);

        //Simple UI Save Configuration Button
        QTreeWidgetItem *saveSimpleUiConfigItem = new QTreeWidgetItem(simpleUiServiceDescriptor);
        QPushButton *saveSimpleUiConfigButton = new QPushButton("Save Simple UI Config", this);
        saveSimpleUiConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveSimpleUiConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(5); });

    ui->serviceDescTreeWidget->setItemWidget(saveSimpleUiConfigItem, 0, saveSimpleUiConfigButton);

    QList <QTreeWidgetItem *> simpleUiChildrenServiceDescriptors;
    simpleUiChildrenServiceDescriptors.append(simpleUiEndpointServiceDescriptor);
    simpleUiChildrenServiceDescriptors.append(simpleUiButtonServiceDescriptor);
    simpleUiChildrenServiceDescriptors.append(simpleUiLabelServiceDescriptor);
    simpleUiChildrenServiceDescriptors.append(saveSimpleUiConfigItem);

    simpleUiServiceDescriptor->addChildren(simpleUiChildrenServiceDescriptors);

    simpleUiServiceDescriptor->setExpanded(true);
        simpleUiEndpointServiceDescriptor->setExpanded(true);
            simpleUiEndpointIDServiceDescriptor->setExpanded(true);
            simpleUiEndpointDataflowNumber->setExpanded(true);
            simpleUiEndpointDistributionServiceDescriptor->setExpanded(true);
            simpleUiEndpointDataPoliciesDescriptor->setExpanded(true);
        simpleUiButtonServiceDescriptor->setExpanded(true);
            simpleUiButtonCountServiceDescriptor->setExpanded(true);
            simpleUiStateCountServiceDescriptor->setExpanded(true);
        simpleUiLabelServiceDescriptor->setExpanded(true);
}


/*************************************************
 *
 * addLrfDescriptor
 *
 * Property UI Elements:
 * lrfEndpointIdComboBox
 * lrfEndpointDistributionComboBox
 * lrfEndpointDataPoliciesComboBox
 * lrfLabelLineEdit
 * **********************************************/
void sqanTestTool::addLrfDescriptorUI()
{
    //LRF Service Descriptor Tree
    QTreeWidgetItem *lrfServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    lrfServiceDescriptor->setText(0, tr("LRF Service"));

        //LRF Endpoint Descriptors
        QTreeWidgetItem *lrfEndpointServiceDescriptor = new QTreeWidgetItem(lrfServiceDescriptor);
        lrfEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *lrfEndpointIDServiceDescriptor = new QTreeWidgetItem(lrfEndpointServiceDescriptor);
            lrfEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *lrfEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(lrfEndpointIDServiceDescriptor);
                lrfEndpointIdComboBox =  new QComboBox(this);
                lrfEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(lrfEndpointIDServiceDescriptorTxt, 0, lrfEndpointIdComboBox);
                lrfEndpointIDServiceDescriptor->addChild(lrfEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *lrfEndpointDataflowNumber = new QTreeWidgetItem(lrfEndpointServiceDescriptor);
            lrfEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *lrfEndpointDataflowNumberTxt = new QTreeWidgetItem(lrfEndpointDataflowNumber);
                lrfEndpointDataflowLineEdit =  new QLineEdit(this);
                lrfEndpointDataflowLineEdit->setReadOnly(true);
                lrfEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(lrfEndpointDataflowNumberTxt, 0, lrfEndpointDataflowLineEdit);
                lrfEndpointDataflowNumber->addChild(lrfEndpointDataflowNumberTxt);

            QTreeWidgetItem *lrfEndpointDistributionServiceDescriptor = new QTreeWidgetItem(lrfEndpointServiceDescriptor);
            lrfEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *lrfEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(lrfEndpointDistributionServiceDescriptor);
                lrfEndpointDistributionComboBox = new QComboBox(this);
                lrfEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(lrfEndpointDistributionServiceDescriptorTxt, 0, lrfEndpointDistributionComboBox);
                lrfEndpointDistributionServiceDescriptor->addChild(lrfEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *lrfEndpointDataPoliciesDescriptor = new QTreeWidgetItem(lrfEndpointServiceDescriptor);
            lrfEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *lrfEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(lrfEndpointDataPoliciesDescriptor);
                lrfEndpointDataPoliciesComboBox =  new QComboBox(this);
                lrfEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(lrfEndpointDataPoliciesDescriptorTxt, 0, lrfEndpointDataPoliciesComboBox);
                lrfEndpointDataPoliciesDescriptor->addChild(lrfEndpointDataPoliciesDescriptorTxt);

        //LRF Label Descriptor
        QTreeWidgetItem *lrfLabelServiceDescriptor = new QTreeWidgetItem(lrfServiceDescriptor);
        lrfLabelServiceDescriptor->setText(0, tr("Label"));
            QTreeWidgetItem *lrfLabelServiceDescriptorTxt = new QTreeWidgetItem(lrfLabelServiceDescriptor);
            lrfLabelLineEdit =  new QLineEdit();
            lrfLabelLineEdit->setValidator(textValidator); // #WIP
            ui->serviceDescTreeWidget->setItemWidget(lrfLabelServiceDescriptorTxt, 0, lrfLabelLineEdit);
            lrfLabelServiceDescriptor->addChild(lrfLabelServiceDescriptorTxt);

        //Control Save Configuration Button
        QTreeWidgetItem *saveLrfConfigItem = new QTreeWidgetItem(lrfServiceDescriptor);
        QPushButton *saveLrfConfigButton = new QPushButton("Save LRF Config", this);
        saveLrfConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    connect(saveLrfConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(7); });

    ui->serviceDescTreeWidget->setItemWidget(saveLrfConfigItem, 0, saveLrfConfigButton);

    QList <QTreeWidgetItem *> lrfChildrenServiceDescriptors;
    lrfChildrenServiceDescriptors.append(lrfEndpointServiceDescriptor);
    lrfChildrenServiceDescriptors.append(lrfLabelServiceDescriptor);
    lrfChildrenServiceDescriptors.append(saveLrfConfigItem);

    lrfServiceDescriptor->addChildren(lrfChildrenServiceDescriptors);

    lrfServiceDescriptor->setExpanded(true);
        lrfEndpointServiceDescriptor->setExpanded(true);
            lrfEndpointIDServiceDescriptor->setExpanded(true);
            lrfEndpointDataflowNumber->setExpanded(true);
            lrfEndpointDistributionServiceDescriptor->setExpanded(true);
            lrfEndpointDataPoliciesDescriptor->setExpanded(true);
        lrfLabelServiceDescriptor->setExpanded(true);
}

/*************************************************
 *
 * addLrfControlDescriptorUI
 *
 * Property UI Elements:
 * lrfControlEndpointIdComboBox
 * lrfControlEndpointDistributionComboBox
 * lrfControlEndpointDataPoliciesComboBox
 * lrfControlLabelLineEdit
 * **********************************************/
void sqanTestTool::addLrfControlDescriptorUI()
{
    //LRF Control Service Descriptor Tree
    QTreeWidgetItem *lrfControlServiceDescriptor = new QTreeWidgetItem(ui->serviceDescTreeWidget);
    lrfControlServiceDescriptor->setText(0, tr("LRF Control Service"));

        //LRF Control Endpoint Descriptors
        QTreeWidgetItem *lrfControlEndpointServiceDescriptor = new QTreeWidgetItem(lrfControlServiceDescriptor);
        lrfControlEndpointServiceDescriptor->setText(0, tr("Endpoint"));

            QTreeWidgetItem *lrfControlEndpointIDServiceDescriptor = new QTreeWidgetItem(lrfControlEndpointServiceDescriptor);
            lrfControlEndpointIDServiceDescriptor->setText(0, tr("Endpoint ID"));
                QTreeWidgetItem *lrfControlEndpointIDServiceDescriptorTxt = new QTreeWidgetItem(lrfControlEndpointIDServiceDescriptor);
                lrfControlEndpointIdComboBox =  new QComboBox(this);
                lrfControlEndpointIdComboBox->insertItems(0, unicastEndpointIds);
                ui->serviceDescTreeWidget->setItemWidget(lrfControlEndpointIDServiceDescriptorTxt, 0, lrfControlEndpointIdComboBox);
                lrfControlEndpointIDServiceDescriptor->addChild(lrfControlEndpointIDServiceDescriptorTxt);

            QTreeWidgetItem *lrfControlEndpointDataflowNumber = new QTreeWidgetItem(lrfControlEndpointServiceDescriptor);
            lrfControlEndpointDataflowNumber->setText(0, tr("Dataflow Number"));
                QTreeWidgetItem *lrfcontrolEndpointDataflowNumberTxt = new QTreeWidgetItem(lrfControlEndpointDataflowNumber);
                lrfControlEndpointDataflowLineEdit =  new QLineEdit(this);
                lrfControlEndpointDataflowLineEdit->setReadOnly(true);
                lrfControlEndpointDataflowLineEdit->setEnabled(false);
                ui->serviceDescTreeWidget->setItemWidget(lrfcontrolEndpointDataflowNumberTxt, 0, lrfControlEndpointDataflowLineEdit);
                lrfControlEndpointDataflowNumber->addChild(lrfcontrolEndpointDataflowNumberTxt);

            QTreeWidgetItem *lrfControlEndpointDistributionServiceDescriptor = new QTreeWidgetItem(lrfControlEndpointServiceDescriptor);
            lrfControlEndpointDistributionServiceDescriptor->setText(0, tr("Endpoint Distribution"));
                QTreeWidgetItem *lrfControlEndpointDistributionServiceDescriptorTxt = new QTreeWidgetItem(lrfControlEndpointDistributionServiceDescriptor);
                lrfControlEndpointDistributionComboBox = new QComboBox(this);
                lrfControlEndpointDistributionComboBox->insertItems(0, endpointDistribution);
                ui->serviceDescTreeWidget->setItemWidget(lrfControlEndpointDistributionServiceDescriptorTxt, 0, lrfControlEndpointDistributionComboBox);
                lrfControlEndpointDistributionServiceDescriptor->addChild(lrfControlEndpointDistributionServiceDescriptorTxt);

            QTreeWidgetItem *lrfControlEndpointDataPoliciesDescriptor = new QTreeWidgetItem(lrfControlEndpointServiceDescriptor);
            lrfControlEndpointDataPoliciesDescriptor->setText(0, tr("Data Policies"));
                QTreeWidgetItem *lrfControlEndpointDataPoliciesDescriptorTxt = new QTreeWidgetItem(lrfControlEndpointDataPoliciesDescriptor);
                lrfControlEndpointDataPoliciesComboBox =  new QComboBox(this);
                lrfControlEndpointDataPoliciesComboBox->insertItems(0, endpointDataPolicies);
                ui->serviceDescTreeWidget->setItemWidget(lrfControlEndpointDataPoliciesDescriptorTxt, 0, lrfControlEndpointDataPoliciesComboBox);
                lrfControlEndpointDataPoliciesDescriptor->addChild(lrfControlEndpointDataPoliciesDescriptorTxt);

        //LRF Control Label Descriptor
        QTreeWidgetItem *lrfControlLabelServiceDescriptor = new QTreeWidgetItem(lrfControlServiceDescriptor);
        lrfControlLabelServiceDescriptor->setText(0, tr("Label"));
        QTreeWidgetItem *lrfControlLabelServiceDescriptorTxt = new QTreeWidgetItem(lrfControlLabelServiceDescriptor);
        lrfControlLabelLineEdit =  new QLineEdit();
        lrfControlLabelLineEdit->setValidator(textValidator); // #WIP
        ui->serviceDescTreeWidget->setItemWidget(lrfControlLabelServiceDescriptorTxt, 0, lrfControlLabelLineEdit);
        lrfControlLabelServiceDescriptor->addChild(lrfControlLabelServiceDescriptorTxt);

        //Control Save Configuration Button
        QTreeWidgetItem *saveLrfControlConfigItem = new QTreeWidgetItem(lrfControlServiceDescriptor);
        QPushButton *saveLrfControlConfigButton = new QPushButton("Save LRF Config", this);
        saveLrfControlConfigButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

        connect(saveLrfControlConfigButton, &QPushButton::clicked, [=] { saveCommonConfigButton_clicked(8); });

        ui->serviceDescTreeWidget->setItemWidget(saveLrfControlConfigItem, 0, saveLrfControlConfigButton);

        QList <QTreeWidgetItem *> lrfControlChildrenServiceDescriptors;
        lrfControlChildrenServiceDescriptors.append(lrfControlEndpointServiceDescriptor);
        lrfControlChildrenServiceDescriptors.append(lrfControlLabelServiceDescriptor);
        lrfControlChildrenServiceDescriptors.append(saveLrfControlConfigItem);

        lrfControlServiceDescriptor->addChildren(lrfControlChildrenServiceDescriptors);

    lrfControlServiceDescriptor->setExpanded(true);
        lrfControlEndpointServiceDescriptor->setExpanded(true);
            lrfControlEndpointIDServiceDescriptor->setExpanded(true);
            lrfControlEndpointDataflowNumber->setExpanded(true);
            lrfControlEndpointDistributionServiceDescriptor->setExpanded(true);
            lrfControlEndpointDataPoliciesDescriptor->setExpanded(true);
        lrfControlLabelServiceDescriptor->setExpanded(true);
}


/*****************************************************
 * Data Traffic Testing
 * **************************************************/
void sqanTestTool::createAvailableLinksTable()
{
    availableLinksTableLock.lock();

    // Table used in the Data Traffic tab
    // Format Active Links Table
    ui->availableLinksTable->setRowCount(20);
    ui->availableLinksTable->setColumnCount(4);

    QStringList tableHeader;
    tableHeader << "Src" << "Dest" << "" << "";
    ui->availableLinksTable->setHorizontalHeaderLabels(tableHeader);
    ui->availableLinksTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->availableLinksTable->horizontalHeader()->setResizeContentsPrecision(true);
    ui->availableLinksTable->horizontalHeader()->setStyleSheet("QHeaderView { font-size: 8pt; }");
    //ui->availableLinksTable->horizontalHeader()->setStretchLastSection(false);

    availableLinksTableLock.unlock();

    ui->availableLinksTable->repaint();
    ui->availableLinksTable->update();
}

void sqanTestTool::clearAvailableLinksTable()
{
    availableLinksTableLock.lock();

    // Remove the active link items and delete any resources
    QTableWidgetItem *sourceTableItem;
    QTableWidgetItem *destinationTableItem;

    for (int i=0; i<ui->availableLinksTable->rowCount(); i++)
    {
        sourceTableItem = ui->availableLinksTable->item(i,0);
        if ( sourceTableItem != nullptr )
        {
            delete(sourceTableItem);
            ui->availableLinksTable->takeItem(i,0);
        }

        destinationTableItem = ui->availableLinksTable->item(i,1);
        if ( destinationTableItem != nullptr )
        {
            delete(destinationTableItem);
            ui->availableLinksTable->takeItem(i,1);
        }
    }

    // Deselects all selected items
    ui->availableLinksTable->clearSelection();

    // Disconnect all signals from table widget ! important !
    ui->availableLinksTable->disconnect();

    // Remove all items
    ui->availableLinksTable->clearContents();

    // Set row count to default (remove rows)
    ui->availableLinksTable->setRowCount(defaultRegSrvRowColCount);

    // Set column count to default (remove columns)
    ui->availableLinksTable->setColumnCount(defaultRegSrvRowColCount);

    availableLinksTableLock.unlock();

    ui->availableLinksTable->repaint();
    ui->availableLinksTable->update();
}

void sqanTestTool::addAvailableLink(uint8_t row, uint8_t sourceIndex, uint8_t destIndex)
{
    availableLinksTableLock.lock();

    //Source Node
    QTableWidgetItem *sourceTableItem = new QTableWidgetItem;
    sourceTableItem->setText(DynArray[sourceIndex].devName);
    ui->availableLinksTable->setItem(row, 0, sourceTableItem);

    //Destination Node
    QTableWidgetItem *destinationTableItem = new QTableWidgetItem;
    destinationTableItem->setText(DynArray[destIndex].devName);
    ui->availableLinksTable->setItem(row, 1, destinationTableItem);

    QPushButton *sendVideoStartButton;
    QPushButton *packetGenStartButton;

    //Start Send video Button
    sendVideoStartButton = new QPushButton( "Send Video", this );
    sendVideoStartButton->setCheckable(true);
    sendVideoStartButton->setChecked(false);
    sendVideoStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    ui->availableLinksTable->setCellWidget(row, 2, sendVideoStartButton);
    connect(sendVideoStartButton, SIGNAL(toggled(bool)), this, SLOT(videoGenStart()));

    //Start Packet Generator data transmition Button
    packetGenStartButton = new QPushButton( "Send Data", this );
    packetGenStartButton->setCheckable(true);
    packetGenStartButton->setChecked(false);
    packetGenStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");

    ui->availableLinksTable->setCellWidget(row, 3, packetGenStartButton);
    connect(packetGenStartButton, SIGNAL(toggled(bool)), this, SLOT(packetGenStart()));

    availableLinksTableLock.unlock();

    ui->availableLinksTable->repaint();
    ui->availableLinksTable->update();
}

void sqanTestTool::removeAvailableLinks(QString devName)
{
    availableLinksTableLock.lock();

    QTableWidgetItem *sourceTableItem = nullptr;
    QTableWidgetItem *destinationTableItem = nullptr;

    // Remove all rows that have this device as the Src
    for (uint8_t rowIndex=0; rowIndex<ui->availableLinksTable->rowCount()+1; rowIndex++)
    {
        sourceTableItem = ui->availableLinksTable->item(rowIndex,0);
        destinationTableItem = ui->availableLinksTable->item(rowIndex,1);
        if ( ((sourceTableItem != nullptr) && (sourceTableItem->text().contains(devName))) ||
             ((destinationTableItem != nullptr) && (destinationTableItem->text().contains(devName))) )
        {
            for (uint8_t colIndex=0; colIndex<ui->availableLinksTable->columnCount(); colIndex++)
            {
                QTableWidgetItem *item = ui->availableLinksTable->item(rowIndex, colIndex);
                if ( item != nullptr )
                {
                    delete(item);
                }
            }
            ui->availableLinksTable->removeRow(rowIndex);
        }
    }
    ui->availableLinksTable->setRowCount(20);
    availableLinksTableLock.unlock();

    ui->availableLinksTable->setCurrentCell(0, 0);
    ui->availableLinksTable->repaint();
    ui->availableLinksTable->update();
}

/*JR
sqanTestTool::CommonButtonParameters* sqanTestTool::commonStartButtonProcess(uint16_t descriptorId)
{
    struct CommonButtonParameters* cbp = new CommonButtonParameters();

    cbp->cindex = ui->availableLinksTable->selectionModel()->currentIndex();

    QTableWidgetItem *sourceColumn = ui->availableLinksTable->item(cbp->cindex.row(),0);
    cbp->source = sourceColumn->text();

    //Get Source Index From Source Map
    cbp->sourceIndex = sourceMap[cbp->source];

    QTableWidgetItem *destColumn = ui->availableLinksTable->item(cbp->cindex.row(),1);
    cbp->destination = destColumn->text();

    cbp->destIndex = sourceMap[cbp->destination];

    //code block find out if unicast/multi/broadcast
    QString serviceName = "";
    QString serviceDistribution = "";
    serviceName.append(cbp->source);

    switch (descriptorId)
    {
        case SqanSolNet::PacketGenerator:
        {
            serviceName.append(" Packet Generator");
            break;
        }
        case SqanSolNet::Video:
        {
            serviceName.append(" Video");
            break;
        }
        default:
        {
            serviceName.append(" Unkown Service Id");
            break;
        }
    }

    qDebug() << "serviceName = " << serviceName;
    serviceDistribution = endpointDistributionMap.value(serviceName);

    if (serviceDistribution == "Unicast")
    {
        cbp->endpointDistribution = 0;
    }
    else if (serviceDistribution == "Broadcast")
    {
        cbp->endpointDistribution = 0xFF;
    }
    else if (serviceDistribution == "Multicast")
    {
        // Get multicast peerIndex from registerServicesTable entry
        for (uint8_t colIndex=1; colIndex < ui->registerServicesTable->columnCount(); colIndex++)
        {
            QLabel *label = nullptr;
            label = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);

            if ( (label != nullptr) &&
                 (label->text().contains(serviceName)) )
            {
                for (auto itr = checkBoxMap.begin(); itr != checkBoxMap.end(); ++itr)
                {
                    tableCell *thisCell = itr.value();

                    if ( (thisCell->rowIndex == cbp->destIndex) && (thisCell->colIndex == colIndex) )
                    {
                        QVariant multicastGroupId = thisCell->distributionLabel->property("peerIndex");
                        cbp->endpointDistribution = multicastGroupId.toUInt();
                        break;
                    }
                }
                break;
            }
        }
    }
    else
    {
        cbp->endpointDistribution = 0;
    }

    for (auto itr = DynArray[cbp->sourceIndex].peerList.begin(); itr != DynArray[cbp->sourceIndex].peerList.end(); ++itr)
    {
        SolNetPeerListEntry *entry = *itr;

        if ( entry->devName == cbp->destination )
        {
            // This is the peerIndex of the destination from the source's peerList
            cbp->peerIndex = entry->peerIndex;
            break;
        }
    }

    // Need to flowId for all cases: unicast, broadcast, multicast
    // Find the dataflowId for the required service on the source
    std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[cbp->sourceIndex].sqanSolNet->GetServices();

    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
    {
        SqanSolNet::sqanSolNetServiceEntry *serviceEntry = &(*services)[dataflowId];

        if ( (serviceEntry->inUse) &&
             (serviceEntry->serviceDesc.header.descriptorId == descriptorId) )
        {
            cbp->flowId = dataflowId;
            break;
        }
    }

    return (cbp);
}


void sqanTestTool::packetGenStart()
{
    QPushButton *packetGenStartButton = (QPushButton *)sender();

    if ( packetGenStartButton->isChecked() )
    {
        packetGenStartButton->setText("In Progress");
        packetGenStartButton->setStyleSheet("QPushButton { background-color : lightgreen; color : darkblue; padding: 1px; }");
        packetGenStartButton->repaint();

        struct CommonButtonParameters *cbp = commonStartButtonProcess(SqanSolNet::PacketGenerator);
        SqanSolNet::sqanSolNetServiceEntry *entry = nullptr;
        bool isRegistered = false;

        // dataflowId of 0xFF means we didn't find the service
        if ( cbp->flowId != 0xFF )
        {
            // Get our services, find the PacketGenerator Service, and see if we are registered
            std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[cbp->sourceIndex].sqanSolNet->GetServices();

            if ( (*services)[cbp->flowId].inUse )
            {
                entry = &((*services)[cbp->flowId]);

                // Check: unicast and registered
                // OR broadcast or multicast
                if ( entry->serviceDesc.header.descriptorId == SqanSolNet::PacketGenerator )
                {
                    if ( ((entry->endpointDistribution == 0) &&
                           entry->registeredPeers[cbp->peerIndex].peerRegistered) ||
                          ((entry->endpointDistribution >= 0x01) && (entry->endpointDistribution <= 0xFF)) )
                    {
                        isRegistered = true;
                    }
                }
             }
        }

        // if we found the service and
        // we are registered
        if ( (entry != nullptr) &&
              isRegistered  )
        {
            // Setup SolNet entry for sending SolNet Data Packets
            SendParams *sendParams = new SendParams();
            sendParams->cbp = cbp;
            sendParams->entry = entry;

            // Open dialog window for test selection
            packetGenerator *packetGenPtr = new packetGenerator(this, sendParams);

            // Connect buttons to this instance of packetGenerator
            connect(packetGenPtr->buttonGroup, SIGNAL(buttonClicked(int)), packetGenPtr, SLOT(onGroupButtonClicked(int)));

            //Register Callback
            DynArray[cbp->destIndex].sqanSolNet->RegisterReceiveDataCallbackFunction(SqanSolNet::defaultPeerIndex, cbp->flowId, packetGenerator::receiveDataCallback, (void *)packetGenPtr);

            packetGenPtr->setAttribute(Qt::WA_DeleteOnClose, true);
            packetGenPtr->show();
            packetGenPtr->raise();
        }
        else
        {
            displayMessage("Not registered for this service");

            packetGenStartButton->setText("Send Data");
            packetGenStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
            packetGenStartButton->setChecked(false);
            packetGenStartButton->repaint();
        }

        packetGenStartButton->setText("Send Data");
        packetGenStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        packetGenStartButton->setChecked(false);
        packetGenStartButton->repaint();
    }
    else
    {
        packetGenStartButton->setText("Send Data");
        packetGenStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        packetGenStartButton->setChecked(false);
        packetGenStartButton->repaint();
    }

    ui->availableLinksTable->repaint();
}


//! Called when the Send Video button is pushed on the data
//! traffic generator page
void sqanTestTool::videoGenStart()
{
    QPushButton *videoStartButton = (QPushButton *)sender();

    if ( videoStartButton->isChecked() )
    {
        videoStartButton->setText("In Progress");
        videoStartButton->setStyleSheet("QPushButton { background-color : lightgreen; color : darkblue; padding: 1px; }");
        videoStartButton->repaint();

        struct CommonButtonParameters *cbp = commonStartButtonProcess(SqanSolNet::Video);
        SqanSolNet::sqanSolNetServiceEntry *entry = nullptr;
        bool isRegistered = false;

        // dataflowId of 0xFF means we didn't find the service
        if ( cbp->flowId != 0xFF )
        {
            // Get our services, find the PacketGenerator Service, and see if we are registered
            std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[cbp->sourceIndex].sqanSolNet->GetServices();

            if ( (*services)[cbp->flowId].inUse )
            {
                entry = &((*services)[cbp->flowId]);

                // Check: unicast and registered
                // OR broadcast or multicast
                if ( entry->serviceDesc.header.descriptorId == SqanSolNet::Video )
                {
                    if ( ((entry->endpointDistribution == 0) &&
                           entry->registeredPeers[cbp->peerIndex].peerRegistered) ||
                          ((entry->endpointDistribution >= 0x01) && (entry->endpointDistribution <= 0xFF)) )
                    {
                        isRegistered = true;
                    }
                }
             }
        }

        // if we found the service and
        // we are registered
        if ( (entry != nullptr) &&
              isRegistered  )
        {
            // Setup SolNet entry for sending SolNet Data Packets
            SendParams *sendParams = new SendParams();
            sendParams->cbp = cbp;
            sendParams->entry = entry;

            // Open dialog window for test selection
            videoGenerator *videoGenPtr = new videoGenerator(this, sendParams);

            connect(videoGenPtr->buttonGroup, SIGNAL(buttonClicked(int)), videoGenPtr, SLOT(onGroupButtonClicked(int)));

            //Register Callback
            DynArray[cbp->destIndex].sqanSolNet->RegisterReceiveDataCallbackFunction(SqanSolNet::defaultPeerIndex, cbp->flowId, videoGenerator::receiveVideoDataCallback, (void *)videoGenPtr);

            videoGenPtr->setAttribute(Qt::WA_DeleteOnClose, true);
            videoGenPtr->show();
            videoGenPtr->raise();
        }
        else
        {
            displayMessage("Not Registered for this service");
            videoStartButton->setText("Send Video");
            videoStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
            videoStartButton->setChecked(false);
            videoStartButton->repaint();
        }

        videoStartButton->setText("Send Video");
        videoStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        videoStartButton->setChecked(false);
        videoStartButton->repaint();
    }
    else
    {
        videoStartButton->setText("Send Video");
        videoStartButton->setStyleSheet("QPushButton { background-color : lightgrey; color : darkblue; padding: 1px; }");
        videoStartButton->setChecked(false);
        videoStartButton->repaint();
    }

    ui->availableLinksTable->repaint();
}

*/

/****************************************************
* SolNet Service Descriptor Configuration slots
* ***************************************************/
/*JR
void sqanTestTool::on_pushServiceDescAddButton()
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("DynArrayIndex", std::to_string(dai).c_str());
    dbLogger->dbLog(dblr);

    // Open dialog window
//    serviceChooser *sc = new serviceChooser(dbLogger, ui->deviceComboBox->currentText(), ui->serviceDescListWidget);
    serviceChooser *sc = new serviceChooser(dbLogger, DynArray[dai].devType, ui->serviceDescListWidget);
    connect(sc, SIGNAL(chooseService(const QString &)), this, SLOT(serviceChosen(const QString &)));
    sc->setAttribute(Qt::WA_DeleteOnClose);
    sc->exec(); // blocks execution until dialog is closed

}
*/

void sqanTestTool::on_pushServiceDescRemoveButton()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    // #WIP Need to implement a way to remove saved service descriptors from the radio
}

void sqanTestTool::on_pushDescPlanImportButton()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    QListWidgetItem *selected = ui->descPlansListWidget->currentItem();
    if (selected ) {
//        openDatabase();
        if (dbConnected) {
            //JR importStoredServiceParameters(selected);
//            closeDatabase();
        }
    }
}

void sqanTestTool::on_pushDescPlanExportButton()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    if ( ui->serviceDescListWidget->count() ) {
//        openDatabase();
        if (dbConnected) {
            //JR exportSavedServiceParameters();
//            closeDatabase();
        }
    }
}

/*JR
void sqanTestTool::serviceChosen(const QString &service)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->getUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    // add the service QString (returned from serviceChooser) to serviceDescListWidget
    QString unsavedStr = service + UNSAVED_STR;
    ui->serviceDescListWidget->addItem(unsavedStr);

    // refresh serviceDescTreeWidget with the appropriate populators, e.g. addControlDescriptorUI()
    showDefaultServiceParameters(service);
}

void sqanTestTool::showDefaultServiceParameters(QString service)
{
    service.remove(UNSAVED_STR);
    ui->serviceDescTreeWidget->clear();

    if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control)) ) {
        addControlDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA)) ) {
        addRtaDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status)) ) {
        addStatusDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator)) ) {
        addPacketGeneratorDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video)) ) {
        addVideoDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI)) ) {
        addSimpleUiDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF)) ) {
        addLrfDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl)) ) {
        addLrfControlDescriptorUI();
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay)) ) {
        addArOverlayDescriptorUI();
    }

    ui->serviceDescTreeWidget->update();
}

void sqanTestTool::on_serviceDescListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString service = item->text();
    int currentIndex = ui->deviceComboBox->currentIndex();

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", service.toStdString());
    dblr.addUIObservation("currentIndex", std::to_string(currentIndex));
    dbLogger->dbLog(dblr);

    if (service.contains(UNSAVED_STR, Qt::CaseSensitive)) {
        ui->serviceDescriptorPlainTextEdit->appendPlainText("Loading default parameters for " + service);
        showDefaultServiceParameters(service);
    } else {
        int dataflowId = service.split(indexDelim)[0].toInt();
        service = service.remove(QString::number(dataflowId) + indexDelim);
        ui->serviceDescriptorPlainTextEdit->appendPlainText("Loading saved parameters for " + service + " at DataFlow " + QString::number(dataflowId));
        showSavedServiceParameters(service, dataflowId, currentIndex);
    }
}

void sqanTestTool::showSavedServiceParameters(QString service, int dataflowId, int currentIndex)
{
    std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[currentIndex].sqanSolNet->GetServices();
    SqanSolNet::sqanSolNetServiceEntry *serviceEntry = nullptr;
    ui->serviceDescTreeWidget->clear();
//    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
//    {
    serviceEntry = &(*services)[static_cast<uint8_t>(dataflowId)];
    if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control))
         && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Control ) {
        addControlDescriptorUI();
        loadControlDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::RTA ) {
        addRtaDescriptorUI();
        loadRtaDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status ) {
        addStatusDescriptorUI();
        loadStatusDescriptorUI(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::PacketGenerator ) {
        addPacketGeneratorDescriptorUI();
        loadPacketGeneratorDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Video ) {
        addVideoDescriptorUI();
        loadVideoDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::SimpleUI ) {
        addSimpleUiDescriptorUI();
        loadSimpleUiDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRF ) {
        addLrfDescriptorUI();
        loadLrfDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRFControl ) {
        addLrfControlDescriptorUI();
        loadLrfControlDescriptor(serviceEntry);
    } else if ( service == QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay))
                && serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::AROverlay ) {
        addArOverlayDescriptorUI();
        loadArOverlayDescriptor(serviceEntry);
    }
    ui->serviceDescTreeWidget->update();
}


void sqanTestTool::on_pushPrintServicesButton()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    for (uint8_t i=0; i<MAX_PEERS; i++)
    {
        SqanInterface *sqanInterface = DynArray[i].sqanInterface;
        if ( (sqanInterface != nullptr) && (DynArray[i].isReady) )
        {
            SqanSolNet *sqanSolNet = sqanInterface->GetSqanSolNet();
            sqanSolNet->PrintServices();
        }
    }
}
*/
void sqanTestTool::on_cancelPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Enable Other Tabs After Cancelling Firmware Update
    for(int a = 0; a < ui->tabWidget->count()-1; a++)
    {
        ui->tabWidget->setTabEnabled(a, true);
    }

    ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setTabEnabled(updateFirmwareTabWidgetLocation, false);
}

void sqanTestTool::on_actionAttenuator_Control_triggered()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //TODO: Add Installation Path
//    QString path = "//home//ctuser//Desktop//SQAN//common//pylib//";
    QString path = qApp->applicationDirPath()+"///Attenuator//common//pylib//";
//    qDebug() << path;
    QString command("python");
    QStringList params = QStringList() << "atten_app.py";

    QProcess *process = new QProcess(this);
    process->startDetached(command, params, path);
    process->waitForFinished();
    process->close();
}

//void sqanTestTool::openDatabase()
//{
//    //Setup SQAN Database Connection
//    db.setHostName("127.0.0.1");
//    db.setPort(6432);
//    db.setDatabaseName("sqantt");
//    db.setUserName("postgres");
//    db.setPassword("password");

//    //Open Database
//    dbConnected = db.open();

//    //Check If Database Is Open
//    if(dbConnected)
//    {
//        qDebug() << "Database connection opened";
//    }
//    else
//    {
//        displayMessage(db.lastError().text());
//        qDebug() << db.lastError().text();
//        qDebug("Failed to open database connection");
//    }
//}

//void sqanTestTool::closeDatabase()
//{
//    //Close Database
//    db.close();

//    dbConnected = false;

//    //Check If Database Is Open
//    if(!dbConnected){
//        qDebug() << "Database connection closed";
////        displayMessage("Database Close");
//    }
//    else {
//        displayMessage(db.lastError().text());
//        qDebug() << db.lastError().text();
//        qDebug() << "Failed to close database connection";
//    }
//}
/*JR
void sqanTestTool::on_serviceDescTreeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    QTreeWidgetItem *curItem = item;
    std::vector <std::string> vCur(1);
    std::stringstream oss;

    if(curItem)
    {
        vCur[0] = curItem->text(column).toStdString();
        while(curItem->parent())
        {
            curItem = curItem->parent();
            vCur.push_back(">");
            vCur.push_back(curItem->text(0).toStdString()); // is the "column" arg relevant to parents as well?
        }
        for(std::vector<std::string>::reverse_iterator rit = vCur.rbegin(); rit != vCur.rend(); ++rit)
        {
            oss << *rit;
        }
    }
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("current", oss.str());
    dbLogger->dbLog(dblr);

    if(vCur.size() == 1)
    {
        if(vCur[0] == std::string("Video"))
        {
            addVideoDataPayloadUI();
        }
        else if(vCur[0] == std::string("RTA"))
        {
            addRtaDataPayloadUI();
        }
        else if(vCur[0] == std::string("Status"))
        {
            //addStatusDataPayloadUI();
        }
    }
}
*/

QString sqanTestTool::getDeviceName()
{
    return deviceNameLineEdit->text();
}

QString sqanTestTool::getDeviceSerialNumber()
{
    return deviceSerialNumberLineEdit->text();
}

QString sqanTestTool::getDeviceManufacturer()
{
    return deviceManufacturerLineEdit->text();
}

QString sqanTestTool::getDeviceFriendlyName()
{
    return deviceFriendlyNameLineEdit->text();
}

int sqanTestTool::getEndpointId(QComboBox *endpointIdComboBox)
{
    return endpointIdComboBox->currentText().toInt();
}

int sqanTestTool::getEndpointDistribution(QComboBox *endpointDistributionComboBox, uint8_t selected)
{
    int itemIndex = endpointDistributionComboBox->currentIndex();
    uint8_t endpointDistribution = 0;

    switch (itemIndex)
    {
        case 0:
        default:
        {
            // Send out on one of the SQAN message endpoints
            endpointDistribution = 0;
            break;
        }
        case 1:
        {
            // Send out broadcast
            endpointDistribution = 0xFF;
            break;
        }
        case 2:
        {
            // Select Multicast Address From Multicast Group List
            bool ok;
            QString mcastAddressInput = QInputDialog::getItem(this, tr("Join Multicast Group"), tr("Select Multicast Address: "),
                                                              DynArray[selected].mcastGroupList, 0,
                                                              false, &ok);

            if (ok && !mcastAddressInput.isEmpty())
            {
                uint16_t mcastAddressInt = mcastAddressInput.toUInt();
                if ( mcastAddressInt == DynArray[selected].multicastGroup.startGroup.mcastAddress )
                {
                    endpointDistribution = DynArray[selected].multicastGroup.startGroup.mcastPeerIndex;
                }
                else if ( mcastAddressInt == DynArray[selected].multicastGroup.joinGroup.mcastAddress )
                {
                    endpointDistribution = DynArray[selected].multicastGroup.joinGroup.mcastAddress;
                }
                else
                {
                    // Couldn't find the group - set unicast
                    endpointDistribution = 0;
                }
            }
            break;
        }
    }

    return (endpointDistribution);
}

int sqanTestTool::getEndpointDistributionIndex(uint8_t endpointDistribution)
{
    qDebug() << Q_FUNC_INFO << " endpointDistribution=" << endpointDistribution;
    // inverse operation of sqanTestTool::getEndpointDistribution()
    switch (endpointDistribution)
    {
    case 0: return (0);
    case 255: return (1);
    }
    if (endpointDistribution > 0 && endpointDistribution < 255) {
        return (2);
    } else {
        qDebug() << Q_FUNC_INFO << " unexpected endpointDistribution: " << endpointDistribution;
        return (0);
    }
}

 /*JR
uint8_t sqanTestTool::getEndpointDataPolicies(QComboBox *endpointDataPoliciesComboBox)
{
    uint8_t dataPolicies = SqanSolNet::HiNoDiscard;  // default

    switch (endpointDataPoliciesComboBox->currentIndex())
    {
    case 1:
    {
        dataPolicies = SqanSolNet::HiDiscard;
        break;
    }
    case 2:
    {
        dataPolicies = SqanSolNet::LowNoDiscard;
        break;
    }
    case 3:
    {
        dataPolicies = SqanSolNet::LowDiscard;
        break;
    }
    case 0:
    default:
    {
        dataPolicies = SqanSolNet::HiNoDiscard;
        break;
    }
    }

    return (dataPolicies);
}

int sqanTestTool::getEndpointDataPoliciesIndex(uint8_t dataPolicies)
{
    // inverse operation of sqanTestTool::getEndpointDataPolicies()
    switch (dataPolicies)
    {
    case SqanSolNet::HiNoDiscard: return (0);
    case SqanSolNet::HiDiscard: return (1);
    case SqanSolNet::LowNoDiscard: return (2);
    case SqanSolNet::LowDiscard: return (3);
    default:
    {
        qDebug() << Q_FUNC_INFO << " unexpected dataPolicies: " << dataPolicies;
        return (0);
    }
    }
}

uint8_t sqanTestTool::getButtonCount(QComboBox *simpleUiButtonCountComboBox)
{
    uint8_t buttonCount = SqanSolNet::TwoButtons;  // default

    switch (simpleUiButtonCountComboBox->currentIndex())
    {
    case 0:
    {
        buttonCount = SqanSolNet::OneButton;
        break;
    }
    case 1:
    {
        buttonCount = SqanSolNet::TwoButtons;
        break;
    }
    case 2:
    {
        buttonCount = SqanSolNet::ThreeButtons;
        break;
    }
    default:
    {
        buttonCount = SqanSolNet::TwoButtons;
        break;
    }
    }

    return (buttonCount);
}

int sqanTestTool::getButtonCountIndex(uint8_t buttonCount)
{
    // inverse operation of sqanTestTool::getButtonCount()
    switch (buttonCount)
    {
    case SqanSolNet::OneButton: return (0);
    case SqanSolNet::TwoButtons: return (1);
    case SqanSolNet::ThreeButtons: return (2);
    default:
    {
        qDebug() << Q_FUNC_INFO << " unexpected buttonCount: " << buttonCount;
        return (1);
    }
    }
}

uint8_t sqanTestTool::getStateCount(QComboBox *simpleUiStateCountComboBox)
{
    uint8_t stateCount = SqanSolNet::TwoState;  // default

    switch (simpleUiStateCountComboBox->currentIndex())
    {
    case 0:
    {
        stateCount = SqanSolNet::OneState;
        break;
    }
    case 1:
    {
        stateCount = SqanSolNet::TwoState;
        break;
    }
    default:
    {
        stateCount = SqanSolNet::TwoState;
        break;
    }
    }

    return (stateCount);
}

int sqanTestTool::getStateCountIndex(uint8_t stateCount)
{
    // inverse operation of sqanTestTool::getStateCount()
    switch (stateCount)
    {
    case SqanSolNet::OneState: return (0);
    case SqanSolNet::TwoState: return (1);
    default:
    {
        qDebug() << Q_FUNC_INFO << " unexpected stateCount: " << stateCount;
        return (1);
    }
    }
}

 */

int sqanTestTool::getLabel(QLineEdit *labelLineEdit, uint8_t *labelBuf)
{
    int length = 0;
    if ( !labelLineEdit->text().isEmpty() )
    {
        length = labelLineEdit->text().length()+1; // +1 for NULL terminator
        memcpy(labelBuf, (uint8_t *)labelLineEdit->text().toStdString().c_str(), length);
    }
    return (length);
}

/*JR
uint16_t sqanTestTool::getVideoFormatResolutionXServiceDescriptor()
{
    uint16_t xRes = 0;
    if ( !videoFormatResolutionXServiceDescriptorLineEdit->text().isEmpty() )
    {
        xRes = videoFormatResolutionXServiceDescriptorLineEdit->text().toInt();
    }

    return (xRes);
}

uint16_t sqanTestTool::getVideoFormatResolutionYServiceDescriptor()
{
    uint16_t yRes = 0;
    if ( !videoFormatResolutionYServiceDescriptorLineEdit->text().isEmpty() )
    {
        yRes = videoFormatResolutionYServiceDescriptorLineEdit->text().toInt();
    }

    return (yRes);
}

float sqanTestTool::getVideoFormatFrameRateDescriptor()
{
    float frameRate = 0.0;
    if ( !videoFormatFrameRateDescriptorLineEdit->text().isEmpty() )
    {
        frameRate = videoFormatFrameRateDescriptorLineEdit->text().toFloat();
    }

    return (frameRate);
}

uint16_t sqanTestTool::getVideoFormatBitDepthDescriptor()
{
    uint16_t bitDepth = 0;
    if ( !videoFormatBitDepthDescriptorLineEdit->text().isEmpty() )
    {
        bitDepth = videoFormatBitDepthDescriptorLineEdit->text().toInt();
    }

    return (bitDepth);
}


uint16_t sqanTestTool::getVideoFormatPixelFormatDescriptor()
{
    uint16_t pixelFormat = 0;

    switch (videoFormatPixelFormatDescriptorComboBox->currentIndex())
    {
        case 0:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_Y;
            break;
        }
        case 1:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_NV21;
            break;
        }
        case 2:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_YUY2;
            break;
        }
        case 3:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_NV16;
            break;
        }
        case 4:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGBA_4444;
            break;
        }
        case 5:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGBA_8888;
            break;
        }
        case 6:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGBA_5551;
            break;
        }
        case 7:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGBX_8888;
            break;
        }
        case 8:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGB_332;
            break;
        }
        case 9:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGB_565;
            break;
        }
        case 10:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_RGB_888;
            break;
        }
        default:
        {
            pixelFormat = SqanSolNet::PIXEL_FORMAT_Y;
            break;
        }
    }

    return (pixelFormat);
}

int sqanTestTool::getVideoFormatPixelFormatDescriptorIndex(uint16_t pixelFormat)
{
    // inverse operation of sqanTestTool::getEndpointDistribution()
    switch (pixelFormat)
    {
        case SqanSolNet::PIXEL_FORMAT_Y: return (0);
        case SqanSolNet::PIXEL_FORMAT_NV21: return (1);
        case SqanSolNet::PIXEL_FORMAT_YUY2: return (2);
        case SqanSolNet::PIXEL_FORMAT_NV16: return (3);
        case SqanSolNet::PIXEL_FORMAT_RGBA_4444: return (4);
        case SqanSolNet::PIXEL_FORMAT_RGBA_8888: return (5);
        case SqanSolNet::PIXEL_FORMAT_RGBA_5551: return (6);
        case SqanSolNet::PIXEL_FORMAT_RGBX_8888: return (7);
        case SqanSolNet::PIXEL_FORMAT_RGB_332: return (8);
        case SqanSolNet::PIXEL_FORMAT_RGB_565: return (9);
        case SqanSolNet::PIXEL_FORMAT_RGB_888: return (10);
        default:
        {
            qDebug() << Q_FUNC_INFO << " unexpected pixelFormat: " << pixelFormat;
            return (0);
        }
    }
}

uint16_t sqanTestTool::getVideoProtocolRawDescriptor()
{
    uint16_t videoProtocol = 0;

    switch (videoProtocolRawDescriptorComboBox->currentIndex())
    {
        case 0:
        {
            videoProtocol = SqanSolNet::VIDEO_PROTOCOL_FVTS;
            break;
        }
        default:
        {
            videoProtocol = SqanSolNet::VIDEO_PROTOCOL_FVTS;
            break;
        }
    }

    return (videoProtocol);
}

int sqanTestTool::getVideoProtocolRawDescriptorIndex(uint16_t videoProtocol)
{
    // inverse operation of sqanTestTool::getVideoProtocolRawDescriptor()
    switch (videoProtocol)
    {
        case SqanSolNet::VIDEO_PROTOCOL_FVTS: return (0);
        default:
        {
            qDebug() << Q_FUNC_INFO << " unexpected videoProtocol: " << videoProtocol;
            return (0);
        }
    }
}

float sqanTestTool::getVideoIfovDescriptor()
{
    float videoIfov = 0.0;
    if ( !videoIfovDescriptorLineEdit->text().isEmpty() )
    {
        videoIfov = videoIfovDescriptorLineEdit->text().toInt();
    }

    return (videoIfov);
}

uint16_t sqanTestTool::getVideoPrincipalPointXDescriptor()
{
    uint16_t videoPPX = 0;
    if ( !videoPPointXLineEdit->text().isEmpty() )
    {
        videoPPX = videoPPointXLineEdit->text().toInt();
    }

    return (videoPPX);
}

uint16_t sqanTestTool::getVideoPrincipalPointYDescriptor()
{
    uint16_t videoPPY = 0;
    if ( !videoPPointYLineEdit->text().isEmpty() )
    {
        videoPPY = videoPPointYLineEdit->text().toInt();
    }

    return (videoPPY);
}

float sqanTestTool::getVideoLensDistortionCoefficientsDescriptor()
{
    float videoLensDistortionCoeff = 0;
    if ( !videoLensDistortionCoefficientsLineEdit->text().isEmpty() )
    {
        videoLensDistortionCoeff = videoLensDistortionCoefficientsLineEdit->text().toFloat();
    }

    return (videoLensDistortionCoeff);
}

uint16_t sqanTestTool::getVideoControlBitmapDescriptor()
{
    uint16_t videoControlBitmap = 0;
    if ( !videoControlBitmapLineEdit->text().isEmpty() )
    {
        videoControlBitmap = videoControlBitmapLineEdit->text().toInt();
    }

    return (videoControlBitmap);
}

uint16_t sqanTestTool::getArOverlayMinWidthDescriptor()
{
    uint16_t arOverlayMinWidth = 0;
    if ( !arOverlayMinWidthLineEdit->text().isEmpty() )
    {
        arOverlayMinWidth = arOverlayMinWidthLineEdit->text().toInt();
    }

    return (arOverlayMinWidth);
}
*/

uint16_t sqanTestTool::getArOverlayMinHeightDescriptor()
{
    uint16_t arOverlayMinHeight = 0;
    if ( !arOverlayMinHeightLineEdit->text().isEmpty() )
    {
        arOverlayMinHeight = arOverlayMinHeightLineEdit->text().toInt();
    }

    return (arOverlayMinHeight);
}

uint8_t sqanTestTool::getArOverlayRleVersionsDescriptor()
{
    uint8_t arOverlayRleVersions = 0;
    if ( !arOverlayRleVersionsLineEdit->text().isEmpty() )
    {
        arOverlayRleVersions = arOverlayRleVersionsLineEdit->text().toInt();
    }

    return (arOverlayRleVersions);
}
uint32_t sqanTestTool::getRtaImuRate()
{
    uint32_t imuRate = 0;
    if ( !rtaImuRateLineEdit->text().isEmpty() )
    {
        imuRate = rtaImuRateLineEdit->text().toInt();
    }

    return (imuRate);
}

void sqanTestTool::on_clearUpdateFirmwareTextPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    ui->firmwareUpdateTextEdit->clear();
}

void sqanTestTool::on_clearServiceDescriptorTextPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    ui->serviceDescriptorPlainTextEdit->clear();
}

void sqanTestTool::on_saveServiceDescriptorTextPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save Service Descriptor Log"), QCoreApplication::applicationDirPath(),
                                                        "Text File (*.txt)");

    if(!saveFileName.isNull())
    {
        QFile outFile(saveFileName);

        if(!outFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return;
        }

        QTextStream out(&outFile);
        out << ui->serviceDescriptorPlainTextEdit->toPlainText() << endl;
    }
}

void sqanTestTool::saveCommonConfigButton_clicked(int appIndex)
{
    //Get Selected Index
    uint8_t currentIndex = ui->deviceComboBox->currentIndex();
    QString selectedDevice = ui->deviceComboBox->currentText();
    int dataflowId = -1;
    QString serviceText = "";

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("selectedDevice", selectedDevice.toStdString().c_str());
    dblr.addUIObservation("currentIndex", std::to_string(currentIndex));
    dblr.addUIObservation("appIndex", std::to_string(appIndex));
    dbLogger->dbLog(dblr);

    switch (appIndex)
    {
       /*JR case 0:
        {
            dataflowId = getControlServiceDescriptor(currentIndex,appIndex);
           //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control));
            break;
        }
        case 1:
        {
            dataflowId = getVideoServiceDescriptor(currentIndex,appIndex);
           //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video));
            break;
        }
        case 2:
        {
            dataflowId = getPacketGeneratorServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
            break;
        }
        case 3:
        {
            dataflowId = getRtaServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA));
            break;
        }
        case 4:
        {
            dataflowId = getStatusServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status));
            break;
        }
        case 5:
        {
            dataflowId = getSimpleUiServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI));
            break;
        }
        case 6:
        {
            dataflowId = getArOverlayServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay));
            break;
        }
        case 7:
        {
            dataflowId = getLrfServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF));
            break;
        }
        case 8:
        {
            dataflowId = getLrfControlServiceDescriptor(currentIndex,appIndex);
            //JR serviceText = QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl));
            break;
        }*/
        default:
            break;
    }

    ui->serviceDescriptorPlainTextEdit->appendPlainText(selectedDevice + " " + serviceText + " Service Descriptor Saved to DataFlow " + QString::number(dataflowId));

    /*JR QString pat = QRegExp::escape(serviceText) + QRegExp::escape(UNSAVED_STR);
    auto matchingServiceList = ui->serviceDescListWidget->findItems(pat, Qt::MatchRegExp);
    if (matchingServiceList.length() == 1) {
        matchingServiceList[0]->setText(QString::number(dataflowId) + indexDelim + serviceText);
    } else if (matchingServiceList.length() > 1) {
        qDebug() << Q_FUNC_INFO << " Found multiple matching services in matchingServiceList.  Only 1 expected.";
    }


    auto unsavedServiceList = ui->serviceDescListWidget->findItems(QRegExp::escape(UNSAVED_STR), Qt::MatchRegExp);
    ui->serviceDescGroupBox->setTitle(QString::number(ui->serviceDescListWidget->count() - unsavedServiceList.length()) + " Service Descriptors");
    */
}

void sqanTestTool::saveVideoDataPayloadConfigButton_clicked()
{
//    //Get Selected Index
//    int currentIndex = ui->deviceComboBox->currentIndex();
//    QString selectedDevice = ui->deviceComboBox->currentText();
//    int status = 0;

//    status = getVideoDataPayload(currentIndex);
//    switch (status)
//    {
//        case 0:
//        {
//            ui->serviceDescriptorPlainTextEdit->appendPlainText(selectedDevice + " Video Data Payload Saved");
//            break;
//        }
//        case -1:
//        {
//            displayMessage("Video Service dataflowId must be > 0");
//            break;
//        }
//        case -2:
//        {
//            displayMessage("Video Service dataflowId in use by another service");
//            break;
//        }
//        default:
//            break;
//    }
}

void sqanTestTool::saveRtaDataPayloadConfigButton_clicked()
{
    //Get Selected Index
    int currentIndex = ui->deviceComboBox->currentIndex();
    QString selectedDevice = ui->deviceComboBox->currentText();
    int status = 0;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("selectedDevice", selectedDevice.toStdString().c_str());
    dblr.addUIObservation("currentIndex", std::to_string(currentIndex));
    dbLogger->dbLog(dblr);

    //JR status = sendRtaDataPayload(currentIndex);
    switch (status)
    {
        case 0:
        {
            ui->serviceDescriptorPlainTextEdit->appendPlainText(selectedDevice + " RTA Data Payload Saved");
            break;
        }
        case -1:
        {
            displayMessage("RTA Service dataflowId must be > 0");
            break;
        }
        case -2:
        {
            displayMessage("RTA Service dataflowId in use by another service");
            break;
        }
        default:
            break;
    }
}

void sqanTestTool::saveStatusDataPayloadConfigButton_clicked()
{
    //Get Selected Index
    int currentIndex = ui->deviceComboBox->currentIndex();
    QString selectedDevice = ui->deviceComboBox->currentText();
    int status = 0;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("selectedDevice", selectedDevice.toStdString().c_str());
    dblr.addUIObservation("currentIndex", std::to_string(currentIndex));
    dbLogger->dbLog(dblr);

    status = sendStatusDataPayload(currentIndex);
    switch (status)
    {
        case 0:
        {
            ui->serviceDescriptorPlainTextEdit->appendPlainText(selectedDevice + " Status Data Payload Saved");
            break;
        }
        case -1:
        {
            displayMessage("Status Service dataflowId must be > 0");
            break;
        }
        case -2:
        {
            displayMessage("Status Service dataflowId in use by another service");
            break;
        }
        default:
            break;
    }
}

/*JR
 * int sqanTestTool::getControlServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear the buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    serviceEntry.serviceControlDesc.header.length = SqanSolNet::selectorSize + sizeof(serviceEntry.serviceControlDesc.protocolId) + sizeof(serviceEntry.serviceControlDesc.reserved);
    serviceEntry.serviceControlDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::Control;

    // Set some defaults
    serviceEntry.serviceControlDesc.serviceSelector = 0;
    serviceEntry.serviceControlDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( deviceEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        deviceEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)deviceEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(deviceEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    endpointDesc.dataPolicies = getEndpointDataPolicies(deviceEndpointDataPoliciesComboBox);
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceControlDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();
    serviceEntry.serviceControlDesc.protocolId = 0;
    serviceEntry.serviceControlDesc.reserved = 0;

    // Device Name Child Descriptor
    std::string deviceNameString = getDeviceName().toStdString();
    if ( !deviceNameString.empty() )
    {
        SqanSolNet::sqanSolNetPropertyDeviceDesc devNameDesc;
        memset(&devNameDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyDeviceDesc));
        devNameDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceName;
        memcpy(devNameDesc.devString, deviceNameString.c_str(), deviceNameString.size()+1);
        // String must be multiple of 4 bytes
        devNameDesc.header.length = deviceNameString.size()+1;
        // Length should be divisible by 4
        while ( (devNameDesc.header.length % 4) > 0 )
        {
            devNameDesc.header.length++;
        }
        // Add Device Name Property Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devNameDesc, (SqanSolNet::commonHeaderSize + devNameDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + devNameDesc.header.length;
        }
    }

    // Device Serial Number Child Descriptor
    std::string deviceSerialNoString = getDeviceSerialNumber().toStdString();
    if ( !deviceSerialNoString.empty() )
    {
        SqanSolNet::sqanSolNetPropertyDeviceDesc devSerialNoDesc;
        memset(&devSerialNoDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyDeviceDesc));
        memset(devSerialNoDesc.devString, 0, 256);
        devSerialNoDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber;
        memcpy(devSerialNoDesc.devString, deviceSerialNoString.c_str(), deviceSerialNoString.size()+1);
        // String must be multiple of 4 bytes
        devSerialNoDesc.header.length = deviceSerialNoString.size()+1;
        while ( (devSerialNoDesc.header.length % 4) > 0 )
        {
            devSerialNoDesc.header.length++;
        }

        // Add Device Serial Number Property Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devSerialNoDesc, (SqanSolNet::commonHeaderSize + devSerialNoDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + devSerialNoDesc.header.length;
        }
    }

    // Device Manufacturer Child Descriptor
    std::string deviceManufacturerString = getDeviceManufacturer().toStdString();
    if ( !deviceManufacturerString.empty() )
    {
        SqanSolNet::sqanSolNetPropertyDeviceDesc devManufacturerDesc;
        memset(&devManufacturerDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyDeviceDesc));
        devManufacturerDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer;
        memcpy(devManufacturerDesc.devString, deviceManufacturerString.c_str(), deviceManufacturerString.size()+1);

        // String must be multiple of 4 bytes
        devManufacturerDesc.header.length = deviceManufacturerString.size()+1;
        while ( (devManufacturerDesc.header.length % 4) > 0 )
        {
            devManufacturerDesc.header.length++;
        }

        // Add Device Manufacturer Property Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devManufacturerDesc, (SqanSolNet::commonHeaderSize + devManufacturerDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + devManufacturerDesc.header.length;
        }
    }

    // Device Friendly Name Descriptor
    std::string deviceFriendlyNameString = getDeviceFriendlyName().toStdString();
    if ( !deviceFriendlyNameString.empty() )
    {
        SqanSolNet::sqanSolNetPropertyDeviceDesc devFriendlyNameDesc;
        memset(&devFriendlyNameDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyDeviceDesc));
        devFriendlyNameDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName;
        memcpy(devFriendlyNameDesc.devString, deviceFriendlyNameString.c_str(), deviceFriendlyNameString.size()+1);

        // String must be multiple of 4 bytes
        devFriendlyNameDesc.header.length = deviceFriendlyNameString.size()+1;
        while ( (devFriendlyNameDesc.header.length % 4) > 0 )
        {
            devFriendlyNameDesc.header.length++;
        }
        // Add Device Friendly Name Property Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devFriendlyNameDesc, (SqanSolNet::commonHeaderSize + devFriendlyNameDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + devFriendlyNameDesc.header.length;
        }
    }

    serviceEntry.serviceControlDesc.header.length += offset;
    serviceEntry.serviceControlDesc.childBlock = &deviceChildblock[0];

    // Add this Control service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    return (serviceEntry.dataflowId);
}

int sqanTestTool::getVideoServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;

    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear the buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Video - with Device child descriptors
    // *****************************************
    // Video Class Descriptor - with child descriptors
    // Video Format Descriptor
    // Video Protocol Descriptor
    // Video IFOV Descriptor
    // Video Principal Point Descriptor
    // Video Lens Distortion Descriptor -- Commented Out
    // Video Control Descriptor -- Commented Out
    // *****************************************

    // Add all the property descriptors to the child block
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::Video;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( videoEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        videoEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)videoEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(videoEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    endpointDesc.dataPolicies = getEndpointDataPolicies(videoEndpointDataPoliciesComboBox);
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    //See if user wants broadcast or multicast
    //Pass to addServiceToRegisterServicesTable below
    uint8_t endpointDistribution = getEndpointDistribution(videoEndpointDistributionComboBox, currentIndex);
    uint8_t multicastGroupId = 0x01;

    if ( (endpointDistribution >= 0x01) && (endpointDistribution <= 0xFE) )
    {
        //The user selected multicast
        //Add the multicast service descriptor
        SqanSolNet::sqanSolNetPropertyMulticastGroupDesc multicastDescriptor;
        multicastDescriptor.header.length = sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc) - SqanSolNet::commonHeaderSize;
        multicastDescriptor.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup;
        multicastDescriptor.mcastAddress = endpointDistribution;
        if ( endpointDistribution == DynArray[currentIndex].multicastGroup.startGroup.mcastAddress )
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.startGroup.mcastPeerIndex;
        }
        else
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.joinGroup.mcastPeerIndex;
        }
        multicastGroupId = multicastDescriptor.mcastGroupId;
        multicastDescriptor.reserved1 = 0x00;
        //Get the MAC Address for this device
        uint8_t *sqanMacAddr = DynArray[currentIndex].sqanIdentity->GetMacAddress();
        for (int i=0; i<6; i++)
        {
            multicastDescriptor.mcastMacAddress[i] = sqanMacAddr[i];
        }
        multicastDescriptor.reserved2 = 0x00;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &multicastDescriptor, sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc);
    }
    else if (endpointDistribution == 0xFF)
    {
        //The user selected broadcast
        //Add and EndpointDescriptor with broadcast indicated
        SqanSolNet::sqanSolNetPropertyEndpointDesc broadcastEndpointDesc;
        broadcastEndpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
        broadcastEndpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
        broadcastEndpointDesc.dataflowId = endpointDesc.dataflowId;  //use the same index into the services table
        broadcastEndpointDesc.endpointId = endpointDesc.endpointId;
        broadcastEndpointDesc.endpointDistribution = endpointDistribution;
        broadcastEndpointDesc.dataPolicies = endpointDesc.dataPolicies;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &broadcastEndpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);
    }
    else
    {
        //Nothing to do - unicast
    }

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //Get the other Video Child descriptors

    //Video Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(videoLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    // Video Format Descriptor
    SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc videoFormatRawDesc;
    memset(&videoFormatRawDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc));
    videoFormatRawDesc.resX = (uint16_t)getVideoFormatResolutionXServiceDescriptor();
    videoFormatRawDesc.resY = (uint16_t)getVideoFormatResolutionYServiceDescriptor();
    videoFormatRawDesc.frameRate = (uint16_t)getVideoFormatFrameRateDescriptor();
    videoFormatRawDesc.bitDepth = (uint16_t)getVideoFormatBitDepthDescriptor();
    videoFormatRawDesc.pixelformat = getVideoFormatPixelFormatDescriptor();
    videoFormatRawDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw;
    videoFormatRawDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc) - SqanSolNet::commonHeaderSize;

    // Add the Video Format Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (videoFormatRawDesc.resX > 0) &&
         (videoFormatRawDesc.resY > 0) &&
         (videoFormatRawDesc.frameRate > 0) &&
         (videoFormatRawDesc.bitDepth > 0) &&
         (videoFormatRawDesc.pixelformat > 0) )
    {
        memcpy(&deviceChildblock[offset], &videoFormatRawDesc, (SqanSolNet::commonHeaderSize + videoFormatRawDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + videoFormatRawDesc.header.length;
    }

    // Video Protocol Raw Descriptor
    SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc videoProtocolDesc;
    memset(&videoProtocolDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc));
    videoProtocolDesc.protocol = getVideoProtocolRawDescriptor();
    videoProtocolDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw;
    videoProtocolDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc) - SqanSolNet::commonHeaderSize;
    // Add the Video Protocol Raw Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) && (videoProtocolDesc.protocol > 0) )
    {
        memcpy(&deviceChildblock[offset], &videoProtocolDesc, (SqanSolNet::commonHeaderSize + videoProtocolDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + videoProtocolDesc.header.length;
    }

    // Video IFOV Descriptor
    SqanSolNet::sqanSolNetPropertyVideoIfovDesc videoIfovDesc;
    memset(&videoIfovDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoIfovDesc));
    videoIfovDesc.instantFieldOfView = getVideoIfovDescriptor();
    videoIfovDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV;
    videoIfovDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoIfovDesc) - SqanSolNet::commonHeaderSize;
    // Add the Video IFOV Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (videoIfovDesc.instantFieldOfView > 0) )
    {
        memcpy(&deviceChildblock[offset], &videoIfovDesc, (SqanSolNet::commonHeaderSize + videoIfovDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + videoIfovDesc.header.length;
    }

    // Video Principal Point Descriptor
    SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc videoPrinciplePointDesc;
    memset(&videoPrinciplePointDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc));
    videoPrinciplePointDesc.pointX = getVideoPrincipalPointXDescriptor();
    videoPrinciplePointDesc.pointY = getVideoPrincipalPointYDescriptor();
    videoPrinciplePointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint;
    videoPrinciplePointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc) - SqanSolNet::commonHeaderSize;
    // Add the Video Principal Point Descriptor
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (videoPrinciplePointDesc.pointX > 0) &&
         (videoPrinciplePointDesc.pointY > 0) )
    {
        memcpy(&deviceChildblock[offset], &videoPrinciplePointDesc, (SqanSolNet::commonHeaderSize + videoPrinciplePointDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + videoPrinciplePointDesc.header.length;
    }

//    // Video Lens Distortion Descriptor
//    SqanSolNet::sqanSolNetPropertyVideoLensDistortionDesc videoLensDistortionDesc;
//    memset(&videoLensDistortionDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoLensDistortionDesc));
//    videoLensDistortionDesc.distortionCoefficients = getVideoLensDistortionCoefficientsDescriptor();
//    videoLensDistortionDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoLensDistortion;
//    videoLensDistortionDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc) - SqanSolNet::commonHeaderSize;

//    // Add the Lens Distortion Descriptor
//    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
//         (videoLensDistortionDesc.distortionCoefficients > 0))
//    {
//        memcpy(&deviceChildblock[offset], &videoLensDistortionDesc, (SqanSolNet::commonHeaderSize + videoLensDistortionDesc.header.length));
//        offset += SqanSolNet::commonHeaderSize + videoLensDistortionDesc.header.length;
//    }

//    // Video Control Descriptor
//    SqanSolNet::sqanSolNetPropertyVideoControlDesc videoControlDesc;
//    memset(&videoControlDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyVideoControlDesc));
//    videoControlDesc.controlBitmap = getVideoControlBitmapDescriptor();
//    videoControlDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoControl;
//    videoControlDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoControlDesc) - SqanSolNet::commonHeaderSize;

//    // Add the Video Control Descriptor
//    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
//         (videoControlDesc.controlBitmap > 0))
//    {
//        memcpy(&deviceChildblock[offset], &videoControlDesc, (SqanSolNet::commonHeaderSize + videoControlDesc.header.length));
//        offset += SqanSolNet::commonHeaderSize + videoControlDesc.header.length;
//    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this Video service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add a new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" Video");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        // Pass endpointDistribution found above - may be bcast, mcast, unicast
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}
*/

uint16_t sqanTestTool::getNextFrameNumber()
{
    nextFrameNumberMutex.lock();

    nextFrameNumber++;
    if(nextFrameNumber == 0)
    {
        // In case it wraps
        nextFrameNumber++;
    }
    nextFrameNumberMutex.unlock();

    return (nextFrameNumber);

}

uint16_t sqanTestTool::getNextPacketCount()
{
    nextPacketCountMutex.lock();

    nextPacketCount++;
    if(nextPacketCount == 0)
    {
        // In case it wraps
        nextPacketCount++;
    }
    nextPacketCountMutex.unlock();

    return (nextPacketCount);
}

/*JR
int sqanTestTool::sendVideoDataPayload(uint8_t currentIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the video data payload tuples
    SqanSolNet::sqanSolNetVideoFrameMetadataTuple videoFrame;
    SqanSolNet::sqanSolNetVideoRtaMetadata videoRta;
    SqanSolNet::sqanSolNetVideoData videoData;

    // Clear the video data payload tuples
    memset(&videoFrame, 0, sizeof(SqanSolNet::sqanSolNetVideoFrameMetadataTuple));
    memset(&videoRta, 0, sizeof(SqanSolNet::sqanSolNetVideoRtaMetadata));
    memset(&videoData, 0, sizeof(SqanSolNet::sqanSolNetVideoData));

    // offset into childblock for copying data
//    uint32_t offset = 0;

    // *****************************************
    // Video Data Payload
    // *****************************************
    // Video Frame Metadata
    // Video RTA Metadata
    // Video Data
    // *****************************************

    // Add Video Frame Metadata
    videoFrame.length = 0x0008;
    videoFrame.tupleId = SqanSolNet::VIDEO_FRAME_METADATA_ID;

    videoFrame.frameNumber = getNextFrameNumber();
    videoFrame.packetCount = getNextPacketCount();
    videoFrame.frameRowNumber = 0;
    videoFrame.frameColumnNumber = 0;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetVideoFrameMetadataTuple);

    // Add Video RTA Metadata
    videoRta.length = 0x0004;
    videoRta.tupleId = SqanSolNet::VIDEO_RTA_METADATA_ID;
    //videoRta.videoPolarity =


    return (0);
}

int sqanTestTool::getRtaServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // RTA - with
    // Endpoint Descriptor
    // Label
    // RTA IMU rate
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::RTA;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    //Associated Service Child Descriptor
    SqanSolNet::sqanSolNetPropertyAssociatedServiceDesc associatedServiceDesc;
    associatedServiceDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyAssociatedServiceDesc) - SqanSolNet::commonHeaderSize;
    associatedServiceDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::AssociatedService;
    associatedServiceDesc.associatedServiceId = 0x0001;
    associatedServiceDesc.associatedServiceSelector = 0x00001000;

    //Add Associated Service
    memcpy(&deviceChildblock[offset], &associatedServiceDesc, sizeof(SqanSolNet::sqanSolNetPropertyAssociatedServiceDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyAssociatedServiceDesc);

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( rtaEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        rtaEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)rtaEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(rtaEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    uint8_t endpointDistribution = 0x01;
    endpointDesc.dataPolicies = getEndpointDataPolicies(rtaEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //RTA Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(rtaLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    // Rta Imu Rate Descriptor
    SqanSolNet::sqanSolNetPropertyRtaImuRateDesc rtaImuRateDesc;
    memset(&rtaImuRateDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyRtaImuRateDesc));
    rtaImuRateDesc.rate = getRtaImuRate();
    rtaImuRateDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate;
    rtaImuRateDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc) - SqanSolNet::commonHeaderSize;
    // Add the Rta Imu Rate Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (rtaImuRateDesc.rate > 0))
    {
        memcpy(&deviceChildblock[offset], &rtaImuRateDesc, (SqanSolNet::commonHeaderSize + rtaImuRateDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + rtaImuRateDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this RTS service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" RTA");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        // Pass endpointDistribution found above - may be bcast, mcast, unicast
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::sendRtaDataPayload(uint8_t currentIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the RTA data payload tuples
    SqanSolNet::sqanSolNetRtaStatusTuple rtaStatus;
    SqanSolNet::sqanSolNetRtaReticleTuple rtaReticle;
    SqanSolNet::sqanSolNetRtaImuDataTuple rtaImuData;

    // Clear the RTA data payload tuples
    memset(&rtaStatus, 0, sizeof(SqanSolNet::sqanSolNetRtaStatusTuple));
    memset(&rtaReticle, 0, sizeof(SqanSolNet::sqanSolNetRtaReticleTuple));
    memset(&rtaImuData, 0, sizeof(SqanSolNet::sqanSolNetRtaImuDataTuple));

    // offset into childblock for copying data
//    uint32_t offset = 0;

    // *****************************************
    // RTA Data Payload
    // *****************************************
    // RTA Status Tuple
    // RTA Reticle Tuple
    // RTA IMU Data Tuple
    // *****************************************

    // Add RTA Reticle
    rtaStatus.length = 0x0008;
    rtaStatus.tupleId = SqanSolNet::RTA_STATUS_ID;

    //TODO: Create Get Functions
    rtaStatus.rtaEnabled = SqanSolNet::RTA_OPERATION_DISABLED;
    rtaStatus.rtaMode = SqanSolNet::SPATIALLY_ALIGNED_MODE;
    rtaStatus.rtaPolarity = SqanSolNet::RTA_POLARITY_WHITE_HOT;
    rtaStatus.rtaZoom = SqanSolNet::RTA_BUBBLE_ZOOM_DISABLED;
    rtaStatus.rtaBubble = SqanSolNet::RTA_BUBBLE_DISABLED;
    rtaStatus.rtaManualAlignment = SqanSolNet::MANUAL_ALIGNMENT_NOT_REQUESTED;
    rtaStatus.reticleInVideo = SqanSolNet::NO_RETICLE_FWS_I_VIDEO;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetVideoFrameMetadataTuple);

    // Add RTA Reticle
    rtaReticle.length = 0x0008;
    rtaReticle.tupleId = SqanSolNet::RTA_RETICLE_ID;

    //TODO: Create Get Functions
    rtaReticle.rtaReticleType = SqanSolNet::CROSS_HAIR;
    rtaReticle.rtaReticleColorRed = Qt::red;
    rtaReticle.rtaReticleColorGreen = Qt::green;
    rtaReticle.rtaReticleColorBlue = Qt::blue;
    rtaReticle.rtaReticleXOffset = 0x0000;
    rtaReticle.rtaReticleYOffset = 0x0000;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetVideoFrameMetadataTuple);

    // Add IMU Data
    rtaImuData.length = 0x0008;
    rtaImuData.tupleId = SqanSolNet::RTA_IMU_DATA_ID;

    //TODO: Create Get Functions
    rtaImuData.quaternionReal = 0x0000;
    rtaImuData.quaternionI = 0x0000;
    rtaImuData.quaternionJ = 0x0000;
    rtaImuData.quaternionK = 0x0000;


//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetVideoFrameMetadataTuple);

    return (0);
}

int sqanTestTool::getArOverlayServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // AR - with
    // Endpoint Descriptor
    // Label
    // Overlay Output
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::AROverlay;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( arOverlayEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        arOverlayEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)arOverlayEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(arOverlayEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    uint8_t endpointDistribution = 0x01;
    endpointDesc.dataPolicies = getEndpointDataPolicies(arOverlayEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //AR Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(arOverlayLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    // AR Overlay Descriptor
    SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc arOverlayDesc;
    memset(&arOverlayDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc));
    arOverlayDesc.minWidth = getArOverlayMinWidthDescriptor();
    arOverlayDesc.minHeight = getArOverlayMinHeightDescriptor();
    arOverlayDesc.supportedRleVersions = getArOverlayRleVersionsDescriptor();
    arOverlayDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput; // SPG: Sprint10 bugfix
    arOverlayDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc) - SqanSolNet::commonHeaderSize;
    // Add the AR Overlay Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (arOverlayDesc.minWidth > 0) && (arOverlayDesc.minHeight > 0) && (arOverlayDesc.supportedRleVersions > 0))
    {
        memcpy(&deviceChildblock[offset], &arOverlayDesc, (SqanSolNet::commonHeaderSize + arOverlayDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + arOverlayDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this AR Overlay service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" AR Overlay");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        // Pass endpointDistribution found above - may be bcast, mcast, unicast
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::getStatusServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Status - with
    // Always:
    //   Endpoint Descriptor - Unicast
    // Possibly:
    //   Endpoint Descriptor - Broadcast / Multicast
    // Always:
    //   Label
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::Status;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor - Unicast
    // The Status Descriptor always sends this so that each side knows how
    // to find this service in the services table
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( statusEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        statusEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)statusEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(statusEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    endpointDesc.dataPolicies = getEndpointDataPolicies(statusEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    //See if user wants broadcast or multicast
    //Pass to addServiceToRegisterServicesTable below
    uint8_t endpointDistribution = getEndpointDistribution(statusEndpointDistributionComboBox, currentIndex);
    uint8_t multicastGroupId = 0x01;

    if ( (endpointDistribution >= 0x01) && (endpointDistribution <= 0xFE) )
    {
        //The user selected multicast
        //Add the multicast service descriptor
        SqanSolNet::sqanSolNetPropertyMulticastGroupDesc multicastDescriptor;
        multicastDescriptor.header.length = sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc) - SqanSolNet::commonHeaderSize;
        multicastDescriptor.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup;
        multicastDescriptor.mcastAddress = endpointDistribution;
        if ( endpointDistribution == DynArray[currentIndex].multicastGroup.startGroup.mcastAddress )
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.startGroup.mcastPeerIndex;
        }
        else
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.joinGroup.mcastPeerIndex;
        }
        multicastGroupId = multicastDescriptor.mcastGroupId;
        multicastDescriptor.reserved1 = 0x00;
        //Get the MAC Address for this device
        uint8_t *sqanMacAddr = DynArray[currentIndex].sqanIdentity->GetMacAddress();
        for (int i=0; i<6; i++)
        {
            multicastDescriptor.mcastMacAddress[i] = sqanMacAddr[i];
        }
        multicastDescriptor.reserved2 = 0x00;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &multicastDescriptor, sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc);
    }
    else if (endpointDistribution == 0xFF)
    {
        //The user selected broadcast
        //Add and EndpointDescriptor with broadcast indicated
        SqanSolNet::sqanSolNetPropertyEndpointDesc broadcastEndpointDesc;
        broadcastEndpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
        broadcastEndpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
        broadcastEndpointDesc.dataflowId = endpointDesc.dataflowId;  //use the same index into the services table
        broadcastEndpointDesc.endpointId = endpointDesc.endpointId;
        broadcastEndpointDesc.endpointDistribution = endpointDistribution;
        broadcastEndpointDesc.dataPolicies = endpointDesc.dataPolicies;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &broadcastEndpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);
    }
    else
    {
        //The user selected unicast - nothing to do
    }

    //Status Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(statusLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    // Set the serviceEntry to use the unicast information except distribution
    // The Status Descriptor might have a broadcast and multicast endpoint
    // but they all use the same dataflowId and serviceSelector
    // distribution will indicate unicast, broadcast, or multicast
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();
    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this Status service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" Status");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, multicastGroupId);
    }

    return (serviceEntry.dataflowId);
}
*/

int sqanTestTool::sendStatusDataPayload(uint8_t currentIndex)
{
    Q_UNUSED(currentIndex);
//    //Get SqanSolNet Friend Class and Print Services
//    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

//    // Create the Status data payload tuples
//    SqanSolNet::sqanSolNetBatterStatusTuple batteryStatus;
//    SqanSolNet::sqanSolNetDeviceStatusTuple deviceStatus;
//    SqanSolNet::sqanSolNetWeaponStatusTuple weaponStatus;

//    // Clear the Status data payload tuples
//    memset(&batteryStatus, 0, sizeof(SqanSolNet::sqanSolNetBatterStatusTuple));
//    memset(&deviceStatus, 0, sizeof(SqanSolNet::sqanSolNetDeviceStatusTuple));
//    memset(&weaponStatus, 0, sizeof(SqanSolNet::sqanSolNetWeaponStatusTuple));

//    // offset into childblock for copying data
//    uint32_t offset = 0;

//    // *****************************************
//    // Status Data Payload
//    // *****************************************
//    // Battery Status Tuple
//    // Device Status Tuple
//    // Weapon Status Tuple
//    // *****************************************

//    // Add Battery Status
//    batteryStatus.length = 0x0004;
//    batteryStatus.tupleId = SqanSolNet::STATUS_BATTERY_ID;

//    //TODO: Create Get Functions
//    batteryStatus.batteryLevel = 0x0000;
//    batteryStatus.batteryState = SqanSolNet::BATTERY_OK;
//    batteryStatus.batteryCharging = SqanSolNet::BATTERY_NOT_CHARGING;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetBatterStatusTuple);

//    // Add Device Status
//    deviceStatus.length = 0x0004;
//    deviceStatus.tupleId = SqanSolNet::STATUS_DEVICE_ID;

//    //TODO: Create Get Functions
//    deviceStatus.deviceState = SqanSolNet::DEVICE_ON;
//    deviceStatus.displayState = SqanSolNet::DISPLAY_OFF;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetDeviceStatusTuple);

//    // Add Weapon Status
//    weaponStatus.length = 0x0004;
//    weaponStatus.tupleId = SqanSolNet::STATUS_WEAPON_ID;

//    //TODO: Create Get Functions
//    weaponStatus.weaponSightMode = SqanSolNet::STANDALONE;
//    weaponStatus.activeReticleId = SqanSolNet::NONE;
//    weaponStatus.symbologyState = SqanSolNet::SYMBOLOGY_OFF;

//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetWeaponStatusTuple);

    return (0);
}

/*JR
int sqanTestTool::getSimpleUiServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Simple UI Service - with
    // Endpoint Descriptor
    // Button Descriptor
    // Label
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::SimpleUI;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( simpleUiEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        simpleUiEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)simpleUiEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(simpleUiEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    uint8_t endpointDistribution = 0x01;
    endpointDesc.dataPolicies = getEndpointDataPolicies(simpleUiEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //Simple UI Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(simpleUiLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    // Button Descriptor
    SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc simpleUiButtonsDesc;
    memset(&simpleUiButtonsDesc, 0, sizeof(SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc));
    simpleUiButtonsDesc.terminalId = 0x01;
    simpleUiButtonsDesc.capabilities = SqanSolNet::STANDARD_FORMAT;
    simpleUiButtonsDesc.buttonCount = getButtonCount(simpleUiButtonCountComboBox);
    simpleUiButtonsDesc.stateCount = getStateCount(simpleUiStateCountComboBox);

    simpleUiButtonsDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::UIButtons;
    simpleUiButtonsDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc) - SqanSolNet::commonHeaderSize;

    // Add the Video Format Descriptor if the user entered something
    if ( (offset < SqanSolNet::payloadMsgBufferSize) &&
         (simpleUiButtonsDesc.terminalId > 0) &&
         (simpleUiButtonsDesc.capabilities > 0) &&
         (simpleUiButtonsDesc.buttonCount > 0) &&
         (simpleUiButtonsDesc.stateCount > 0) )
    {
        memcpy(&deviceChildblock[offset], &simpleUiButtonsDesc, (SqanSolNet::commonHeaderSize + simpleUiButtonsDesc.header.length));
        offset += SqanSolNet::commonHeaderSize + simpleUiButtonsDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this Simple UI service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" Simple UI");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::getPacketGeneratorServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Packet Generator Service - with
    // Endpoint Descriptor
    // Label
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::PacketGenerator;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( packetGenEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        packetGenEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)packetGenEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(packetGenEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    endpointDesc.dataPolicies = getEndpointDataPolicies(packetGenEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    //See if user wants broadcast or multicast
    //Pass to addServiceToRegisterServicesTable below
    uint8_t endpointDistribution = getEndpointDistribution(packetGenEndpointDistributionComboBox, currentIndex);
    uint8_t multicastGroupId = 0x01;

    if ( (endpointDistribution >= 0x01) && (endpointDistribution <= 0xFE) )
    {
        //The user selected multicast
        //Add the multicast service descriptor
        SqanSolNet::sqanSolNetPropertyMulticastGroupDesc multicastDescriptor;
        multicastDescriptor.header.length = sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc) - SqanSolNet::commonHeaderSize;
        multicastDescriptor.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup;
        multicastDescriptor.mcastAddress = endpointDistribution;
        if ( endpointDistribution == DynArray[currentIndex].multicastGroup.startGroup.mcastAddress )
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.startGroup.mcastPeerIndex;
        }
        else
        {
            multicastDescriptor.mcastGroupId = DynArray[currentIndex].multicastGroup.joinGroup.mcastPeerIndex;
        }
        multicastGroupId = multicastDescriptor.mcastGroupId;
        multicastDescriptor.reserved1 = 0x00;
        //Get the MAC Address for this device
        uint8_t *sqanMacAddr = DynArray[currentIndex].sqanIdentity->GetMacAddress();
        for (int i=0; i<6; i++)
        {
            multicastDescriptor.mcastMacAddress[i] = sqanMacAddr[i];
        }
        multicastDescriptor.reserved2 = 0x00;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &multicastDescriptor, sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyMulticastGroupDesc);
    }
    else if (endpointDistribution == 0xFF)
    {
        //The user selected broadcast
        //Add and EndpointDescriptor with broadcast indicated
        SqanSolNet::sqanSolNetPropertyEndpointDesc broadcastEndpointDesc;
        broadcastEndpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
        broadcastEndpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
        broadcastEndpointDesc.dataflowId = endpointDesc.dataflowId;  //use the same index into the services table
        broadcastEndpointDesc.endpointId = endpointDesc.endpointId;
        broadcastEndpointDesc.endpointDistribution = endpointDistribution;
        broadcastEndpointDesc.dataPolicies = endpointDesc.dataPolicies;

        // Total length of data in childblock area
        memcpy(&deviceChildblock[offset], &broadcastEndpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
        offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);
    }
    else
    {
        //The user selected unicast - nothing to do
    }

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //Packet Generator Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(packetGenLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this Packet Generator service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" Packet Generator");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::getLrfServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // LRF Service - with
    // Endpoint Descriptor
    // Label
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::LRF;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( lrfEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        lrfEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)lrfEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(lrfEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    uint8_t endpointDistribution = 0x01;
    endpointDesc.dataPolicies = getEndpointDataPolicies(lrfEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //LRF Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(lrfLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this LRF service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" LRF");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::getLrfControlServiceDescriptor(uint8_t currentIndex, int appIndex)
{
    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Create the service descriptors
    SqanSolNet::sqanSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(SqanSolNet::sqanSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[SqanSolNet::payloadMsgBufferSize];
    // Clear buffer
    memset(deviceChildblock, 0, SqanSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // LRF Control Service - with
    // Endpoint Descriptor
    // Label
    // *****************************************
    serviceEntry.serviceDesc.header.length = SqanSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = SqanSolNet::sqanSolNetServiceIds::LRFControl;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc) - SqanSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint;
    if ( lrfControlEndpointDataflowLineEdit->text() == "" ) {
        endpointDesc.dataflowId = DynArray[currentIndex].sqanSolNet->GetNextDataflowId();
        lrfControlEndpointDataflowLineEdit->setText(QString::number(endpointDesc.dataflowId));
    } else {
        endpointDesc.dataflowId = (uint8_t)lrfControlEndpointDataflowLineEdit->text().toUInt((bool *)false);
    }
    endpointDesc.endpointId = getEndpointId(lrfControlEndpointIdComboBox);
    endpointDesc.endpointDistribution = SqanSolNet::Unicast;
    uint8_t endpointDistribution = 0x01;
    endpointDesc.dataPolicies = getEndpointDataPolicies(lrfControlEndpointDataPoliciesComboBox);

    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

    //Label Descriptor
    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);

    int labelLength = getLabel(lrfControlLabelLineEdit, labelDesc.label);
    // Add the label if the user entered something
    if ( labelLength > 0 )
    {
        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
        labelDesc.header.length = labelLength;
        while ( (labelDesc.header.length % 4) > 0 )
        {
            labelDesc.header.length++;
        }
        // Add Label Descriptor
        if ( offset < SqanSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
        }
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    // Add this LRF Control service descriptor with all the child property descriptors
    // to the SqanSolNet->sqanSolNetServices vector
    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

    // DBLogger
    dbLogService(currentIndex, &serviceEntry);

    // Add this new service to the registerServicesTable
    QString serviceName = DynArray[currentIndex].devName;
    serviceName.append(" LRF Control");

    if(!serviceList.contains(serviceName))
    {
        serviceList.push_back(serviceName);
        addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);
    }

    return (serviceEntry.dataflowId);
}

int sqanTestTool::sendLrfDataPayload(uint8_t currentIndex)
{
    // offset into childblock for copying data
    uint32_t offset = 0;

    //Get SqanSolNet Friend Class and Print Services
    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

    // Get our services
    std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[currentIndex].sqanSolNet->GetServices();

    //Use sendBuf to send data
    uint8_t sendBuf[SqanSolNet::payloadDataBufferSize];

    //Initialize sendBuf to zero
    memset(sendBuf, 0, SqanSolNet::payloadDataBufferSize);
    uint16_t sendbufLength = SqanSolNet::payloadDataBufferSize;

    SqanSolNet::sqanSolNetLrfTargetDataTuple targetDataPayload;
    targetDataPayload.length = sizeof(SqanSolNet::sqanSolNetLrfTargetDataTuple) - SqanSolNet::commonHeaderSize;
    targetDataPayload.tupleId = SqanSolNet::LRF_TARGET;

    memcpy(sendBuf, &targetDataPayload, sendbufLength);
    offset += sizeof(SqanSolNet::sqanSolNetLrfTargetDataTuple);

//    SqanSolNet::sqanSolNetLrfTargetDataTuple targetDataPayload;
//    targetDataPayload.length = sizeof(SqanSolNet::sqanSolNetLrfTargetDataTuple) - SqanSolNet::commonHeaderSize;
//    targetDataPayload.tupleId = SqanSolNet::LRF_TARGET;


    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
    {
        if ( !(*services)[dataflowId].inUse )
        {
            continue;
        }

        SqanSolNet::sqanSolNetServiceEntry *entry = &((*services)[dataflowId]);

        if (entry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRFControl)
        {
            // Send this service data to peer
            // dataflowId bit 7 = 0 to indicate a Producer of the service data
            uint8_t policies = 0; //Bit 0: Payload checksum policy (0 = not present (0), 1 = present)
                                  //Bit 1 and 2: Acknowledgment policy (0 = no ack, 1 = ack now, others reserved)
            //uint8_t policies = SqanSolNet::POLICY_PAYLOAD_CHECKSUM; //Add checksum for payload
            DynArray[currentIndex].sqanSolNet->SendSolNetDataPacket(0xFF,   //peerIndex for broadcast
                                                                   0,      // endpointId for broadcast
                                                                   entry->dataflowId,
                                                                   policies,
                                                                   (uint8_t*)sendBuf,
                                                                   sendbufLength);
        }
    }


//    // Total length of data in childblock area
//    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc));
//    offset += sizeof(SqanSolNet::sqanSolNetPropertyEndpointDesc);

//    // Application sets these per device/app
//    serviceEntry.dataflowId = endpointDesc.dataflowId;
//    serviceEntry.endpointId = endpointDesc.endpointId;
//    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
//    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
//    serviceEntry.serviceDesc.serviceSelector = DynArray[currentIndex].sqanSolNet->GetNextServiceNo();

//    //Packet Generator Label Descriptor
//    SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
//    memset(&labelDesc.label, 0, SqanSolNet::SIZE_LABEL_TEXT);
//    QString labelText = getLabel(lrfLabelLineEdit);
//    if ( !labelText.isEmpty() )
//    {
//        memcpy(&labelDesc.label, labelText.data(), labelText.size());
//        labelDesc.header.descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label;
//        labelDesc.header.length = labelText.size()+1;
//        while ( (labelDesc.header.length % 4) > 0 )
//        {
//            labelDesc.header.length++;
//        }
//        // Add Label Descriptor
//        if ( offset < SqanSolNet::payloadMsgBufferSize )
//        {
//            memcpy(&deviceChildblock[offset], &labelDesc, (SqanSolNet::commonHeaderSize + labelDesc.header.length));
//            offset += SqanSolNet::commonHeaderSize + labelDesc.header.length;
//        }
//    }

//    serviceEntry.serviceDesc.header.length += offset;
//    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

//    // Add this LRF Control service descriptor with all the child property descriptors
//    // to the SqanSolNet->sqanSolNetServices vector
//    DynArray[currentIndex].sqanSolNet->AddSolNetService(SqanSolNet::defaultPeerIndex, &serviceEntry);

//    // Add this new service to the registerServicesTable
//    QString serviceName = DynArray[currentIndex].devName;
//    serviceName.append(" LRF Control");
//    serviceList.push_back(serviceName);
//    uint8_t endpointDistribution = ?? set bcast,mcast, or unicast
//    addServiceToRegisterServicesTable(currentIndex, serviceName, endpointDistribution, 0);

    return (0);
}
*/
int sqanTestTool::sendLrfControlDataPayload(uint8_t currentIndex)
{
//    uint8_t peerIndex = 0;

//    for (auto itr = DynArray[currentIndex].peerList.begin(); itr != DynArray[currentIndex].peerList.end(); ++itr)
//    {
//       if ( destService.contains((*itr)->devName) )
//       {
//           peerIndex = (*itr)->peerIndex;
//           break;
//       }
//    }

//    //Get SqanSolNet Friend Class and Print Services
//    DynArray[currentIndex].sqanSolNet = DynArray[currentIndex].sqanInterface->GetSqanSolNet();

//    // Get our services
//    std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[currentIndex].sqanSolNet->GetServices();

//    //Use sendBuf to send data
//    uint16_t sendBuf[SqanSolNet::payloadDataBufferSize];

//    //Initialize sendBuf to zero
//    memset(sendBuf, 0, SqanSolNet::payloadDataBufferSize);
//    int sendbufLength = SqanSolNet::payloadDataBufferSize;

//    SqanSolNet::sqanSolNetLrfRangeDataTuple lrfRangePayload;
//    lrfRangePayload.length = 0x0000;
//    lrfRangePayload.tupleId = SqanSolNet::LRF_CTRL_RANGE;

//    memcpy(sendBuf, &lrfRangePayload, SqanSolNet::payloadDataBufferSize);

//    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
//    {
//        if ( !(*services)[dataflowId].inUse )
//        {
//            continue;
//        }

//        SqanSolNet::sqanSolNetServiceEntry *entry = &((*services)[dataflowId]);

//        if (entry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::LRFControl)
//        {
//            // Send this service data to peer
//            // dataflowId bit 7 = 0 to indicate a Producer of the service data
//            uint8_t policies = 0; //Bit 0: Payload checksum policy (0 = not present (0), 1 = present)
//                                  //Bit 1 and 2: Acknowledgment policy (0 = no ack, 1 = ack now, others reserved)
//            //uint8_t policies = SqanSolNet::POLICY_PAYLOAD_CHECKSUM; //Add checksum for payload
//            sqanSolNet->SendSolNetDataPacket(0xFF,   //peerIndex for broadcast
//                                            0,      // endpointId for broadcast
//                                            entry->dataflowId,
//                                            policies,
//                                            (uint8_t*)sendBuf,
//                                            sendbufLength);
//        }
//    }

    return (0);
}



/*************************************************
 *
 * loadControlDescriptor
 *
 * Property UI Elements:
 * deviceEndpointIdComboBox             x
 * deviceEndpointDistributionComboBox   x
 * deviceEndpointDataPoliciesComboBox   x
 * deviceNameLineEdit                   x
 * deviceSerialNumberLineEdit           x
 * deviceManufacturerLineEdit           x
 * deviceFriendlyNameLineEdit           x
 * **********************************************/
/*JR void sqanTestTool::loadControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
// these redundant serviceEntry endpoint fields are problematic to rely on when Broadcast (and potentially multicast) are chosen
//    deviceEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    deviceEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    deviceEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceControlDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceControlDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            deviceEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            deviceEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            deviceEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            deviceEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceName ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            deviceNameLineEdit->setText(QString(reinterpret_cast<char *>(devNameDesc->devString)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            deviceSerialNumberLineEdit->setText(QString(reinterpret_cast<char *>(devNameDesc->devString)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            deviceManufacturerLineEdit->setText(QString(reinterpret_cast<char *>(devNameDesc->devString)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            deviceFriendlyNameLineEdit->setText(QString(reinterpret_cast<char *>(devNameDesc->devString)));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/
/*************************************************
 *
 * loadVideoDescriptor
 *
 * Property UI Elements:
 * videoEndpointIdComboBox                          x
 * videoEndpointDistributionComboBox                x
 * videoEndpointDataPoliciesComboBox                x
 * videoLabelLineEdit                               x
 * videoFormatResolutionXServiceDescriptorLineEdit  x
 * videoFormatResolutionYServiceDescriptorLineEdit  x
 * videoFormatFrameRateDescriptorLineEdit           x
 * videoFormatBitDepthDescriptorLineEdit            x
 * videoFormatPixelFormatDescriptorComboBox         x
 * videoProtocolRawDescriptorComboBox               x
 * videoIfovDescriptorLineEdit                      x
 * videoPPointXLineEdit                             x
 * videoPPointYLineEdit                             x
 * **********************************************/
/*JRvoid sqanTestTool::loadVideoDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    videoEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    videoEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    videoEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            videoEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            videoEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            videoEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            videoEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            videoLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw ) {
            SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc *videoFormatRawDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc *>(data);
            videoFormatResolutionXServiceDescriptorLineEdit->setText(QString::number(videoFormatRawDesc->resX));
            videoFormatResolutionYServiceDescriptorLineEdit->setText(QString::number(videoFormatRawDesc->resY));
            videoFormatFrameRateDescriptorLineEdit->setText(QString::number(videoFormatRawDesc->frameRate));
            videoFormatBitDepthDescriptorLineEdit->setText(QString::number(videoFormatRawDesc->bitDepth));
            videoFormatPixelFormatDescriptorComboBox->setCurrentIndex(getVideoFormatPixelFormatDescriptorIndex(videoFormatRawDesc->pixelformat));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw ) {
            SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc *videoProtocolDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc *>(data);
            videoProtocolRawDescriptorComboBox->setCurrentIndex(getVideoProtocolRawDescriptorIndex(videoProtocolDesc->protocol));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV ) {
            SqanSolNet::sqanSolNetPropertyVideoIfovDesc *videoIfovDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoIfovDesc *>(data);
            videoIfovDescriptorLineEdit->setText(QString::number(videoIfovDesc->instantFieldOfView));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint ) {
            SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc *videoPrinciplePointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc *>(data);
            videoPPointXLineEdit->setText(QString::number(videoPrinciplePointDesc->pointX));
            videoPPointYLineEdit->setText(QString::number(videoPrinciplePointDesc->pointY));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/
/*************************************************
 *
 * loadRtaDescriptor
 *
 * Property UI Elements:
 * rtaEndpointIdComboBox            x
 * rtaEndpointDistributionComboBox  x
 * rtaEndpointDataPoliciesComboBox  x
 * rtaLabelLineEdit                 x
 * rtaImuRateLineEdit               x
 * **********************************************/
/*JRvoid sqanTestTool::loadRtaDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    rtaEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    rtaEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    rtaEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            rtaEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            rtaEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            rtaEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            rtaEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            rtaLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate ) {
            SqanSolNet::sqanSolNetPropertyRtaImuRateDesc *rtaImuRateDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyRtaImuRateDesc *>(data);
            rtaImuRateLineEdit->setText(QString::number(rtaImuRateDesc->rate));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/
/*************************************************
 *
 * loadArOverlayDescriptor
 *
 * Property UI Elements:
 * arOverlayEndpointIdComboBox              x
 * arOverlayEndpointDistributionComboBox    x
 * arOverlayEndpointDataPoliciesComboBox    x
 * arOverlayLabelLineEdit                   x
 * arOverlayMinWidthLineEdit                x
 * arOverlayMinHeightLineEdit               x
 * arOverlayRleVersionsLineEdit             x
 * **********************************************/
/*JRvoid sqanTestTool::loadArOverlayDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    arOverlayEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    arOverlayEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    arOverlayEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            arOverlayEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            arOverlayEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            arOverlayEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            arOverlayEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            arOverlayLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput ) {
            SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc *arOverlayDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc *>(data);
            arOverlayMinWidthLineEdit->setText(QString::number(arOverlayDesc->minWidth));
            arOverlayMinHeightLineEdit->setText(QString::number(arOverlayDesc->minHeight));
            arOverlayRleVersionsLineEdit->setText(QString::number(arOverlayDesc->supportedRleVersions));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/

/*************************************************
 *
 * loadPacketGeneratorDescriptor
 *
 * Property UI Elements:
 * packetGenEndpointIdComboBox              x
 * packetGenEndpointDistributionComboBox    x
 * packetGenEndpointDataPoliciesComboBox    x
 * packetGenLabelLineEdit                   x
 * **********************************************/
/*JR void sqanTestTool::loadPacketGeneratorDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    packetGenEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    packetGenEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    packetGenEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            packetGenEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            packetGenEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            packetGenEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            packetGenEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
//            packetGenLabelLineEdit->setText(QString::fromUtf8(reinterpret_cast<char *>(labelDesc->label), labelDesc->header.length));
            packetGenLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/

/*************************************************
 *
 * loadStatusDescriptorUI
 *
 * Property UI Elements:
 * statusEndpointIdComboBox             x
 * statusEndpointDistributionComboBox   x
 * statusEndpointDataPoliciesComboBox   x
 * statusLabelLineEdit                  x
 * **********************************************/
/*JR void sqanTestTool::loadStatusDescriptorUI(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    statusEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    statusEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    statusEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            statusEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            statusEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            statusEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            statusEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            statusLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/

/*************************************************
 *
 * loadSimpleUiDescriptor
 *
 * Property UI Elements:
 * simpleUiEndpointIdComboBox           x
 * simpleUiEndpointDistributionComboBox x
 * simpleUiEndpointDataPoliciesComboBox x
 * simpleUiLabelLineEdit                x
 * simpleUiButtonCountComboBox          x
 * simpleUiStateCountComboBox           x
 * **********************************************/
/*JR void sqanTestTool::loadSimpleUiDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    simpleUiEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    simpleUiEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    simpleUiEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            simpleUiEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            simpleUiEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            simpleUiEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            simpleUiEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            simpleUiLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::UIButtons ) {
            SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc *simpleUiButtonsDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc *>(data);
            simpleUiButtonCountComboBox->setCurrentIndex(getButtonCountIndex(simpleUiButtonsDesc->buttonCount));
            simpleUiStateCountComboBox->setCurrentIndex(getStateCountIndex(simpleUiButtonsDesc->stateCount));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/

/*************************************************
 *
 * loadLrfDescriptor
 *
 * Property UI Elements:
 * lrfEndpointIdComboBox            x
 * lrfEndpointDistributionComboBox  x
 * lrfEndpointDataPoliciesComboBox  x
 * lrfLabelLineEdit                 x
 * **********************************************/
/*JR void sqanTestTool::loadLrfDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    lrfEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    lrfEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    lrfEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            lrfEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            lrfEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            lrfEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            lrfEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            lrfLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/
/*************************************************
 *
 * loadLrfControlDescriptor
 *
 * Property UI Elements:
 * lrfControlEndpointIdComboBox
 * lrfControlEndpointDistributionComboBox
 * lrfControlEndpointDataPoliciesComboBox
 * lrfControlLabelLineEdit
 * **********************************************/
/*JR void sqanTestTool::loadLrfControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry)
{
//    lrfControlEndpointIdComboBox->setCurrentText(QString::number(serviceEntry->endpointId));
//    lrfControlEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(serviceEntry->endpointDistribution));
//    lrfControlEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(serviceEntry->dataPolicies));

    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            lrfControlEndpointIdComboBox->setCurrentText(QString::number(endpointDesc->endpointId));
            lrfControlEndpointDataflowLineEdit->setText(QString::number(endpointDesc->dataflowId));
            lrfControlEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex(endpointDesc->endpointDistribution));
            lrfControlEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex(endpointDesc->dataPolicies));
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            lrfControlLabelLineEdit->setText(QString(reinterpret_cast<char *>(labelDesc->label)));
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
}
*/
/*****************************************************
 * SqanSolNet Register Services Configuration
 * **************************************************/
/*JR
void sqanTestTool::addDeviceToRegisterServicesTable(uint8_t selected)
{
    registerServicesTableLock.lock();

    // Add the row at the same place as the device index (plus 1)
    // row zero has column labels
    uint8_t rowIndex = selected + 1;
    ui->registerServicesTable->insertRow(rowIndex);

    // Set the row header to be the device name
    QLabel *rowLabel = new QLabel;
    rowLabel->setAlignment(Qt::AlignCenter);
    rowLabel->setTextFormat(Qt::RichText);
    rowLabel->setStyleSheet("QLabel { background-color : lightgrey; color : black; font-weight: bold }");
    rowLabel->setText(DynArray[selected].devName);

    // Add a Browse pushbutton to this row header
    QPushButton *browseButton = new QPushButton("Browse Services", this);
    browseButton->setProperty("deviceIndex", DynArray[selected].sqanInterface->GetIndex());
    browseButton->setStyleSheet("QLabel { background-color : lightgrey; color : darkblue; }");

    // Add the pushbutton to a vertical layout
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(rowLabel);
    layout->addWidget(browseButton);
    layout->setAlignment(Qt::AlignCenter);

    // Set the cell widget to this layout
    QWidget *widget = new QWidget();
    widget->setStyleSheet("QWidget { background-color : lightgrey; color : darkblue; }");
    widget->setLayout(layout);
    ui->registerServicesTable->setCellWidget(rowIndex, 0, widget);

    // Connect the SLOT function to the push button
    connect(browseButton, SIGNAL(clicked()), this, SLOT(browseAllServices_clicked()));

    // Add widgets to register for existing services
    for (uint8_t colIndex=1; colIndex < ui->registerServicesTable->columnCount(); colIndex++)
    {
        QLabel *label = nullptr;
        label = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);

        if ( (label != nullptr) &&
             (label->text().contains(DynArray[selected].devName)) )
        {
            // Make cell not available for node to register for it's own services
            QTableWidgetItem *qtwi = new QTableWidgetItem();
            QBrush brush(Qt::gray);
            qtwi->setBackground(brush);
            Qt::ItemFlags flags = qtwi->flags() & ~Qt::ItemIsSelectable & ~Qt::ItemIsEditable;
            qtwi->setFlags(flags);
            ui->registerServicesTable->setItem(rowIndex, colIndex, qtwi);
        }
        else
        {
            // Add GetStatus checkBox
            // Source is requesting it's registration status for the dst service
            // This status is available or not available - the source can
            // register or they can't
            QCheckBox *getStatusCheckBox = new QCheckBox("Get Status", this);
            getStatusCheckBox->setCheckable(true);
            getStatusCheckBox->setChecked(false);

            // Add registerService checkBox
            // Source is registering for the destination
            QCheckBox *registerCheckBox = new QCheckBox("Register", this);
            registerCheckBox->setCheckable(true);
            registerCheckBox->setChecked(false);

            // Add deregisterService checkBox
            // Source is deregistering from the destination
            QCheckBox *deregisterCheckBox = new QCheckBox("Deregister", this);
            deregisterCheckBox->setCheckable(true);
            deregisterCheckBox->setChecked(false);

            // Add revokeService checkBox
            // Destination Service is revoked for the source
            QCheckBox *revokeCheckBox = new QCheckBox("Revoke", this);
            revokeCheckBox->setCheckable(true);
            revokeCheckBox->setChecked(false);

            QLabel *distributionLabel = new QLabel;
            distributionLabel->setFixedWidth(178);
            distributionLabel->setWordWrap(false);
            distributionLabel->setAlignment(Qt::AlignCenter);
            distributionLabel->setTextFormat(Qt::RichText);
            //Initialize the property
            distributionLabel->setProperty("peerIndex", 0);
            QString serviceDistribution = "";
            serviceDistribution = endpointDistributionMap.value(label->text());
            distributionLabel->setText(serviceDistribution);
            distributionLabel->setStyleSheet("QLabel { "
                                             "background-color : lightgreen; "
                                             "color : black; "
                                             "border-style: outset; "
                                             "border-color: darkgreen;"
                                             "border-width: 2px;"
                                             "padding: 2px; }");

            QLabel *statusLabel = new QLabel;
            statusLabel->setFixedWidth(178);
            statusLabel->setWordWrap(false);
            statusLabel->setAlignment(Qt::AlignCenter);
            statusLabel->setTextFormat(Qt::RichText);
            statusLabel->setText("Need To Browse");
            statusLabel->setStyleSheet("QLabel { "
                                       "background-color : lightgrey; "
                                       "color : black; "
                                       "border-style: outset; "
                                       "border-color: black;"
                                       "border-width: 2px;"
                                       "padding: 2px; }");

            // Add the checkBox's to a vertical layout
            QVBoxLayout *layout = new QVBoxLayout;
            layout->addWidget(getStatusCheckBox);
            layout->addWidget(registerCheckBox);
            layout->addWidget(deregisterCheckBox);
            layout->addWidget(revokeCheckBox);
            layout->addWidget(distributionLabel);
            layout->addWidget(statusLabel);
            layout->setAlignment(Qt::AlignCenter);
            layout->setContentsMargins(1,1,1,1);

            // Set the cell widget to this layout
            QWidget *widget = new QWidget();
            widget->setStyleSheet("QLayout { padding: 2px; }");
            widget->setLayout(layout);
            ui->registerServicesTable->setCellWidget(rowIndex, colIndex, widget);

            // Connect the SLOT functions to each checkbox
            connect(registerCheckBox, SIGNAL(clicked(bool)), this, SLOT(registerBox_checked(bool)));
            connect(deregisterCheckBox, SIGNAL(clicked(bool)), this, SLOT(deregisterBox_checked(bool)));
            connect(revokeCheckBox, SIGNAL(clicked(bool)), this, SLOT(revokeBox_checked(bool)));
            connect(getStatusCheckBox, SIGNAL(clicked(bool)), this, SLOT(getRegistrationStatusBox_checked(bool)));

            // Setup a map of checkBox to cell item row/column index
            // so that when the button is pushed we can find the cell
            tableCell *thisCell = new tableCell;
            thisCell->rowIndex = rowIndex;
            thisCell->colIndex = colIndex;
            thisCell->distributionLabel = distributionLabel;
            thisCell->statusLabel = statusLabel;  //This label gets updated as other slot functions complete
            checkBoxMap.insert(registerCheckBox, thisCell);
            checkBoxMap.insert(deregisterCheckBox, thisCell);
            checkBoxMap.insert(revokeCheckBox, thisCell);
            checkBoxMap.insert(getStatusCheckBox, thisCell);
        }
    }

    registerServicesTableLock.unlock();

    ui->registerServicesTable->repaint();
}
*/

void sqanTestTool::removeDeviceFromRegisterServicesTable(uint8_t selected)
{
    registerServicesTableLock.lock();

    // Remove any services this device provides
    for (uint8_t colIndex=1; colIndex < ui->registerServicesTable->columnCount(); colIndex++)
    {
        QLabel *label = nullptr;
        label = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);

        if ( label != nullptr )
        {
            if ( label->text().contains(DynArray[selected].devName) )
            {
                ui->registerServicesTable->removeColumn(colIndex);
                ui->registerServicesTable->update();
            }
        }
    }

    // Remove any row with for this device
    uint8_t rowIndex = selected + 1;
    ui->registerServicesTable->removeRow(rowIndex);
    ui->registerServicesTable->update();
    ui->registerServicesTable->repaint();

    registerServicesTableLock.unlock();
}

void sqanTestTool::createRegisterServicesTable()
{
    registerServicesTableLock.lock();

    // Draw the table
    // Set the upper left corner text and buttons
    QLabel *cornerLabel = new QLabel;
    cornerLabel->setAlignment(Qt::AlignCenter);
    cornerLabel->setTextFormat(Qt::RichText);
    cornerLabel->setStyleSheet("QLabel { background-color : lightgrey; color : black; font-weight: bold }");
    cornerLabel->setText("Client\\Service");
    cornerLabel->setContentsMargins(1,1,1,1);

    // Add a Browse All pushbutton to this upper left header
    // All sources send a SolNet Browse msg to all their peers
    browseAllButton = new QPushButton("Browse All", this);
    browseAllButton->setProperty("deviceIndex", 0xFF);
    browseAllButton->setStyleSheet("QLabel { color : darkblue; font-size : 12px; }");

    // Add a GetAllStatus pushbutton to this upper left header
    // All sources send a SolNet Browse msg to all their peers
    getAllStatusButton = new QPushButton("Get All Status", this);
    getAllStatusButton->setProperty("deviceIndex", 0xFF);
    getAllStatusButton->setStyleSheet("QLabel { color : darkblue; font-size : 12px; }");

    // Add a Register All pushbutton to this upper left header
    // All sources send a SolNet Register msg to all their peers services
    regAllButton = new QPushButton("Register All", this);
    regAllButton->setProperty("deviceIndex", 0xFF);
    regAllButton->setStyleSheet("QLabel { color : darkblue; font-size : 12px; }");

    // Add a Deregister All pushbutton to this upper left header
    // All sources send a SolNet Register msg to all their peers services
    deregAllButton = new QPushButton("Deregister All", this);
    deregAllButton->setProperty("deviceIndex", 0xFF);
    deregAllButton->setStyleSheet("QLabel { color : darkblue; font-size : 12px; }");

    // Add the pushbutton to a vertical layout
    QVBoxLayout *cornerLayout = new QVBoxLayout;
    cornerLayout->addWidget(cornerLabel);
    cornerLayout->addWidget(browseAllButton);
    cornerLayout->addWidget(getAllStatusButton);
    cornerLayout->addWidget(regAllButton);
    cornerLayout->addWidget(deregAllButton);
    cornerLayout->setAlignment(Qt::AlignCenter);

    // Set the cell widget to this layout
    QWidget *cornerWidget = new QWidget();
    cornerWidget->setStyleSheet("QWidget { background-color : lightgrey; color : darkblue; }");
    cornerWidget->setLayout(cornerLayout);
    ui->registerServicesTable->setCellWidget(0, 0, cornerWidget);

    // Connect the SLOT function to the push button
    /*JR connect(browseAllButton, SIGNAL(clicked()), this, SLOT(browseAllServices_clicked()));
    connect(getAllStatusButton, SIGNAL(clicked()), this, SLOT(getAllStatusButton_clicked()));
    connect(regAllButton, SIGNAL(clicked()), this, SLOT(registerAllButton_clicked()));
    connect(deregAllButton, SIGNAL(clicked()), this, SLOT(deregisterAllButton_clicked()));
    */
    registerServicesTableLock.unlock();

   /*JR for (int i=0; i<MAX_PEERS; i++)
    {
        // Add Attached devices to the table
        if ( (DynArray[i].sqanInterface != nullptr) && (DynArray[i].isReady) )
        {
            addDeviceToRegisterServicesTable(i);
        }
    }*/
    ui->registerServicesTable->repaint();
}

void sqanTestTool::clearRegisterServicesTable()
{
    registerServicesTableLock.lock();

 //JR checkBoxMap.clear();

    // Deselects all selected items
    ui->registerServicesTable->clearSelection();

    // Disconnect all signals from table widget ! important !
    ui->registerServicesTable->disconnect();

    // Remove all items
    ui->registerServicesTable->clearContents();

    // Set row count to default (remove rows)
    ui->registerServicesTable->setRowCount(defaultRegSrvRowColCount);

    // Set column count to default (remove columns)
    ui->registerServicesTable->setColumnCount(defaultRegSrvRowColCount);

    registerServicesTableLock.unlock();
}

/*JR
void sqanTestTool::addServiceToRegisterServicesTable(uint8_t selected, QString serviceName, uint8_t endpointDistribution, uint8_t multicastGroupId)
{
    registerServicesTableLock.lock();

    // Add a column for this service

    ui->registerServicesTable->insertColumn(ui->registerServicesTable->columnCount());
    uint8_t colIndex = ui->registerServicesTable->columnCount()-1;

    QLabel *columnLabel = new QLabel;
    columnLabel->setAlignment(Qt::AlignCenter);
    columnLabel->setTextFormat(Qt::RichText);
    columnLabel->setContentsMargins(1,1,1,1);
    columnLabel->setStyleSheet("QLabel { background-color : lightgrey; color : black; font-weight: bold }");
    columnLabel->setText(serviceName);
    ui->registerServicesTable->setCellWidget(0, colIndex, columnLabel);

    // Add the buttons for each source to register/deregister for this service
    for (uint8_t rowIndex=1; rowIndex < ui->registerServicesTable->rowCount(); rowIndex++)
    {
        // Check if the column header text with destination service is for
        // the source device.  Don't allow a source to register for it's own service.
        // Grey out the cell.
        QString rowLabel = DynArray[rowIndex-1].devName;
        if ( serviceName.contains(rowLabel) )
        {
            // Make cell not available for node to register for it's own services
            QTableWidgetItem *qtwi = new QTableWidgetItem();
            QBrush brush(Qt::gray);
            qtwi->setBackground(brush);
            Qt::ItemFlags flags = qtwi->flags() & ~Qt::ItemIsSelectable & ~Qt::ItemIsEditable;
            qtwi->setFlags(flags);
            ui->registerServicesTable->setItem(rowIndex, colIndex, qtwi);
        }
        else
        {
            // Add GetStatus checkBox
            // Source is requesting it's registration status for the dst service
            // This status is available or not available - the source can
            // register or they can't
            QCheckBox *getStatusCheckBox = new QCheckBox("Get Status", this);
            getStatusCheckBox->setCheckable(true);
            getStatusCheckBox->setChecked(false);

            // Add registerService checkBox
            // Source is registering for the destination
            QCheckBox *registerCheckBox = new QCheckBox("Register", this);
            registerCheckBox->setCheckable(true);
            registerCheckBox->setChecked(false);

            // Add deregisterService checkBox
            // Source is deregistering from the destination
            QCheckBox *deregisterCheckBox = new QCheckBox("Deregister", this);
            deregisterCheckBox->setCheckable(true);
            deregisterCheckBox->setChecked(false);

            // Add revokeService checkBox
            // Destination Service is revoked for the source
            QCheckBox *revokeCheckBox = new QCheckBox("Revoke", this);
            revokeCheckBox->setCheckable(true);
            revokeCheckBox->setChecked(false);

            QLabel *distributionLabel = new QLabel;
            distributionLabel->setFixedWidth(178);
            distributionLabel->setWordWrap(false);
            distributionLabel->setAlignment(Qt::AlignCenter);
            distributionLabel->setTextFormat(Qt::RichText);

            //Initialize the property and setup endpointDist map
            distributionLabel->setProperty("peerIndex", 0);

            if ( (endpointDistribution >= 0x01) && (endpointDistribution <= 0xFE) )
            {
                distributionLabel->setText("Multicast");
                distributionLabel->setProperty("peerIndex", multicastGroupId);                
                endpointDistributionMap.insert(serviceName,"Multicast");
            }
            else if (endpointDistribution == 0xFF)
            {
                distributionLabel->setText("Broadcast");
                distributionLabel->setProperty("peerIndex", 0xFF);
                endpointDistributionMap.insert(serviceName,"Broadcast");
            }
            else
            {
                distributionLabel->setText("Unicast");
                endpointDistributionMap.insert(serviceName,"Unicast");
            }
            distributionLabel->setStyleSheet("QLabel { "
                                             "background-color : lightgreen; "
                                             "color : black; "
                                             "border-style: outset; "
                                             "border-color: darkgreen;"
                                             "border-width: 2px;"
                                             "padding: 2px; }");

            QLabel *statusLabel = new QLabel;
            statusLabel->setFixedWidth(178);
            statusLabel->setWordWrap(false);
            statusLabel->setAlignment(Qt::AlignCenter);
            statusLabel->setTextFormat(Qt::RichText);
            statusLabel->setText("Need To Browse");
            statusLabel->setStyleSheet("QLabel { "
                                       "background-color : lightgrey; "
                                       "color : black; "
                                       "border-style: outset; "
                                       "border-color: black;"
                                       "border-width: 2px;"
                                       "padding: 2px; }");

            // Add the checkBox's to a vertical layout
            QVBoxLayout *layout = new QVBoxLayout;
            layout->addWidget(getStatusCheckBox);
            layout->addWidget(registerCheckBox);
            layout->addWidget(deregisterCheckBox);
            layout->addWidget(revokeCheckBox);
            layout->addWidget(distributionLabel);
            layout->addWidget(statusLabel);
            layout->setAlignment(Qt::AlignCenter);
            layout->setContentsMargins(1,1,1,1);

            // Set the cell widget to this layout
            QWidget *widget = new QWidget();
            widget->setStyleSheet("QLayout { padding: 2px; }");
            widget->setLayout(layout);
            ui->registerServicesTable->setCellWidget(rowIndex, colIndex, widget);

            // Connect the SLOT functions to each checkbox
            connect(registerCheckBox, SIGNAL(clicked(bool)), this, SLOT(registerBox_checked(bool)));
            connect(deregisterCheckBox, SIGNAL(clicked(bool)), this, SLOT(deregisterBox_checked(bool)));
            connect(revokeCheckBox, SIGNAL(clicked(bool)), this, SLOT(revokeBox_checked(bool)));
            connect(getStatusCheckBox, SIGNAL(clicked(bool)), this, SLOT(getRegistrationStatusBox_checked(bool)));

            // Setup a map of checkBox to cell item row/column index
            // so that when the button is pushed we can find the cell
            tableCell *thisCell = new tableCell;
            thisCell->rowIndex = rowIndex;
            thisCell->colIndex = colIndex;
            thisCell->distributionLabel = distributionLabel;
            thisCell->statusLabel = statusLabel;  //This label gets updated as other slot functions complete
            checkBoxMap.insert(registerCheckBox, thisCell);
            checkBoxMap.insert(deregisterCheckBox, thisCell);
            checkBoxMap.insert(revokeCheckBox, thisCell);
            checkBoxMap.insert(getStatusCheckBox, thisCell);
        }
    }
    registerServicesTableLock.unlock();

    ui->registerServicesTable->repaint();
}
*/

/*JR
int sqanTestTool::registerForService(uint8_t selected, QString destService)
{
    int status = 0;

    // Find the serviceId number from the column header text
    // describing the service
    uint16_t serviceId = 0xFFFF;
    getServiceIdFromText(destService, &serviceId);

    if ( serviceId == 0xFFFF )
    {
        qDebug() << "Can't find serviceId for " << destService;
        status = -1;
    }
    else
    {
        registerMutex.lock();
        // Default value in case we don't have peers
        if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
        {
            uint8_t peerIndex = 0xFF;
            for (auto itr = DynArray[selected].peerList.begin(); itr != DynArray[selected].peerList.end(); ++itr)
            {
               if ( destService.contains((*itr)->devName) )
               {
                   peerIndex = (*itr)->peerIndex;
                   break;
               }
            }

            if ( peerIndex != 0xFF )
            {
                std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();
                SqanSolNet::sqanSolNetPeerServicesEntry *peerServiceEntry = &(*peerServices)[peerIndex];
                bool foundService = false;

                if ( peerServiceEntry->inUse )
                {
                    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                    {
                        if ( peerServiceEntry->services[dataflowId].inUse )
                        {
                            if ( peerServiceEntry->services[dataflowId].serviceDesc.header.descriptorId == serviceId )
                            {
                                // Get the service we want to register for
                                uint32_t serviceSelector = peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector;
                                foundService = true;

                                //autonomy: 0 = wait to be polled; 1 = send data automatically to registered node(s)
                                uint8_t autonomy = 1;
                                status = DynArray[selected].sqanSolNet->SendRegisterRequestMessage(peerIndex, serviceSelector, autonomy);

                                if ( status == 0 )
                                {
                                    // Wait for the RegisterResponse to update ImRegistered flag
                                    // Verify that we are registered with this peer for this service
                                    int count = 0;
                                    while ((!peerServiceEntry->services[dataflowId].ImRegistered) && (count < 100))
                                    {
                                        QApplication::processEvents();
                                        sleep(1); // 1 second
                                        count++;
                                    }

                                    if ( peerServiceEntry->services[dataflowId].ImRegistered )
                                    {
                                        //displayMessage("Registration Completed!");
                                        status = 0;
                                    }
                                    else
                                    {
                                        //displayMessage("Registration Failed!");
                                        status = 1;
                                    }
                                }
                                else
                                {
                                    // We are not allowed to register for this service
                                    // it's already been revoked
                                    status = -1;
                                }
                           }
                        }
                    }
                    if ( !foundService )
                    {
                        status = -1;
                    }
                }
                else
                {
                    status = -1;
                }
            }
            else
            {
                qDebug() << DynArray[selected].devName << " Couldn't find peer service " << destService;
                status = -1;
            }
        }
        registerMutex.unlock();
    }

    return (status);
}

int sqanTestTool::deregisterFromService(uint8_t selected, QString destService)
{
    int status = 0;

    // Find the serviceId number from the column header text
    // describing the service
    uint16_t serviceId = 0xFFFF;
    getServiceIdFromText(destService, &serviceId);

    if ( serviceId == 0xFFFF)
    {
        qDebug() << "Can't find serviceId for " << destService;
        status = -1;
    }
    else
    {
        deregisterMutex.lock();
        // Default value in case we don't have peers
        if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
        {
            uint8_t peerIndex = 0xFF;
            for (auto itr = DynArray[selected].peerList.begin(); itr != DynArray[selected].peerList.end(); ++itr)
            {
               if ( destService.contains((*itr)->devName) )
               {
                   peerIndex = (*itr)->peerIndex;
                   break;
               }
            }

            if ( peerIndex != 0xFF )
            {
                std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();
                SqanSolNet::sqanSolNetPeerServicesEntry *peerServiceEntry = &(*peerServices)[peerIndex];

                if ( peerServiceEntry->inUse )
                {
                    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                    {
                        if ( peerServiceEntry->services[dataflowId].inUse )
                        {
                            // Get the service we want to deregister from
                            if ( peerServiceEntry->services[dataflowId].serviceDesc.header.descriptorId == serviceId )
                            {
                                if ( (peerServiceEntry->services[dataflowId].status == SqanSolNet::RegStatusUnavailableForRegistration) ||
                                     (peerServiceEntry->services[dataflowId].status == SqanSolNet::RegStatusUnavailable) )
                                {
                                    // This service has been revoked and is unavailable
                                    status = -1;
                                }
                                else
                                {
                                    if ( peerServiceEntry->services[dataflowId].ImRegistered )
                                    {
                                        // If the source is registered, then deregister
                                        uint32_t serviceSelector = peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector;

                                        if ( peerServiceEntry->services[dataflowId].autonomy == 0 )
                                        {
                                            // We registered with autonomy set to zero (or wait to be polled)
                                            uint8_t flowState = 0; // Stop sending data!
                                            DynArray[selected].sqanSolNet->SendAutonomousStartStopRequestMessage(peerIndex, serviceSelector, flowState);
                                        }

                                        // Deregister for this service
                                        DynArray[selected].sqanSolNet->SendDeregisterRequestMessage(peerIndex, serviceSelector);

                                        //displayMessage("Please wait for Deregistration to be Completed");

                                        // Wait for the DeregisterResponse to update ImRegistered flag
                                        // Verify that we are registered with this peer for this service
                                        int count = 0;
                                        while ((peerServiceEntry->services[dataflowId].ImRegistered) && (count < 100))
                                        {
                                            QApplication::processEvents();
                                            sleep(1); // 1 second
                                            count++;
                                        }

                                        if ( !peerServiceEntry->services[dataflowId].ImRegistered )
                                        {
                                            //displayMessage("Deregistration Completed!");
                                            status = 0;
                                        }
                                        else
                                        {
                                            //displayMessage("Deregistration Failed!");
                                            status = -1;
                                        }
                                    }
                                }
                                break;
                            }
                            else
                            {
                                // Couldn't find service
                                status = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                qDebug() << DynArray[selected].devName << QString::number(selected) << " Couldn't find peer service " << destService;
                status = -1;
            }
        }
        deregisterMutex.unlock();
    }

    return (status);
}

int sqanTestTool::revokeRegistrationOfService(uint8_t rowIndex, QString destService)
{
    int status = 0;

    // Find the serviceId number from the column header text
    // describing the service
    uint16_t serviceId = 0xFFFF;
    getServiceIdFromText(destService, &serviceId);

    if ( serviceId == 0xFFFF)
    {
        qDebug() << "Can't find serviceId for " << destService;
        // It's not available so status is zero in this case
        status = 0;
    }
    else
    {
        // All the failure cases for revoke status will
        // return status=0 meaning the service is not available
        // so there is not as much checking in this method
        revokeMutex.lock();
        // First find the device that is revoking service
        int selected = 0xFF;  // Set to 0xFF so we know if found the revoking device

        for (int i=0; i<MAX_PEERS; i++)
        {
            // Look for this service in the attached devices
            if ( (DynArray[i].sqanInterface != nullptr) && (DynArray[i].isReady) )
            {
                if ( destService.contains(DynArray[i].devName) )
                {
                    selected = i;
                    break;
                }
            }
        }

        // Then find the correct peerIndex based on the revoking devices'
        // peer list (not the DynArray index)
        uint8_t peerIndex = 0xFF;
        for (uint8_t sqanPeer=0; sqanPeer<MAX_PEERS; sqanPeer++)
        {
            if ( DynArray[selected].peerList[sqanPeer]->devName == DynArray[rowIndex].devName )
            {
                peerIndex = DynArray[selected].peerList[sqanPeer]->peerIndex;
                break;
            }
        }

        // If we found the revoking service, it's a device that is ready state
        // and we found the correct peerIndex for the device being revoked - go ahead
        if ( (selected != 0xFF) &&
             (DynArray[selected].sqanInterface != nullptr) && (DynArray[selected].isReady) &&
             (peerIndex != 0xFF) )
        {
            std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[selected].sqanSolNet->GetServices();

            for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
            {
                SqanSolNet::sqanSolNetServiceEntry *serviceEntry = &(*services)[dataflowId];

                if ( serviceEntry->inUse )
                {
                    if ( serviceEntry->serviceDesc.header.descriptorId == serviceId )
                    {
                        // Revoke the registration for this peer on this service
                        status = DynArray[selected].sqanSolNet->SendRevokeRegistrationMessage(peerIndex, serviceEntry->serviceDesc.serviceSelector);

                        if ( status == 0 )
                        {
                            // We don't have to wait here because this device is revoking a peer
                            switch (serviceEntry->registeredPeers[peerIndex].status)
                            {
                                case SqanSolNet::RegStatusAvailableToRegister:
                                case SqanSolNet::RegStatusAvailableForYourUseOnly:
                                case SqanSolNet::RegStatusAvailableForUseYouAndOthers:
                                {
                                    // For some reason, this service was not revoked for this peer
                                    status = -1;
                                    break;
                                }
                                case SqanSolNet::RegStatusUnavailableForRegistration:
                                case SqanSolNet::RegStatusUnavailable:
                                default:
                                {
                                    status = 0;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        revokeMutex.unlock();
    }

    return (status);
}

int sqanTestTool::getRegistrationStatus(int selected, QString destService)
{
    int status = 0;
    bool foundService = false;

    // Find the descriptorId number from the column header text
    // describing the service
    uint16_t descriptorId = 0xFFFF;
    getServiceIdFromText(destService, &descriptorId);

    if ( descriptorId == 0xFFFF)
    {
        qDebug() << "Can't find descriptor for " << destService;
        status = -1;
    }
    else
    {
        getStatusMutex.lock();
        // Default value in case we don't have peers
        if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
        {
            uint8_t peerIndex = 0xFF;
            for (auto itr = DynArray[selected].peerList.begin(); itr != DynArray[selected].peerList.end(); ++itr)
            {
               if ( destService.contains((*itr)->devName) )
               {
                   peerIndex = (*itr)->peerIndex;
                   break;
               }
            }

            if ( peerIndex != 0xff )
            {
                std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();
                SqanSolNet::sqanSolNetPeerServicesEntry *peerServiceEntry = &(*peerServices)[peerIndex];

                if ( peerServiceEntry->inUse )
                {
                    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                    {
                        if ( peerServiceEntry->services[dataflowId].inUse )
                        {
                            if ( peerServiceEntry->services[dataflowId].serviceDesc.header.descriptorId == descriptorId )
                            {
                                foundService = true;
                                // Get the service we want to register for
                                status = DynArray[selected].sqanSolNet->SendGetStatusRequestMessage(peerIndex, peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector);

                                if ( status == 0 )
                                {
                                    // Wait for the RegisterResponse to update ImRegistered flag
                                    // Verify that we are registered with this peer for this service
                                    sleep(3);

                                    switch (peerServiceEntry->services[dataflowId].status)
                                    {
                                    case SqanSolNet::RegStatusAvailableToRegister:
                                    case SqanSolNet::RegStatusAvailableForYourUseOnly:
                                    case SqanSolNet::RegStatusAvailableForUseYouAndOthers:
                                    {
                                        if ( peerServiceEntry->services[dataflowId].ImRegistered )
                                        {
                                            status = 0;
                                        }
                                        else
                                        {
                                            status = 1;
                                        }
                                        break;
                                    }
                                    case SqanSolNet::RegStatusUnavailableForRegistration:
                                    case SqanSolNet::RegStatusUnavailable:
                                    default:
                                    {
                                        status = -1;
                                        break;
                                    }
                                    }
                                }
                                else
                                {
                                    // We are not allowed to register for this service
                                    // it's already been revoked
                                    status = -1;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        getStatusMutex.unlock();

        if ( !foundService )
        {
            status = -1;
        }
    }

    return (status);
}

void sqanTestTool::browseAllServices_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    // Get Selected Item
    // Row 1 is Device zero due to headers being in row/column zero
    int selected = sender()->property("deviceIndex").toInt();

    displayMessage("Please Wait... Browsing All Services");

    // Browse for all devices
    if ( selected == 0xFF )
    {
        for (int i=0; i<MAX_PEERS; i++)
        {
            //Send Browse Service To Each Peer
            for (auto itr = DynArray[i].peerList.begin(); itr != DynArray[i].peerList.end(); ++itr)
            {
                SolNetPeerListEntry *entry = *itr;
                DynArray[i].sqanSolNet->SendBrowseMessage(entry->peerIndex);
                QApplication::processEvents();
                sleep(1);
            }
        }


        QList<tableCell *> tableCellList = checkBoxMap.values(); // get a list of all the values
        foreach(tableCell *thisCell, tableCellList)
        {
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: black;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");
            thisCell->statusLabel->setText("Check Status");
            thisCell->statusLabel->repaint();
            QApplication::processEvents();
        }
    }
    else
    {
        //Send Browse Service To Each Peer
        for (auto itr = DynArray[selected].peerList.begin(); itr != DynArray[selected].peerList.end(); ++itr)
        {
            SolNetPeerListEntry *entry = *itr;
            DynArray[selected].sqanSolNet->SendBrowseMessage(entry->peerIndex);
            sleep(1);
        }

        // Device 0 is in row 1
        uint8_t rowIndex = selected + 1;
        QList<tableCell *> tableCellList = checkBoxMap.values(); // get a list of all the values

        for (uint8_t colIndex=1; colIndex<ui->registerServicesTable->columnCount(); colIndex++)
        {
            foreach(tableCell *thisCell, tableCellList)
            {
                if ( (thisCell->rowIndex == rowIndex) && (thisCell->colIndex == colIndex) )
                {
                    thisCell->statusLabel->setStyleSheet("QLabel { "
                                                         "background-color : lightgrey; "
                                                         "color : black; "
                                                         "border-style: outset; "
                                                         "border-color: black;"
                                                         "border-width: 2px;"
                                                         "padding: 2px; }");
                    thisCell->statusLabel->setText("Check Status");
                    thisCell->statusLabel->repaint();
                    break;
                }
            }
            QApplication::processEvents();
        }
    }
    ui->registerServicesTable->repaint();
    displayMessage("Send Browse Service Message Completed");
}

void sqanTestTool::registerBox_checked(bool checked)
{
    if ( checked == true )
    {
        //qDebug() << "Register box clicked on";

        QCheckBox *registerCheckBox = (QCheckBox *)QObject::sender();
        tableCell *thisCell = checkBoxMap[registerCheckBox];
        QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, thisCell->colIndex);

        // DBLogger UI
        DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
        dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
        dblr.addUIObservation("checked", std::to_string(checked));
        dblr.addUIObservation("selected dai", std::to_string(thisCell->rowIndex-1));
        dblr.addUIObservation("destination service", columnHdr->text().toStdString().c_str());
        dbLogger->dbLog(dblr);

        // Rows start at 1 for Device zero
        int status = registerForService(thisCell->rowIndex-1, columnHdr->text());

        if  ( status == 0 )
        {
            // This service is available to this source
            // and the source is registered already
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgreen; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: darkgreen;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Available/Registered");
            thisCell->statusLabel->repaint();
        }
        else if ( status == 1)
        {
            // This service is available to this source
            // but the source is not registered yet
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: darkgreen;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Available/Not Registered");
            thisCell->statusLabel->repaint();
        }
        else if ( status == -1 )
        {
            // This service is not available to this source
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : darkred; "
                                                 "border-style: outset; "
                                                 "border-color: black;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Unavailable");
            thisCell->statusLabel->repaint();
        }
        else
        {
            //Nothing
        }

        registerCheckBox->setCheckState(Qt::Unchecked);
        registerCheckBox->repaint();
    }
    else
    {
        //qDebug() << "Register box clicked off";
    }
}

void sqanTestTool::deregisterBox_checked(bool checked)
{
    if ( checked == true )
    {
        //qDebug() << "Deregister box clicked on";

        QCheckBox *deregisterCheckBox = (QCheckBox *)QObject::sender();
        tableCell *thisCell = checkBoxMap[deregisterCheckBox];

        //qDebug() << "rowIndex=" << QString::number(thisCell->rowIndex);
        //qDebug() << "colIndex=" << QString::number(thisCell->colIndex);

        QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, thisCell->colIndex);
        // Rows start at 1 for Device zero
        int status = deregisterFromService(thisCell->rowIndex-1, columnHdr->text());

        if  ( status == 0 )
        {
            // Deregistration completed successfully
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: darkgreen;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Available/Not Registered");
            thisCell->statusLabel->repaint();
        }
        else
        {
            // Deregistration was not successful
            // Set statusLabel text to Unavailable
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : darkred; "
                                                 "border-style: outset; "
                                                 "border-color: black;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Unavailable");
            thisCell->statusLabel->repaint();
        }

        deregisterCheckBox->setCheckState(Qt::Unchecked);
        deregisterCheckBox->repaint();
    }
    else
    {
        //qDebug() << "Deregister box  clicked off";
    }
}

void sqanTestTool::revokeBox_checked(bool checked)
{
    if ( checked == true )
    {
        //qDebug() << "Revoke box clicked on";

        QCheckBox *revokeCheckBox = (QCheckBox *)QObject::sender();
        tableCell *thisCell = checkBoxMap[revokeCheckBox];

        //qDebug() << "rowIndex=" << QString::number(thisCell->rowIndex);
        //qDebug() << "colIndex=" << QString::number(thisCell->colIndex);

        QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, thisCell->colIndex);
        // Rows start at 1 for Device zero
        int status = revokeRegistrationOfService(thisCell->rowIndex-1, columnHdr->text());

        if  ( (status == 0)  || (status == -1) )
        {
            // Registration completed successfully
            // Set statusLabel text to Not Registered
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : darkred; "
                                                 "border-style: outset; "
                                                 "border-color: black;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Unavailable");
            thisCell->statusLabel->repaint();
        }
        else if ( status == -2 )
        {
            // Message did not go through
            // do nothing
            thisCell->statusLabel->repaint();
        }

        revokeCheckBox->setCheckState(Qt::Unchecked);
        revokeCheckBox->repaint();
    }
    else
    {
        //qDebug() << "Revoke box clicked off";
    }
}

void sqanTestTool::getRegistrationStatusBox_checked(bool checked)
{
    if ( checked == true )
    {
        //qDebug() << "Get Status box clicked on";

        QCheckBox *getStatusCheckBox = (QCheckBox *)QObject::sender();
        tableCell *thisCell = checkBoxMap[getStatusCheckBox];

        //qDebug() << "rowIndex=" << QString::number(thisCell->rowIndex);
        //qDebug() << "colIndex=" << QString::number(thisCell->colIndex);

        QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, thisCell->colIndex);
        //qDebug() << "columnHdr=" << columnHdr->text();

        // Rows start at 1 for Device zero
        int status = getRegistrationStatus(thisCell->rowIndex-1, columnHdr->text());

        if  ( status == 0 )
        {
            // This service is available to this source
            // and the source is registered already
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgreen; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: darkgreen;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Available/Registered");
            thisCell->statusLabel->repaint();
        }
        else if ( status == 1)
        {
            // This service is available to this source
            // but the source is not registered yet
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : black; "
                                                 "border-style: outset; "
                                                 "border-color: darkgreen;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Available/Not Registered");
            thisCell->statusLabel->repaint();
        }
        else if ( status == -1 )
        {
            // This service is not available to this source
            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                 "background-color : lightgrey; "
                                                 "color : darkred; "
                                                 "border-style: outset; "
                                                 "border-color: black;"
                                                 "border-width: 2px;"
                                                 "padding: 2px; }");

            thisCell->statusLabel->setText("Unavailable");
            thisCell->statusLabel->repaint();
        }
        else
        {
            //Nothing
        }

        getStatusCheckBox->setCheckState(Qt::Unchecked);
        getStatusCheckBox->repaint();
    }
    else
    {
        //qDebug() << "Get Status box clicked off";
    }
}
*/

/*JR
void sqanTestTool::getAllStatusButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    for (uint8_t rowIndex=1; rowIndex<ui->registerServicesTable->rowCount(); rowIndex++)
    {
        for (uint8_t colIndex=1; colIndex<ui->registerServicesTable->columnCount(); colIndex++)
        {
            // Check if the column header text with destination service is for
            // the source device.  Don't allow a source to register for it's own service.
            // Grey out the cell.
            QString rowLabel = DynArray[rowIndex-1].devName;
            // Columns start at one for first service
            QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);
            QString colLabel = columnHdr->text();

            if ( colLabel.contains(rowLabel) )
            {
                continue;
            }
            else
            {
                // Rows start at 1 for Device zero
                int status = getRegistrationStatus(rowIndex-1, columnHdr->text());

                for (auto itr = checkBoxMap.begin(); itr != checkBoxMap.end(); ++itr)
                {
                    tableCell *thisCell = itr.value();

                    if ( (thisCell->rowIndex == rowIndex) && (thisCell->colIndex == colIndex) )
                    {
                        if  ( status == 0 )
                        {
                            // GetRegistrationStatus completed successfully
                            // And we are registered already
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgreen; "
                                                                 "color : black; "
                                                                 "border-style: outset; "
                                                                 "border-color: darkgreen;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Available/Registered");
                            thisCell->statusLabel->repaint();
                        }
                        else if ( status == 1 )
                        {
                            // GetRegistrationStatus completed successfully
                            // But we are not registered
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgrey; "
                                                                 "color : black; "
                                                                 "border-style: outset; "
                                                                 "border-color: darkgreen;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Available/Not Registered");
                            thisCell->statusLabel->repaint();
                        }
                        else if ( status == -1 )
                        {
                            // This service is not available or does not exist
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgrey; "
                                                                 "color : darkred; "
                                                                 "border-style: outset; "
                                                                 "border-color: black;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Unavailable");
                            thisCell->statusLabel->repaint();
                        }
                        ui->registerServicesTable->repaint();
                        break;
                    }
                }
            }
            QApplication::processEvents();
        }
        QApplication::processEvents();
    }
    ui->registerServicesTable->repaint();
}

void sqanTestTool::registerAllButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    for (uint8_t rowIndex=1; rowIndex<ui->registerServicesTable->rowCount(); rowIndex++)
    {
        for (uint8_t colIndex=1; colIndex<ui->registerServicesTable->columnCount(); colIndex++)
        {
            // Check if the column header text with destination service is for
            // the source device.  Don't allow a source to register for it's own service.
            // Grey out the cell.
            QString rowLabel = DynArray[rowIndex-1].devName;
            // Columns start at one for first service
            QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);
            QString colLabel = columnHdr->text();

            if ( colLabel.contains(rowLabel) )
            {
                continue;
            }
            else
            {
                // Rows start at 1 for Device zero
                int status = registerForService(rowIndex-1, columnHdr->text());

                // Registration completed successfully
                // Set statusLabel text to Registered
                for (auto itr = checkBoxMap.begin(); itr != checkBoxMap.end(); ++itr)
                {
                    tableCell *thisCell = itr.value();

                    if ( (thisCell->rowIndex == rowIndex) && (thisCell->colIndex == colIndex) )
                    {
                        if ( status == 0 )
                        {
                            // This source is registered
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgreen; "
                                                                 "color : black; "
                                                                 "border-style: outset; "
                                                                 "border-color: darkgreen;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Available/Registered");
                            thisCell->statusLabel->repaint();
                        }
                        else
                        {
                            // Couldn't find service or it is unavailable
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgrey; "
                                                                 "color : darkred; "
                                                                 "border-style: outset; "
                                                                 "border-color: black;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Unavailable");
                            thisCell->statusLabel->repaint();
                        }
                        break;
                    }
                }
            }
            QApplication::processEvents();
        }
        QApplication::processEvents();
    }
}

void sqanTestTool::deregisterAllButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    for (uint8_t rowIndex=1; rowIndex<ui->registerServicesTable->rowCount(); rowIndex++)
    {
        for (uint8_t colIndex=1; colIndex<ui->registerServicesTable->columnCount(); colIndex++)
        {
            // Check if the column header text with destination service is for
            // the source device.  Don't allow a source to register for it's own service.
            // Grey out the cell.
            QString rowLabel = DynArray[rowIndex-1].devName;
            // Columns start at one for first service
            QLabel *columnHdr = (QLabel *)ui->registerServicesTable->cellWidget(0, colIndex);
            QString colLabel = columnHdr->text();

            if ( colLabel.contains(rowLabel) )
            {
                continue;
            }
            else
            {
                for (auto itr = checkBoxMap.begin(); itr != checkBoxMap.end(); ++itr)
                {
                    tableCell *thisCell = itr.value();

                    if ( (thisCell->rowIndex == rowIndex) && (thisCell->colIndex == colIndex) )
                    {
                        // Rows start at 1 for Device zero
                        int status = deregisterFromService(rowIndex-1, columnHdr->text());

                        if  ( status == 0 )
                        {
                            // The source was deregistered
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgrey; "
                                                                 "color : black; "
                                                                 "border-style: outset; "
                                                                 "border-color: darkgreen;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Available/Not Registered");
                            thisCell->statusLabel->repaint();
                        }
                        else
                        {
                            // The service does not exist or is not available
                            thisCell->statusLabel->setStyleSheet("QLabel { "
                                                                 "background-color : lightgrey; "
                                                                 "color : darkred; "
                                                                 "border-style: outset; "
                                                                 "border-color: black;"
                                                                 "border-width: 2px;"
                                                                 "padding: 2px; }");

                            thisCell->statusLabel->setText("Unavailable");
                            thisCell->statusLabel->repaint();
                        }
                        break;
                    }
                }
            }
            QApplication::processEvents();
        }
        QApplication::processEvents();
    }
    ui->registerServicesTable->repaint();
}

*/

//void sqanTestTool::on_savePlotPushButton_clicked()
//{
//    // DBLogger UI
//    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
//    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
//    dbLogger->dbLog(dblr);
//    QString plotFilename = QFileDialog::getSaveFileName(this, "Save Plot",
//                                                        QCoreApplication::applicationDirPath(), "JPEG (*.JPEG);;PNG (*.png");

//    if(!plotFilename.isNull())
//    {
//        QPixmap pixMap = this->ui->dataPlotGraphicsView_2->grab();
//        pixMap.save(plotFilename);
//        displayMessage("Plot Saved to " + plotFilename);
//    }
//}



//void sqanTestTool::on_dataPlotGraphicsView_customContextMenuRequested(const QPoint &pos)
//{
//    // DBLogger UI
//    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
//    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
//    dbLogger->dbLog(dblr);

//}

void sqanTestTool::on_savePowerPlotPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    QString plotFilename = QFileDialog::getSaveFileName(this, "Save Plot",
                                                        QCoreApplication::applicationDirPath(), "JPEG (*.JPEG);;PNG (*.png");

    if(!plotFilename.isNull())
    {
        QPixmap pixMap = this->ui->powerMeasurementsGraphicsView->grab();
        pixMap.save(plotFilename);
        displayMessage("Plot Saved to " + plotFilename);
    }
}

/*JR
void sqanTestTool::getServiceIdFromText(QString serviceText, uint16_t *descriptorId)
{
    if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Reserved1))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Reserved1; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Video; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Audio))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Audio; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PositionSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::PositionSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::EnvironmentSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::EnvironmentSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::HealthSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::HealthSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::SimpleUI; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control)))
              && !serviceText.contains("LRF")
              && !serviceText.contains("AR") ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Control; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Status; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::RTA; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Reserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Reserved2; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::HybridSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::HybridSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::MotionSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::MotionSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::TemperatureSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::TemperatureSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::MoistureSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::MoistureSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::VibrationSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::VibrationSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::ChemicalSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::ChemicalSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::FlowSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::FlowSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::ForceSense))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::ForceSense; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF)))
              && !serviceText.contains("Control") ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::LRF; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::LRFControl; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::AROverlay; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::ARControl))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::ARControl; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Container))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::Container; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::ReservedMin))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::ReservedMin; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::ReservedMax))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::ReservedMax; }
    else if ( serviceText.contains(QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator))) ) { *descriptorId = SqanSolNet::sqanSolNetServiceIds::PacketGenerator; }
    else {
        qDebug() << Q_FUNC_INFO << ": Unknown Service Id";
        *descriptorId = 0xFFFF;
    }
}

void sqanTestTool::getPropertyDescriptorIdFromText(QString descriptorText, uint16_t *descriptorId)
{
    if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Label; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Version))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Version; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Endpoint; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Encoding))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::Encoding; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::FlowPolicy))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::FlowPolicy; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceName))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceName; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::AssociatedService))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::AssociatedService; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved1)))
              && !descriptorText.contains("10")
              && !descriptorText.contains("11")
              && !descriptorText.contains("12") ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved1; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved2; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved3))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved3; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved4))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved4; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved5))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved5; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved6))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved6; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved7))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved7; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved8))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved8; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoLensDistortion))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoLensDistortion; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved9))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved9; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved10))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved10; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoControl))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoControl; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved11))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved11; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved12))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved12; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::UIMotion))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::UIMotion; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::UIButtons))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::UIButtons; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMin))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMin; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMax))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMax; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::IMUFloatUpdateRate))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::IMUFloatUpdateRate; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::IMUSensorNoise))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::IMUSensorNoise; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved1))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved1; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved2; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::NetworkingIP))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::NetworkingIP; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::NetworkingTCPPort))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::NetworkingTCPPort; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::NetworkingUDPPort))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::NetworkingUDPPort; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved1))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved1; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved2; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::ARReserved1))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::ARReserved1; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::ARReserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::ARReserved2; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::PowerBattery))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::PowerBattery; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved1))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved1; }
    else if ( descriptorText.contains(QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved2))) ) { *descriptorId = SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved2; }
    else {
        qDebug() << Q_FUNC_INFO << ": Unknown Descriptor Id";
        *descriptorId = 0xFFFF;
    }
}
*/

/*******************************************
 * Power Measurment Slots
 ******************************************/
/*JR
void sqanTestTool::generatePowerMeasurementsDataPlot()
{
    // Create Power Measurements Plot Chart and Graphics Scene
    powerMeasurementScene =  new QGraphicsScene(this);
    ui->powerMeasurementsGraphicsView->setScene(powerMeasurementScene);
    powerMeasurementsChartView =  new QChartView();
    pwrPlotChart->setAnimationOptions(QChart::AllAnimations);
    powerMeasurementScene->addItem(pwrPlotChart);
    ui->powerMeasurementsGraphicsView->setScene(powerMeasurementScene);
    pwrPlotChart->startPlot();
}

void sqanTestTool::on_pwrMeasurementStartBtn_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    dmmV = new Dmm("169.254.4.62");
    dmmI = new Dmm("169.254.4.61");
    dmmI->connect();
    dmmV->connect();

    pwr_timer = new QTimer(this);
    connect(pwr_timer, SIGNAL(timeout()), this, SLOT(refreshPwr()));
    pwr_timer->start(1000);



    pwrPlotChart = new PlotChart(0,0,1000,10,1, QDir::currentPath() + "/pwrOutput.txt");
    pwrPlotChart->setPreferredSize(900,500);
    pwrPlotChart->setYreal(Power);
    generatePowerMeasurementsDataPlot();
}

void sqanTestTool::refreshPwr()
{
    Voltage = dmmV->getVolt();
    Current = dmmI->getCurrent();
    ui->currentLcdNumber->display(Current*1000);
    ui->voltageLcdNumber->display(Voltage);
    Power = Voltage * Current;
    ui->powerLcdNumber->display(Power*1000);
    pwrPlotChart->setYreal(Power);
}
*/

void sqanTestTool::on_performancePlotPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    QString plotFilename = QFileDialog::getSaveFileName(this, "Save Performance Plot",
                                                        QCoreApplication::applicationDirPath(), "JPEG (*.JPEG);;PNG (*.png)");

    if(!plotFilename.isNull())
    {
        QPixmap pixMap = this->ui->dataPlotGraphicsView_2->grab();
        pixMap.save(plotFilename);
        displayMessage("Plot Saved to " + plotFilename);
    }
}
/*JR
void sqanTestTool::on_pwrMeasurementStopBtn_clicked()
{
    dmmV->disconnect();
    dmmI->disconnect();
}
*/

/*******************************************
 * SQAN Validation Slots
 ******************************************/
void sqanTestTool::on_solnetValidationButton_clicked()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->testAutomationDeviceList->currentItem();
    int selected = ui->testAutomationDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    if(ui->testAutomationDeviceList->selectedItems().count() == 0)
    {
        displayMessage("Please Select A Device");
    }
    else
    {
        serviceList.removeDuplicates();

        displayMessage("Please Wait... Validating SQAN SolNet Command Compliance");

      /*JR  validateBrowseAdvertiseServices(selected);
        sleep(5);
        validateRegisterRequestResponse(selected);
        sleep(5);
        validateGetStatusRequestResponse(selected);
        sleep(5);
        validateDeregisterRequestResponse(selected);
        sleep(5);
        validateRevokeRegistrationRequestResponse(selected);
        sleep(5);
        validateRevokeNetAssociationRequestResponse(selected);
        sleep(5);
        */

        displayMessage("SQAN SolNet Command Compliance Completed");
    }
}

/*JR void sqanTestTool::on_serviceDescTreeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    QTreeWidgetItem *item = current;
    std::vector <std::string> vCur(1);
    std::vector <std::string> vPrev(1);
    std::stringstream coss;
    std::stringstream poss;

    if(current)
    {
        vCur[0] = item->text(0).toStdString();
        while(item->parent())
        {
            item = item->parent();
            vCur.push_back(">");
            vCur.push_back(item->text(0).toStdString());
        }
        for(std::vector<std::string>::reverse_iterator rit = vCur.rbegin(); rit != vCur.rend(); ++rit)
        {
            coss << *rit;
        }
    }
    if(previous)
    {
        item = previous;
        vPrev[0] = item->text(0).toStdString();
        while(item->parent())
        {
            item = item->parent();
            vPrev.push_back(">");
            vPrev.push_back(item->text(0).toStdString());
        }
        for(std::vector<std::string>::reverse_iterator rit = vPrev.rbegin(); rit != vPrev.rend(); ++rit)
        {
            poss << *rit;
        }
    }
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("current", coss.str());
    dblr.addUIObservation("previous", poss.str());
    dbLogger->dbLog(dblr);
}
*/

void sqanTestTool::on_deviceComboBox_activated(const QString &arg1)
{
    Q_UNUSED(arg1)
    QString currentText = ui->deviceComboBox->currentText();
    int dai = currentText.split(indexDelim)[0].toInt();

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("currentText", currentText.toStdString().c_str());
    dblr.addUIObservation("DynArrayIndex", std::to_string(dai).c_str());
    dbLogger->dbLog(dblr);

    // Enable Related UI Elements
    ui->printServicesPushButton->setEnabled(true);

    ui->serviceDescTreeWidget->setEnabled(true);

    ui->serviceDescGroupBox->setEnabled(true);
    ui->serviceDescListWidget->setEnabled(true);
    ui->serviceDescAddPushButton->setEnabled(true);
    ui->serviceDescRemovePushButton->setEnabled(true);

    ui->descPlansGroupBox->setEnabled(true);
    ui->descPlansListWidget->setEnabled(true);
    ui->descPlanImportPushButton->setEnabled(true);
    ui->descPlanExportPushButton->setEnabled(true);

    ui->serviceDescListWidget->clear();
    ui->descPlansListWidget->clear();
    ui->serviceDescTreeWidget->clear();

    ui->descPlansGroupBox->setTitle("0 Descriptor Plans");
    ui->serviceDescGroupBox->setTitle("0 Service Descriptors");

    ui->serviceDescriptorPlainTextEdit->appendPlainText(currentText + " Device Selected");

   //JR showServiceDescriptors(); // #WIP Shawn10

//    openDatabase();
    if (dbConnected) {
        // Populate any existing plans in descPlansListWidget
        QSqlQuery plans;
        plans.prepare("SELECT ts, string_agg(DISTINCT service_name, ',') AS tooltip FROM tb_descriptor_plans WHERE device_type = :devtype GROUP BY ts ORDER BY ts");
      //JR  plans.bindValue(":devtype", QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType)));

        if ( !plans.exec() ) {
            qDebug() << plans.lastQuery();
            qDebug() << plans.boundValues();
            qDebug() << plans.lastError().databaseText();
        }
        int i = 0;
        while( plans.next() ) {
            i++;
            QListWidgetItem *tsItem = new QListWidgetItem(plans.value("ts").toString());
            tsItem->setToolTip(plans.value("tooltip").toString());
            ui->descPlansListWidget->addItem(tsItem);
        }
        ui->descPlansGroupBox->setTitle(QString("%1 Descriptor Plans").arg(i));
//        closeDatabase();
    }
}

/*JR
void sqanTestTool::enableObserverMode()
{
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);

    SquadLeaderModeDialog *squadLeaderModeDialog = new SquadLeaderModeDialog();
    squadLeaderModeDialog->exec();

    uint8_t type = SqanProduct::ENABLE;

    uint8_t rssiConnect = squadLeaderModeDialog->getRssiConnect();
    uint8_t rssiDisconnect = squadLeaderModeDialog->getRssiDisconnect();
    uint8_t phyRateCap = squadLeaderModeDialog->getPhyRateCapIndex();
    uint16_t devType = DynArray[selected].devType;

    if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
    {
        if(devType == 0x1001)
        {
            //Send Enable Observer Mode Command to Firmware
            int status = DynArray[selected].sqanProduct->SendObserverModeSetCmd(type, rssiConnect, rssiDisconnect, phyRateCap, devType);
            sleep(2);
            //Clear Association
            DynArray[selected].sqanAssociation->SendClearAssociationCmd();
            sleep(2);

            // Reset the radio
            DynArray[selected].sqanSystem->SendResetRadioCmd();
            sleep(2);

            updateSqanAttr(selected);

            // Update the view of this device
            updateDeviceUiForSelected(selected);

            //DBLog DynArray changes
            dbLogDevice(selected);

            if(status == 0)
            {
                displayMessage("Observer Mode Enabled for " + DynArray[selected].devName);
            }
        }
    }
}

void sqanTestTool::disableObserverMode()
{
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dblr.addUIObservation("selected", std::to_string(selected));
    dbLogger->dbLog(dblr);

    uint8_t type = SqanProduct::DISABLE; //Disable
    uint8_t rssiConnect = 188;
    uint8_t rssiDisconnect = 180;
    uint8_t phyRateCap = SqanProduct::Mbps_320; //320 Mbps
    uint16_t devType = DynArray[selected].devType;

    if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
    {
        int status = DynArray[selected].sqanProduct->SendObserverModeSetCmd(type, rssiConnect, rssiDisconnect, phyRateCap, devType);
        sleep(2);

        //Get Observer Mode Status
        DynArray[selected].sqanProduct->SendObserverModeGetCmd();
        sleep(2);

        DynArray[selected].sqanSystem->SendResetRadioCmd();

        updateSqanAttr(selected);

        // Update the view of this device
        updateDeviceUiForSelected(selected);

        //DBLog DynArray changes
        dbLogDevice(selected);

        if(status == 0)
        {
            displayMessage("Observer Mode Disabled for " + DynArray[selected].devName);
        }
    }
}
*/

void sqanTestTool::on_clearTestAutomationTextPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    ui->testAutomationPlainTextEdit->clear();
}

void sqanTestTool::on_saveTestAutomationTextPushButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save Test Automation Log"), QCoreApplication::applicationDirPath(),
                                                        "Text File (*.txt)");

    if(!saveFileName.isNull())
    {
        QFile outFile(saveFileName);

        if(!outFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return;
        }

        QTextStream out(&outFile);
        out << ui->testAutomationPlainTextEdit->toPlainText() << endl;
    }
}

void sqanTestTool::on_apiComplianceButton_clicked()
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //static bool testSqanDone = false;
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->testAutomationDeviceList->currentItem();
    int selected = ui->testAutomationDeviceList->row(item);

    if(ui->testAutomationDeviceList->selectedItems().count() == 0)
    {
        displayMessage("Please Select A Device");
    }
    else
    {
        // Validate SqanIdentity, SqanSecurity, SqanAssociation, SqanStream, SqanLink, SqanMetrics, SqanProduct, and SqanSystem Commands
        displayMessage("Please Wait... Validating SQAN API Command Compliance");

       /*JR validateSqanIdentity(selected);
        sleep(5);
        validateSqanSecurity(selected);
        sleep(5);
        validateSqanAssociation(selected);
        sleep(5);
        validateSqanStream(selected);
        sleep(5);
        validateSqanLink(selected);
        sleep(5);
        validateSqanMetrics(selected);
        sleep(5);
        validateSqanProduct(selected);
        sleep(5);
        validateSqanSystem(selected);
        sleep(5);
        */

        //testSqanDone = true;
        displayMessage("SQAN API Compliance Completed");
    }
}

/*JR
void sqanTestTool::setHibernationMode()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Default -- Soft Hibernation
    uint8_t hibernationMode = SqanProduct::HARD_HIBERNATION;
    uint8_t persist = 1;
    uint16_t idleTime = 15; // 10 Seconds -- Default - 15 Seconds
    uint8_t softHibernationTime = 16; // 4 Seconds -- Default - 16 Seconds

    if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
    {
        DynArray[selected].sqanProduct->SendSetHibernationModeCmd(hibernationMode, persist, idleTime, softHibernationTime);
        usleep(10);
        displayMessage(DynArray[selected].devName + " Set Hibernation Mode Completed");
    }
}

void sqanTestTool::getHibernationMode()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->netDeviceList->currentItem();
    int selected = ui->netDeviceList->row(item);


    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    if ( (DynArray[selected].sqanInterface != nullptr) && DynArray[selected].isReady )
    {
        DynArray[selected].sqanProduct->SendGetHibernationModeCmd();
        usleep(10);
        displayMessage(DynArray[selected].devName + " Get Hibernation Mode Completed");
    }
}

void sqanTestTool::validateSqanIdentity(int selected)
{
    uint8_t coordinatorCapable = 0;
    uint8_t coordinatorPrecedence = 0;
    uint8_t persist = 1;

    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanIdentity = DynArray[selected].sqanInterface->GetSqanIdentity();

            usleep(10);

            //Set Device Type Command
            int status_setDeviceType = DynArray[selected].sqanIdentity->SendSetDeviceTypeCmd(DynArray[selected].devType);

            if(status_setDeviceType == 0)
            {
                ui->testAutomationPlainTextEdit->setPlainText(DynArray[selected].devName + " Set Device Type Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Device Type Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Get Device Type Command
            int status_getDeviceType = DynArray[selected].sqanIdentity->SendGetDeviceTypeCmd();

            if(status_getDeviceType == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Device Type Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Device Type Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Set Coordinator
            int status_setCoordinator = DynArray[selected].sqanIdentity->SendSetCoordinatorCmd(coordinatorCapable, coordinatorPrecedence, persist);

            if(status_setCoordinator == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Coordinator Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Coordinator Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Get Coordinator
            int status_getCoordinator = DynArray[selected].sqanIdentity->SendGetCoordinatorCmd();

            if(status_getCoordinator == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Coordinator Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Coordinator Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Set MAC Address
//            QChar* macAddr = QString::fromStdString(DynArray[selected].sqanIdentity->GetMacAddressStr()).data();
//            qDebug() <<"MAC Address: " << macAddr;
//            int status_setMacAddress = DynArray[selected].sqanIdentity->SendSetMacAddressCmd((uint8_t* )macAddr);

//            if(status_setMacAddress == 0)
//            {
//                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set MAC Address Command Compliant");
//                ui->testAutomationPlainTextEdit->repaint();
//                ui->testAutomationPlainTextEdit->update();
//            }
//            else
//            {
//                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set MAC Address Command Non-Compliant");
//                ui->testAutomationPlainTextEdit->repaint();
//                ui->testAutomationPlainTextEdit->update();
//            }

            //Get MAC Address
            int status_getMacAddress = DynArray[selected].sqanIdentity->SendGetMacAddressCmd();

            if(status_getMacAddress == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get MAC Address Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get MAC Address Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            }
        }
}

void sqanTestTool::validateSqanSecurity(int selected)
{
    if ( DynArray[selected].sqanInterface != nullptr )
    {
        if ( DynArray[selected].sqanInterface->IsReady() )
        {
            DynArray[selected].sqanSecurity = DynArray[selected].sqanInterface->GetSqanSecurity();

            usleep(10);

            //Authenticate Role
            int status_authenticasteRole = DynArray[selected].sqanSecurity->SendAuthenticateRoleCmd();
            if(status_authenticasteRole == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Authenticate Role Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Authenticate Role Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Start Crypto Session
            int status_startCryptoSession = DynArray[selected].sqanSecurity->SendCryptoSessionStartCmd();
            if(status_startCryptoSession == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Crypto-Session Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Crypto-Session Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
        }
    }
}

void sqanTestTool::validateSqanAssociation(int selected)
{
    uint8_t timeout = 30;

    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanAssociation = DynArray[selected].sqanInterface->GetSqanAssociation();

            uint8_t type = 1; //! invite

            int status_associationInvite = DynArray[selected].sqanAssociation->SendStartAssociationCmd(type, timeout);
            sleep(1);

            if(status_associationInvite == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Start Association - Invite Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName+ " Start Association - Invite Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            sleep(1);

            type = 2;

            int status_associationSearch = DynArray[1].sqanAssociation->SendStartAssociationCmd(type, timeout);

            // Wait for the two devices to connect
            sleep(timeout);

            if(status_associationSearch == 0)
            {
               ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Start Association - Search Command Compliant");
               ui->testAutomationPlainTextEdit->repaint();
               ui->testAutomationPlainTextEdit->update();
            }
            else
            {
               ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Start Association - Search Command Non-Compliant");
               ui->testAutomationPlainTextEdit->repaint();
               ui->testAutomationPlainTextEdit->update();
            }
        }
    }
}

void sqanTestTool::validateSqanStream(int selected)
{
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanStream = DynArray[selected].sqanInterface->GetSqanStream();

            //! Just use the first device and run these test on one device
            if (DynArray[selected].sqanInterface->GetIndex() == 0)
            {
                //! Test SetStreamsSpec on one device
                std::array<SqanStream::sqanPeerRecord, MAX_PEERS> &testPeers = DynArray[selected].sqanStream->GetPeerRef();
                uint8_t peerIndex = 0;
                for (int i=0; i<(int)testPeers.size(); i++)
                {
                    if (testPeers[i].inUse)
                    {
                        peerIndex = i;
                        break; //! out of for loop
                    }
                }

                //! Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the sub-stream is High Priority
                uint8_t txPriority = 1;
                //! Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the SQAN-MAC
                //! may automatically discard the sub-stream data in the event of network congestions
                uint8_t txDiscard = 3;
                //! Max duration (in microseconds) between transmit opportunities
                uint32_t maxTxLatency = 100;

                //Set Stream Spec
                int status_SetStreamSpec = DynArray[selected].sqanStream->SendSetStreamsSpecCmd(peerIndex, txPriority, txDiscard, maxTxLatency);

                if(status_SetStreamSpec == 0)
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Stream Spec Command Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
                else
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Stream Spec Command Non-Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }

                //Get Stream Spec
                int status_GetStreamSpec = DynArray[selected].sqanStream->SendGetStreamSpecCmd(peerIndex);
                if(status_GetStreamSpec == 0)
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Stream Spec Command Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
                else
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Stream Spec Command Non-Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }

                //Get Streams Status
                int status_GetStreamStatus = DynArray[selected].sqanStream->SendGetStreamsStatusCmd();
                if(status_GetStreamStatus == 0)
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Stream Status Command Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
                else
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Stream Status Command Non-Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }

                //Get Extended Streams Status
                uint8_t version = 2;
                int status_GetExtendedStreamsStatus = DynArray[selected].sqanStream->SendGetExtendedStreamsStatusCmd(version);
                if(status_GetExtendedStreamsStatus == 0)
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Extended Streams Status Command Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
                else
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Extended Streams Status Command Non-Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }

                //Get Peer Firmware Version
                int status_GetPeerFirmwareVersion = DynArray[selected].sqanStream->SendGetPeerFirmwareVersionCmd(peerIndex);
                if(status_GetPeerFirmwareVersion == 0)
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Peer Firmware Version Command Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
                else
                {
                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Peer Firmware Version Command Non-Compliant");
                    ui->testAutomationPlainTextEdit->repaint();
                    ui->testAutomationPlainTextEdit->update();
                }
            }
        }
    }
}

void sqanTestTool::validateSqanLink(int selected)
{
    uint16_t startupScanDuration = 2;
    uint8_t scanDutyCycle = 4;
    uint8_t persist = 0;
    uint16_t idleScanFrequency = 60;
    uint32_t maxServiceInterval = 32000;

    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanLink = DynArray[selected].sqanInterface->GetSqanLink();

            //Set Scan Duty Cycle
            int status_SetScanDutyCycle = DynArray[selected].sqanLink->SendSetScanDutyCycleCmd(startupScanDuration, scanDutyCycle, persist);

            if(status_SetScanDutyCycle == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Scan Duty Cycle Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Scan Duty Cycle Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Get Scan Duty Cycle
            int status_GetScanDutyCycle = DynArray[selected].sqanLink->SendGetScanDutyCycleCmd();

            if(status_GetScanDutyCycle == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Scan Duty Cycle Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Scan Duty Cycle Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Set Idle Scan Frequency
            int status_SetIdleScanFrequency = DynArray[selected].sqanLink->SendSetIdleScanFrequencyCmd(idleScanFrequency, persist);

            if(status_SetIdleScanFrequency == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Idle Scan Frequency Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Idle Scan Frequency Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Get Idle Scan Frequency
            int status_GetIdleScanFrequency = DynArray[selected].sqanLink->SendGetIdleScanFrequencyCmd();

            if(status_GetIdleScanFrequency == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Idle Scan Frequency Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Idle Scan Frequency Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Reset Service Interval
            int status_ResetServiceInterval = DynArray[selected].sqanLink->SendResetMaxServiceIntervalCmd();

            if(status_ResetServiceInterval == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName+ " Set Reset Max Service Interval Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Reset Max Service Interval Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Set Maximum Service Interval
            int status_MaxServiceInterval = DynArray[selected].sqanLink->SendSetMaxServiceIntervalCmd(maxServiceInterval);

            if(status_MaxServiceInterval == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Max Service Interval Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Set Max Service Interval Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
        }
    }
}

void sqanTestTool::validateSqanMetrics(int selected)
{
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanMetrics = DynArray[selected].sqanInterface->GetSqanMetrics();

            //Get Wireless Metrics
            int status_GetWirelessMetrics = DynArray[selected].sqanMetrics->SendGetWirelessMetrics();
            if(status_GetWirelessMetrics == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Wireless Metrics Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Wireless Metrics Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
        }
    }
}

void sqanTestTool::validateSqanProduct(int selected)
{
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanProduct = DynArray[selected].sqanInterface->GetSqanProduct();

            //Get Network ID
            int status_GetNetworkId = DynArray[selected].sqanProduct->SendGetNetworkIdCmd();
            if(status_GetNetworkId == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Network Id Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Network Id Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
        }
    }
}

void sqanTestTool::validateSqanSystem(int selected)
{
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanSystem = DynArray[selected].sqanInterface->GetSqanSystem();

            //Reset Radio
            int status_ResetRadio = DynArray[selected].sqanSystem->SendResetRadioCmd();
            if(status_ResetRadio == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Reset Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Reset Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Idle Radio
            int status_IdleRadio = DynArray[selected].sqanSystem->SendIdleRadioCmd();
            if(status_IdleRadio == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Idle Radio Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Idle Radio Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }

            //Reset Device
            int status_ResetDevice = DynArray[selected].sqanSystem->SendResetDeviceCmd();
            if(status_ResetDevice == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Reset Device Command Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Reset Device Command Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            sleep(1);
        }
    }
}

void sqanTestTool::validateBrowseAdvertiseServices(int selected)
{

    if ( DynArray[selected].sqanInterface->IsReady() )
    {

        DynArray[selected].sqanStream = DynArray[selected].sqanInterface->GetSqanStream();
        DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

        std::array<SqanStream::sqanPeerRecord, MAX_PEERS> peers;
        DynArray[selected].sqanStream->GetPeers(&peers);

        uint8_t peerIndex = 0;
        bool sendBrowse = false;

        for (uint8_t i=0; i<peers.size(); i++)
        {
            peerIndex = i;
            sendBrowse = false;

            if ( peers[i].inUse )
            {
                switch( peers[i].recordVersion )
                {
                case 1:
                {
                    if ( peers[i].sqanPeerRecord1.linkStatus == 1 )
                    {
                        sendBrowse = true;
                    }
                    break;
                }
                case 2:
                {
                    if ( peers[i].sqanPeerRecord2.linkStatus == 1 )
                    {
                        sendBrowse = true;
                    }
                    break;
                }
                case 3:
                {
                    if ( peers[i].sqanPeerRecord3.linkStatus == 1 )
                    {
                        sendBrowse = true;
                    }
                    break;
                }
                case 4:
                {
                    if ( peers[i].sqanPeerRecord4.linkStatus == 1 )
                    {
                        sendBrowse = true;
                    }
                    break;
                }
                default:
                {
                    break;
                }
                }

                if ( sendBrowse )
                {
                    DynArray[selected].sqanSolNet->SetBrowse(peerIndex, true);
//                            uint8_t count = 5;

//                            while ( --count > 0 )
//                            {
//                                std::cout << "Waiting for peers to Browse and get new services..." << std::endl;
//                                sleep(5);
//                            }

                    ui->testAutomationPlainTextEdit->setPlainText(DynArray[selected].devName + " Browse/Advertise Services Commands Compliant");
                }
                else
                {
                    ui->testAutomationPlainTextEdit->setPlainText(DynArray[selected].devName + " Browse/Advertise Services Commands Non-Compliant");
                }
            }
        }

    }
}

void sqanTestTool::validateRegisterRequestResponse(int selected)
{
    //Register Request/Response
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

            std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();

            for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
            {
                SqanSolNet::sqanSolNetPeerServicesEntry *entry = &(*peerServices)[peerIndex];

                if (!entry->inUse)
                {
                    continue;
                }

                for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                {
                    if (entry->services[dataflowId].inUse)
                    {
                        if ((entry->services[dataflowId].serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status) &&
                             (!entry->services[dataflowId].ImRegistered))
                        {
                            // Get the service we want to register for
                            uint32_t serviceSelector = entry->services[dataflowId].serviceDesc.serviceSelector;
                            //autonomy = 0; Polled test                           
                            //autonomy = 1; send data automatically to registered node(s)

                            // Not Polled test
                            uint8_t autonomy = 1;

                            DynArray[selected].sqanSolNet->SendRegisterRequestMessage(peerIndex, serviceSelector, autonomy);

                            int count = 0;
                            while ((!entry->services[dataflowId].ImRegistered) && (count < 100))
                            {
                                sleep(1);
                                count++;
                            }
                            sleep(5);

                            if (entry->services[dataflowId].ImRegistered)
                            {
                                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Register Request/Response Commands Compliant");
                                break;
                            }
                            else
                            {
                                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Register Request/Response Commands Non - Compliant");
                                break;
                            }
                            sleep(5);
                        }
                    }
                }
            }
        }
    }
    sleep(1);
}

void sqanTestTool::validateGetStatusRequestResponse(int selected)
{
    //Get Status Request/Response
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

            std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();

            for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
            {
                SqanSolNet::sqanSolNetPeerServicesEntry *peerServiceEntry = &((*peerServices)[peerIndex]);

                if (peerServiceEntry->inUse)
                {
                    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                    {
                        if (peerServiceEntry->services[dataflowId].inUse)
                        {
                            if ((peerServiceEntry->services[dataflowId].serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status) &&
                                 (peerServiceEntry->services[dataflowId].ImRegistered))
                            {
                                // Set the status to RegStatusAvailableToRegister
                                DynArray[selected].sqanSolNet->SetRegistrationStatus(peerIndex,
                                                                 peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector,
                                                                 SqanSolNet::RegStatusAvailableToRegister);

                                sleep(1);

                                int status = DynArray[selected].sqanSolNet->SendGetStatusRequestMessage(peerIndex,
                                                                       peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector);

                                if(status == 0)
                                {
                                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Status Request/Response Commands Compliant");
                                    break;
                                }
                                else
                                {
                                    ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Get Status Request/Response Commands Non-Compliant");
                                    break;
                                }

                                //Wait for response
                                sleep(1);
                            }
                            // Set the status to RegStatusAvailableToRegister
                            DynArray[selected].sqanSolNet->SetRegistrationStatus(peerIndex,
                                                             peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector,
                                                             SqanSolNet::RegStatusAvailableToRegister);

                            sleep(1);

                            DynArray[selected].sqanSolNet->SendGetStatusRequestMessage(peerIndex,
                                                                   peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector);

                            //Wait for response
                            sleep(1);
                        }
                    }
                }
            }
        }
    }
    sleep(1);
}

void sqanTestTool::validateRevokeRegistrationRequestResponse(int selected)
{
    //Revoke Registration Request/Response
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

            std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[selected].sqanSolNet->GetServices();

            for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
            {
                if ((*services)[dataflowId].inUse)
                {
                    SqanSolNet::sqanSolNetServiceEntry *serviceEntry = &(*services)[dataflowId];

                    for (uint8_t i=0; i<MAX_PEERS; i++)
                    {
                        // If this peer is registered for this service
                        if (serviceEntry->registeredPeers[i].peerRegistered)
                        {
                            // Revoke their registration
                            DynArray[selected].sqanSolNet->SendRevokeRegistrationMessage(i, serviceEntry->serviceDesc.serviceSelector);
                            break;
                        }

                        // Wait for the RevokeRegistrationRequests to propogate
                        sleep(3);

                        if(!serviceEntry->registeredPeers[i].peerRegistered)
                        {
                            ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Revoke Registration Request/Response Commands Compliant");
                            break;
                        }
                        else
                        {
                            ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Revoke Registration Request/Response Commands Non-Compliant");
                            break;
                        }
                        sleep(1);
                    }
                }
            }
        }
    }
}

void sqanTestTool::validateDeregisterRequestResponse(int selected)
{
    // Deregister Request/Response
    if (DynArray[selected].sqanInterface!= nullptr)
    {
        if ( DynArray[selected].sqanInterface->IsReady() )
        {
            DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();

            std::array<SqanSolNet::sqanSolNetPeerServicesEntry, MAX_PEERS> *peerServices = DynArray[selected].sqanSolNet->GetPeerServices();

            for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
            {
                SqanSolNet::sqanSolNetPeerServicesEntry *entry = &(*peerServices)[peerIndex];

                if ( !entry->inUse )
                {
                    continue;
                }

                for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
                {
                    if (entry->services[dataflowId].inUse)
                    {
                        if ((entry->services[dataflowId].serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status) &&
                             (entry->services[dataflowId].ImRegistered))
                        {
                            // Get the service we registered for from the first peer
                            uint32_t serviceSelector = entry->services[dataflowId].serviceDesc.serviceSelector;

                            //Autonomous Start/Stop Request/Response
                            uint8_t flowState = 0; // Stop sending data!
                            DynArray[selected].sqanSolNet->SendAutonomousStartStopRequestMessage(peerIndex, serviceSelector, flowState);

                            //Wait for response
                            sleep(1);

                            // Deregister for this service
                            DynArray[selected].sqanSolNet->SendDeregisterRequestMessage(peerIndex, serviceSelector);
                            sleep(5);

                            // Wait for the DeregisterResponse to update ImRegistered flag
                            // Verify that we are registered with this peer for this service
                            int count = 0;
                            while ((entry->services[dataflowId].ImRegistered) && (count < 100))
                            {
                                sleep(1);
                                count++;
                            }
                        }
                        if (!entry->services[dataflowId].ImRegistered)
                        {
                            ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Deregister Request/Response Commands Compliant");
                            break;
                        }
                        else
                        {
                            ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Deregister Request/Response Commands Non-Compliant");
                            break;
                        }
                        sleep(1);
                    }
                }
            }
        }
    }
}

void sqanTestTool::validateRevokeNetAssociationRequestResponse(int selected)
{
    //Revoke Network Association Request/Response
    if (DynArray[selected].sqanInterface != nullptr)
    {
        if (DynArray[selected].sqanInterface->IsReady())
        {
            DynArray[selected].sqanSolNet = DynArray[selected].sqanInterface->GetSqanSolNet();
            int status  = DynArray[selected].sqanSolNet->SendRevokeNetworkAssociationRequestMessage(0);
            if(status == 0)
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Revoke Network Association Request/Response Commands Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            else
            {
                ui->testAutomationPlainTextEdit->appendPlainText(DynArray[selected].devName + " Revoke Network Association Request/Response Commands Non-Compliant");
                ui->testAutomationPlainTextEdit->repaint();
                ui->testAutomationPlainTextEdit->update();
            }
            sleep(1);
        }
    }
    // Wait to see that peer clears association and resets
    sleep(5);
}
*/

/****************************************************
* SqanTestTool utilities
* ***************************************************/

QString sqanTestTool::findNameFromDeviceType(uint16_t devType)
{
    QString devName = "";
    switch(devType){
   /*JR case SqanIdentity::TWS:
        devName = "TWS: Thermal Weapon Sight";
        break;
    case SqanIdentity::HMD:
        devName = "HMD: Helmet-Mounted Display";
        break;
    case SqanIdentity::PHN:
        devName = "PHN: Smartphone/Computer";
        break;
    case SqanIdentity::STM:
        devName = "STM: STORM Laser Range Finder";
        break;
    case SqanIdentity::DGR:
        devName = "DGR: DAGR GPS Device";
        break;
    case SqanIdentity::RLH:
        devName = "RLH: SQAN-Mac Relay, USB Host connected";
        break;
    case SqanIdentity::RLD:
        devName = "RLD: SQAN-Mac Relay, ISB Device connected";
        break;
    case SqanIdentity::DPT:
        devName = "DPT: Depot Device";
        break;
    case SqanIdentity::NET:
        devName = "NET: SQAN-Mac Network Device";
        break;
    case SqanIdentity::ENC:
        devName = "ENC: Video Encoder (Camera)";
        break;
    case SqanIdentity::KPD:
        devName = "KPD: Keypad";
        break;
    case SqanIdentity::NES:
        devName = "NES: SQAN-Mac Network Device - Slave Only";
        break;
    case SqanIdentity::HTR:
        devName = "HTR: Handheld Tactical Radio";
        break;
    case SqanIdentity::CHD:
        devName = "CHD: Command Mode Head Mounted Device";
        break;
    case SqanIdentity::GNM:
        devName = "GNM: Generic Coordinator-Capable Device";
        break;
    case SqanIdentity::GNS:
        devName = "GNS: Generic Peer Device (Non-Coordinator)";
        break;
    case SqanIdentity::ReservedLegacy:
        devName = "RESERVED:  for Legacy Compatibility";
        break;
    case SqanIdentity::NET1:
        devName = "NET1: General Network Node Priority 1";
        break;
    case SqanIdentity::NET2:
        devName = "NET2: General Network Node Priority 2";
        break;
    case SqanIdentity::NET3:
        devName = "NET3: General Network Node Priority 3";
        break;
    case SqanIdentity::NET4:
        devName = "NET4: General Network Node Priority 4";
        break;
    case SqanIdentity::NET5:
        devName = "NET5: General Network Node Priority 5";
        break;
    case SqanIdentity::Generic_Biometric_Sensor:
        devName = "GEN_BIO: Biometric Sensor";
        break;
    case SqanIdentity::Generic_Environmental_Sensor:
        devName = "GEN_ENV: Environmental Sensor";
        break;
    case SqanIdentity::Generic_Positioning_Sensor:
        devName = "GEN_POS: Positioning Sensor";
        break;*/
    default:
        devName = unsupportedDevTypeStr;
        break;
    }

    return (devName);
}

void sqanTestTool::hoverEnter(QHoverEvent *event)
{
    Q_UNUSED(event);
    //Nothing
}

void sqanTestTool::hoverLeave(QHoverEvent *event)
{
    Q_UNUSED(event);
    //Nothing
}

void sqanTestTool::hoverMove(QHoverEvent *event)
{
    Q_UNUSED(event);
    //Nothing
}

bool sqanTestTool::event(QEvent *event)
{
    bool status = false;
    switch(event->type())
        {
        case QEvent::HoverEnter:
        {
            hoverEnter(static_cast<QHoverEvent*>(event));
            status = true;
            break;
        }
        case QEvent::HoverLeave:
        {
            hoverLeave(static_cast<QHoverEvent*>(event));
            status = true;
            break;
        }
        case QEvent::HoverMove:
        {
            hoverMove(static_cast<QHoverEvent*>(event));
            status = true;
            break;
        }
        default:
    {
            status = QWidget::event(event);
            break;
    }
        }
    return (status);
}

void sqanTestTool::displayMessage(QString string)
{
   /*JR msgBox.setWindowTitle("SQAN iV&V Kit " + kitVersion);
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);
    */

    QTimer timer;
    timer.setSingleShot(true);
    //JR connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(5000);

    //JR msgBox.exec();
}



int sqanTestTool::getSqanArrayIndex(QString devName)
{
    uint8_t index = -1;
    for (uint8_t i=0; i<MAX_DEVICES; i++)
    {
        if ( DynArray[i].devName == devName )
        {
            index = i;
            break;
        }
    }

    return (index);
}

/*************************************************
 *
 * exportSavedServiceParameters
 *
 * Export saved service parameters to DB
 * **********************************************/
/*JR
void sqanTestTool::exportSavedServiceParameters()
{
    if (dbConnected) {
        if (!db.transaction()) {
            qDebug() << Q_FUNC_INFO << " unable to begin transaction";
            return;
        }
        int status = 0;
        QString ts = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
        QStringList tooltip;
//        int currentIndex = ui->deviceComboBox->currentIndex();
        int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
        std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[dai].sqanSolNet->GetServices();
        SqanSolNet::sqanSolNetServiceEntry *serviceEntry = nullptr;
        for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
        {
            serviceEntry = &(*services)[dataflowId];
            if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Control ) {
                status = exportControlDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::RTA ) {
                status = exportRtaDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status ) {
                status = exportStatusDescriptorUI(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::PacketGenerator ) {
                status = exportPacketGeneratorDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Video ) {
                status = exportVideoDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::SimpleUI ) {
                status = exportSimpleUiDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRF ) {
                status = exportLrfDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRFControl ) {
                status = exportLrfControlDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl));
            } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::AROverlay ) {
                status = exportArOverlayDescriptor(serviceEntry, ts);
                tooltip << QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay));
            }
            if (status) {
                if (!db.rollback()) {
                    qDebug() << Q_FUNC_INFO << "unable to rollback transaction";
                }
                return;
            }
        }
        if (!db.commit()) {
            qDebug() << Q_FUNC_INFO << " unable to commit transaction";
            return;
        }
        QListWidgetItem *tsItem = new QListWidgetItem(ts);
        tsItem->setToolTip(tooltip.join(","));
        ui->descPlansListWidget->addItem(tsItem);
        int tnum = ui->descPlansGroupBox->title().split(" ").at(0).toInt((bool *)false) + 1;
        ui->descPlansGroupBox->setTitle(QString("%1 Descriptor Plans").arg(tnum));
    } else {
        qDebug() << "No database connection";
    }
}

bool sqanTestTool::insertPropertyField(QString ts, QString deviceType, QString serviceName, int ord, QString propertyType, QString propertyName, NameVal nv)
{
    QSqlQuery propertyQry;
    bool status;

    propertyQry.prepare("INSERT INTO tb_descriptor_plans (ts, device_type, service_name, ord, property_type, property_name, field_name, field_value) "
                     "VALUES (:ts, :device_type, :service_name, :ord, :property_type, :property_name, :field_name, :field_value)");
    propertyQry.bindValue(":ts", ts);
    propertyQry.bindValue(":device_type", deviceType);
    propertyQry.bindValue(":service_name", serviceName);
    propertyQry.bindValue(":ord", ord);
    propertyQry.bindValue(":property_type", propertyType);
    propertyQry.bindValue(":property_name", propertyName);
    propertyQry.bindValue(":field_name", nv.name);
    propertyQry.bindValue(":field_value", nv.val);
    status = propertyQry.exec();
    if (propertyQry.lastError().isValid() ) {
        qDebug() << propertyQry.lastQuery();
        qDebug() << propertyQry.boundValues();
        qDebug() << propertyQry.lastError().databaseText();
    }
    return status;
}
*/

/**************************************************************************************************
* getPropertyType
*
* Returns a string describing the type of property passed in.
* Some properties are "General"; others are service-specific.
* Defined in SQAN Embedment Guide section A.4.
*************************************************************************************************/
 /*JR
QString sqanTestTool::getPropertyType(uint16_t e)
{
    // Consider defining these in a header if they become used elsewhere.
    QString general = "General";
    QString video = "Video";
    QString ui = "UI";
    QString rta = "RTA";
    QString motionReserved1 = "MotionReserved1";
    QString motionReserved2 = "MotionReserved2";
    QString networking = "Networking";
    QString arOverlayOutput = "ArOverlayOutput";
    QString arReserved1 = "ArReserved1";
    QString arReserved2 = "ArReserved2";
    QString powerBattery = "PowerBattery";
    QString powerReserved1 = "PowerReserved1";
    QString powerReserved2 = "PowerReserved2";
    QString unknown = "UNKNOWN_sqanSolNetPropertyDescIds";

    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::Label ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::Version ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::Encoding ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::FlowPolicy ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::DeviceName ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::AssociatedService ) return general;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved1 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved2 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved3 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved4 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved5 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved6 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved7 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved8 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoLensDistortion ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved9 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved10 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoControl ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved11 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::VideoReserved12 ) return video;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::UIMotion ) return ui;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::UIButtons ) return ui;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMin ) return ui;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::UIReservedMax ) return ui;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate ) return rta;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::IMUFloatUpdateRate ) return rta;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::IMUSensorNoise ) return rta;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved1 ) return motionReserved1;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::MotionReserved2 ) return motionReserved2;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::NetworkingIP ) return networking;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::NetworkingTCPPort ) return networking;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::NetworkingUDPPort ) return networking;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved1 ) return networking;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::NetworkingReserved2 ) return networking;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput ) return arOverlayOutput;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::ARReserved1 ) return arReserved1;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::ARReserved2 ) return arReserved2;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::PowerBattery ) return powerBattery;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved1 ) return powerReserved1;
    if ( e == SqanSolNet::sqanSolNetPropertyDescIds::PowerReserved2 ) return powerReserved2;
    return unknown;
}
*/

QString sqanTestTool::varName(const char* c)
{
    QString name = QString(c);
    QRegExp regex("^&?([_a-zA-Z]\\w*(->|\\.|::))*([_a-zA-Z]\\w*)$");
    if ((regex.indexIn(name, 0)) != -1) {
        return regex.cap(regex.captureCount());
    } else {
        return name;
    }
}

/*************************************************
 *
 * exportControlDescriptor
 * **********************************************/
 /*JR
int sqanTestTool::exportControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceControlDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceControlDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        }
        else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceName ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(devNameDesc->devString)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(devNameDesc->devString)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(devNameDesc->devString)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName ) {
            SqanSolNet::sqanSolNetPropertyDeviceDesc *devNameDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyDeviceDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(devNameDesc->devString)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/
/*************************************************
 *
 * exportVideoDescriptor
 * **********************************************/
/*JR
int sqanTestTool::exportVideoDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw ) {
            SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc *videoFormatRawDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoFormatRawDesc->resX))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoFormatRawDesc->resY))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoFormatRawDesc->frameRate))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoFormatRawDesc->bitDepth))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoFormatRawDesc->pixelformat)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw ) {
            SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc *videoProtocolDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoProtocolDesc->protocol)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV ) {
            SqanSolNet::sqanSolNetPropertyVideoIfovDesc *videoIfovDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoIfovDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoIfovDesc->instantFieldOfView)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint ) {
            SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc *videoPrinciplePointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoPrinciplePointDesc->pointX))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(videoPrinciplePointDesc->pointY)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/
/*************************************************
 *
 * exportRtaDescriptor
 * **********************************************/

/*JR
int sqanTestTool::exportRtaDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate ) {
            SqanSolNet::sqanSolNetPropertyRtaImuRateDesc *rtaImuRateDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyRtaImuRateDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(rtaImuRateDesc->rate)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/

/*************************************************
 *
 * exportArOverlayDescriptor
 * **********************************************/
/*JR int sqanTestTool::exportArOverlayDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput ) {
            SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc *arOverlayDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(arOverlayDesc->minWidth))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(arOverlayDesc->minHeight))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(arOverlayDesc->supportedRleVersions)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/
/*************************************************
 *
 * exportPacketGeneratorDescriptor
 * **********************************************/
/*JRint sqanTestTool::exportPacketGeneratorDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/
/*************************************************
 *
 * exportStatusDescriptorUI
 * **********************************************/
/*JR int sqanTestTool::exportStatusDescriptorUI(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/
/*************************************************
 *
 * exportSimpleUiDescriptor
 * **********************************************/
/*JR int sqanTestTool::exportSimpleUiDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::UIButtons ) {
            SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc *simpleUiButtonsDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(simpleUiButtonsDesc->buttonCount))
                 || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(simpleUiButtonsDesc->stateCount)) ) {
                 //|| !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(simpleUiButtonsDesc->terminalId))
                 //|| !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(simpleUiButtonsDesc->capabilities)) ) {
                return 1;
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/

/*************************************************
 *
 * exportLrfDescriptor
 * **********************************************/
/*JR int sqanTestTool::exportLrfDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
     */

/*************************************************
 *
 * exportLrfControlDescriptor
 * **********************************************/
/*JR int sqanTestTool::exportLrfControlDescriptor(SqanSolNet::sqanSolNetServiceEntry *serviceEntry, QString ts)
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
    QString service = QString::fromStdString(SqanSolNet::GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId));
    int ord = 1;
    uint16_t childblocklength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
    uint16_t offset = 0;
    bool firstEndpoint = true;

    while (offset < childblocklength ) {
        SqanSolNet::sqanSolNetDescCommonHeader *commonHeader = reinterpret_cast<SqanSolNet::sqanSolNetDescCommonHeader *>(&serviceEntry->serviceDesc.childBlock[offset]);
        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
        QString propertyType = getPropertyType(commonHeader->descriptorId);
        QString propertyName = QString::fromStdString(SqanSolNet::GetPropertyIdString(commonHeader->descriptorId));

        if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Endpoint ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc *endpointDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyEndpointDesc *>(data);
            if ( firstEndpoint ) {
                firstEndpoint = false;
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataflowId))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution))
                     || !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->dataPolicies)) ) {
                    return 1;
                }
            } else { // create a new endpointDistribution field value that will override the previous when re-populating the GUI, since it is read last
                if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_NUM(endpointDesc->endpointDistribution)) ) {
                    return 1;
                }
            }
        } else if ( commonHeader->descriptorId == SqanSolNet::sqanSolNetPropertyDescIds::Label ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc *labelDesc = reinterpret_cast<SqanSolNet::sqanSolNetPropertyLabelDesc *>(data);
            if ( !insertPropertyField(ts, deviceType, service, ord++, propertyType, propertyName, NAME_VAL_TEXT(labelDesc->label)) ) {
                return 1;
            }
        }
        offset += commonHeader->length + SqanSolNet::commonHeaderSize;
    }
    return 0;
}
*/

/*************************************************
 *
 * importSavedServiceParameters
 *
 * Import stored service parameters from DB
 * **********************************************/
/*JR
void sqanTestTool::importStoredServiceParameters(QListWidgetItem *selected)
{
    if (dbConnected) {
        if (!db.transaction()) {
            qDebug() << Q_FUNC_INFO << " unable to begin transaction";
            return;
        }
//        uint8_t currentIndex = ui->deviceComboBox->currentIndex();
        QString ts = selected->text();
        int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
        QString deviceType = QString::fromStdString(SqanIdentity::GetDeviceTypeString(DynArray[dai].devType));
        QSqlQuery servicesQry(db);
        servicesQry.prepare("SELECT service_name FROM tb_descriptor_plans WHERE ts = :ts AND device_type = :device_type "
                         "AND property_type = 'General' AND field_name = 'dataflowId' ORDER BY field_value");
        servicesQry.bindValue(":ts", ts);
        servicesQry.bindValue(":device_type", deviceType);
        if ( servicesQry.exec() ) {
            while ( servicesQry.next() ) {
                uint16_t serviceId = 0xFFFF;
                getServiceIdFromText(servicesQry.value(0).toString(), &serviceId);
                if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Reserved1 ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Video ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importVideoDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(1);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Audio ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::PositionSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::EnvironmentSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::HealthSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::SimpleUI ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importSimpleUiDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(5);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Control ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importControlDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(0);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Status ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importStatusDescriptorUI(ts, deviceType);
                    saveCommonConfigButton_clicked(4);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::RTA ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importRtaDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(3);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Reserved2 ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::HybridSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::MotionSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::TemperatureSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::MoistureSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::VibrationSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::ChemicalSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::FlowSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::ForceSense ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::LRF ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importLrfDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(7);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::LRFControl ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importLrfControlDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(8);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::AROverlay ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importArOverlayDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(6);
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::ARControl ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::Container ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::ReservedMin ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::ReservedMax ) {
                    // TBD
                } else if ( serviceId == SqanSolNet::sqanSolNetServiceIds::PacketGenerator ) {
                    serviceChosen(QString::fromStdString(SqanSolNet::GetServiceIdString(serviceId)));
                    importPacketGeneratorDescriptor(ts, deviceType);
                    saveCommonConfigButton_clicked(2);
                }
            }
        } else {
            qDebug() << servicesQry.lastQuery();
            qDebug() << servicesQry.boundValues();
            qDebug() << servicesQry.lastError().databaseText();
        }
        selected->font().setBold(true);
    } else {
        qDebug() << "No database connection";
    }
    auto unsavedServiceList = ui->serviceDescListWidget->findItems(QRegExp::escape(UNSAVED_STR), Qt::MatchRegExp);
    ui->serviceDescGroupBox->setTitle(QString::number(ui->serviceDescListWidget->count() - unsavedServiceList.length()) + " Service Descriptors");
}
*/

QSqlQuery sqanTestTool::selectStoredServiceProperties(QString ts, QString deviceType, QString serviceName)
{
    QSqlQuery propertyQry;
    propertyQry.prepare("SELECT property_type, property_name, field_name, field_value FROM tb_descriptor_plans "
                        "WHERE ts = :ts AND device_type = :deviceType AND service_name = :serviceName "
                        "AND NOT (property_type = 'General' AND property_name = 'Endpoint' AND field_name = 'dataflowId') ORDER BY ord");
    propertyQry.bindValue(":ts", ts);
    propertyQry.bindValue(":deviceType", deviceType);
    propertyQry.bindValue(":serviceName", serviceName);
    if ( !propertyQry.exec() ) {
        qDebug() << " " << propertyQry.lastQuery();
        qDebug() << " " << propertyQry.boundValues();
        qDebug() << " " << propertyQry.lastError().databaseText();
    }
    return propertyQry;
}

/*************************************************
 *
 * importControlDescriptor
 *
 * Property UI Elements:
 * deviceEndpointIdComboBox             x
 * deviceEndpointDistributionComboBox   x
 * deviceEndpointDataPoliciesComboBox   x
 * deviceNameLineEdit                   x
 * deviceSerialNumberLineEdit           x
 * deviceManufacturerLineEdit           x
 * deviceFriendlyNameLineEdit           x
 * **********************************************/
/*JR void sqanTestTool::importControlDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                deviceEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                deviceEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                deviceEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceName)) ) {
            deviceNameLineEdit->setText(fieldValue);
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceSerialNumber)) ) {
            deviceSerialNumberLineEdit->setText(fieldValue);
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceManufacturer)) ) {
            deviceManufacturerLineEdit->setText(fieldValue);
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::DeviceFriendlyName)) ) {
            deviceFriendlyNameLineEdit->setText(fieldValue);
        }
    }
}
*/

/*************************************************
 *
 * importVideoDescriptor
 *
 * Property UI Elements:
 * videoEndpointIdComboBox                          x
 * videoEndpointDistributionComboBox                x
 * videoEndpointDataPoliciesComboBox                x
 * videoFormatResolutionXServiceDescriptorLineEdit  x
 * videoFormatResolutionYServiceDescriptorLineEdit  x
 * videoFormatFrameRateDescriptorLineEdit           x
 * videoFormatBitDepthDescriptorLineEdit            x
 * videoFormatPixelFormatDescriptorComboBox         x
 * videoProtocolRawDescriptorComboBox               x
 * videoIfovDescriptorLineEdit                      x
 * videoPPointXLineEdit                             x
 * videoPPointYLineEdit                             x
 * videoLabelLineEdit                               x
 * **********************************************/
/*JR void sqanTestTool::importVideoDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                videoEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                videoEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                videoEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                videoLabelLineEdit->setText(fieldValue);
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoFormatRaw)) ) {
            SqanSolNet::sqanSolNetPropertyVideoFormatRawDesc videoFormatRawDesc;
            if ( fieldName == GET_VAR_NAME(videoFormatRawDesc.resX) ) {
                videoFormatResolutionXServiceDescriptorLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(videoFormatRawDesc.resY) ) {
                videoFormatResolutionYServiceDescriptorLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(videoFormatRawDesc.frameRate) ) {
                videoFormatFrameRateDescriptorLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(videoFormatRawDesc.bitDepth) ) {
                videoFormatBitDepthDescriptorLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(videoFormatRawDesc.pixelformat) ) {
                videoFormatPixelFormatDescriptorComboBox->setCurrentIndex(getVideoFormatPixelFormatDescriptorIndex((uint16_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoProtocolRaw)) ) {
            SqanSolNet::sqanSolNetPropertyVideoProtocolRawDesc videoProtocolDesc;
            if ( fieldName == GET_VAR_NAME(videoProtocolDesc.protocol) ) {
                videoProtocolRawDescriptorComboBox->setCurrentIndex(getVideoProtocolRawDescriptorIndex((uint16_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoIFOV)) ) {
            SqanSolNet::sqanSolNetPropertyVideoIfovDesc videoIfovDesc;
            if ( fieldName == GET_VAR_NAME(videoIfovDesc.instantFieldOfView) ) {
                videoIfovDescriptorLineEdit->setText(fieldValue);
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::VideoPrinciplePoint)) ) {
            SqanSolNet::sqanSolNetPropertyVideoPrincipalPointDesc videoPrinciplePointDesc;
            if ( fieldName == GET_VAR_NAME(videoPrinciplePointDesc.pointX) ) {
                videoPPointXLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(videoPrinciplePointDesc.pointY) ) {
                videoPPointYLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

/*************************************************
 *
 * importRtaDescriptor
 *
 * Property UI Elements:
 * rtaEndpointIdComboBox            x
 * rtaEndpointDistributionComboBox  x
 * rtaEndpointDataPoliciesComboBox  x
 * rtaLabelLineEdit                 x
 * rtaImuRateLineEdit               x
 * **********************************************/
/*JR void sqanTestTool::importRtaDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                rtaEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                rtaEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                rtaEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                rtaLabelLineEdit->setText(fieldValue);
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::IMURawUpdateRate)) ) {
            SqanSolNet::sqanSolNetPropertyRtaImuRateDesc rtaImuRateDesc;
            if ( fieldName == GET_VAR_NAME(rtaImuRateDesc.rate) ) {
                rtaImuRateLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

/*************************************************
 *
 * importArOverlayDescriptor
 *
 * Property UI Elements:
 * arOverlayEndpointIdComboBox              x
 * arOverlayEndpointDistributionComboBox    x
 * arOverlayEndpointDataPoliciesComboBox    x
 * arOverlayLabelLineEdit                   x
 * arOverlayMinWidthLineEdit                x
 * arOverlayMinHeightLineEdit               x
 * arOverlayRleVersionsLineEdit             x
 * **********************************************/
/*JR void sqanTestTool::importArOverlayDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                arOverlayEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                arOverlayEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                arOverlayEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                arOverlayLabelLineEdit->setText(fieldValue);
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::AROverlayOutput)) ) {
            SqanSolNet::sqanSolNetPropertyArOverlayOutputDesc arOverlayDesc;
            if ( fieldName == GET_VAR_NAME(arOverlayDesc.minWidth) ) {
                arOverlayMinWidthLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(arOverlayDesc.minHeight) ) {
                arOverlayMinHeightLineEdit->setText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(arOverlayDesc.supportedRleVersions) ) {
                arOverlayRleVersionsLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

/*************************************************
 *
 * importPacketGeneratorDescriptor
 *
 * Property UI Elements:
 * packetGenEndpointIdComboBox              x
 * packetGenEndpointDistributionComboBox    x
 * packetGenEndpointDataPoliciesComboBox    x
 * packetGenLabelLineEdit                   x
 * **********************************************/
/*JR void sqanTestTool::importPacketGeneratorDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                packetGenEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                packetGenEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                packetGenEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                packetGenLabelLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

/*************************************************
 *
 * importStatusDescriptorUI
 *
 * Property UI Elements:
 * statusEndpointIdComboBox             x
 * statusEndpointDistributionComboBox   x
 * statusEndpointDataPoliciesComboBox   x
 * statusLabelLineEdit                  x
 * **********************************************/
/*JR void sqanTestTool::importStatusDescriptorUI(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                statusEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                statusEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                statusEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::MulticastGroup)) ) {
            qDebug() << Q_FUNC_INFO << " MulticastGroup property found..."; // #WIP
//            statusEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
//            SqanSolNet::sqanSolNetPropertyMulticastGroupDesc multicastDesc;
//            if ( field_name == GET_VAR_NAME(multicastDesc.) ) {

//            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                statusLabelLineEdit->setText(fieldValue);
            }
        }
    }
}
*/
/*************************************************
 *
 * importSimpleUiDescriptor
 *
 * Property UI Elements:
 * simpleUiEndpointIdComboBox           x
 * simpleUiEndpointDistributionComboBox x
 * simpleUiEndpointDataPoliciesComboBox x
 * simpleUiLabelLineEdit                x
 * simpleUiButtonCountComboBox          x
 * simpleUiStateCountComboBox           x
 * **********************************************/
/*JR void sqanTestTool::importSimpleUiDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                simpleUiEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                simpleUiEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                simpleUiEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                simpleUiLabelLineEdit->setText(fieldValue);
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::UIButtons)) ) {
            SqanSolNet::sqanSolNetPropertySimpleUiButtonsDesc simpleUiButtonsDesc;
            if ( fieldName == GET_VAR_NAME(simpleUiButtonsDesc.buttonCount) ) {
                simpleUiButtonCountComboBox->setCurrentIndex(getButtonCountIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(simpleUiButtonsDesc.stateCount) ) {
                simpleUiStateCountComboBox->setCurrentIndex(getStateCountIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        }
    }
}
*/
/*************************************************
 *
 * importLrfDescriptor
 *
 * Property UI Elements:
 * lrfEndpointIdComboBox            x
 * lrfEndpointDistributionComboBox  x
 * lrfEndpointDataPoliciesComboBox  x
 * lrfLabelLineEdit                 x
 * **********************************************/
/*JR void sqanTestTool::importLrfDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                lrfEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                lrfEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                lrfEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                lrfLabelLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

/*************************************************
 *
 * importLrfControlDescriptor
 *
 * Property UI Elements:
 * lrfControlEndpointIdComboBox
 * lrfControlEndpointDistributionComboBox
 * lrfControlEndpointDataPoliciesComboBox
 * lrfControlLabelLineEdit
 * **********************************************/
/*JR void sqanTestTool::importLrfControlDescriptor(QString ts, QString deviceType)
{
    QSqlQuery propertyQry = selectStoredServiceProperties(ts, deviceType, QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl)));
    while ( propertyQry.next() ) {
        QString propertyType = propertyQry.value(0).toString();
        QString propertyName = propertyQry.value(1).toString();
        QString fieldName = propertyQry.value(2).toString();
        QString fieldValue = propertyQry.value(3).toString();
        if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Endpoint)) ) {
            SqanSolNet::sqanSolNetPropertyEndpointDesc endpointDesc;
            if ( fieldName == GET_VAR_NAME(endpointDesc.endpointId) ) {
                lrfControlEndpointIdComboBox->setCurrentText(fieldValue);
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.endpointDistribution) ) {
                qDebug() << Q_FUNC_INFO << " distribution = " << fieldValue;
                lrfControlEndpointDistributionComboBox->setCurrentIndex(getEndpointDistributionIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            } else if ( fieldName == GET_VAR_NAME(endpointDesc.dataPolicies) ) {
                lrfControlEndpointDataPoliciesComboBox->setCurrentIndex(getEndpointDataPoliciesIndex((uint8_t)fieldValue.toUInt((bool *)false)));
            }
        } else if ( propertyName == QString::fromStdString(SqanSolNet::GetPropertyIdString(SqanSolNet::sqanSolNetPropertyDescIds::Label)) ) {
            SqanSolNet::sqanSolNetPropertyLabelDesc labelDesc;
            if ( fieldName == GET_VAR_NAME(labelDesc.label) ) {
                lrfControlLabelLineEdit->setText(fieldValue);
            }
        }
    }
}
*/

void sqanTestTool::showDescriptorPlansListContextMenu(const QPoint &pos)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dbLogger->dbLog(dblr);

    //Handle Global Position
    QPoint globalPos = ui->descPlansListWidget->mapToGlobal(pos);

    //Create Menu and Insert Actions
    QMenu *descPlansListMenu = new QMenu();

    descPlansListMenu->addAction("Delete", this, SLOT(deleteDescPlan()));

    //Show Context Menu At Handling Position
    descPlansListMenu->exec(globalPos);
}

void sqanTestTool::deleteDescPlan()
{
    QListWidgetItem *item = ui->descPlansListWidget->currentItem();

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("item->text()", item->text().toStdString().c_str());
    dbLogger->dbLog(dblr);

    // delete from DB
//    openDatabase();
    if (dbConnected) {
        QSqlQuery qryDeleteTs;
        qryDeleteTs.prepare("DELETE FROM tb_descriptor_plans WHERE ts = :ts");
        qryDeleteTs.bindValue(":ts", item->text());
        if ( !qryDeleteTs.exec() ) {
            qDebug() << qryDeleteTs.lastQuery();
            qDebug() << qryDeleteTs.boundValues();
            qDebug() << qryDeleteTs.lastError().databaseText();
        }
//        closeDatabase();
    }
    delete item;
    int tnum = ui->descPlansGroupBox->title().split(" ").at(0).toInt((bool *)false) - 1;
    ui->descPlansGroupBox->setTitle(QString("%1 Descriptor Plans").arg(tnum));
}

/*JR
void sqanTestTool::showServiceDescriptors()
{
    int dai = ui->deviceComboBox->currentText().split(indexDelim)[0].toInt();
    std::array<SqanSolNet::sqanSolNetServiceEntry, SqanSolNet::MAX_NUMBER_APPS> *services = DynArray[dai].sqanSolNet->GetServices();
    SqanSolNet::sqanSolNetServiceEntry *serviceEntry = nullptr;
    for (uint8_t dataflowId=0; dataflowId<SqanSolNet::MAX_NUMBER_APPS; dataflowId++)
    {
        serviceEntry = &(*services)[dataflowId];
        if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Control ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Control)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::RTA ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::RTA)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Status ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Status)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::PacketGenerator ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::PacketGenerator)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::Video ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::Video)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::SimpleUI ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::SimpleUI)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRF ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRF)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::LRFControl ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::LRFControl)));
        } else if ( serviceEntry->serviceDesc.header.descriptorId == SqanSolNet::sqanSolNetServiceIds::AROverlay ) {
            ui->serviceDescListWidget->addItem(QString::number(dataflowId) + indexDelim + QString::fromStdString(SqanSolNet::GetServiceIdString(SqanSolNet::sqanSolNetServiceIds::AROverlay)));
        } else {
            // Do Nothing
        }
    }
    auto unsavedServiceList = ui->serviceDescListWidget->findItems(QRegExp::escape(UNSAVED_STR), Qt::MatchRegExp);
    ui->serviceDescGroupBox->setTitle(QString::number(ui->serviceDescListWidget->count() - unsavedServiceList.length()) + " Service Descriptors");
}
/*JR

/*************************************************
 *
 * getDynArrayIndexFromMac
 *
 * This function should be called while DynArray is already locked.
 * Returns the DynArray index of the first member whose macAddr matches
 * the passed parameter.  This index may be used to lookup DynArray
 * properties when defining peer properties (e.g. devName).
 * **********************************************/
int sqanTestTool::getDynArrayIndexFromMac(QString macaddr)
{
    int dai = -1;
    for (int i=0; i<DYN_ARRAY_SIZE; i++)
    {
        if (!DynArray[i].isReady) { continue; }
        if (DynArray[i].macAddr == macaddr) {
            dai = i;
            break;
        }
    }
    return dai;
}
