//!#################################################
//! Filename: DBLogRecord.h
//! Description: Class to represent individual
//!              records written by DBLogger.
//!              These enable multiple observations
//!              with multiple attributes to be
//!              logged ona single line more easily.
//!#################################################
#ifndef DBLOGRECORD_H
#define DBLOGRECORD_H

#include <sstream>
#include <QString>

struct RecordTag
{
    RecordTag(std::string tag, int nAttr) : tag(tag), nAttr(nAttr) {}
    std::string tag = "";
    int nAttr = 0;
};

extern RecordTag tbStart;
extern RecordTag tbStop;
extern RecordTag tbUI;
extern RecordTag tbDevices;
extern RecordTag tbServices;
extern RecordTag tbTput;

class DBLogRecord
{

public:

    DBLogRecord(RecordTag &, long long, int);
    ~DBLogRecord();

    void delimitNewObservation();

    void addStartObservation(std::string);

    void addUIObservation(std::string, std::string);

    void addDevicesObservation(int, QString, QString, QString, uint32_t, uint8_t, uint8_t, uint8_t, uint16_t, float, uint16_t, uint8_t, uint16_t, uint8_t);

    void addServicesObservation(int, uint8_t, uint8_t, uint8_t, uint8_t, std::stringstream *, std::stringstream *, std::stringstream *, std::stringstream *);

    void addTputObservation(int, int, uint16_t);

    std::string getString();

    static inline void replaceAll(std::string &, const std::string &, const std::string &);

    ////////////////////////////////////////// Setup string quoting for DBLogRecord //////////////////////////////////////////
    // Single Quotes are the escape character used to preserve existing single quotes
    // during PostgreSQL COPY operations.  This is the default COPY behavior, however if
    // changed this must match the COPY ... WITH ... ESCAPE argument from the parser script
    static const std::string &ESCAPE()
    {
        static std::string E("'");
        return E;
    }

    // Single Quotes are used to encompass the Observations field of a DBLogRecord
    // This allows PostgreSQL COPY to treat the entire Observations field as a single string
    // This must match the COPY ... WITH ... QUOTE argument from the parser script
    // Instances of single quotes within the body of the Observations field must be escaped
    static const std::string &QUOTE()
    {
        static std::string Q("'");
        return Q;
    }

    // For std::string
    struct quoted
    {
        const char * _text;
        quoted( const char * text ) : _text(text) {}
        operator std::string () const
        {
            std::string quotedStr = QUOTE();
            quotedStr += _text;
            quotedStr += QUOTE();
            return quotedStr;
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const quoted & q )
    {
        ostr << QUOTE() << q._text << QUOTE();
        return ostr;
    }

    // For stringstreams
    struct ssquoted
    {
        const std::stringstream * _text;
        ssquoted( const std::stringstream * text ) : _text(text) {}
        operator std::stringbuf * () const
        {
            std::stringstream quotedStr;
            quotedStr << QUOTE() << _text << QUOTE();
            return quotedStr.rdbuf();
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const ssquoted & q )
    {
        ostr << QUOTE() << q._text->rdbuf() << QUOTE();
        return ostr;
    }

    // Outer Array Quotes are used to encompass top-level arrays in a DBLogRecord
    // These may be plain 1D arrays or special 1D string arrays whose strings are also formatted as 1D arrays
    // The parser regex_to_array will ignore commas embedded in these strings when splitting attributes
    static const std::string& OAQUOTE()
    {
        static std::string Q("`");
        return Q;
    }

    // For std::string
    struct oaquoted
    {
        const char * _text;
        oaquoted( const char * text ) : _text(text) {}
        operator std::string () const
        {
            std::string quotedStr = OAQUOTE();
            quotedStr += _text;
            quotedStr += OAQUOTE();
            return quotedStr;
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const oaquoted & q )
    {
        ostr << OAQUOTE() << q._text << OAQUOTE();
        return ostr;
    }

    // For stringstreams
    struct ssoaquoted
    {
        const std::stringstream * _text;
        ssoaquoted( const std::stringstream * text ) : _text(text) {}
        operator std::stringbuf * () const
        {
            std::stringstream quotedStr;
            quotedStr << OAQUOTE() << _text << OAQUOTE();
            return quotedStr.rdbuf();
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const ssoaquoted & q )
    {
        ostr << OAQUOTE() << q._text->rdbuf() << OAQUOTE();
        return ostr;
    }

    // Inner Array Quotes are used to encompass certain special strings within 1D arrays in a DBLogRecord
    // Such strings are special because they are also formatted as 1D arrays, enabling casting from string to string[]
    // This allows post-processing to expand these 1D string arrays as if they are 2D string arrays
    static const std::string& IAQUOTE()
    {
        static std::string Q("\"");
        return Q;
    }

    // For std::string
    struct iaquoted
    {
        const char * _text;
        iaquoted( const char * text ) : _text(text) {}
        operator std::string () const
        {
            std::string quotedStr = IAQUOTE();
            quotedStr += _text;
            quotedStr += IAQUOTE();
            return quotedStr;
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const iaquoted & q )
    {
        ostr << IAQUOTE() << q._text << IAQUOTE();
        return ostr;
    }

    // For stringstreams
    struct ssiaquoted
    {
        const std::stringstream * _text;
        ssiaquoted( const std::stringstream * text ) : _text(text) {}
        operator std::stringbuf * () const
        {
            std::stringstream quotedStr;
            quotedStr << IAQUOTE() << _text << IAQUOTE();
            return quotedStr.rdbuf();
        }
    };

    friend std::ostream & operator<< ( std::ostream & ostr, const ssiaquoted & q )
    {
        ostr << IAQUOTE() << q._text->rdbuf() << IAQUOTE();
        return ostr;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

private:

    RecordTag rt;

    long long et = -1;

    int cid = -1;

    int nObs = 0;

    std::stringstream oss;
};

#endif //! DBLOGRECORD_H
