//!#################################################
//! Filename: DBLogger.h
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues and prints to the log file
//!              in a background task.  The resulting
//!              CSV will be ingested by a database.
//!#################################################
#ifndef DBLOGGER_H
#define DBLOGGER_H

#include <iostream>
#include <queue>
#include <mutex>
#include <QString>
#include <sstream>
#include "DBLogRecord.h"

class DBLogger
{

public:
    DBLogger(std::string filename, QString version);
    ~DBLogger();

    void dbLog(DBLogRecord &);

    void deQueue();

    long long updateEpochTime() {return (et = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());}

    long long getEpochTime() {return et;}

    int updateUIIndex() {return ++uiIndex;}

    int getUIIndex() {return uiIndex;}

    int resetUIIndex() {return (uiIndex = 0);}

    QString getDBLVersion() {return DBLVersion;}

private:
    void CreateLogFile();

    void InitLog(QString);

    void FinalizeLog();

    void EmptyQueue();

    std::string GetUUID();

    std::mutex logMutex;

    std::queue<std::string> logQueue;

    std::string logFileName;

    QString DBLVersion;

    FILE *logFile;

    long long et;

    int uiIndex = 0;
};

#endif //! DBLOGGER_H
