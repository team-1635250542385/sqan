//!#################################################
//! Filename: Logger.h
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues and prints to the log file
//!              in a background task.
//!#################################################
#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <queue>
#include <mutex>

class Logger
{

public:
    Logger(std::string filename, uint16_t debugLevel);
    ~Logger();

    uint64_t GetTimeStamp();
    void LogMessage(std::string message, uint16_t index);   //!Adds timestamp to string
    void DebugMessage(std::string message, uint16_t index);  //!No timestamp
    void deQueue();

    //! Public Use these to set debugLevel
    static const uint16_t DEBUG_USB_INTERFACE    = 0x0001;
    static const uint16_t DEBUG_SQAN_INTERFACE   = 0x0002;

    //! Public Use these to check if debugging is on in source code
    uint8_t DEBUG_SqanInterface = 0;
    uint8_t DEBUG_UsbInterface = 0;

private:
    void CreateLogFile();
    void EmptyQueue();

    std::mutex logMutex;
    std::queue<std::string> logQueue;
    std::string logFileName;
    FILE *logFile;
};

#endif //! LOGGER_H
