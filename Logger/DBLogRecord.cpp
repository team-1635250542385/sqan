//!#################################################
//! Filename: DBLogRecord.h
//! Description: Class to represent individual
//!              records written by DBLogger.
//!              These enable multiple observations
//!              with multiple attributes to be
//!              logged ona single line more easily.
//!#################################################
#include "DBLogRecord.h"

// define target table tag names along with their number of expected attributes found in the observations field
RecordTag tbStart = {"#START", 1};
RecordTag tbStop = {"#STOP", 0};
RecordTag tbUI = {"tb_ui", 2};
RecordTag tbDevices = {"tb_devices", 12};
RecordTag tbServices = {"tb_services", 12};
RecordTag tbTput = {"tb_tput", 3};

DBLogRecord::DBLogRecord(RecordTag &rt, long long et, int cid) :
    rt(rt)
    ,et(et)
    ,cid(cid)
{
    oss << "";
}

DBLogRecord::~DBLogRecord()
{
    // Nothing
}

void DBLogRecord::delimitNewObservation()
{ // must be called at the beginning of all add...Observation() functions
    if (oss.rdbuf()->in_avail())
    {
        oss << ",";
    }
}

void DBLogRecord::addStartObservation(std::string attr)
{
    delimitNewObservation();
    oss << attr;    // 1
    nObs++;
}

void DBLogRecord::addUIObservation(std::string name, std::string val)
{
    delimitNewObservation();
    oss << name         // 1
        << "," << val;  // 2
    nObs++;
}

void DBLogRecord::addDevicesObservation(int i, QString devName, QString macAddr, QString firmwareVer, uint32_t netId, uint8_t coordPriority, uint8_t role, uint8_t channel, uint16_t devType, float phy_rate, uint16_t mcastAddress1, uint8_t mcastPeerIndex1,  uint16_t mcastAddress2, uint8_t mcastPeerIndex2)
{
    delimitNewObservation();
    oss << i                                        // 1
        << "," << devName.toStdString()             // 2
        << "," << macAddr.toStdString()             // 3
        << "," << firmwareVer.toStdString()         // 4
        << "," << std::to_string(netId)             // 5
        << "," << std::to_string(coordPriority)     // 6
        << "," << std::to_string(role)              // 7
        << "," << std::to_string(channel)           // 8
        << "," << std::to_string(devType)           // 9
        << "," << std::to_string(phy_rate)          // 10
        << "," << std::to_string(mcastAddress1)     // 11
        << "," << std::to_string(mcastPeerIndex1)   // 12
        << "," << std::to_string(mcastAddress2)     // 13
        << "," << std::to_string(mcastPeerIndex2);  // 14
    nObs++;
}

void DBLogRecord::addServicesObservation(int selected, uint8_t dataflowId, uint8_t endpointId, uint8_t endpointDistribution, uint8_t dataPolicies, std::stringstream *sdss, std::stringstream *cdss, std::stringstream *pss, std::stringstream *vss)
{
    delimitNewObservation();
    oss << selected                                                     // 1
        << "," << std::to_string(dataflowId)                            // 2
        << "," << std::to_string(endpointId)                            // 3
        << "," << std::to_string(endpointDistribution)                  // 4
        << "," << std::to_string(dataPolicies)                          // 5
        << "," << sdss->rdbuf()                                         // 6 7 8 9
        << "," << OAQUOTE() << "{" << cdss->rdbuf() << "}" << OAQUOTE() // 10
        << "," << OAQUOTE() << "{" << pss->rdbuf() << "}" << OAQUOTE()  // 11
        << "," << OAQUOTE() << "{" << vss->rdbuf() << "}" << OAQUOTE(); // 12
    nObs++;
}

void DBLogRecord::addTputObservation(int src_usbi, int dst_usbi, uint16_t tput)
{
//    delimitNewObservation(); // exception to the rule: do not expect to have multiple tput observations.  This may change if any consolidation occurs on logging multiple simultaneous data rates.
//    oss.str(""); // reset/clear the stringstream
    oss << std::to_string(src_usbi)         // 1
        << "," << std::to_string(dst_usbi)  // 2
        << "," << std::to_string(tput);     // 3
    nObs++;
}

std::string DBLogRecord::getString()
{
    std::stringstream ss;
    char e[1] = "";
    std::string tmpString = oss.str();
    ss  << rt.tag // Record Tag
        << "," << et // Epoch Time (microseconds)
        << "," << ((cid < 0) ? e : std::to_string(cid)) // Context ID
        << "," << nObs // Number of Observations
        << "," << rt.nAttr; // Number of Attributes per Observation
    if (oss.rdbuf()->in_avail()) { // Observations
        replaceAll( tmpString, QUOTE(), ESCAPE() + QUOTE() ); // escape any QUOTE instances
        ss << "," << quoted(tmpString.c_str()) << std::endl;
    } else {
        ss << "," << quoted(e) << std::endl;
    }
    return ss.str();
}

inline void DBLogRecord::replaceAll(std::string &str, const std::string &from, const std::string &to)
{
    if (from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}
