//!#################################################
//! Filename: Logger.cpp
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues an prints to the log file.
//!              Should be run in background task.
//!#################################################
#include "Logger.h"
#include <time.h>
#include <sys/time.h>
#include <sstream>
#include <unistd.h>

#define LOGNAME_FORMAT "_%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 20


Logger::Logger(std::string filename, uint16_t debugLevel) :
    logFileName(filename)
{
    CreateLogFile();

    if ( debugLevel & 0x0001 )
    {
        DEBUG_SqanInterface = 1;
    }
  /*JR  if ( debugLevel & 0x0002 )
    {
        DEBUG_SqanSecurity = 1;
    }
    if ( debugLevel & 0x0004 )
    {
        DEBUG_SqanFirmware = 1;
    }
    if ( debugLevel & 0x0008 )
    {
        DEBUG_SqanIdentity = 1;
    }
    if ( debugLevel & 0x0010 )
    {
        DEBUG_SqanSystem = 1;
    }
    if ( debugLevel & 0x0020 )
    {
        DEBUG_SqanAssociation = 1;
    }
    if ( debugLevel & 0x0040 )
    {
        DEBUG_SqanStream = 1;
    }
    if ( debugLevel & 0x0080 )
    {
        DEBUG_SqanSolNet =  1;
    }
    if ( debugLevel & 0x0100 )
    {
        DEBUG_SqanMetrics =  1;
    }
    if ( debugLevel & 0x0200 )
    {
        DEBUG_SqanLink =  1;
    }
    if ( debugLevel & 0x0400 )
    {
        DEBUG_SqanProduct =  1;
    }

    */

}

Logger::~Logger()
{
    EmptyQueue();
    fflush(logFile);
    fclose(logFile);
    logQueue.empty();
}

void Logger::LogMessage(std::string message, uint16_t index)
{
    logMutex.lock();

    uint64_t now = GetTimeStamp();
    std::stringstream ss;
    ss  << "Device " << std::to_string(index) << " " << message.c_str() << " at " << std::to_string(now) << std::endl;
    std::string tmpString;
    tmpString.append(ss.str());
    logQueue.push(tmpString);

    logMutex.unlock();
}

void Logger::DebugMessage(std::string message, uint16_t index)
{
    //! No timestamp - useful for printing
    //! headers and databufs
    logMutex.lock();
    message.append("\n");
    logQueue.push(message);
    logMutex.unlock();
}

void Logger::deQueue()
{
    logMutex.lock();
    std::string tmpString;

    if (!logQueue.empty())
    {
        tmpString = logQueue.front();
        fwrite(tmpString.data(), sizeof(char), tmpString.size(), logFile);
        fflush(logFile);
        logQueue.pop();
    }

    logMutex.unlock();
}

uint64_t Logger::GetTimeStamp()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (tv.tv_sec*(uint64_t)1000000 + tv.tv_usec);
}

void Logger::CreateLogFile()
{
    static char name[LOGNAME_SIZE];
    time_t now = time(0);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));
    logFileName.append(name);
    logFileName.append(".txt");

    logFile = fopen(logFileName.c_str(), "w+");
    if (logFile == NULL)
    {
        std::cout << "Can't open log file with name: " << logFileName << std::endl;
    }
}

void Logger::EmptyQueue()
{
    //! Called from destructor on shutdown
    while (!logQueue.empty())
    {
        deQueue();
        usleep(10);
    }
    fflush(logFile);
}

